{*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @version  Release: $Revision: 14008 $
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{capture name=path}{l s='Contact'}{/capture}
{include file="$tpl_dir./breadcrumb.tpl"}

<h1>{l s='Customer Service'} - {if isset($customerThread) && $customerThread}{l s='Your reply'}{else}{l s='Contact us'}{/if}</h1>

{if isset($confirmation)}
	<p>{l s='Your message has been successfully sent to our team.'}</p>
	<ul class="footer_links">
		<li><a href="{$base_dir}"><img class="icon" alt="" src="{$img_dir}icon/home.gif"/></a><a href="{$base_dir}">{l s='Home'}</a></li>
	</ul>
{elseif isset($alreadySent)}
	<p>{l s='Your message has already been sent.'}</p>
	<ul class="footer_links">
		<li><a href="{$base_dir}"><img class="icon" alt="" src="{$img_dir}icon/home.gif"/></a><a href="{$base_dir}">{l s='Home'}</a></li>
	</ul>
{else}
	<p class="bold">{l s='For questions about an order or for more information about our products'}.</p>
	{include file="$tpl_dir./errors.tpl"}
	<form action="{$request_uri|escape:'htmlall':'UTF-8'}" method="post" class="std" id="contact-form" enctype="multipart/form-data">
		<fieldset>
			<h3>{l s='Send a Enquiry'}</h3>
            <p class="text">
				<label for="first_name">{l s='First name'}</label>
				{if isset($customer.firstname)}
					<input type="text" id="first_name" name="first_name" value="{$customer.firstname}" readonly="readonly" />
				{else}
					<input type="text" id="first_name" name="first_name" value="{$first_name}" />
				{/if}
			</p>
            <p class="text">
				<label for="last_name">{l s='Last name'}</label>
                {if isset($customer.firstname)}
					<input type="text" id="last_name" name="last_name" value="{$customer.lastname}" readonly="readonly" />
				{else}
					<input type="text" id="last_name" name="last_name" value="{$last_name}" />
				{/if}
			</p>
            
			<p class="text">
				<label for="email">{l s='E-mail address'}</label>
				{if isset($customerThread.email)}
					<input type="text" id="email" name="from" value="{$customerThread.email}" readonly="readonly" />
				{else}
					<input type="text" id="email" name="from" value="{$email}" />
				{/if}
			</p>
            
            <p class="text">
				<label for="name">{l s='Tel'}</label>
					<input type="text" id="tel" name="tel" value="{$tel}" />
			</p>
            
            <p class="text">
				<label for="name">{l s='Requested delivery date / deadline'}</label>
				<input type="text" id="delivery_date" name="delivery_date" value="{$delivery_date}" />
			</p>
            
            <p class="textarea">
			<label for="delivery_address">{l s='Delivery address'}</label>
			 <textarea id="delivery_address" name="delivery_address" rows="15" cols="20" style="width:340px;height:110px">{if isset($delivery_address)}{$delivery_address|escape:'htmlall':'UTF-8'|stripslashes}{/if}</textarea>
			</p>
		
       		 <p class="textarea">
			<label for="billing_address">{l s='Billing address'}</label>
			 <textarea id="billing_address" name="billing_address" rows="15" cols="20" style="width:340px;height:110px">{if isset($billing_address)}{$billing_address|escape:'htmlall':'UTF-8'|stripslashes}{/if}</textarea>
			</p>
             
            
            
             <p class="text">
				<label for="quantity">{l s='Quantity'}</label>
				<input type="text" id="quantity" name="quantity" value="{if isset($quantity)}{$quantity}{/if}" style="width:40px" />
			</p>
            
			<p class="select">
				<label for="Quality">{l s='Quality'}</label> 
			
				<select id="quality" name="quality">
					<option value="Low">{l s='Low'}</option>
                    <option value="Medium">{l s='Medium'}</option>
                    <option value="High">{l s='High'}</option>
				</select>
			</p>
            
            
		
		
		<p class="textarea">
			<label for="message">{l s='Description'}</label>
			 <textarea id="message" name="message" rows="15" cols="20" style="width:340px;height:110px">{if isset($message)}{$message|escape:'htmlall':'UTF-8'|stripslashes}{/if}</textarea>
		</p>
        
        <p class="text">
			<label for="fileUpload">{l s='Attach File'}</label>
				<input type="hidden" name="MAX_FILE_SIZE" value="2000000" />
				<input type="file" name="fileUpload" id="fileUpload" />
			</p>
            
            
		<p class="submit">
			<input type="submit" name="submitMessage" id="submitMessage" value="{l s='Send'}" class="button_large" onclick="$(this).hide();" />
		</p>
	</fieldset>
</form>
{/if}
