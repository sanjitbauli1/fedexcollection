{*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @version  Release: $Revision: 14008 $
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{capture name=path}{l s='account'}{/capture}
{include file="$tpl_dir./breadcrumb.tpl"}


<div id="PG_Zone1" class="PageZone">
        <h1>Account Management</h1>
        
        <div class="pw-acctsection">
               
            <div class="pw-acctsection-cat"><h2>Account</h2><div class="smalltext">Account Settings, Address Book &amp; Order History</div></div>
               
            <div class="pw-acctsection-menu">
                <ul>
                    <li class="pw-acctsection-menugroup"><span class="bold">Account Settings</span>
                        <ul class="pw-acctsection-menuitems">
                            <li><a href="{$link->getPageLink('identity.php', true)}">Edit my profile</a></li>
                        </ul>
                    </li>
                    <li class="pw-acctsection-menugroup"><span class="bold">Address Book</span>
                        <ul class="pw-acctsection-menuitems">
                            <li><a href="{$link->getPageLink('addresses.php', true)}">Manage Address Book</a></li>
                        </ul>
                    </li>
                    <li class="pw-acctsection-menugroup"><span class="bold">Order History</span>
                        <ul class="pw-acctsection-menuitems">
                            <li><a href="{$link->getPageLink('history.php', true)}">View Orders</a></li>
                        </ul>
                    </li>
                    {if $returnAllowed}
                    <li class="pw-acctsection-menugroup"><span class="bold">Merchandise Returns</span>
                        <ul class="pw-acctsection-menuitems">
                            <li><a href="{$link->getPageLink('order-follow.php', true)}">View Merchandise Returns</a></li>
                        </ul>
                    </li>
                    {/if}
                    <li class="pw-acctsection-menugroup"><span class="bold">Credit Slips</span>
                        <ul class="pw-acctsection-menuitems">
                            <li><a href="{$link->getPageLink('order-slip.php', true)}">View Orders Slips</a></li>
                        </ul>
                    </li>
                    {if $voucherAllowed}
                    <li class="pw-acctsection-menugroup"><span class="bold">My Vouchers</span>
                        <ul class="pw-acctsection-menuitems">
                            <li><a href="{$link->getPageLink('discount.php', true)}">View Vouchers</a></li>
                        </ul>
                    </li>
                    {/if}
                </ul>
                <div class="clear"></div>
            </div>
            <div class="clear"></div>
        </div>
    </div>

{literal}
<style type="text/css">
#center_column {
width: 100%;
margin: 0 0 30px 0;
}
.PageZone{padding: 10px; margin:0 auto;}
h1 {padding: 0px; font-size: 1.30em; margin: 10px 0px 0px 0px; text-transform:capitalize; height:auto; color:#000;}
.pw-acctsection {margin-bottom: 0px; border: 1px solid #909090; border-radius: 8px; margin-top: 15px;}
.pw-acctsection .pw-acctsection-cat {float: left; padding: 0px 5px 5px 5px; width: 180px;}
.pw-acctsection h2 {font-size: 15px; padding: 0px; margin: 10px 0px 0px 0px; text-transform:capitalize; color:#666;}
.smalltext {font-size: 0.70em;}
.pw-acctsection .pw-acctsection-menu {border-left: 1px solid #909090; margin-left: 200px; padding: 10px;}
.pw-acctsection ul {list-style-type: none; margin: 0px; padding: 0px;}
.pw-acctsection ul li {float: left; width: 250px;}
div.pw-acctsection-menu ul li.pw-acctsection-menugroup {margin-bottom: 10px;}
.bold {font-weight: bold;}
.pw-acctsection ul {list-style-type: none; margin: 0px; padding: 0px;}
.pw-acctsection a {text-decoration: none; color: #4d148c; line-height: 140%;font-size: 0.85em;}
</style>
{/literal}

<!--<h1>{l s='My account'}</h1>
<h4>{l s='Welcome to your account. Here you can manage your addresses and orders.'}</h4>
<ul>
	<li><a href="{$link->getPageLink('history.php', true)}" title="{l s='Orders'}"><img src="{$img_dir}icon/order.gif" alt="{l s='Orders'}" class="icon" /></a><a href="{$link->getPageLink('history.php', true)}" title="{l s='Orders'}">{l s='History and details of my orders'}</a></li>
	{*if $returnAllowed*}
		<li><a href="{$link->getPageLink('order-follow.php', true)}" title="{l s='Merchandise returns'}"><img src="{$img_dir}icon/return.gif" alt="{l s='Merchandise returns'}" class="icon" /></a><a href="{$link->getPageLink('order-follow.php', true)}" title="{l s='Merchandise returns'}">{l s='My merchandise returns'}</a></li>
	{*/if*}
	<li><a href="{$link->getPageLink('order-slip.php', true)}" title="{l s='Credit slips'}"><img src="{$img_dir}icon/slip.gif" alt="{l s='Credit slips'}" class="icon" /></a><a href="{$link->getPageLink('order-slip.php', true)}" title="{l s='Credit slips'}">{l s='My credit slips'}</a></li>
	<li><a href="{$link->getPageLink('addresses.php', true)}" title="{l s='Addresses'}"><img src="{$img_dir}icon/addrbook.gif" alt="{l s='Addresses'}" class="icon" /></a><a href="{$link->getPageLink('addresses.php', true)}" title="{l s='Addresses'}">{l s='My addresses'}</a></li>
	<li><a href="{$link->getPageLink('identity.php', true)}" title="{l s='Information'}"><img src="{$img_dir}icon/userinfo.gif" alt="{l s='Information'}" class="icon" /></a><a href="{$link->getPageLink('identity.php', true)}" title="{l s='Information'}">{l s='My personal information'}</a></li>
	{*if $voucherAllowed*}
		<li><a href="{$link->getPageLink('discount.php', true)}" title="{l s='Vouchers'}"><img src="{$img_dir}icon/voucher.gif" alt="{l s='Vouchers'}" class="icon" /></a><a href="{$link->getPageLink('discount.php', true)}" title="{l s='Vouchers'}">{l s='My vouchers'}</a></li>
	{*/if*}
	{*$HOOK_CUSTOMER_ACCOUNT*}
</ul>
<p><a href="{$base_dir}" title="{l s='Home'}"><img src="{$img_dir}icon/home.gif" alt="{l s='Home'}" class="icon" /></a><a href="{$base_dir}" title="{l s='Home'}">{l s='Home'}</a></p>
-->