<script type="text/javascript" src="{$this_fedex_path}js/fedex-preview.js"></script>
<script type="text/javascript" src="{$this_fedex_path}js/statesManagement.js"></script>
<script type="text/javascript">
	//<![CDATA[
	fedex_countries = new Array();
	{foreach from=$fedex_countries item='country'}
		{if isset($country.states) && $country.contains_states}
			fedex_countries[{$country.id_country|intval}] = new Array();
			{foreach from=$country.states item='state' name='states'}
				fedex_countries[{$country.id_country|intval}].push({ldelim}'id' : '{$state.id_state}', 'name' : '{$state.name|escape:'htmlall':'UTF-8'}'{rdelim});
			{/foreach}
		{/if}
	{/foreach}
	
{if $psVersion == 1.6}
	$(document).ready(function(){
		if(typeof uniform == 'function')
		{
			$("#fedex_dest_country{$fedex_is_cart}, #fedex_dest_state{$fedex_is_cart}, #fedex_dest_city{$fedex_is_cart}, #fedex_dest_zip{$fedex_is_cart}").uniform();
			
			$("#uniform-fedex_dest_country{$fedex_is_cart}, #uniform-fedex_dest_state{$fedex_is_cart}").css('width', '100%');
			$("#uniform-fedex_dest_country{$fedex_is_cart}, #uniform-fedex_dest_state{$fedex_is_cart}").children("span").css('width', '100%');
		}
	});
{/if}
</script>

{** IF DESTINATION COUNTRY IS NOT SELECTED YET *}
{if (!isset($fedex_dest_country) || !$fedex_dest_country) && isset($fedex_default_country)}
	{assign var='fedex_dest_country' value=$fedex_default_country}
{/if}
{***}       
	  
{** GET SHIPMENT VARIALBES *}
{assign var="shippingDetails" value=""}

{* IF COUNTRY FIELD IS DISPLAYING AND IT IS DEFINED *}
{if $fedex_address_display.country && isset($fedex_dest_country) && $fedex_dest_country}
	{* CONCAT COUNTRY TO SHIPPING DETAILS *}
	{assign var="shippingDetails" value="`$shippingDetails` `$fedex_dest_country`"} 
{/if}

{* IF STATE FIELD IS DISPLAYING AND IT IS DEFINED *}
{if $fedex_address_display.state && isset($fedex_dest_state_name) && $fedex_dest_state_name}
	{* IF COUNTRY FIELD IS DISPLAYING AND IT IS DEFINED *}
	{if $fedex_address_display.country && isset($fedex_dest_country) && $fedex_dest_country}
		{* CONCAT STATE TO SHIPPING DETAILS *}
		{assign var="shippingDetails" value="`$shippingDetails`, `$fedex_dest_state_name`"}
	{else}
		{* CONCAT STATE TO SHIPPING DETAILS *}
		{assign var="shippingDetails" value="`$shippingDetails` `$fedex_dest_state_name`"}
	{/if} 
{/if}

{* IF CITY FIELD IS DISPLAYING AND IT IS DEFINED *}
{if $fedex_address_display.city && isset($fedex_dest_city) && $fedex_dest_city}
	{* IF COUNTRY AND STATE FIELDS ARE DISPLAYING AND ARE DEFINED *}
	{if ($fedex_address_display.country && isset($fedex_dest_country) && $fedex_dest_country) || ($fedex_address_display.state && isset($fedex_dest_state_name) && $fedex_dest_state_name)}
		{* CONCAT CITY TO SHIPPING DETAILS *}
		{assign var="shippingDetails" value="`$shippingDetails` - `$fedex_dest_city`"}
	{else}
		{* CONCAT CITY TO SHIPPING DETAILS *}
		{assign var="shippingDetails" value="`$shippingDetails` `$fedex_dest_city`"}
	{/if} 
{/if}

{* IF CITY FIELD IS DISPLAYING AND IT IS DEFINED *}
{if $fedex_address_display.zip && isset($fedex_dest_zip) && $fedex_dest_zip}
	{* IF COUNTRY, STATE AND CITY FIELDS ARE DISPLAYING AND ARE DEFINED *}
	{if ($fedex_address_display.country && isset($fedex_dest_country) && $fedex_dest_country) 
	|| ($fedex_address_display.state && isset($fedex_dest_state_name) && $fedex_dest_state_name)
	|| ($fedex_address_display.city && isset($fedex_dest_city) && $fedex_dest_city)}
		{* CONCAT ZIP CODE TO SHIPPING DETAILS *}
		{assign var="shippingDetails" value="`$shippingDetails`, `$fedex_dest_zip`"}
	{else}
		{* CONCAT ZIP CODE TO SHIPPING DETAILS *}
		{assign var="shippingDetails" value="`$shippingDetails` `$fedex_dest_zip`"}
	{/if} 
{/if}
{***}

<div class="fedex_preview_container{$fedex_is_cart}" {if $psVersion == 1.6}style="margin:10px 0;float: none; {if $fedex_is_cart != ''}background-color: #f6f6f6; padding: 0 0 10px 0;{/if}"{/if}>
						  
	<div id="fedex_address{$fedex_is_cart}">
		<a href="javascript:void(0)" id="fedex_shipping_rates_button" class="fedex_hide_button">X</a>
		
		<span style="font-weight: bold;">
			{$shippingDetails}
		</span>
		<span class="fedex_pointer" onclick="     
			$(this).hide();
			$('#fedex_rate_content{$fedex_is_cart}').css('display','none');
			$('#fedex_dest_change{$fedex_is_cart}').fadeIn(800);
		">({l s='change' mod='fedex'})</span>
	</div>

	<div id="fedex_dest_change{$fedex_is_cart}">
	
		{assign var='inLine' value=0}  
		<div id="fedex_line" {if $psVersion == 1.6}style="padding: 0 5px;"{/if}>
			
			{if $fedex_address_display.country}
				{assign var='inLine' value=$inLine+1}
				{*<div style="float: {if (!$fedex_is_cart && $inLine == 1)}right{else}left{/if};">*}
					<span>
						<select onchange="fedex_preview_update_state('{$fedex_is_cart}', 0)" name="fedex_dest_country{$fedex_is_cart}" id="fedex_dest_country{$fedex_is_cart}">
							{foreach from=$fedex_countries item=fedex_country}
								<option value="{$fedex_country.id_country}" {if isset($fedex_dest_country) && $fedex_dest_country == $fedex_country.iso_code} selected="selected"{/if}>{$fedex_country.name|escape:'htmlall':'UTF-8'}</option>
							{/foreach}
						</select>
					</span>
				{*</div>*}
			{/if}
			
		{if (!$fedex_is_cart && $inLine == 1) || ($fedex_is_cart && $inLine == 1)}
			</div>
			{assign var='inLine' value=0}
			<div id="fedex_line" {if $psVersion == 1.6}style="padding: 0 5px;"{/if}>
		{/if}
			
			{if $fedex_address_display.state}
				{assign var='inLine' value=$inLine+1}
				{*<div style="float: {if (!$fedex_is_cart && $inLine == 1)}right{else}left{/if};">*}
					<span>
						<select name="fedex_dest_state{$fedex_is_cart}" id="fedex_dest_state{$fedex_is_cart}">
							<option value="">-- {l s='State' mod='fedex'} --</option>
						</select>
					</span>
				{*</div>*}
			{/if}
				
		{if (!$fedex_is_cart && $inLine == 1) || ($fedex_is_cart && $inLine == 1)}
			</div>
			{assign var='inLine' value=0}
			<div id="fedex_line" {if $psVersion == 1.6}style="padding: 0 5px;"{/if}>
		{/if}

			{if $fedex_address_display.city}
				{assign var='inLine' value=$inLine+1}
				{*<div style="float: {if (!$fedex_is_cart && $inLine == 1)}right{else}left{/if};">*}
					<span>
						<input placeholder="{l s='City Name' mod='fedex'}" type="text" name="fedex_dest_city{$fedex_is_cart}" id="fedex_dest_city{$fedex_is_cart}"
							{if isset($fedex_dest_city) && $fedex_dest_city != ''}
								value="{$fedex_dest_city}"
							{elseif $fedex_is_cart != ''}
								value="{l s='City' mod='fedex'}" 
								onclick="$(this).val('')"
							{/if} 
						/>
					</span>
				{*</div>*}
			{/if}
				
		{if (!$fedex_is_cart && $inLine == 1) || ($fedex_is_cart && $inLine == 1)}
			</div>
			{assign var='inLine' value=0}
			<div id="fedex_line" {if $psVersion == 1.6}style="padding: 0 5px;"{/if}>
		{/if}
				
			{if $fedex_address_display.zip}
				{assign var='inLine' value=$inLine+1}
				{*<div style="float: {if (!$fedex_is_cart && $inLine == 1)}right{else}left{/if};">*}
					<span>
						<input placeholder="{l s='Zip Code' mod='fedex'}" type="text" {if !$fedex_address_display.zip}hide="1"{/if} name="fedex_dest_zip{$fedex_is_cart}" id="fedex_dest_zip{$fedex_is_cart}"
							{if isset($fedex_dest_zip) && $fedex_dest_zip != ''}
								value="{$fedex_dest_zip}"
							{elseif $fedex_is_cart != ''}
								value="{l s='Zip' mod='fedex'}" 
								onclick="$(this).val('')"
							{/if}
						/>
					</span>
				{*</div>*}
			{/if}
			
		</div>                   

		<span class="submit_button">
			<a href="javascript:void(0)" id="fedex_submit_location{$fedex_is_cart}" onclick="fedex_get_rates(true,'{$fedex_is_cart}')" class="{if $psVersion == 1.6}btn btn-default{else}exclusive{/if}" style="{if $fedex_is_cart != ''}margin: auto;{/if} {if $psVersion == 1.6}color: #333333;{/if}">{l s='Submit' mod='fedex'}</a>
		</span>
			
	</div>
	<div id="fedex_rate_content{$fedex_is_cart}" {if $psVersion == 1.6}style="padding: 0 5px;"{/if}>
		{if $fedex_is_cart != '' && $fedex_cart_products == 0}
			<div><b>{l s='Your cart is empty!' mod='fedex'}</b></div>
		{elseif $is_downloadable > 0}
			<div>{l s='This product is available for download and does not require shipping' mod='fedex'}.</div>
		{elseif isset($fedex_not_available) || $fedex_product_rates|@count == 0}
			<div>{l s='Shipping rates preview is not available in your area' mod='fedex'}.</div>
		{else}
			{foreach from=$fedex_product_rates key=fedex_product_carrier item=fedex_product_rate}
			<div class="fedex_carrier_name{$fedex_is_cart}">
				<span class="fedex_carrier_radio">
					<input id="id_carrier_new_{$fedex_product_rate.1}{$fedex_is_cart}" name="id_carrier_new" class="carrier_selection_radio" type="radio" {if $ps_carrier_default == $fedex_product_rate.1}checked="checked"{/if} onclick="fedex_change_carrier({$fedex_product_rate.1}, $(this))" />
					<label class="fedex_carrier_label" class="carrier_selection_name" for="id_carrier_new_{$fedex_product_rate.1}{$fedex_is_cart}">{$fedex_product_carrier}:</label>
				</span>
			</div>
			<div class="fedex_carrier_rate">&nbsp;&nbsp;{$fedex_product_rate.0}</div>
			<div class="fedex_carrier_buffer"></div>
			{/foreach}
		{/if}
	</div>
</div>

<script type="text/javascript">
	var fedex_cart_empty = "{l s='Your shopping cart is empty, add an item before selecting a shipping method' mod='fedex'}.";
	fedex_preview_update_state('{$fedex_is_cart}', {$fedex_dest_state|intval});         
</script>