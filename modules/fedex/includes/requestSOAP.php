<?php
	require_once(dirname(__FILE__).'/../../../config/config.inc.php');
	require_once(dirname(__FILE__).'/../../../init.php');
	require_once(_PS_MODULE_DIR_.'fedex/fedex.php');
	ini_set("soap.wsdl_cache_enabled", "0");
		
	$fedex = new Fedex();
	$request = unserialize(base64_decode(Tools::getValue('request')));
	$path_to_wsdl = $fedex->_fedex_mode == 'live' ? _PS_MODULE_DIR_.'fedex/includes/RateService_v13.wsdl' : _PS_MODULE_DIR_.'fedex/includes/TestRateService_v13.wsdl';
	$client = new SoapClient($path_to_wsdl, array('trace' => 1)); // Refer to http://us3.php.net/manual/en/ref.soap.php for more information
		
	try
	{
		$try = 0;
		while ($try < 3)
		{
            $response = $client->getRates($request);

            if (is_object($response) && sizeof($response) && $response->HighestSeverity != 'WARNING' ||
                (!isset($response->Notifications->Code) && (is_array($response) && !isset($response->Notifications[0]->Code))) ||
                (isset($response->Notifications->Code) && $response->Notifications->Code != 608 && (is_array($response) && $response->Notifications[0]->Code != 608)))
                break;
                
            $try++;
		}
 
		echo json_encode($response);
	}
	catch (SoapFault $exception)
	{
		echo json_encode($exception);
	}
?>