<?php
include_once(dirname(__FILE__) . '/../../config/config.inc.php');
include_once(dirname(__FILE__) . '/../../init.php');
include_once(dirname(__FILE__) . '/fedex.php');
$fedex = new Fedex();
if (Tools::getValue('fedex_random') != $fedex->_fedex_random)
	exit;
$ps_version = floatval(substr(_PS_VERSION_,0,3));

if (Tools::isSubmit('save') AND Tools::getValue('id_order') > 0)
{
	$id_order = Tools::getValue('id_order');
	
	/* REMOVE NULL PRODUCTS FROM THE BOX */
	foreach($_POST as $field => $value){
		if($field == 'pack'){
			foreach($value as $box => $info){
				foreach($info['products'] as $key => $product){
					if($product['Quantity'] <= 0 && $product['Description'] == '')
						unset($_POST['pack'][$box]['products'][$key]);
				}
				
				/* FIX PACKAGE DELETING ACTION */
				if(!isset($info['type']) || (!isset($info['h']) && !isset($info['w']) && !isset($info['d'])))
					unset($_POST['pack'][$box]);
			}
		}
	}    
	
	$data = $_POST;
	unset($data['save']);
	foreach ($data as &$item) {
		$item = is_string($item) ? htmlspecialchars(stripslashes($item)) : $item;
	}
	$data_ser = serialize($data);

	$label_info = Db::getInstance()->executeS('
		SELECT *
		FROM `'._DB_PREFIX_.'fe_fedex_labels_info`
		WHERE `id_order` = '.(int)$id_order.'
	');

	if(is_array($label_info) && sizeof($label_info) > 0)
	{
		Db::getInstance()->execute('
			UPDATE `'._DB_PREFIX_.'fe_fedex_labels_info`
			SET info = \''.($ps_version >= 1.5?Db::getInstance()->_escape($data_ser):mysql_real_escape_string($data_ser)).'\'
			WHERE id_order = '.(int)$id_order.'
		');
	}
	else
	{
		Db::getInstance()->execute('
			INSERT INTO `'._DB_PREFIX_.'fe_fedex_labels_info`
			(id_order, info)
			VALUES ('.(int)$id_order.', \''.($ps_version >= 1.5?Db::getInstance()->_escape($data_ser):mysql_real_escape_string($data_ser)).'\')
		');
	}
	if ($err = Db::getInstance()->getMsgError())
		print "Err = $err";

}
elseif(Tools::isSubmit('load') AND Tools::getValue('id_order') > 0)
{
	$id_order = Tools::getValue('id_order');
	$order = new Order($id_order);
	$data =  Db::getInstance()->executeS('
		SELECT *
		FROM `'._DB_PREFIX_.'fe_fedex_labels_info`
		WHERE `id_order` = '.(int)$id_order.'
		LIMIT 1
	');

	$fedex_carrier = new Carrier($order->id_carrier);

	if(is_array($data) && sizeof($data) > 0)
	{
		$tracking_id_type = $data[0]['tracking_id_type'];
		if(!strlen($tracking_id_type))
			$tracking_id_type = NULL;
		$return_tracking_id_type = $data[0]['return_tracking_id_type'];
		if(!strlen($return_tracking_id_type))
			$return_tracking_id_type = NULL;

		$data = unserialize($data[0]['info']);

		$shop_name = $data['shop_name'];
		$attention_name = $data['attention_name'];
		$phone_number = $data['phone_number'];
		$address1 = $data['address1'];
		$address2 = $data['address2'];
		$shop_city = $data['shop_city'];
		$countries = Country::getCountries($cookie->id_lang, true);
		$states = array();
		$shop_country = $data['shop_country'];
		$shop_state = (isset($data['shop_state']) AND isset($countries[$shop_country]['states'])) ? $data['shop_state'] : '';
		$states = isset($countries[$shop_country]['states']) ? $countries[$shop_country]['states'] : array();
		$shop_postal = $data['shop_postal'];

		$address = new Address($order->id_address_delivery);
		$shipto_company = $data['shipto_company'];
		$shipto_attention_name = $data['shipto_attention_name'];
		$shipto_phone = $data['shipto_phone'];
		$shipto_address1 = $data['shipto_address1'];
		$shipto_address2 = $data['shipto_address2'];
		$shipto_city = $data['shipto_city'];
		$shipto_postcode = $data['shipto_postcode'];
		$shipto_country = $data['shipto_country'];
		$shipto_state = $data['shipto_state'];
		$shipto_states = isset($countries[$shipto_country]['states']) ? $countries[$shipto_country]['states'] : array();

		$shipping_method = $data['shipping_type'];

		$label_margin = $data['label_margin'];
		$label_format = $data['label_format'];
		$label_stock_type = $data['label_stock_type'];

		$label_return_shipping_method = $data['label_return_shipping_method'];
		$label_return = isset($data['label_return'])?$data['label_return']:'';
		$currency = isset($data['currency']) ? $data['currency'] : 'USD';  
	}
	else //new label
	{
		$tracking_id_type = NULL;
		$return_tracking_id_type = NULL;
		$shop_name = Configuration::get('FEDEX_SHIPPER_SHOP_NAME') ? trim(Configuration::get('FEDEX_SHIPPER_SHOP_NAME')) : trim(Configuration::get('PS_SHOP_NAME'));
		$attention_name = trim(Configuration::get('FEDEX_SHIPPER_ATTENTION_NAME'));
		$phone_number = Configuration::get('FEDEX_SHIPPER_PHONE') ? trim(Configuration::get('FEDEX_SHIPPER_PHONE')) : trim(Configuration::get('PS_SHOP_PHONE'));
		$address1 = Configuration::get('FEDEX_SHIPPER_ADDR1') ? trim(Configuration::get('FEDEX_SHIPPER_ADDR1')) : trim(Configuration::get('PS_SHOP_ADDR1'));
		$address2 = Configuration::get('FEDEX_SHIPPER_ADDR2') ? trim(Configuration::get('FEDEX_SHIPPER_ADDR2')) : trim(Configuration::get('PS_SHOP_ADDR2'));
		$shop_city = Configuration::get('FEDEX_SHIPPER_CITY') ? trim(Configuration::get('FEDEX_SHIPPER_CITY')) : trim(Configuration::get('PS_SHOP_CITY'));
		$countries = Country::getCountries($cookie->id_lang, true);
		$states = array();
		$shop_country = Configuration::get('FEDEX_SHIPPER_COUNTRY') ? trim(Configuration::get('FEDEX_SHIPPER_COUNTRY')) : trim(Configuration::get('FEDEX_ORIGIN_COUNTRY'));
		$shop_country = Country::getByIso($shop_country);
		$shop_state = Configuration::get('FEDEX_SHIPPER_STATE') ? trim(Configuration::get('FEDEX_SHIPPER_STATE')) : trim(Configuration::get('FEDEX_ORIGIN_STATE'));
		$shop_state = State::getIdByIso($shop_state);
		$states = isset($countries[$shop_country]['states']) ? $countries[$shop_country]['states'] : array();
		$shop_postal = Configuration::get('FEDEX_SHIPPER_POSTCODE') ? trim(Configuration::get('FEDEX_SHIPPER_POSTCODE')) : trim(Configuration::get('PS_SHOP_CODE'));

		$address = new Address($order->id_address_delivery);
		$shipto_company = $address->company;
		$shipto_attention_name = $address->firstname.' '.$address->lastname;
		$shipto_phone = $address->phone ? $address->phone : $address->phone_mobile;
		$shipto_address1 = $address->address1;
		$shipto_address2 = $address->address2;
		$shipto_city = $address->city;
		$shipto_postcode = $address->postcode;
		$shipto_country = $address->id_country;
		$shipto_state = $address->id_state;
		$shipto_states = isset($countries[$shipto_country]['states']) ? $countries[$shipto_country]['states'] : array();

		$shipping_method =  Db::getInstance()->executeS('
			SELECT `method`
			FROM `'._DB_PREFIX_.'fe_fedex_method`
			WHERE `id_carrier` = '.(int)$order->id_carrier.'
			LIMIT 1
		');
		$shipping_method = (isset($shipping_method[0]['method']) ? $shipping_method[0]['method'] : 'none');

		$label_margin = $fedex->_fedex_label_margin;
		$label_format = $fedex->_fedex_label_format;
		$label_stock_type = $fedex->_fedex_label_stock_type;

		$label_return_shipping_method = 'FEDEX_GROUND'; // "FEDEX_GROUND" by default
		$label_return = false;
		$currency = 'USD';
	}

	$fedex_currencies = $fedex->getFedexCurrencies();
	$ship_meth = $fedex->getShippingMethods();
	$package_types = $fedex->getPackageTypes();

	$cart = new Cart($order->id_cart);
	if(!sizeof($data))
	{
		$pack_dim = $fedex->getBoxes($order->id_carrier, $order->getTotalWeight(), $cart, 0, 0, 1, 'YOUR_PACKAGING');
		foreach ($pack_dim as &$pack){
			$pack['type'] = Configuration::get('FEDEX_PACK');
			$pack['insurance'] = 0;
		}
		unset($pack);
	}
	else
		$pack_dim = (isset($data['pack']) ? $data['pack'] : array());
		
	/** COMPABILITY WITH OLD CLEARANCE PARAMS */
	if(is_array($pack_dim) && count($pack_dim))
	{
		foreach($pack_dim as $key => $pack)
		{
			if(
				isset($pack['number_of_pieces']) && isset($pack['description']) && isset($pack['country_of_manufacture'])
			&& !isset($pack['Description']) && !isset($pack['Quantity']) && !isset($pack['CountryOfManufacture'])
			)
			{
				$pack_dim[$key]['products'] = array();
				$pack_dim[$key]['products'][0] = array(
					'Description' => $pack['description'],
					'Quantity' => $pack['number_of_pieces'],
					'CountryofManufacture' => $pack['country_of_manufacture'],
					'Weight' => $pack['weight'],
					'Value' => $pack['unit_price']
				);
				$pack_dim[$key]['ShippersLoadandCount'] = $pack['shippers_load_and_count'];
				$pack_dim[$key]['BookingConfirmationNumber'] = $pack['booking_confirmation_number'];
			}
			
			if(!isset($pack['DutyPayer']))
				$pack_dim[$key]['DutyPayer'] = 'SENDER';
			if(!isset($pack['DutyPayerAccount']))
				$pack_dim[$key]['DutyPayerAccount'] = '';
			if(!isset($pack['DutyPayerCountry']))
				$pack_dim[$key]['DutyPayerCountry'] = $shop_country;
		}
	}

	//previously generated labels
	$types = array('png', 'pdf', 'dpl', 'epl2', 'epl', 'zplii', 'zpl');
	$prev_lab_html = '';
	foreach($types as $type)
	{
		$files = glob('labels/'.$id_order.'/*.'.$type);
		if(is_array($files) AND sizeof($files) > 0)
		{
			$prev_lab_html .= strtoupper($type).': ';
			foreach ($files as $file)
			{
				$number = array();
				preg_match('/.*_(.+)\.'.$type.'/', $file, $number);
				$number = $number[1];
				$prev_lab_html .= '<a target="_index" style="text-decoration:underline;" href="'._MODULE_DIR_.$fedex->name.'/labels/plugin/download_file.php?file='._MODULE_DIR_.$fedex->name.'/'.$file.'">'.$fedex->l('Label #', 'ajaxLabelSettings').$number.'</a>, ';
			}
			$prev_lab_html = substr($prev_lab_html, 0, strlen($prev_lab_html) - 2);
			$prev_lab_html .= '<br>';
		}
	}

	$html = '
	<link type="text/css" rel="stylesheet" href="'._MODULE_DIR_.$fedex->name.'/css/tooltipster.css" />
	<script type="text/javascript" src="'._MODULE_DIR_.$fedex->name.'/js/jquery.tooltipster.min.js"></script>
	
	<fieldset style="padding:8px; font-weight:normal;  margin:5px 0 10px 0; font-size:13px;">
		<legend>'.$fedex->l('Addresses', 'ajaxLabelSettings').'</legend>
	
		<div style="float:left;width: 48%;margin: 0 4% 0 0">
			<h3>'.$fedex->l('Shipper', 'ajaxLabelSettings').'</h3>
			
			<div style="margin: 5px 0;">
				<label for="shop_name" style="'.($fedex->getPSV() != 1.6 ? ($fedex->getPSV() == 1.5 ? 'width:140px;' : 'width:150px;') : '').' text-align: left;">'.$fedex->l('Shop Name:', 'ajaxLabelSettings').'</label>
				<input type="text" name="shop_name" id="shop_name" value="'.$shop_name.'" class="label_info">
			</div>

			<div style="margin: 5px 0;">
				<label for="attention_name" style="'.($fedex->getPSV() != 1.6 ? ($fedex->getPSV() == 1.5 ? 'width:140px;' : 'width:150px;') : '').' text-align: left;">'.$fedex->l('Your Name', 'ajaxLabelSettings').': <sup>*</sup></label>
				<input type="text" name="attention_name" id="attention_name" value="'.$attention_name.'" class="label_info">
			</div>

			<div style="margin: 5px 0;">
				<label for="phone_number" style="'.($fedex->getPSV() != 1.6 ? ($fedex->getPSV() == 1.5 ? 'width:140px;' : 'width:150px;') : '').' text-align: left;">'.$fedex->l('Phone Number', 'ajaxLabelSettings').': <sup>*</sup></label>
				<input type="text" name="phone_number" id="phone_number" value="'.$phone_number.'" class="label_info">
			</div>

			<div style="margin: 5px 0;">
				<label for="address1" style="'.($fedex->getPSV() != 1.6 ? ($fedex->getPSV() == 1.5 ? 'width:140px;' : 'width:150px;') : '').' text-align: left;">'.$fedex->l('Address Line 1', 'ajaxLabelSettings').': <sup>*</sup></label>
				<input type="text" name="address1" id="address1" value="'.$address1.'" class="label_info">
			</div>

			<div style="margin: 5px 0;">
				<label for="address2" style="'.($fedex->getPSV() != 1.6 ? ($fedex->getPSV() == 1.5 ? 'width:140px;' : 'width:150px;') : '').' text-align: left;">'.$fedex->l('Address Line 2', 'ajaxLabelSettings').':</label>
				<input type="text" name="address2" id="address2" value="'.$address2.'" class="label_info">
			</div>

			<div style="margin: 5px 0;">
				<label for="shop_city" style="'.($fedex->getPSV() != 1.6 ? ($fedex->getPSV() == 1.5 ? 'width:140px;' : 'width:150px;') : '').' text-align: left;">'.$fedex->l('Shop City', 'ajaxLabelSettings').': <sup>*</sup></label>
				<input type="text" name="shop_city" id="shop_city" value="'.$shop_city.'" class="label_info">
			</div>

			<div style="margin: 5px 0;">
				<label for="shop_country" style="'.($fedex->getPSV() != 1.6 ? ($fedex->getPSV() == 1.5 ? 'width:140px;' : 'width:150px;') : '').' text-align: left;">'.$fedex->l('Shop Country', 'ajaxLabelSettings').': <sup>*</sup></label>
				<select name="shop_country" id="shop_country" style="'.($fedex->getPSV() != 1.6 ?  'width:140px;' : '').'" class="label_info">';
					foreach ($countries as $country)
					{
						$html .= '<option value="'.$country['id_country'].'" '.($shop_country == $country['id_country']?"selected":"").'>'.$country['name'].'</option>';
					}
					$html .= '
				</select>
			</div>

			<div style="margin: 5px 0;">
				<label for="shop_state" style="'.($fedex->getPSV() != 1.6 ? ($fedex->getPSV() == 1.5 ? 'width:140px;' : 'width:150px;') : '').' text-align: left;">'.$fedex->l('Shop State', 'ajaxLabelSettings').':</label>';
				$html .= '<select name="shop_state" id="shop_state" style="'.($fedex->getPSV() != 1.6 ?  'width:140px;' : '').'" class="label_info">
							<option value="">'.$fedex->l('-- Select state --', 'ajaxLabelSettings').'</option>';
					foreach ($states as $state)
							$html .= '<option value="'.$state['id_state'].'" '.($shop_state == $state['id_state']?"selected":"").'>'.$state['name'].'</option>';
					$html .= '
				</select>
			</div>

			<div style="margin: 5px 0;">
				<label for="shop_postal" style="'.($fedex->getPSV() != 1.6 ? ($fedex->getPSV() == 1.5 ? 'width:140px;' : 'width:150px;') : '').' text-align: left;">'.$fedex->l('Shop Postal Code', 'ajaxLabelSettings').': <sup>*</sup></label>
				<input type="text" name="shop_postal" id="shop_postal" value="'.$shop_postal.'" class="label_info">
			</div>
		</div>

		<div style="float:left;width: 48%;">
			<h3>'.$fedex->l('Ship to', 'ajaxLabelSettings').'</h3>

			<div style="margin: 5px 0;">
				<label for="shipto_company" style="'.($fedex->getPSV() != 1.6 ? ($fedex->getPSV() == 1.5 ? 'width:140px;' : 'width:150px;') : '').' text-align: left;">'.$fedex->l('Company', 'ajaxLabelSettings').':</label>
				<input type="text" name="shipto_company" id="shipto_company" value="'.$shipto_company.'" class="label_info">
			</div>

			<div style="margin: 5px 0;">
				<label for="shipto_attention_name" style="'.($fedex->getPSV() != 1.6 ? ($fedex->getPSV() == 1.5 ? 'width:140px;' : 'width:150px;') : '').' text-align: left;">'.$fedex->l('Attention Name', 'ajaxLabelSettings').': <sup>*</sup></label>
				<input type="text" name="shipto_attention_name" id="shipto_attention_name" value="'.$shipto_attention_name.'" class="label_info">
			</div>

			<div style="margin: 5px 0;">
				<label for="shipto_phone" style="'.($fedex->getPSV() != 1.6 ? ($fedex->getPSV() == 1.5 ? 'width:140px;' : 'width:150px;') : '').' text-align: left;">'.$fedex->l('Phone', 'ajaxLabelSettings').': <sup>*</sup></label>
				<input type="text" name="shipto_phone" id="shipto_phone" value="'.$shipto_phone.'" class="label_info">
			</div>

			<div style="margin: 5px 0;">
				<label for="shipto_address1" style="'.($fedex->getPSV() != 1.6 ? ($fedex->getPSV() == 1.5 ? 'width:140px;' : 'width:150px;') : '').' text-align: left;">'.$fedex->l('Address Line 1', 'ajaxLabelSettings').': <sup>*</sup></label>
				<input type="text" name="shipto_address1" id="shipto_address1" value="'.$shipto_address1.'" class="label_info">
			</div>

			<div style="margin: 5px 0;">
				<label for="shipto_address2" style="'.($fedex->getPSV() != 1.6 ? ($fedex->getPSV() == 1.5 ? 'width:140px;' : 'width:150px;') : '').' text-align: left;">'.$fedex->l('Address Line 2', 'ajaxLabelSettings').':</label>
				<input type="text" name="shipto_address2" id="shipto_address2" value="'.$shipto_address2.'" class="label_info">
			</div>

			<div style="margin: 5px 0;">
				<label for="shipto_city" style="'.($fedex->getPSV() != 1.6 ? ($fedex->getPSV() == 1.5 ? 'width:140px;' : 'width:150px;') : '').' text-align: left;">'.$fedex->l('City', 'ajaxLabelSettings').': <sup>*</sup></label>
				<input type="text" name="shipto_city" id="shipto_city" value="'.$shipto_city.'" class="label_info">
			</div>

			<div style="margin: 5px 0;">
				<label for="shipto_country" style="'.($fedex->getPSV() != 1.6 ? ($fedex->getPSV() == 1.5 ? 'width:140px;' : 'width:150px;') : '').' text-align: left;">'.$fedex->l('Country', 'ajaxLabelSettings').': <sup>*</sup></label>
				<select name="shipto_country" id="shipto_country" style="'.($fedex->getPSV() != 1.6 ?  'width:140px;' : '').'" class="label_info">';
					foreach ($countries as $country)
					{
						$html .= '<option value="'.$country['id_country'].'" '.($shipto_country == $country['id_country']?"selected":"").'>'.$country['name'].'</option>';
					}
					$html .= '
				</select>
			</div>

			<div style="margin: 5px 0;">
				<label for="shipto_state" style="'.($fedex->getPSV() != 1.6 ? ($fedex->getPSV() == 1.5 ? 'width:140px;' : 'width:150px;') : '').' text-align: left;">'.$fedex->l('State', 'ajaxLabelSettings').':</label>';
				$html .= '
				<select name="shipto_state" id="shipto_state" style="'.($fedex->getPSV() != 1.6 ?  'width:140px;' : '').'" class="label_info">
					<option value="">'.(sizeof($shipto_states) ? $fedex->l('-- Select state --', 'ajaxLabelSettings') : $fedex->l('--', 'ajaxLabelSettings')).'</option>';
					foreach ($shipto_states as $state)
						$html .= '<option value="'.$state['id_state'].'" '.($shipto_state == $state['id_state']?"selected":"").'>'.$state['name'].'</option>';
					$html .= '
				</select>
			</div>

			<div style="margin: 5px 0;">
				<label for="shipto_postcode" style="'.($fedex->getPSV() != 1.6 ? ($fedex->getPSV() == 1.5 ? 'width:140px;' : 'width:150px;') : '').' text-align: left;">'.$fedex->l('Postal Code', 'ajaxLabelSettings').': <sup>*</sup></label>
				<input type="text" name="shipto_postcode" id="shipto_postcode" value="'.$shipto_postcode.'" class="label_info">
			</div>

			<div style="margin: 5px 0;">
				<label for="shipping_type" style="'.($fedex->getPSV() != 1.6 ? ($fedex->getPSV() == 1.5 ? 'width:140px;' : 'width:150px;') : '').' text-align: left;">'.$fedex->l('Shipping Type', 'ajaxLabelSettings').': <sup>*</sup></label>
				<select name="shipping_type" id="shipping_type" style="'.($fedex->getPSV() != 1.6 ?  'width:140px;' : '').'" class="label_info">';
					/** IF IT IS A ORDER WITHOUT A SHIPPING METHOD SELECTED (ORDERS THAT WERE NOT CREATED USING FedEx MODULE) */
					if($shipping_method == 'none')
						$html .= '<option value="none" selected="selected">'.$fedex->l('-- Select Shipping Type --', 'ajaxLabelSettings').'</option>';
			
					if(is_array($ship_meth) && count($ship_meth))
					{
						foreach ($ship_meth AS $code => $code_lang)
						{
							$html .='<option value="'.$code.'" '.($shipping_method == $code ? "selected" : "").'>'.$code_lang.'</option>';
						}
					}
					
					$html .= '
				</select>
			</div>

		</div>
	</fieldset>

	<fieldset style="padding:8px; font-weight:normal;  margin:5px 0 10px 0; font-size:13px;">       
		<legend>'.$fedex->l('Boxes', 'ajaxLabelSettings').'</legend>
		
		<div style="float: left; width: 100%">
			<h3>'.$fedex->l('Add Boxes -', 'ajaxLabelSettings').' <span style="font-weight:normal;">'.$fedex->l('add new boxes for this shipment. Once added, they will appear below under "Selected Boxes"', 'ajaxLabelSettings').'</span></h3>
			
			<p style="margin-top: 10px;float: left;width: 100%;">
				'.$fedex->l('Units').': <b class="always_show">'.(($fedex->_fedex_unit) == "LBS" ? $fedex->l('LBS/IN', 'ajaxLabelSettings') : $fedex->l('CM/KG', 'ajaxLabelSettings')).'</b>
			</p>
			
			<p style="float: left; width: 100%">
				<span style="float:left;margin: 0 2% 0 0;">
					'.$fedex->l('Package type', 'ajaxLabelSettings').': <sup>*</sup>
					<br />
					<select name="fedex_pack" id="fedex_pack" style="width:110px;">';
						foreach ($package_types as $code => $package_name)
						{
							$html .= '<option value="'.$code.'" '.(Tools::getValue('fedex_pack', $fedex->_fedex_pack) == $code ? "selected" : "").'>'.$package_name.'</option>';
						}
						$html .= '
					</select>
				</span>
				
				<span style="float:left;margin: 0 2% 0 0;" id="dimensions">
					'.$fedex->l('Width', 'ajaxLabelSettings').':
					<br />
					<input type="text" name="fedex_width" style="width:40px;" id="fedex_width" value="0" />
				</span>
				
				<span style="float:left;margin: 0 2% 0 0;" id="dimensions">
					'.$fedex->l('Height', 'ajaxLabelSettings').':
					<br />
					<input type="text" name="fedex_height" style="width:40px;" id="fedex_height" value="0" />
				</span>
				
				<span style="float:left;margin: 0 2% 0 0;" id="dimensions">
					'.$fedex->l('Depth', 'ajaxLabelSettings').':
					<br />
					<input type="text" name="fedex_depth" style="width:40px;" id="fedex_depth" value="0" />
				</span>
				
				<span style="float:left;margin: 0 2% 0 0;">
					'.$fedex->l('Weight', 'ajaxLabelSettings').': <sup>*</sup>
					<br />
					<input type="text" name="fedex_weight" style="width:55px;" id="fedex_weight" value="0" class="always_show" />
				</span>
				
				<span style="float:left;margin: 0 2% 0 0;">
					'.$fedex->l('Insured Value', 'ajaxLabelSettings').':
					<br />
					<input type="text" name="fedex_insurance" style="width:85px;" id="fedex_insurance" value="0.00" class="very_always_show" />
				</span>
				
				<span style="float:left;margin: 0 2% 0 0;">
					'.$fedex->l('COD Collection Type', 'ajaxLabelSettings').':
					<br />
					<select name="fedex_cod_type" id="fedex_cod_type" style="width:105px;" class="very_always_show">
						<option></option>
						<option value="ANY">'.$fedex->l('Any', 'ajaxLabelSettings').'</options>
						<option value="CASH">'.$fedex->l('Cash', 'ajaxLabelSettings').'</options>
						<option value="COMPANY_CHECK">'.$fedex->l('Company Check', 'ajaxLabelSettings').'</options>
						<option value="GUARANTEED_FUNDS">'.$fedex->l('Guaranteed Funds', 'ajaxLabelSettings').'</options>
						<option value="PERSONAL_CHECK">'.$fedex->l('Personal Check', 'ajaxLabelSettings').'</options>
					</select>
				</span>
				
				<span style="float:left;margin: 0 2% 0 0;">
					'.$fedex->l('COD Amount', 'ajaxLabelSettings').':
					<br />
					<input type="text" name="fedex_cod_amount" style="width:85px;" id="fedex_cod_amount" value="0.00" class="very_always_show" />
				</span>
				
				<span style="float:left;margin: 20px 0 0 0;">
					<img src="'._MODULE_DIR_.'fedex/img/add.gif" style="cursor:pointer;position:relative;top:-2px;vertical-align:middle;" id="add_pack">
				</span>
			</p>

			<p style="margin-top: 10px;float: left;width: 100%;">
				'.$fedex->l('Add all boxes you will be using to ship (Click the', 'ajaxLabelSettings').' <img src="'._MODULE_DIR_.'fedex/img/add.gif"> '.$fedex->l('above to add)', 'ajaxLabelSettings').'
			</p>

			<div id="packages_list" style="margin: 20px 0 20px 0;float: left; width: 100%">
				<h3>'.$fedex->l('Selected Boxes -', 'ajaxLabelSettings').' <span style="font-weight:normal;">'.$fedex->l('edit/delete the boxes that will be used for this shipment').'</span></h3>
				
				<div style="float: left; width: 100%;" id="package_list_body">
	';

	if (!$pack_dim)
		$pack_dim = array();

	$international_methods = array('INTERNATIONAL_ECONOMY', 'INTERNATIONAL_PRIORITY', 'INTERNATIONAL_FIRST', 'INTERNATIONAL_ECONOMY_FREIGHT', 'EUROPE_FIRST_INTERNATIONAL_PRIORITY');
	
	foreach ($pack_dim as $id => $package)
	{
		/* IF NO PRODUCTS IS DEFINED OR A QUANTITY OF PRODUCTS IN THE BOX LOWER THAN 0 */   
		if(!isset($package['productsPerBox']) || $package['productsPerBox'] <= 0)
			$package['productsPerBox'] = 1;
			
		/* IF PRODUCTS IN THE BOX HAVE BEEN SET BEFORE, AND THE INPUT VALUE IS LOWER THAN THE PRODUCT QUANTITY */
		if(isset($package['products']) && count($package['products']) && $package['productsPerBox'] < count($package['products']))
			$package['productsPerBox'] = count($package['products']);
		
		$html .= '
				<p class="package_item" style="float: left; width: 100%; '.($id > 0 ? 'margin: 20px 0 10px 0;border-top: 1px solid #cccccc;padding: 16px 0 0 0;' : '').'">
					<span style="float:left;margin: 0 2% 0 0;">
						'.$fedex->l('Package Type', 'ajaxLabelSettings').': <sup>*</sup>
						<br />
						<select class="pack_type_select" name="pack['.$id.'][type]" id="fedex_pack'.$id.'" style="width:110px;">';
						if(is_array($package_types) && count($package_types))
						{
							foreach ($package_types as $code => $package_name)
							{
								$html .= '<option value="'.$code.'" '.($package['type'] == $code ? "selected" : "").'>'.$package_name.'</option>';
							}
						}
		$html .= '
						</select>
					</span>
					
					<span style="float:left;margin: 0 2% 0 0;">
						'.$fedex->l('Width', 'ajaxLabelSettings').':
						<br />
						<input type="text" class="pack_dimensions" id="pack_width_'.$id.'" name="pack['.$id.'][w]" style="width:40px;'.($package['type'] != "YOUR_PACKAGING" ? 'display:none;' : '').'" value="'.$package['w'].'">
					</span>
					
					<span style="float:left;margin: 0 2% 0 0;">
						'.$fedex->l('Height', 'ajaxLabelSettings').':
						<br />
						<input type="text" class="pack_dimensions" id="pack_height_'.$id.'" name="pack['.$id.'][h]" style="width:40px;'.($package['type'] != "YOUR_PACKAGING" ? 'display:none;' : '').'" value="'.$package['h'].'">
					</span>
					
					<span style="float:left;margin: 0 2% 0 0;">
						'.$fedex->l('Depth', 'ajaxLabelSettings').':
						<br />
						<input type="text" class="pack_dimensions" id="pack_depth_'.$id.'" name="pack['.$id.'][d]" style="width:40px;'.($package['type'] != "YOUR_PACKAGING" ? 'display:none;' : '').'" value="'.$package['d'].'">
					</span>
					
					<span style="float:left;margin: 0 2% 0 0;">
						'.$fedex->l('Weight', 'ajaxLabelSettings').': <sup>*</sup>
						<br />
						<input type="text" id="pack_weight_'.$id.'" name="pack['.$id.'][weight]" style="width:55px;" value="'.$package['weight'].'" class="package_weight">
					</span>
					
					<span style="float:left;margin: 0 2% 0 0;">
						'.$fedex->l('Insured Value', 'ajaxLabelSettings').':
						<br />
						<input type="text" id="pack_insurance_'.$id.'" name="pack['.$id.'][insurance]" style="width:85px;" value="'.$package['insurance'].'" class="package_insurance">
					</span>
					
					<span style="float:left;margin: 0 2% 0 0;">
						'.$fedex->l('COD Collection Type', 'ajaxLabelSettings').':
						<br />
						<select name="pack['.$id.'][cod_code]" id="pack_cod_code_'.$id.'" style="width:105px;">
							<option></option>
							<option value="ANY" '.(isset($package['cod_code']) && $package['cod_code'] == "ANY"?"selected":"").'>'.$fedex->l('Any', 'ajaxLabelSettings').'</options>
							<option value="CASH" '.(isset($package['cod_code']) && $package['cod_code'] == "CASH"?"selected":"").'>'.$fedex->l('Cash', 'ajaxLabelSettings').'</options>
							<option value="COMPANY_CHECK" '.(isset($package['cod_code']) && $package['cod_code'] == "COMPANY_CHECK"?"selected":"").'>'.$fedex->l('Company Check', 'ajaxLabelSettings').'</options>
							<option value="GUARANTEED_FUNDS" '.(isset($package['cod_code']) && $package['cod_code'] == "GUARANTEED_FUNDS"?"selected":"").'>'.$fedex->l('Guaranteed Funds', 'ajaxLabelSettings').'</options>
							<option value="PERSONAL_CHECK" '.(isset($package['cod_code']) && $package['cod_code'] == "PERSONAL_CHECK"?"selected":"").'>'.$fedex->l('Personal Check', 'ajaxLabelSettings').'</options>
						</select>
					</span>
					
					<span style="float:left;margin: 0 2% 0 0;">
						'.$fedex->l('COD Amount', 'ajaxLabelSettings').':
						<br />
						<input type="text" id="pack_cod_amount_'.$id.'" name="pack['.$id.'][cod_amount]" style="width:85px;" value="'.(isset($package['cod_amount'])?$package['cod_amount']:'').'" class="package_cod_amount">
					</span>
					
					<span style="float:left;margin: 18px 1% 0 0;">
						<input type="button" class="'.($fedex->getPSV() == 1.6 ? 'btn btn-default' : 'button').' show_intl_settings" rel="'.$id.'" value="'.((in_array($shipping_method, $international_methods) OR (in_array($label_return_shipping_method, $international_methods) AND $label_return)) ? $fedex->l('Hide Intl settings') : $fedex->l('Show Intl settings')).'" />
					</span>
					
					<span style="float:left;margin: 20px 0 0 0;">
						<img src="'._MODULE_DIR_.'fedex/img/delete.gif" id="pack_delete_'.$id.'" class="pack_delete" style="cursor:pointer;">  
					</span>
				</p>
				
				<h3 id="internationalSettings" class="international_settings_'.$id.'" style="'.((in_array($shipping_method, $international_methods) OR (in_array($label_return_shipping_method, $international_methods) AND $label_return)) ? '' : 'display:none;').'">
					<span style="float: left;">'.$fedex->l('Product(s)', 'ajaxLabelSettings').' - </span><span style="font-weight:normal;float: left;">'.$fedex->l('Number of product(s) in the Box', 'ajaxLabelSettings').':</span>
					<input type="text" name="pack['.$id.'][productsPerBox]" id="pack['.$id.']_productsPerBox" value="'.$package['productsPerBox'].'" style="width: 40px; float: left; margin: 3px;" onblur="location.reload();" />
				</h3>
				
				<div class="international_settings_'.$id.'" id="internationalSettings" style="float: left; width: 100%; '.((in_array($shipping_method, $international_methods) OR (in_array($label_return_shipping_method, $international_methods) AND $label_return)) ? '' : 'display:none;').'">
					<div id="pack['.$id.']_products" style="float: left; width: 100%">';
							
					$intiCount = 0;                       
					if(isset($package['products']) && count($package['products']))
					{
						foreach($package['products'] as $key => $product)
						{
							$html .= ' 
								<p style="float: left; width: 100%" id="pack['.$id.']_productIdentification'.$key.'">
									<span style="font-weight:bold;float:left;width:100%;">'.$fedex->l('Product ', 'ajaxLabelSettings').($key+1).': </span>    
								</p>
								
								<p id="pack['.$id.']_product'.$key.'" style="float: left; width: 100%">
								
									<span style="float:left;margin: 0 2% 0 0;">
										'.$fedex->l('Description', 'ajaxLabelSettings').': <sup>**</sup>
										<br />
										<input name="pack['.$id.'][products]['.$key.'][Description]" id="pack['.$id.']_product'.$key.'_Description" type="text" value="'.$product['Description'].'" maxlength="56" style="width: 130px;" />
									
										<span class="info_tooltip" title="
											'.$fedex->l('Complete and accurate description of this commodity.').'
										"></span> 
									</span>
									
									<span style="float:left;margin: 0 2% 0 0;">
										'.$fedex->l('Quantity', 'ajaxLabelSettings').': <sup>**</sup>
										<br />
										<input name="pack['.$id.'][products]['.$key.'][Quantity]" id="pack['.$id.']_product'.$key.'_Quantity" type="text" value="'.$product['Quantity'].'" style="width: 25px;" />
										
										<span class="info_tooltip" title="
											'.$fedex->l('Total number of pieces of this commodity.').'
										"></span> 
									</span>
									
									<span style="float:left;margin: 0 2% 0 0;">
										'.$fedex->l('Unit Value', 'ajaxLabelSettings').': <sup>**</sup>
										<br />
										<input name="pack['.$id.'][products]['.$key.'][Value]" id="pack['.$id.']_product'.$key.'_Value" type="text" value="'.$product['Value'].'" style="width: 45px;" />
										
										<span class="info_tooltip" title="
											'.$fedex->l('Value of each unit in Quantity.').'
										"></span> 
									</span>
									
									<span style="float:left;margin: 0 2% 0 0;">
										'.$fedex->l('Unit Weight', 'ajaxLabelSettings').': <sup>**</sup>
										<br />
										<input name="pack['.$id.'][products]['.$key.'][Weight]" id="pack['.$id.']_product'.$key.'_Weight" type="text" value="'.$product['Weight'].'" style="width: 45px;" />
										
										<span class="info_tooltip" title="
											'.$fedex->l('Weight of each unit in Quantity.').'
										"></span> 
									</span>
									
									<span style="float:left;margin: 0 2% 0 0;">
										'.$fedex->l('Country of Manufacture', 'ajaxLabelSettings').': <sup>**</sup>
										<br />
										<input name="pack['.$id.'][products]['.$key.'][CountryofManufacture]" id="pack['.$id.']_product'.$key.'_CountryofManufacture" type="text" value="'.$product['CountryofManufacture'].'" style="width: 130px;" />
										
										<span class="info_tooltip" title="
											'.$fedex->l('Country code where commodity contents were produced or manufactured in their final form.').'
										"></span> 
									</span>
								</p>
							';
									
							$intiCount++;    
						}
					} 
							
					for($c = $intiCount; $c < $package['productsPerBox']; $c++)
					{ 
						$html .= '
						<p style="float: left; width: 100%" id="pack['.$id.']_productIdentification'.$c.'">
							<span style="font-weight:bold;float:left;width:100%;">'.$fedex->l('Product ', 'ajaxLabelSettings').($c+1).': </span>    
						</p>
						
						<p id="pack['.$id.']_product'.$c.'" style="float: left; width: 100%">
						
							<span style="float:left;margin: 0 2% 0 0;">
								'.$fedex->l('Description', 'ajaxLabelSettings').': <sup>**</sup>
								<br />
								<input name="pack['.$id.'][products]['.$c.'][Description]" id="pack['.$id.']_product'.$c.'_Description" type="text" value="" maxlength="56" style="width: 130px;" />
							
								<span class="info_tooltip" title="
									'.$fedex->l('Complete and accurate description of this commodity.').'
								"></span> 
							</span>
							
							<span style="float:left;margin: 0 2% 0 0;">
								'.$fedex->l('Quantity', 'ajaxLabelSettings').': <sup>**</sup>
								<br />
								<input name="pack['.$id.'][products]['.$c.'][Quantity]" id="pack['.$id.']_product'.$c.'_Quantity" type="text" value="0" style="width: 25px;" />
								
								<span class="info_tooltip" title="
									'.$fedex->l('Total number of pieces of this commodity.').'
								"></span> 
							</span>
							
							<span style="float:left;margin: 0 2% 0 0;">
								'.$fedex->l('Unit Value', 'ajaxLabelSettings').': <sup>**</sup>
								<br />
								<input name="pack['.$id.'][products]['.$c.'][Value]" id="pack['.$id.']_product'.$c.'_Value" type="text" value="0.00" style="width: 45px;" />
								
								<span class="info_tooltip" title="
									'.$fedex->l('Value of each unit in Quantity.').'
								"></span> 
							</span>
							
							<span style="float:left;margin: 0 2% 0 0;">
								'.$fedex->l('Unit Weight', 'ajaxLabelSettings').': <sup>**</sup>
								<br />
								<input name="pack['.$id.'][products]['.$c.'][Weight]" id="pack['.$id.']_product'.$c.'_Weight" type="text" value="0" style="width: 45px;" />
								
								<span class="info_tooltip" title="
									'.$fedex->l('Weight of each unit in Quantity.').'
								"></span>
							</span>
							
							<span style="float:left;margin: 0 2% 0 0;">
								'.$fedex->l('Country of Manufacture', 'ajaxLabelSettings').': <sup>**</sup>
								<br />
								<input name="pack['.$id.'][products]['.$c.'][CountryofManufacture]" id="pack['.$id.']_product'.$c.'_CountryofManufacture" type="text" value="" style="width: 130px;" />
								
								<span class="info_tooltip" title="
									'.$fedex->l('Country code where commodity contents were produced or manufactured in their final form.').'
								"></span> 
							</span>
						</p>
						';
					}
							
	$html .= '
					</div>
				</div>
				
				<div style="float: left; width: 100%; '.((in_array($shipping_method, $international_methods) OR (in_array($label_return_shipping_method, $international_methods) AND $label_return)) ? '' : 'display:none;').'" class="international_settings_'.$id.' package_item" id="internationalSettings">
					<h3>'.$fedex->l('Box Information', 'ajaxLabelSettings').'</h3>
										 
					<div style="margin: 5px 0;float: left; width: 100%">
						<label for="pack['.$id.']_DutyPayer" style="float:left;width:220px;font-weight:normal;">'.$fedex->l('Duty Payer', 'ajaxLabelSettings').': <sup>**</sup></label>
						<select name="pack['.$id.'][DutyPayer]" id="pack['.$id.']_DutyPayer" style="width: 140px; float: left; margin: 0 5px 0 0;">
							<option value="SENDER" '.($package['DutyPayer'] == 'SENDER' ? 'selected="selected"' : '').'>'.$fedex->l('Sender', 'ajaxLabelSettings').'</option>
							<option value="RECIPIENT" '.($package['DutyPayer'] == 'RECIPIENT' ? 'selected="selected"' : '').'>'.$fedex->l('Recipient', 'ajaxLabelSettings').'</option>
							<option value="THIRD_PARTY" '.($package['DutyPayer'] == 'THIRD_PARTY' ? 'selected="selected"' : '').'>'.$fedex->l('Third Party', 'ajaxLabelSettings').'</option>  
						</select>
						
						<span class="info_tooltip" title="
							'.$fedex->l('Define who is responsible for the products importation taxes', 'ajaxLabelSettings').'. '.$fedex->l('A FedEx Account Number is required for Third Party Payer', 'ajaxLabelSettings').'.
						"></span> 
					</div>  
					
					<div style="margin: 5px 0;float: left; width: 100%">
						<label for="pack['.$id.']_DutyPayerAccount" style="float:left;width:220px;font-weight:normal;">'.$fedex->l('Duty Payer FedEx Account', 'ajaxLabelSettings').':</label>
						<input name="pack['.$id.'][DutyPayerAccount]" id="pack['.$id.']_DutyPayerAccount" type="text" value="'.($package['DutyPayerAccount'] ? $package['DutyPayerAccount'] : '').'" style="width: 130px; float: left; margin: 0 5px 0 0;" />
						
						<span class="info_tooltip" title="
							'.$fedex->l('The FedEx account number associated with the duty payment', 'ajaxLabelSettings').'. '.$fedex->l('If payer is Sender, your FedEx Account number will be used', 'ajaxLabelSettings').'.
						"></span> 
					</div>   
					
					<div style="margin: 5px 0;float: left; width: 100%">
						<label for="pack['.$id.']_DutyPayerCountry" style="float:left;width:220px;font-weight:normal;">'.$fedex->l('Duty Payer Country', 'ajaxLabelSettings').':</label>
						<select name="pack['.$id.'][DutyPayerCountry]" id="pack['.$id.']_DutyPayerCountry" style="width: 140px; float: left; margin: 0 5px 0 0;">';
							foreach ($countries as $country)
							{
								$html .= '<option value="'.$country['id_country'].'" '.($package['DutyPayerCountry'] == $country['id_country'] ? 'selected="selected"' : '').'>'.$country['name'].'</option>';
							}
							$html .= '
						</select>
					</div>  
					
					<div style="margin: 5px 0;float: left; width: 100%">
						<label for="pack['.$id.']_ShipmentPurpose" style="float:left;width:220px;font-weight:normal;">'.$fedex->l('Shipment Purpose', 'ajaxLabelSettings').': <sup>**</sup></label>
						<select name="pack['.$id.'][ShipmentPurpose]" id="pack['.$id.']_ShipmentPurpose" style="width: 140px; float: left; margin: 0 5px 0 0;">
							<option value="SOLD" '.(isset($package['ShipmentPurpose']) && $package['ShipmentPurpose'] == 'SOLD' ? 'selected="selected"' : '').'>'.$fedex->l('Sold', 'ajaxLabelSettings').'</option>
							<option value="NOT_SOLD" '.(isset($package['ShipmentPurpose']) && $package['ShipmentPurpose'] == 'NOT_SOLD' ? 'selected="selected"' : '').'>'.$fedex->l('Not Sold', 'ajaxLabelSettings').'</option>
							<option value="PERSONAL_EFFECTS" '.(isset($package['ShipmentPurpose']) && $package['ShipmentPurpose'] == 'PERSONAL_EFFECTS' ? 'selected="selected"' : '').'>'.$fedex->l('Personal Effects', 'ajaxLabelSettings').'</option>
							<option value="REPAIR_AND_RETURN" '.(isset($package['ShipmentPurpose']) && $package['ShipmentPurpose'] == 'REPAIR_AND_RETURN' ? 'selected="selected"' : '').'>'.$fedex->l('Repair and Return', 'ajaxLabelSettings').'</option>
							<option value="SAMPLE" '.(isset($package['ShipmentPurpose']) && $package['ShipmentPurpose'] == 'SAMPLE' ? 'selected="selected"' : '').'>'.$fedex->l('Sample', 'ajaxLabelSettings').'</option>
							<option value="GIFT" '.(isset($package['ShipmentPurpose']) && $package['ShipmentPurpose'] == 'GIFT' ? 'selected="selected"' : '').'>'.$fedex->l('Gift', 'ajaxLabelSettings').'</option>
						</select>
						
						<span class="info_tooltip" title="
							'.$fedex->l('Specify purpose of shipment. Required for some destinations.').'
						"></span> 
					</div>  
					
					<div style="margin: 5px 0;float: left; width: 100%">
						<label for="pack['.$id.']_ShippersLoadandCount" style="float:left;width:220px;font-weight:normal;">'.$fedex->l('Shippers Load and Count', 'ajaxLabelSettings').': <sup>**</sup></label>
						<input name="pack['.$id.'][ShippersLoadandCount]" id="pack['.$id.']_ShippersLoadandCount" type="text" value="'.(isset($package['ShippersLoadandCount']) ? $package['ShippersLoadandCount'] : '').'" style="width: 130px; float: left; margin: 0 5px 0 0;" />
						
						<span class="info_tooltip" title="
							'.$fedex->l('Total shipment pieces. E.g. 3 boxes and 3 pallets of 100 pieces each = Shippers Load and Count of 303. Applicable to International Priority Freight and International Economy Freight. Values must be in the range of 1 - 99999.').'
						"></span> 
					</div>   
					
					<div style="margin: 5px 0;float: left; width: 100%">
						<label for="pack['.$id.']_BookingConfirmationNumber" style="float:left;width:220px;font-weight:normal;">'.$fedex->l('Booking Confirmation Number', 'ajaxLabelSettings').':</label>
						<input name="pack['.$id.'][BookingConfirmationNumber]" id="pack['.$id.']_BookingConfirmationNumber" type="text" value="'.(isset($package['BookingConfirmationNumber']) ? $package['BookingConfirmationNumber'] : '').'" style="width: 130px; float: left; margin: 0 5px 0 0;" />
						
						<span class="info_tooltip" title="
							'.$fedex->l('An advance booking number is required for FedEx International Priority Freight. When you call 1.800.332.0807 to book your freight shipment, you will receive a booking number. This booking number can be included in the Ship request and prints on the shipping label.').'
						"></span> 
					</div>    
					
					<div style="margin: 5px 0;float: left; width: 100%; '.(!$label_return ? 'display:none;' : '').'" class="customs_options_type">
						<label for="pack_customs_options_type_'.$id.'" style="float:left;width:220px;font-weight:normal;">'.$fedex->l('Customs Options Type').': <sup>**</sup></label>
						<select id="pack_customs_options_type_'.$id.'" name="pack['.$id.'][customs_options_type]" style="width: 140px; float: left; margin: 0 5px 0 0;" class="package_customs_options_type intl_field">';
							foreach ($fedex->getCustomsOptionsTypes() as $code => $type)
							{
								$html .= '<option '.((isset($package['customs_options_type']) && $package['customs_options_type'] == $code) ? 'selected="selected"' : '').' value="'.$code.'">'.$type.'</option>';
							}
						$html .= '
						</select>
						
						<span class="info_tooltip" title="
							'.$fedex->l('Details the return reason used for clearance processing of international dutiable outbound and international dutiable return shipments.').'
						"></span> 
						
						<span style="font-size:11px;position:relative;top:-1px;">'.$fedex->l('(Return label only)').'</span>
					</div> 
					
					<div style="margin: 5px 0;float: left; width: 100%; '.(($label_return && isset($package['customs_options_type']) && $package['customs_options_type'] == 'OTHER') ? '' : 'display:none;').'" class="customs_options_type">
						<label for="pack_customs_options_description_'.$id.'" style="float:left;width:220px;font-weight:normal;">'.$fedex->l('Customs Options Description').': <sup>**</sup></label>
						<input type="text" id="pack_customs_options_description_'.$id.'" name="pack['.$id.'][customs_options_description]" style="width: 130px; float: left; margin: 0 5px 0 0;" value="'.(isset($package['customs_options_description']) ? $package['customs_options_description'] : '').'" class="package_customs_options_description">
						
						<span class="info_tooltip" title="
							'.$fedex->l('Specifies additional description about customs options.').'
						"></span> 
						
						<span style="font-size:11px;position:relative;top:-1px;">'.$fedex->l('(Return label only)').'</span>
					</div> 
				</div>
			';
	}

	$html .= '
			</div>                
		</div>
	</fieldset> 

	
	<fieldset style="padding:8px; font-weight:normal;  margin:10px 0 0 0; font-size:13px;">
		<legend>'.$fedex->l('Label Settings', 'ajaxLabelSettings').'</legend>

		<div style="margin: 5px 0;width: 100%;float: left;">
			<label for="label_margin" style="float:left;width:220px;">'.$fedex->l('Margin between multiple labels:', 'ajaxLabelSettings').'</label>
			<input type="text" name="label_margin" id="label_margin" value="'.$label_margin.'" class="label_info"  style="width:30px;float: left;margin: 0 5px 0 0;"> px
		</div>

		<div style="margin: 5px 0;width: 100%;float: left;">
			<label for="label_format" style="float:left;width:220px;">'.$fedex->l('Label Format', 'ajaxLabelSettings').': <sup>*</sup></label>
			<select name="label_format" id="label_format" style="width:140px;float: left;margin: 0 5px 0 0;" class="label_info">
				<option value="PNG" '.($label_format == 'PNG' ? 'selected' : '').'>PNG</option>
				<option value="PDF" '.($label_format == 'PDF' ? 'selected' : '').'>PDF</option>
				<option value="DPL" '.($label_format == 'DPL' ? 'selected' : '').'>DPL</option>
				<option value="EPL2" '.($label_format == 'EPL2' ? 'selected' : '').'>EPL2</option>
				<option value="ZPLII" '.($label_format == 'ZPLII' ? 'selected' : '').'>ZPLII</option>
			</select>
			
			<span class="info_tooltip" title="
				'.$fedex->l('Specifies the image format used for a shipping document.').'
			"></span> 
		</div>

		<div style="margin: 5px 0;width: 100%;float: left;">
			<label for="label_stock_type" style="float:left;width:220px;">'.$fedex->l('Label Stock Type', 'ajaxLabelSettings').': <sup>*</sup></label>
			<select name="label_stock_type" id="label_stock_type" style="width:140px;float: left;margin: 0 5px 0 0;" class="label_info">
				<optgroup label="'.$fedex->l('For PNG and PDF', 'ajaxLabelSettings').'">
					<option value="PAPER_4X6" '.($label_stock_type == 'PAPER_4X6' ? 'selected' : '').'>PAPER_4X6</option>
					<option value="PAPER_4X8" '.($label_stock_type == 'PAPER_4X8' ? 'selected' : '').'>PAPER_4X8</option>
					<option value="PAPER_4X9" '.($label_stock_type == 'PAPER_4X9' ? 'selected' : '').'>PAPER_4X9</option>
					<option value="PAPER_7X4.75" '.($label_stock_type == 'PAPER_7X4.75' ? 'selected' : '').'>PAPER_7X4.75</option>
					<option value="PAPER_8.5X11_BOTTOM_HALF_LABEL" '.($label_stock_type == 'PAPER_8.5X11_BOTTOM_HALF_LABEL' ? 'selected' : '').'>PAPER_8.5X11_BOTTOM_HALF_LABEL</option>
					<option value="PAPER_8.5X11_TOP_HALF_LABEL" '.($label_stock_type == 'PAPER_8.5X11_TOP_HALF_LABEL' ? 'selected' : '').'>PAPER_8.5X11_TOP_HALF_LABEL</option>
					<option value="PAPER_LETTER" '.($label_stock_type == 'PAPER_LETTER' ? 'selected' : '').'>PAPER_LETTER</option>
				</optgroup>
				<optgroup label="'.$fedex->l('For Thermal Labels', 'ajaxLabelSettings').'">
					<option value="STOCK_4X6" '.($label_stock_type == 'STOCK_4X6' ? 'selected' : '').'>STOCK_4X6</option>
					<option value="STOCK_4X6.75_LEADING_DOC_TAB" '.($label_stock_type == 'STOCK_4X6.75_LEADING_DOC_TAB' ? 'selected' : '').'>STOCK_4X6.75_LEADING_DOC_TAB</option>
					<option value="STOCK_4X6.75_TRAILING_DOC_TAB" '.($label_stock_type == 'STOCK_4X6.75_TRAILING_DOC_TAB' ? 'selected' : '').'>STOCK_4X6.75_TRAILING_DOC_TAB</option>
					<option value="STOCK_4X8" '.($label_stock_type == 'STOCK_4X8' ? 'selected' : '').'>STOCK_4X8</option>
					<option value="STOCK_4X9_LEADING_DOC_TAB" '.($label_stock_type == 'STOCK_4X9_LEADING_DOC_TAB' ? 'selected' : '').'>STOCK_4X9_LEADING_DOC_TAB</option>
					<option value="STOCK_4X9_TRAILING_DOC_TAB" '.($label_stock_type == 'STOCK_4X9_TRAILING_DOC_TAB' ? 'selected' : '').'>STOCK_4X9_TRAILING_DOC_TAB</option>
				</optgroup>
			</select>
			
			<span class="info_tooltip" title="
				'.$fedex->l('Indicates the size of the label and the location of the doc tab if present.').'
			"></span> 
		</div>

		<div style="margin: 5px 0;width: 100%;float: left;">
			<label for="label_return" style="float:left;width:220px;">'.$fedex->l('Generate Return Label:', 'ajaxLabelSettings').'</label>
			<input type="checkbox" style="margin-top:4px;" id="label_return" name="label_return" '.($label_return ? 'checked="checked"' : '').' '.($return_tracking_id_type ? 'disabled="disabled"' : '').'>
		</div>

		<div style="margin: 5px 0;width: 100%;float: left;">
			<label for="label_return_shipping_method" style="float:left;width:220px;">'.$fedex->l('Return Label Shipping Type:', 'ajaxLabelSettings').'</label>
			<select name="label_return_shipping_method" id="label_return_shipping_method" style="width:140px;float: left;margin: 0 5px 0 0;" class="label_info">';
				foreach ($ship_meth AS $code => $code_lang) {
					$html .='<option value="'.$code.'" '.($label_return_shipping_method == $code ? "selected" : "").'>'.$code_lang.'</option>';
				}
			$html .=
			'</select>
		</div>

		<div style="margin: 5px 0;width: 100%;float: left;">
			<label for="currency" style="float:left;width:220px;">'.$fedex->l('Currency', 'ajaxLabelSettings').': <sup>*</sup></label>
			<select name="currency" id="currency" style="width:140px;float: left;margin: 0 5px 0 0;" class="label_info">';
				foreach ($fedex_currencies AS $code => $currency_name) {
					$html .='<option value="'.$code.'" '.($currency == $code ? "selected" : "").'>'.$currency_name.'</option>';
				}
			$html .=
			'</select>
		</div>
	</fieldset>

	<input type="submit" class="'.($fedex->getPSV() == 1.6 ? 'btn btn-default' : 'button').'" '.($tracking_id_type ? 'disabled="disabled"' : '').' id="generateLabel" value="'.$fedex->l('Generate label', 'ajaxLabelSettings').'" style="margin-top:30px;">
	<img src="'._PS_IMG_.'loader.gif" id="labelLoader" style="display:none;">
	<input type="button" class="'.($fedex->getPSV() == 1.6 ? 'btn btn-default' : 'button').'" '.($tracking_id_type ? '' : 'disabled="disabled"').' id="voidShipment" value="'.$fedex->l('Delete Shipment', 'ajaxLabelSettings').'" style="margin:30px 0 0 50px;">
	<img src="'._PS_IMG_.'loader.gif" id="voidLoader" style="display:none;">
	<input type="button" class="'.($fedex->getPSV() == 1.6 ? 'btn btn-default' : 'button').'" '.($return_tracking_id_type ? '' : 'disabled="disabled"').' id="voidReturnShipment" value="'.$fedex->l('Delete Return Shipment', 'ajaxLabelSettings').'" style="margin:30px 0 0 50px;">
	<img src="'._PS_IMG_.'loader.gif" id="voidLoader1" style="display:none;">
	
	<p id="errorsLabelGeneration"></p>
	
	<p style="margin-top:15px;"><sup>*</sup> '.$fedex->l('Required field.').'</p>
	<p style="margin-top:0;"><sup>**</sup> '.$fedex->l('Required field for international shipping.').'</p>
	<div style="font-weight:bold;margin:10px 0 0 0;" id="void_response"></div>
	<div id="labels_links" style="line-height:1.5;padding-top:10px;"></div>';
	if(strlen($prev_lab_html) > 0)
	{
		$html .= '
		<div id="previous_labels_list">
		
			<hr style="width: 100%;height: 1px;background-color: #CCCED7;margin: 20px 0 0 0;" />
			
			<p style="font-weight:bold;">'.$fedex->l('Previously generated labels', 'ajaxLabelSettings').':</p>
			<p style="line-height:1.5;" id="">'.$prev_lab_html.'</p>
			<input style="margin-top:10px;" type="button" id="delete_labels" class="'.($fedex->getPSV() == 1.6 ? 'btn btn-default' : 'button').'" value="'.$fedex->l('Delete Labels', 'ajaxLabelSettings').'">
		</div>
		';
	}
	else
	{
		$html .= '
		<div id="previous_labels_list">
		</div>
		';
	}
	
	if($ps_version == 1.6)
	{
		$html .= '
		<style>
			form#labelForm sup {
				color: #CC0000;
				font-weight: bold;
			}
			form#labelForm fieldset {
				padding: 15px;
				font-weight: normal;
				margin: 5px 0 10px 0;
				font-size: 13px;
				border: solid 1px #cccccc;
				float: left;
				width: 100%;
			}
			form#labelForm fieldset  legend{
				border: 1px solid #cccccc;
				width: auto;
				padding: 3px 10px;
				color: #555555;
				margin: 0;
			}
			form#labelForm h3 {
				margin: 0 0 26px 0 !important; 
				float: left; 
				width: 100%;
			}
		</style>
		';
	}

	echo $html;
}
elseif(Tools::isSubmit('get_states'))
{
	$id_order = Tools::getValue('id_order');
	$order = new Order($id_order);
	$data =  Db::getInstance()->getRow('
		SELECT *
		FROM `'._DB_PREFIX_.'fe_fedex_labels_info`
		WHERE `id_order` = '.(int)$id_order.'
	');
	$data = unserialize($data['info']);
	if(Tools::getValue('type') == 'shop_country')
		$id_selected_state = ((int)$data['shop_state'] ? $data['shop_state'] : 0);
	else
		$id_selected_state = ((int)$data['shop_state'] ? $data['shop_state'] : 0);
	$id_country = (int)Tools::getValue('id_country');
	$states = State::getStatesByIdCountry($id_country);
	$html = '';
	if(!sizeof($states))
	{
		$html .= '<option>'.$fedex->l('--').'</option>';
	}
	else
	{
		$html .= '<option>'.$fedex->l('-- Select state --').'</option>';
		foreach ($states as $state)
		{
			$html .= '<option value="'.$state['id_state'].'" '.(($id_selected_state && $id_selected_state == $state['id_state']) ? 'selected="selected"' : '').'>'.$state['name'].'</option>';
		}
	}
	echo $html;
}