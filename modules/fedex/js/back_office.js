$(function(){
	$('#fedex_form select, #fedex_form input').unbind('mouseover');
	$("#fedex_pack").bind("change", function(){
		if ($('#fedex_pack').val() != 'YOUR_PACKAGING') {
			$('#fedex_my_pack').fadeOut(1200);
		}
		else {
			$('#fedex_my_pack').fadeIn(1200);
		}
	});
});

/*
vars invalid_site_id, invalid_pass, invalid_zip, module_path from fedex.php
*/
function validate_as()
{
	if ($("#fedex_account").val() == "")
	{
		alert(invalid_account);
		return false;
	}
	if ($("#fedex_pass").val() == "")
	{
		alert(invalid_pass);
		return false;
	}
	if ($("#fedex_key").val() == "")
	{
		alert(invalid_key);
		return false;
	}
	if ($("#fedex_meter").val() == "")
	{
		alert(invalid_meter);
		return false;
	}
	if ($("#fedex_origin_zip").val() == "")
	{
		alert(invalid_zip);
		return false;
	}
	return true;
}

function show_package_size()
{
	if ($("#fedex_packages_single").attr("checked"))
	{
		$("#fedex_package_size_product").attr('disabled',true);
		$("#fedex_package_size_fixed").attr("checked",true);
	}
	else
		$("#fedex_package_size_product").attr('disabled',false);
		
	if ($("#fedex_packages_single").attr("checked") && $("#fedex_package_size_fixed").attr("checked"))
	{
		$('#product_box').fadeOut('fast');
		$('#fixed_multi_box').fadeOut('fast');
		$('#fixed_box_multiple').fadeOut('fast');
		$('#fixed_box').fadeIn('fast');
	}
	else if ($("#fedex_packages_single").attr("checked") && $("#fedex_package_size_product").attr("checked"))
	{
		$('#fixed_box_multiple').fadeOut('fast');
		$('#fixed_multi_box').fadeOut('fast');
		$('#fixed_box').fadeOut('fast');
		$('#product_box').fadeIn('fast');
	}
	else if ($("#fedex_packages_multiple").attr("checked") && $("#fedex_package_size_product").attr("checked"))
	{
		$('#fixed_box_multiple').fadeOut('fast');
		$('#fixed_box').fadeOut('fast');
		$('#fixed_multi_box').fadeIn('fast');
		$('#product_box').fadeIn('fast');
	}
	else if ($("#fedex_packages_multiple").attr("checked") && $("#fedex_package_size_fixed").attr("checked"))
	{
		$('#product_box').fadeOut('fast');
		$('#fixed_multi_box').fadeOut('fast');
		$('#fixed_box').fadeIn('fast');
		$('#fixed_box_multiple').fadeIn('fast');
	}

	if($('#fedex_pack').val() != 'YOUR_PACKAGING')
	{
		$("#fedex_package_size_product").attr('disabled',true);
		$("#fedex_package_size_fixed").attr("checked",true);
	}
}

function add_box() {
	$.ajax({
		type: 'POST',
		url: module_path + 'manage_boxes.php',
		async: false,
		cache: false,
		dataType : "json",
		data: {'action':'add','width':$("#fedex_box_width").val(),'height':$("#fedex_box_height").val(),'depth':$("#fedex_box_depth").val(),'weight':$("#fedex_box_weight").val()},
		success:function(feed) {
			$("#fedex_boxes").html(feed.boxes);
			$("#fedex_box_width").val("");
			$("#fedex_box_height").val("");
			$("#fedex_box_depth").val("");
			$("#fedex_box_weight").val("");
		  }
	});
}

function expand_box(id)
{
	if ($("#expand_box_"+id).attr("stat") == "off")
	{
		$("#expand_box_"+id).attr("src", module_path + "img/up.gif");
		$("#expand_box_"+id).attr("stat","on");
		$(".expanded_"+id).show();
	}
	else
	{
		$("#expand_box_"+id).attr("src", module_path + "img/down.gif");
		$("#expand_box_"+id).attr("stat","off");
		$(".expanded_"+id).hide();
	}
}

function edit_boxes(id) {
	$.ajax({
		type: 'POST',
		url: module_path + 'manage_boxes.php',
		async: false,
		cache: false,
		dataType : "json",
		data: {'action':'edit','box':id,'name':$("#name_"+id).val(),'width':$("#width_"+id).val(),'height':$("#height_"+id).val(),'depth':$("#depth_"+id).val(),'max':$("#max_"+id).val(),'productid':$("#productid_"+id).val(),'categoryid':$("#categoryid_"+id).val(),'manufacturerid':$("#manufacturerid_"+id).val(),'supplierid':$("#supplierid_"+id).val()},
		success:function(feed) {
			$("#edit_box_"+id).attr("src", module_path + "img/ok.gif");
			setTimeout(function() {
				$("#edit_box_"+id).attr("src", module_path + "img/update.gif");
			}, 1000);
		}
	});
}

function remove_box(id) {
	$.ajax({
		type: 'POST',
		url: module_path + 'manage_boxes.php',
		async: false,
		cache: false,
		dataType : "json",
		data: {'action':'remove','box':id},
		success:function(feed) {
			$("#fedex_boxes").html(feed.boxes);
		}
	});
}

$(document).ready(function() {
	show_package_size();
	$.ajax({
		type: 'POST',
		url: module_path + 'manage_boxes.php',
		async: false,
		cache: false,
		dataType : "json",
		success:function(feed) {
			$("#fedex_boxes").html(feed.boxes);
		}
	});

	$(".hint_img").live({
		mouseenter:
		   function()  {
				$(this).siblings(".hint").show();
		   },
		mouseleave:
		   function() {
				$(this).siblings(".hint").stop().hide();
		   }
	});
});