function fedex_get_rates(location, is_cart)
{
	var vars = new Object();
	vars['ajax'] = true;
	
	vars['id_product'] = is_cart == ''?id_product:0;
	vars['id_product_attribute'] = is_cart == ''?$('#idCombination').val():0;
	vars['fedex_is_cart'] = is_cart;
	if (is_cart == '')
		vars['qty'] = $("#quantity_wanted").val();
	if (location)
	{
		if (!$("#fedex_dest_zip"+is_cart).val() && $("#fedex_dest_zip"+is_cart).val() != undefined && $("#fedex_dest_zip"+is_cart).attr('hide') != 1)
		{
			alert(fedex_invalid_zip);
			return;
		}
		vars['fedex_dest_zip'] = $("#fedex_dest_zip"+is_cart).val();
		vars['fedex_dest_state'] = $("#fedex_dest_state"+is_cart).val();
		vars['fedex_dest_country'] = $("#fedex_dest_country"+is_cart).val();
		vars['fedex_dest_city'] = $("#fedex_dest_city"+is_cart).val();
		$('#fedex_submit_location'+is_cart).html(fedex_please_wait);
		$('#fedex_submit_location'+is_cart).attr('disabled', 'disabled');
		$('#fedex_submit_location'+is_cart).unbind('click');
	}
	else
	{
		$('#fedex_shipping_rates_button'+is_cart).html(fedex_please_wait);
		$('#fedex_submit_location'+is_cart).unbind('click');
	}
	$.ajax({
			type: 'POST',
			url: baseDir + 'modules/fedex/fedex_preview.php',
			async: true,
			cache: false,
			dataType : "json",
			data: vars,
			success: function(json)
			{
				// add appliance to whishlist module
				$('#fedex_shipping_rates_button'+is_cart).parent().hide();
				$('a[class="fedex_hide_button"]').live('click', function(){
					
					$('#fedex_shipping_rates'+is_cart).html('');
					$('#fedex_shipping_rates_button'+is_cart).html(fedex_show);
					
					$('#fedex_shipping_rates_button'+is_cart).parent().show();
					return false;
					
				});
				
				$('#fedex_shipping_rates'+is_cart).html(json.fedex_rate_tpl);
				fedex_cart_need_refresh = true;
			},
			error: function ()
			{
				alert('Error!');
			}
	});
}

function fedex_change_carrier(id_carrier, fedex_radio)
{
	$.ajax({
		type: 'POST',
		url: baseDir + 'modules/fedex/fedex_preview.php',
		async: true,
		cache: false,
		data: {'new_id_carrier': id_carrier},
		success: function(json)
		{
			if (json == 0)
			{
				alert(fedex_cart_empty);
				fedex_radio.attr('checked', false);
			}
			else
			{
				if (typeof ajaxCart != 'undefined')
					ajaxCart.refresh();
				if (window.getCarrierListAndUpdate)
					getCarrierListAndUpdate();
			}
		}
	});
}

function fedex_city_display(duration, is_cart)
{
	var id_country = $('#fedex_dest_country' + is_cart).val();
	if(id_country == 21) //if USA
		$('.fedex_id_city' + is_cart).hide(duration);
	else
		$('.fedex_id_city' + is_cart).show(duration);
}

function fedex_define_hide_button(element, is_cart)
{
	element.hide();
	
	$('a[class="fedex_hide_button"]').live('click', function(){
		
		$('#fedex_shipping_rates'+is_cart).hide();
		
		element.html(fedex_show);
		element.show();
		
		$('#fedex_shipping_rates_button'+is_cart).unbind('click').click(function(){
			$('#fedex_shipping_rates' + is_cart).fadeIn(800);
			fedex_city_display(0, is_cart);
		});
		return false;
	});
}

var fedex_cart_need_refresh = false;

$(document).ready(function () {
	$('form[id="buy_block"]').keypress(function(e){
		if (e.keyCode == '13')
			e.preventDefault();
	});
	
	if (typeof ajaxCart != 'undefined' && $('#fedex_shipping_rates_cart').length)
	{
		var origrefresh = ajaxCart.overrideButtonsInThePage;
		ajaxCart.overrideButtonsInThePage = function() {
			origrefresh();
			if (fedex_cart_need_refresh)
			{
				$("a[class='fedex_hide_button']").click();
				fedex_cart_need_refresh = false;
			}
		}
	}
	if (typeof findCombination != 'undefined')
	{
		var origrfcomb = findCombination;
		findCombination = function() {
			origrfcomb();
			$("a[class='fedex_hide_button']").click();
		}
	}

	$('#fedex_dest_country_cart').live('change', function(){
		fedex_city_display(100, '_cart');
	});
	$('#fedex_dest_country').live('change', function(){
		fedex_city_display(100, '');
	});
});
