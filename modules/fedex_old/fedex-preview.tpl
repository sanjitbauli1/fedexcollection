<!-- FEDEX SHIPPING RATE PREVIEW -->
<script type="text/javascript" src="{$this_fedex_path}js/fedex-preview.js"></script>
<link rel="stylesheet" type="text/css" href="{$this_fedex_path}css/fedex.css" />
<script type="text/javascript" src="{$this_fedex_path}js/statesManagement.js"></script>
<script type="text/javascript">
//<![CDATA[
	fedex_countries = new Array();
	{foreach from=$fedex_countries item='country'}  
		{if isset($country.states) && $country.contains_states}
			fedex_countries[{$country.id_country|intval}] = new Array();
			{foreach from=$country.states item='state' name='states'}
				fedex_countries[{$country.id_country|intval}].push({ldelim}'id' : '{$state.id_state}', 'name' : '{$state.name|escape:'htmlall':'UTF-8'}'{rdelim});
			{/foreach}
		{/if}
	{/foreach}
	var fedex_invalid_zip = "{l s='Invalid Zipcode, if your country does not use zipcodes, enter 11111' mod='fedex' js=1}";
	var fedex_please_wait = "{l s='Please Wait' mod='fedex' js=1}";
	var fedex_hide = "{l s='Hide' mod='fedex' js=1}";
	var fedex_show = "{l s='Shipping Rates' mod='fedex' js=1}";
	
{if $psVersion == 1.6}
	$(document).ready(function(){
		if(typeof uniform == 'function')
		{
			$("#fedex_dest_country{$fedex_is_cart}, #fedex_dest_state{$fedex_is_cart}, #fedex_dest_city{$fedex_is_cart}, #fedex_dest_zip{$fedex_is_cart}").uniform();
			
			$("#uniform-fedex_dest_country{$fedex_is_cart}, #uniform-fedex_dest_state{$fedex_is_cart}").css('width', '100%');
			$("#uniform-fedex_dest_country{$fedex_is_cart}, #uniform-fedex_dest_state{$fedex_is_cart}").children("span").css('width', '100%');
		}
	});
{/if}
</script>

{** IF DESTINATION COUNTRY IS NOT SELECTED YET *}
{if (!isset($fedex_dest_country) || !$fedex_dest_country) && isset($fedex_default_country)}
	{assign var='fedex_dest_country' value=$fedex_default_country}
{/if}
{***}

<div class="fedex_preview_container{$fedex_is_cart}" {if $psVersion == 1.6}style="margin:10px 0;float: none; {if $fedex_is_cart != ''}padding: 0 0 10px 0;{/if}"{/if}>
	<p class="buttons_bottom_block" style="padding: 5px;{if $psVersion == 1.6 && $fedex_is_cart != ''}margin: 0; text-align: center;{/if}">
		<a href="javascript:void(0)" id="fedex_shipping_rates_button{$fedex_is_cart}" onclick="
			{if $fedex_get_rates != 1}
				$('#fedex_shipping_rates{$fedex_is_cart}').fadeIn(800);
				fedex_city_display(0, '{$fedex_is_cart}');
				fedex_define_hide_button($(this), '{$fedex_is_cart}'); 
			{else}
				fedex_get_rates(false,'{$fedex_is_cart}');
				fedex_city_display(0, '{$fedex_is_cart}');
			{/if}" class="{if $psVersion == 1.6}btn btn-default{else}exclusive{/if}" style="{if $fedex_is_cart != ''}margin: auto;{/if} {if $psVersion == 1.6}color: #333333; width: 100%;{/if}">{l s='Shipping Rates' mod='fedex'}</a>
	</p>
	<div id="fedex_shipping_rates{$fedex_is_cart}" style="{if $fedex_get_rates != 1}display:none;{/if}">
	{if $fedex_get_rates != 1}
		<div id="fedex_address{$fedex_is_cart}">  
			<a href="javascript:void(0)" id="fedex_shipping_rates_button" class="fedex_hide_button">X</a>
			
			<span style="font-weight: bold;">
				{l s='Please' mod='fedex'}
				<a href="{$base_dir}authentication.php" style="text-decoration: underline">{l s='Login' mod='fedex'}</a>{l s=', or enter your' mod='fedex'}
			</span>
		</div>

		<div id="fedex_dest_change{$fedex_is_cart}" style="{if $fedex_get_rates != 1}display:block;{/if}">
		
			{assign var='inLine' value=0}  
			<div id="fedex_line" {if $psVersion == 1.6}style="padding: 0 5px;"{/if}>
				
				{if $fedex_address_display.country}
					{assign var='inLine' value=$inLine+1}
					{*<div style="float: {if (!$fedex_is_cart && $inLine == 1)}right{else}left{/if};">*}
						<span>
							<select onchange="fedex_preview_update_state('{$fedex_is_cart}', 0)" name="fedex_dest_country{$fedex_is_cart}" id="fedex_dest_country{$fedex_is_cart}">
								{foreach from=$fedex_countries item=fedex_country}
									<option value="{$fedex_country.id_country}" {if isset($fedex_dest_country) && $fedex_dest_country == $fedex_country.iso_code} selected="selected"{/if}>{$fedex_country.name|escape:'htmlall':'UTF-8'}</option>
								{/foreach}
							</select>
						</span>
					{*</div>*}
				{/if}
				
			{if (!$fedex_is_cart && $inLine == 1) || ($fedex_is_cart && $inLine == 1)}
				</div>
				{assign var='inLine' value=0}
				<div id="fedex_line" {if $psVersion == 1.6}style="padding: 0 5px;"{/if}>
			{/if}
				
				{if $fedex_address_display.state}
					{assign var='inLine' value=$inLine+1}
					{*<div style="float: {if (!$fedex_is_cart && $inLine == 1)}right{else}left{/if};">*}
						<span>
							<select name="fedex_dest_state{$fedex_is_cart}" id="fedex_dest_state{$fedex_is_cart}">
								<option value="">-- {l s='State' mod='fedex'} --</option>
							</select>
						</span>
					{*</div>*}
				{/if}
					
			{if (!$fedex_is_cart && $inLine == 1) || ($fedex_is_cart && $inLine == 1)}
				</div>
				{assign var='inLine' value=0}
				<div id="fedex_line" {if $psVersion == 1.6}style="padding: 0 5px;"{/if}>
			{/if}

				{if $fedex_address_display.city}
					{assign var='inLine' value=$inLine+1}
					{*<div style="float: {if (!$fedex_is_cart && $inLine == 1)}right{else}left{/if};">*}
						<span>
							<input placeholder="{l s='City Name' mod='fedex'}" type="text" name="fedex_dest_city{$fedex_is_cart}" id="fedex_dest_city{$fedex_is_cart}"
								{if isset($fedex_dest_city) && $fedex_dest_city != ''}
									value="{$fedex_dest_city}"
								{elseif $fedex_is_cart != ''}
									value="{l s='City' mod='fedex'}" 
									onclick="$(this).val('')"
								{/if} 
							/>
						</span>
					{*</div>*}
				{/if}
					
			{if (!$fedex_is_cart && $inLine == 1) || ($fedex_is_cart && $inLine == 1)}
				</div>
				{assign var='inLine' value=0}
				<div id="fedex_line" {if $psVersion == 1.6}style="padding: 0 5px;"{/if}>
			{/if}
					
				{if $fedex_address_display.zip}
					{assign var='inLine' value=$inLine+1}
					{*<div style="float: {if (!$fedex_is_cart && $inLine == 1)}right{else}left{/if};">*}
						<span>
							<input placeholder="{l s='Zip Code' mod='fedex'}" type="text" {if !$fedex_address_display.zip}hide="1"{/if} name="fedex_dest_zip{$fedex_is_cart}" id="fedex_dest_zip{$fedex_is_cart}"
								{if isset($fedex_dest_zip) && $fedex_dest_zip != ''}
									value="{$fedex_dest_zip}"
								{elseif $fedex_is_cart != ''}
									value="{l s='Zip' mod='fedex'}" 
									onclick="$(this).val('')"
								{/if}
							/>
						</span>
					{*</div>*}
				{/if}
				
			</div>          

			<span class="submit_button">
				<a href="javascript:void(0)" id="fedex_submit_location{$fedex_is_cart}" onclick="fedex_get_rates(true,'{$fedex_is_cart}')" class="{if $psVersion == 1.6}btn btn-default{else}exclusive{/if}" style="{if $fedex_is_cart != ''}margin: auto;{/if} {if $psVersion == 1.6}color: #333333;{/if}">{l s='Submit' mod='fedex'}</a>
			</span>
				
		</div>
	{/if}
	</div>
</div>
<script type="text/javascript">
fedex_preview_update_state('{$fedex_is_cart}', {if isset($fedex_dest_state)}{$fedex_dest_state|intval}{else}0{/if});
</script>
<!-- /FEDEX SHIPPNG RATE PREVIEW -->
