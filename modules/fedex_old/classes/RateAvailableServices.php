<?php

require_once(dirname(__FILE__) . '/../fedex.php');

class FedexRate extends Fedex
{
	public function getRate($id_carrier, $id_zone, $totalWeight, $dest_zip = "", $dest_state = "", $dest_country = "", $dest_city = "", $product_price = 0, $id_product = 0, $id_product_attribute = 0, $qty = 1, $params = "", $products = NULL)
	{
		$cart = $this->context->cart;
		$log = false;

		if ($params != "")
			$cart = $params;

		$this->saveLog('call_log.txt', "getRate\n\r", $log);
		$this->saveLog('fedex_rate_log.txt',"Starting $id_zone, $dest_zip, $dest_country, $dest_city \n\r\n", $log);

		$fs_arr = Db::getInstance()->getRow('SELECT free_shipping_product, free_shipping_category, free_shipping_manufacturer, free_shipping_supplier FROM '._DB_PREFIX_.'fe_fedex_method WHERE id_carrier = "'.(int)$id_carrier.'"');

		// Get customer info //
		$customerInfo = $this->getCustomerInfo($id_zone, $dest_zip, $dest_country, $dest_city, $cart);

		if(!$customerInfo)
			return false;
		$dest_zip = $customerInfo['dest_zip'];
		$dest_country = $customerInfo['dest_country'];
		$dest_city = $customerInfo['dest_city'];
		$id_zone = $customerInfo['id_zone'];
		$is_company = $customerInfo['is_company'];
		
		if (($dest_zip == "" && $this->_fedex_address_display['zip'] == 1) OR (int)$id_zone == 0)
			return false;

		$st = time();

		$this->saveLog('fedex_rate_log.txt',"1) $id_carrier, $id_zone, $totalWeight, $id_product (p $product_price) (qty $qty) , $dest_zip, $dest_country, $dest_city\n\r\n", $log);

		$totalWeight = $this->getOrderTotalWeight($cart->id, $id_product, $qty, $totalWeight);
		$orderTotal = $this->getTotal($product_price, $qty, $cart);
		
		if(!$this->checkShippingRanges($id_carrier, $totalWeight, $orderTotal))
			return false;
		
		// Check to see if it's fedex shipping
		$carrier = $this->getCarrier($id_carrier, $id_zone);

		if (!$carrier)
			return false;

		$this->saveLog('fedex_rate_log.txt', "3) 2 ($orderTotal) $id_carrier, $id_zone, $totalWeight, $dest_zip , $dest_country, $dest_city\n\r ", $log);

		$this->saveLog('fedex_rate_log.txt', "6) Hashcache check: $dest_country, $dest_zip\n\r", $log);

		$products = $products ? $products : $cart->getProducts();

		$hash_rate = $this->getHash($id_carrier, $products, $id_product, $id_product_attribute, $qty, $dest_country, $dest_state, $dest_zip, true);         

		if ($hash_rate !== false)
		{
			$this->saveLog('fedex_rate_log.txt',"7) Cache\n\r"."hashcahsh = $hash_rate\n\r",$log);
			return $hash_rate <= -1 ? false : $hash_rate;
		}

		// Check Invalid Destination cache
		$invalid_dest = $this->checkInvalidDestination($carrier, $dest_zip, $dest_country, $log);

		if (is_array($invalid_dest) && sizeof($invalid_dest) >= 1)
		{
			$this->saveLog('fedex_rate_log.txt', "10) \n\r result".print_r($invalid_dest,true), $log);
			return false;
		}

		// Calculate insurance (if needed)
		$iamount = $this->calculateInsurance($carrier, $orderTotal, $id_product, $products, $qty, $cart);

		$this->saveLog('fedex_rate_log.txt', "\n\r11) TIME1: ".(time() - $st)."\nChecking cache $totalWeight,  $id_product, $qty", $log);

		$pack_dim = $this->getBoxes($id_carrier, $totalWeight, $cart, $id_product, $id_product_attribute, $qty, "YOUR_PACKAGING");
		if (is_array($pack_dim) && sizeof($pack_dim) == 0)
		{
			$this->saveLog('fedex_rate_log.txt', "\n\r12) Package Dim problem, ".print_r($pack_dim, true), $log);
			return false;
		}

		// Free Shipping per product;
		if ($pack_dim == false)
			return 0;

		$this->saveLog('fedex_rate_log.txt', "13) TIME2: ".(time() - $st)."\n", $log);

		// More than maximum rate, return and don't cache //
		if ((sizeof($pack_dim) * 150 < $totalWeight && !$this->is_free_ship_cart($products, $fs_arr)) || sizeof($pack_dim) > 50)
		{
			$this->saveLog('fedex_rate_log.txt', "\n\r14)  Weight / Package Dim problem", $log);
			return false;
		}      

		/** FEDEX ONLY ACCEPT 2 DECIMALS */
		foreach ($pack_dim as $key => $val)
		{
			$pack_dim[$key]['w'] = max($val['w'], 0.01);
			$pack_dim[$key]['h'] = max($val['h'], 0.01);
			$pack_dim[$key]['d'] = max($val['d'], 0.01);
			$pack_dim[$key]['weight'] = max($val['weight'], 0.01);
		}
		
		/**
		* GET CUBIC WEIGHT
		* 
		* IF LBs MUST DIVIDE FOR 139
		* ELSE (KGs) FOR 5000
		*/
		if ($this->_fedex_pack == "YOUR_PACKAGING")
		{
			$originalPack_dim = $pack_dim;
			
			foreach ($pack_dim as $key => $val)
			{
				if($this->_fedex_unit == 'LBS')
				{
					$val['w'] = $this->convertDimensionToIn($val['w']);
					$val['h'] = $this->convertDimensionToIn($val['h']);
					$val['d'] = $this->convertDimensionToIn($val['d']);
					$val['weight'] = $this->getWeightInLb($val['weight']);
				}
				else
				{
					$val['w'] = $this->convertDimensionToCm($val['w']);
					$val['h'] = $this->convertDimensionToCm($val['h']);
					$val['d'] = $this->convertDimensionToCm($val['d']);
					$val['weight'] = $this->getWeightInKg($val['weight']); 
				}
				
				$dimen = round($val['w'], 2) * round($val['h'], 2) * round($val['d'], 2);
				/** SAVE VOLUME OF PACKAGE TO LATER VERIFY GROUND SERVICES */
				$pack_dim[$key]['volume'] = $dimen;
				
				$dimen_weight = $dimen / ($this->_fedex_unit == 'LBS' ? 166 : 6000);
				/** SWITCH PACKAGE DIMENSIONS AND WEIGHT WITH CONVERTED VALUES */
				$pack_dim[$key]['w'] = round($val['w'], 2);
				$pack_dim[$key]['h'] = round($val['h'], 2);
				$pack_dim[$key]['d'] = round($val['d'], 2);
				$pack_dim[$key]['weight'] = round(max($val['weight'], $dimen_weight), 2);    
			}
		}
		
		/** GET ALL AVAILABLE CARRIERS */
		$fedex_carriers = $this->getCarriers($id_zone, $cart, Tools::getValue($this->name.'_is_cart', false), $id_product);      
		$eachCarrierRequest = array();
		/** VERIFY IF ANY OF THE CARRIERS HAS INSURANCE VALUE OR METHOD IS DOMESTIC (US) GROUND */
		if(is_array($fedex_carriers) && count($fedex_carriers))
		{
			foreach($fedex_carriers as $newCarrier)
			{
				$iamount = $this->calculateInsurance($newCarrier, $orderTotal, $id_product, $products, $qty, $cart);
				/** IF IS GROUD METHOD AND USES CUSTOMER PACKAGE */
				if(($newCarrier['method'] == 'FEDEX_GROUND' || $newCarrier['method'] == 'GROUND_HOME_DELIVERY') && $this->_fedex_pack == "YOUR_PACKAGING")
				{
					foreach ($originalPack_dim as $key => $val)
					{
						if($this->_fedex_unit == 'LBS')
						{
							$val['w'] = $this->convertDimensionToIn($val['w']);
							$val['h'] = $this->convertDimensionToIn($val['h']);
							$val['d'] = $this->convertDimensionToIn($val['d']);
							$val['weight'] = $this->getWeightInLb($val['weight']);
						}
						else
						{
							$val['w'] = $this->convertDimensionToCm($val['w']);
							$val['h'] = $this->convertDimensionToCm($val['h']);
							$val['d'] = $this->convertDimensionToCm($val['d']);
							$val['weight'] = $this->getWeightInKg($val['weight']); 
						}
						
						$dimen = round($val['w'], 2) * round($val['h'], 2) * round($val['d'], 2);						
						$dimen_weight = $dimen / ($this->_fedex_unit == 'LBS' ? 166 : 6000);   
						
						$min_volume_ground = ($this->_fedex_unit == 'LBS' ? 5184 : 84951);   
						/** IF OTHER SHIPMENTS WILL USE DIMENSIONAL WEIGHT, AND PACKAGE VOLUME IS LOWER THAN THE GROUND DIMENSIONAL WEIGHT STARTING VALUE */
						if($dimen < $min_volume_ground && $val['weight'] < $dimen_weight)
							$eachCarrierRequest['GROUNDS'][] = $newCarrier['method'];
						else
							$eachCarrierRequest['STANDARD'][] = $newCarrier['method'];
					}
				}
				/** IF HAS INSURANCE */
				elseif($iamount > 0)
					$eachCarrierRequest[$iamount][] = $newCarrier['method'];
				else
					$eachCarrierRequest['STANDARD'][] = $newCarrier['method'];	
			}
		} 
		
		/** IF SEPARATELY REQUEST IS NECESSARY */
		$requests = array();
		$response = false;
		if(is_array($eachCarrierRequest) && count($eachCarrierRequest))
		{
			$this->saveLog('fedex_rate_log.txt', "Grouped Requests: ".print_r($eachCarrierRequest, true)."\n\r", $log);
			
			$response = new stdClass();
			$response->HighestSeverity = 'NOTE';
			$response->Notifications = array();
			$response->RateReplyDetails = array();
			/** SET GRADE OF NOTE SEVERITY */
			$severityGrade = array(
				'ERROR' => 900,
				'FAILURE' => 850,
				'WARNING' => 800,
				'NOTE' => 0,
			);
			
			/** SEPARATE GROUP REQUESTS */
			foreach($eachCarrierRequest as $group => $eachCarrier)
			{
				/** FOREACH AVAILABLE CARRIER */
				foreach($fedex_carriers as $newCarrier)
				{
					/** IF IS GROUNDS METHODS GROUP AND IS CARRIER IN GROUP */
					if($group == 'GROUNDS' && in_array($newCarrier['method'], $eachCarrier) === true)
					{
						if(is_array($originalPack_dim) && count($originalPack_dim))
						{
							$newPack_dim = $originalPack_dim;
							foreach ($newPack_dim as $key => $val)
							{
								if($this->_fedex_unit == 'LBS')
								{
									$val['w'] = $this->convertDimensionToIn($val['w']);
									$val['h'] = $this->convertDimensionToIn($val['h']);
									$val['d'] = $this->convertDimensionToIn($val['d']);
									$val['weight'] = $this->getWeightInLb($val['weight']);
								}
								else
								{
									$val['w'] = $this->convertDimensionToCm($val['w']);
									$val['h'] = $this->convertDimensionToCm($val['h']);
									$val['d'] = $this->convertDimensionToCm($val['d']);
									$val['weight'] = $this->getWeightInKg($val['weight']); 
								}
								
								$dimen_weight = 0;
								$dimen = round($val['w'], 2) * round($val['h'], 2) * round($val['d'], 2);
								/** IF CUBIC WEIGHT > BOX WEIGHT AND METHOD IS NOT FEDEX GROUND AND VOLUME > (lb = 5184; kg = 84951) USE CUBIC WEIGHT */
								$min_volume_ground = ($this->_fedex_unit == 'LBS' ? 5184 : 84951);
								if($dimen > $min_volume_ground)
									$dimen_weight = $dimen / ($this->_fedex_unit == 'LBS' ? 166 : 6000);
								
								/** SWITCH PACKAGE DIMENSIONS AND WEIGHT WITH CONVERTED VALUES */
								$newPack_dim[$key]['w'] = round($val['w'], 2);
								$newPack_dim[$key]['h'] = round($val['h'], 2);
								$newPack_dim[$key]['d'] = round($val['d'], 2);
								$newPack_dim[$key]['weight'] = round(max($val['weight'], $dimen_weight), 2);    
							}
						}
					
						$iamount = $this->calculateInsurance($newCarrier, $orderTotal, $id_product, $products, $qty, $cart);
						/** CREATE REQUEST BUT DOES NOT EXECUTE IT */
						$requests[$group] = $this->requestRate($newPack_dim, $iamount, $dest_country, $dest_city, $dest_zip, $is_company, $log, false, true); 
						break;
					}	
					/** ELSE WILL BE INSURED GROUP
					* ELSEIF IS CARRIER IN GROUP */
					elseif(in_array($newCarrier['method'], $eachCarrier) === true)
					{
						$iamount = $this->calculateInsurance($newCarrier, $orderTotal, $id_product, $products, $qty, $cart);
						/** CREATE REQUEST BUT DOES NOT EXECUTE IT */
						$requests[$group] = $this->requestRate($pack_dim, $iamount, $dest_country, $dest_city, $dest_zip, $is_company, $log, false, true);
						break;
					}				
				}
			}   
		}
		else
			$response = $this->requestRate($pack_dim, $iamount, $dest_country, $dest_city, $dest_zip, $is_company, $log);  
			
		if(is_array($requests) && count($requests))
		{
			$useSSL = (Configuration::get('PS_SSL_ENABLED') || Tools::usingSecureMode() ? true : false);
			$protocol_content = (($useSSL) ? 'https://' : 'http://');
			$post_url = $protocol_content.Tools::getHttpHost().__PS_BASE_URI__.'modules/fedex/includes/requestSOAP.php';
	
			$requestResponses = $this->multiRequest($requests, $post_url);
			
			if(is_array($requestResponses) && count($requestResponses))
			{
				foreach($requestResponses as $key => $post_response)
				{
					if($post_response)
						$requestResponse = json_decode($post_response);
						
					if(is_object($requestResponse) && sizeof($requestResponse))
					{
						$request = str_replace('request=', '', $requests[$key]);
						$request = unserialize(base64_decode($request));
						
						$this->saveLog($this->_fedex_log_filename, 
							'// ------- Request: '."\n\r".print_r($request, true)."\n\r".
							'// ------- Response: '."\n\r".print_r($requestResponse, true)."\n\r
							\n\r", 
						$this->_fedex_xml_log);
					
						/** IF RETURNED ANY NOTIFICATION */            
						if(is_array($requestResponse->Notifications) && count($requestResponse->Notifications))
						{
							/** IF SEVERITY IS HIGHER */
							if($severityGrade[$requestResponse->HighestSeverity] > $severityGrade[$response->HighestSeverity])
								$response->HighestSeverity = $requestResponse->HighestSeverity;
								
							foreach($requestResponse->Notifications as $not)
							{
								/** USING THE NOTIFICATION CODE AS INDEX AVOID REPEATED NOTIFICATIONS */
								$response->Notifications[$not->Code] = $not;
							}
						}
						
						if(is_array($requestResponse->RateReplyDetails) && count($requestResponse->RateReplyDetails))
						{
							foreach($requestResponse->RateReplyDetails as $reply)
							{								
								if(in_array($reply->ServiceType, $eachCarrierRequest[$key]) === true)
									$response->RateReplyDetails[] = $reply;
							}
						}
					}
					else
					{
						$this->saveLog($this->_fedex_log_filename, 
							'// ------- Server Error: '.print_r($post_response, true).' - Please contact your hosting provider for more details'."\n\r",
						$this->_fedex_xml_log);
					}
				}
			}
			
			/** CREATE A GENERAL CASE WITH THE "ONE REQUEST" RESPONSE */
			$response = json_encode($response);
		}

		if($response)
		{    
			$response = json_decode($response); 
				  
			if (is_object($response) && sizeof($response))
			{
				$ret_amount = false;
				$this_carriers = $this->getCarriers($id_zone, $cart, Tools::getValue($this->name.'_is_cart', false), $id_product);

				$this->saveLog('fedex_rate_log.txt', "23) TIME4: ".(time() - $st)."\nGot rates, comparing to (".print_r($this_carriers, true).")\n\r", $log);

				foreach ($this_carriers as $this_carrier)
				{
					$method_found = false;
					$method_valid = false;
					
					/** CONVERT VALUES IF CURRENT_CURRENCY != DEFAULT_CURRENCY */
					if($this->context->cookie->id_currency != Configuration::get('PS_CURRENCY_DEFAULT'))
					{
						$currency = new Currency((int) $this->context->cookie->id_currency);
						$this_carrier['free_shipping'] = Tools::convertPrice($this_carrier['free_shipping'], $currency); 
					}
		
					if (is_array($response->RateReplyDetails))
					{
						$c = 0;
						foreach ($response->RateReplyDetails as $rateReply)
						{
							$serviceType = $rateReply -> ServiceType;
							$c++;
							$this->saveLog('fedex_rate_log.txt', "\n$c) / ".count($response->RateReplyDetails).") ".$this_carrier['method']." -- $serviceType\n", $log);
							// Check it there are no free shipping exceptions
							if ($this_carrier['method'] == $serviceType)
							{
								$method_valid = true;
								if ($this_carrier['id_carrier'] == $id_carrier || ($fs_arr['free_shipping_product'] == '' && $fs_arr['free_shipping_category'] == '' &&
									$fs_arr['free_shipping_manufacturer'] == '' && $fs_arr['free_shipping_supplier'] == '' &&
									$this_carrier['free_shipping_product'] == '' && $this_carrier['free_shipping_category'] == '' &&
									$this_carrier['free_shipping_manufacturer'] == '' && $this_carrier['free_shipping_supplier'] == ''))
								{
									if ($this_carrier['id_carrier'] == $id_carrier)
										$method_found = true;
								}
								else
								{
									$this->saveLog(
										'fedex_rate_log.txt',
										"\nNo method found ($c / ".count($response->RateReplyDetails).").".$this_carrier['id_carrier']." == $id_carrier || (".$fs_arr['free_shipping_product']." == '' && ".$fs_arr['free_shipping_category']." == '' &&".
										$fs_arr['free_shipping_manufacturer']." == '' && ".$fs_arr['free_shipping_supplier']." == '' &&".
										$this_carrier['free_shipping_product']." == '' && ".$this_carrier['free_shipping_category']." == '' && ".
										$this_carrier['free_shipping_manufacturer']." == '' && ".$this_carrier['free_shipping_supplier']." == ''))\n\r",
										$log
									);
									continue;
								}
							}
							else
								continue;

							if (is_object($rateReply->RatedShipmentDetails))
							{
								$amount = floatval($rateReply->RatedShipmentDetails->ShipmentRateDetail->TotalNetCharge->Amount);
								$fedex_currency = $this->FedexCurrencyConversion($rateReply->RatedShipmentDetails->ShipmentRateDetail->TotalNetCharge->Currency);
							}
							else
							{
								$amount = floatval($rateReply->RatedShipmentDetails[0]->ShipmentRateDetail->TotalNetCharge->Amount);
								$fedex_currency = $this->FedexCurrencyConversion($rateReply->RatedShipmentDetails[0]->ShipmentRateDetail->TotalNetCharge->Currency);
							}

							$fedex_id_currency = Currency::getIdByIsoCode($fedex_currency);
							if(!$fedex_id_currency)
								return -1;
							if (Configuration::get('PS_CURRENCY_DEFAULT') != $fedex_id_currency)
							{
								$default_currency = new Currency(Configuration::get('PS_CURRENCY_DEFAULT'));
								$fedex_currency = new Currency($fedex_id_currency);
								$amount = round(round($amount / (float)$fedex_currency->conversion_rate,2) * (float)$default_currency->conversion_rate,3);
							}
						
							$this->saveLog('fedex_rate_log.txt', $amount." == ".$serviceType." (".print_r($rateReply->RatedShipmentDetails, true).")\n\n\n", $log);
								
							// Check free shipping
							if($this_carrier['method'] == $serviceType && $this_carrier['id_carrier'] == $id_carrier && $this_carrier['free_shipping'] > 0 && $this_carrier['free_shipping'] <= $orderTotal)
							{
								$this->saveLog(
									'fedex_rate_log.txt',
									"\n". "(".$this_carrier['free_shipping']." > 0 && ".$this_carrier['free_shipping']." <= ".$orderTotal.")" ."\n\r",
									$log
								);
								$amount = 0;
							}
						
							// Write to cache //
							$query = 'INSERT INTO `'._DB_PREFIX_.'fe_fedex_rate_cache` (id_carrier, origin_zip, origin_country, dest_zip, dest_country, dest_resident, method, insurance, dropoff, packing, packages, weight, rate_code, rate, quote_date) VALUES
								("'.$this_carrier['id_carrier'].'","'.$this->_fedex_origin_zip.'","'.$this->_fedex_origin_country.'","'.$dest_zip.'","'.$dest_country.'","'.($is_company?'0':'1').'","'.$serviceType.'","'.$iamount.'","'.$this->_fedex_dropoff.'","'.$this->_fedex_pack.'","'.sizeof($pack_dim).'","'.$totalWeight.'","1","'.$amount.'","'.time().'")';
							Db::getInstance()->execute($query);
							$id_rate = Db::getInstance()->Insert_ID();
							// New hash cache
							$query = 'INSERT INTO `'._DB_PREFIX_.'fe_fedex_hash_cache` (id_fedex_rate, hash, hash_date) VALUES
								("'.$id_rate.'","'.$this->getHash($this_carrier['id_carrier'], $products, $id_product, $id_product_attribute, $qty, $dest_country, $dest_state, $dest_zip).'","'.time().'")';
							Db::getInstance()->execute($query);

							foreach ($pack_dim as $package)
							{
								$query = 'INSERT INTO `'._DB_PREFIX_.'fe_fedex_package_rate_cache` (id_fedex_rate, weight, width, height, depth) VALUES
									("'.$id_rate.'","'.$package['weight'].'","'.$package['w'].'","'.$package['h'].'","'.$package['d'].'")';
								Db::getInstance()->execute($query);
							}

							if ($carrier['method'] == $serviceType && $this_carrier['id_carrier'] == $id_carrier)
							{
								if ($carrier['extra_shipping_type'] == 2)
									$amount += $carrier['extra_shipping_amount'];
								elseif ($carrier['extra_shipping_type'] == 1)
									$amount += $carrier['extra_shipping_amount'] * $orderTotal / 100;
								elseif ($carrier['extra_shipping_type'] == 3)
									$amount += $carrier['extra_shipping_amount'] * $amount / 100;
								$ret_amount =  number_format($amount,2,".","");
							}
						}
						if (!$method_found && !$method_valid)
						{
							$this->saveInvalidDestination($this_carrier, $dest_zip, $dest_country);
						}
					}
					else
					{
						$rateReply = $response->RateReplyDetails;
						$serviceType = $rateReply->ServiceType;
						if ($this_carrier['method'] == $serviceType)
						{
							$method_valid = true;
							if ($this_carrier['id_carrier'] == $id_carrier || ($fs_arr['free_shipping_product'] == '' && $fs_arr['free_shipping_category'] == '' &&
								$fs_arr['free_shipping_manufacturer'] == '' && $fs_arr['free_shipping_supplier'] == '' &&
								$this_carrier['free_shipping_product'] == '' && $this_carrier['free_shipping_category'] == '' &&
								$this_carrier['free_shipping_manufacturer'] == '' && $this_carrier['free_shipping_supplier'] == ''))
							{
								if ($this_carrier['id_carrier'] == $id_carrier)
									$method_found = true;
							}
							else
							{
								$this->saveLog(
									'fedex_rate_log.txt',
									"\n".$this_carrier['id_carrier']." == $id_carrier || (".$fs_arr['free_shipping_product']." == '' && ".$fs_arr['free_shipping_category']." == '' &&".
									$fs_arr['free_shipping_manufacturer']." == '' && ".$fs_arr['free_shipping_supplier']." == '' &&".
									$this_carrier['free_shipping_product']." == '' && ".$this_carrier['free_shipping_category']." == '' && ".
									$this_carrier['free_shipping_manufacturer']." == '' && ".$this_carrier['free_shipping_supplier']." == ''))\n\r",
									$log
								);
								continue;
							}
						}
						else
							continue;

						if (is_object($rateReply->RatedShipmentDetails))
						{
							$amount = floatval($rateReply->RatedShipmentDetails->ShipmentRateDetail->TotalNetCharge->Amount);
							$fedex_currency = $this->FedexCurrencyConversion($rateReply->RatedShipmentDetails->ShipmentRateDetail->TotalNetCharge->Currency);
						}
						else
						{
							$amount = floatval($rateReply->RatedShipmentDetails[0]->ShipmentRateDetail->TotalNetCharge->Amount);
							$fedex_currency = $this->FedexCurrencyConversion($rateReply->RatedShipmentDetails[0]->ShipmentRateDetail->TotalNetCharge->Currency);
						}

						$this->saveLog('fedex_rate_log.txt', $amount." ===== ".print_r($rateReply->RatedShipmentDetails,true)."\n\n", $log);

						$fedex_id_currency = Currency::getIdByIsoCode($fedex_currency);

						if (Configuration::get('PS_CURRENCY_DEFAULT') != $fedex_id_currency)
						{
							$default_currency = new Currency(Configuration::get('PS_CURRENCY_DEFAULT'));
							$fedex_currency = new Currency($fedex_id_currency);
							$amount = round(round($amount / (float)$fedex_currency->conversion_rate,2) * (float)$default_currency->conversion_rate,3);
						}

						$this->saveLog('fedex_rate_log.txt', $amount." == ".$serviceType." (".print_r($rateReply->RatedShipmentDetails, true).")\n\n\n", $log);
							
						// Check free shipping
						if($this_carrier['method'] == $serviceType && $this_carrier['id_carrier'] == $id_carrier && $this_carrier['free_shipping'] > 0 && $this_carrier['free_shipping'] <= $orderTotal)
						{
							$this->saveLog(
								'fedex_rate_log.txt',
								"\n". "(".$this_carrier['free_shipping']." > 0 && ".$this_carrier['free_shipping']." <= ".$orderTotal.")" ."\n\r",
								$log
							);
							$amount = 0;
						}

						// Write to cache //
						$query = 'INSERT INTO `'._DB_PREFIX_.'fe_fedex_rate_cache` (id_carrier, origin_zip, origin_country, dest_zip, dest_country, dest_resident, method, insurance, dropoff, packing, packages, weight, rate_code, rate, quote_date) VALUES
						("'.$this_carrier['id_carrier'].'","'.$this->_fedex_origin_zip.'","'.$this->_fedex_origin_country.'","'.$dest_zip.'","'.$dest_country.'","'.($is_company?'0':'1').'","'.$serviceType.'","'.$iamount.'","'.$this->_fedex_dropoff.'","'.$this->_fedex_pack.'","'.sizeof($pack_dim).'","'.$totalWeight.'","1","'.$amount.'","'.time().'")';
						Db::getInstance()->execute($query);
						$id_rate = Db::getInstance()->Insert_ID();
						// New hash cache
						$query = 'INSERT INTO `'._DB_PREFIX_.'fe_fedex_hash_cache` (id_fedex_rate, hash, hash_date) VALUES
						("'.$id_rate.'","'.$this->getHash($this_carrier['id_carrier'], $products, $id_product, $id_product_attribute, $qty, $dest_country, $dest_state, $dest_zip).'","'.time().'")';
						Db::getInstance()->execute($query);

						$this->saveLog('fedex_rate_log.txt', "writing to cache $query\n\r", $log);

						foreach ($pack_dim as $package)
						{
							$query = 'INSERT INTO `'._DB_PREFIX_.'fe_fedex_package_rate_cache` (id_fedex_rate, weight, width, height, depth) VALUES
							("'.$id_rate.'","'.$package['weight'].'","'.$package['w'].'","'.$package['h'].'","'.$package['d'].'")';
							Db::getInstance()->execute($query);
						}

						if ($carrier['method'] == $serviceType && $this_carrier['id_carrier'] == $id_carrier)
						{
							if ($carrier['extra_shipping_type'] == 2)
								$amount += $carrier['extra_shipping_amount'];
							elseif ($carrier['extra_shipping_type'] == 1)
								$amount += $carrier['extra_shipping_amount'] * $orderTotal / 100;
							elseif ($carrier['extra_shipping_type'] == 3)
								$amount += $carrier['extra_shipping_amount'] * $amount / 100;
							$ret_amount =  number_format($amount,2,".","");
						}
					}
					if (!$method_found && !$method_valid)
					{
						$this->saveInvalidDestination($this_carrier, $dest_zip, $dest_country);
					}
				}
				
				$this->saveLog('fedex_rate_log.txt', "\n 25) ret_amount $ret_amount TOTAL TIME: ".(time() - $st)."\n\r", $log);

				return $ret_amount;
			}
			else
			{
				$this->saveInvalidDestination($carrier, $dest_zip, $dest_country);
				return false;
			}
		}
		else
		{
			$this->saveInvalidDestination($carrier, $dest_zip, $dest_country);
			return false;
		}
	}

	protected function getOrderTotalWeight($id_cart, $id_product, $qty, $totalWeight)
	{
		if ($id_product == 0)
		{
			$result = Db::getInstance()->getRow('
				SELECT SUM((p.`weight` + pa.`weight`) * cp.`quantity`) as nb
				FROM `'._DB_PREFIX_.'cart_product` cp
				LEFT JOIN `'._DB_PREFIX_.'product` p ON cp.`id_product` = p.`id_product`
				LEFT JOIN `'._DB_PREFIX_.'product_attribute` pa ON cp.`id_product_attribute` = pa.`id_product_attribute`
				WHERE (cp.`id_product_attribute` IS NOT NULL AND cp.`id_product_attribute` != 0)
				AND cp.`id_cart` = '.(int)($id_cart));
			$result2 = Db::getInstance()->getRow('
				SELECT SUM(p.`weight` * cp.`quantity`) as nb
				FROM `'._DB_PREFIX_.'cart_product` cp
				LEFT JOIN `'._DB_PREFIX_.'product` p ON cp.`id_product` = p.`id_product`
				WHERE (cp.`id_product_attribute` IS NULL OR cp.`id_product_attribute` = 0)
				AND cp.`id_cart` = '.(int)($id_cart));
			$totalWeight = round((float)($result['nb']) + (float)($result2['nb']), 3);
		}
		else
			$totalWeight *= $qty;
		// Convert Weight to lbs or kgs based on fedex default
		if ($this->_fedex_unit == 'LBS')
			$totalWeight = $this->getWeightInLb($totalWeight);
		else
			$totalWeight = $this->getWeightInKg($totalWeight);

		return $totalWeight;
	}

	protected function getTotal($product_price, $qty, $cart)
	{
		if ($product_price > 0)
			$orderTotal = $product_price * $qty;
		else
			$orderTotal = $cart->getOrderTotal(true, 7);

		return $orderTotal;
	}

	protected function getCustomerInfo($id_zone, $dest_zip, $dest_country, $dest_city, $cart)
	{
		$cookie_zip = $this->context->cookie->postcode ? $this->context->cookie->postcode :$this->context->cookie->pc_dest_zip;
		$cookie_country = $this->context->cookie->id_country ? $this->context->cookie->id_country :$this->context->cookie->pc_dest_country;

		$is_company = false;
		if ($this->_fedex_destin_resident == "0")
			$is_company = true;
			
		// Check if customer is logged in, and cart has an address selected.
		if ($cart->id_address_delivery > 0 && $this->context->customer->logged)
		{
			$address = new Address(intval($cart->id_address_delivery));
			if (!Validate::isLoadedObject($address))
			{
				$id_address = Address::getFirstCustomerAddressId($cart->id_customer, true);
				if ($id_address > 0)
					$address = new Address(intval($id_address));
				if (!Validate::isLoadedObject($address))
					return false;
			}

			if ($dest_zip == "")
				$dest_zip = $address->postcode;

			$country = new Country($address->id_country);
			if ($dest_country == "")
				$dest_country = $country->iso_code;

			if($dest_city == "")
				$dest_city = $address->city;

			if($address->company != "")
				$is_company = true;
		}
		else
		{
			if ($dest_zip == "" && $cookie_zip)
			{
				$dest_zip = $cookie_zip;
				$dest_city = $this->context->cookie->pc_dest_city;
			}
			elseif ($dest_zip == "" && $this->_fedex_address_display['zip'] == 1)
				return false;

			if($dest_country == "" && $cookie_country)
			{
				$dest_country = $cookie_country;
				$country = new Country($dest_country);
				$dest_country = $country->iso_code;
			}
		}

		if((int)$id_zone == 0)
		{
			$id_country = $cookie_country;
			if((int)$id_country > 0)
				$id_zone = Country::getIdZone($id_country);
			if((int)$id_country == 0 OR (int)$id_zone == 0)
				return false;
		}

		return array(
			'dest_zip' => $dest_zip,
			'dest_country' => $dest_country,
			'dest_city' => $dest_city,
			'id_zone' => $id_zone,
			'is_company' => $is_company,
		);
	}

	protected function checkInvalidDestination($carrier, $dest_zip, $dest_country, $log)
	{
		$cache_timeout = 86400; // Seconds.
		$query = '
			SELECT *
			FROM `'._DB_PREFIX_.'fe_fedex_invalid_dest`
			WHERE method = "'.$carrier['method'].'" AND zip = "'.$dest_zip.'" AND country = "'.$dest_country.'" AND ondate > '.(time()-$cache_timeout);

		$this->saveLog('fedex_rate_log.txt', "\n\r9) Invalid query $query\n\r", $log);

		$invalid_dest = Db::getInstance()->executeS($query);

		return $invalid_dest;
	}

	protected function calculateInsurance($carrier, $orderTotal, $id_product, $products, $qty, $cart)
	{
		$iamount = 0;
		if ($carrier['insurance_type'] != 0)
		{
			if ($orderTotal >= $carrier['insurance_minimum'])
			{
				if ($id_product == 0)
				{
					if ($carrier['insurance_type'] == 1)
					{
						$order_total = 0;
						foreach ($products AS $product)
						{
							$pro = new Product($product['id_product']);
							$price = floatval($pro->wholesale_price);
							$total_price = $price * intval($product['cart_quantity']);
							$order_total += $total_price;
						}
						$iamount = $order_total;
					}
					else if ($carrier['insurance_type'] == 2)
						$iamount = floatval($carrier['insurance_amount']) * $orderTotal / 100;
				}
				else
				{
					$pro = new Product($id_product);
					if ($carrier['insurance_type'] == 1)
						$iamount = floatval($pro->wholesale_price) * $qty;
					else if ($carrier['insurance_type'] == 2)
						$iamount = floatval($carrier['insurance_amount']) * (floatval($pro->price) * $qty) / 100;
				}
				$iamount = intval($iamount,2);
				if ($cart->id_currency != Configuration::get('PS_CURRENCY_DEFAULT'))
				{
					$currency = new Currency($cart->id_currency);
					$iamount = Tools::convertPrice($iamount, $currency);
				}
				if ($carrier['insurance_minimum'] > $orderTotal)
					$iamount = 0;
			}
		}

		return $iamount;
	}

	protected function saveInvalidDestination($carrier, $dest_zip, $dest_country)
	{
		$query = '
			DELETE
			FROM `'._DB_PREFIX_.'fe_fedex_invalid_dest`
			WHERE method = "'.$carrier['method'].'" AND zip = "'.$dest_zip.'" AND country = "'.$dest_country.'"';
		Db::getInstance()->execute($query);
		// Add invalid zip / carrier to cache.
			$query = '
			INSERT INTO `'._DB_PREFIX_.'fe_fedex_invalid_dest`
			(method, zip, state, country, ondate)
			VALUES ("'.$carrier['method'].'","'.$dest_zip.'", "", "'.$dest_country.'","'.time().'")
		';
		Db::getInstance()->execute($query);
	}

	protected function getDate()
	{
		$date = new DateTime();
		//if day is Saturday or Sunday it should be changed
		if($date->format('w') == 0 OR $date->format('w') == 6)
			$date->modify('+2 day');
		$date = $date->format('Y-m-d');

		return $date;
	}

	protected function requestRate($pack_dim, $iamount, $dest_country, $dest_city, $dest_zip, $is_company, $log, $carrier_method = false, $noRequest = false)
	{
		$request['WebAuthenticationDetail'] = array('UserCredential' =>	array('Key' => $this->_fedex_key, 'Password' => $this->_fedex_pass));
		$request['ClientDetail'] = array('AccountNumber' => $this->_fedex_account, 'MeterNumber' => $this->_fedex_meter);
		$request['TransactionDetail'] = array('CustomerTransactionId' => 'Rate a Single Package v13');
		$request['Version'] = array('ServiceId' => 'crs', 'Major' => '13', 'Intermediate' => '0', 'Minor' => '0');
		$request['ReturnTransitAndCommit'] = 1;
		$request['RequestedShipment']['ShipTimestamp'] = date('c');
		if($carrier_method)
			$request['RequestedShipment']['ServiceType'] = $carrier_method;
		$request['RequestedShipment']['DropoffType'] = $this->_fedex_dropoff; // valid values REGULAR_PICKUP, REQUEST_COURIER, ...
		$request['RequestedShipment']['PackagingType'] = $this->_fedex_pack;
		$request['RequestedShipment']['Shipper'] = array(
			'Address' => array(
				'StreetLines' => array(''), // Origin details
				'City' => $this->_fedex_origin_city,
				'StateOrProvinceCode' => '',
				'PostalCode' => $this->_fedex_origin_zip,
				'CountryCode' => $this->_fedex_origin_country,
				'Residential' => $this->_fedex_origin_resident == '1'?'1':'0'
			)
		);
		$request['RequestedShipment']['Recipient'] = array(
			'Address' => array(
				'StreetLines' => array(''), // Destination details
				'City' => $dest_city,
				'StateOrProvinceCode' => '',
				'PostalCode' => $dest_zip,
				'CountryCode' => $dest_country,
				'Residential' => !$is_company?'1':'0'
			)
		);
		$request['RequestedShipment']['ShippingChargesPayment'] = array(
			'PaymentType' => 'SENDER',
			'Payor' => array('AccountNumber' => $this->_fedex_account,
			'CountryCode' => $this->_fedex_origin_country)
		);
		$request['RequestedShipment']['RateRequestTypes'] = 'ACCOUNT';
		$request['RequestedShipment']['PackageCount'] = sizeof($pack_dim);
		$request['RequestedShipment']['PackageDetail'] = 'INDIVIDUAL_PACKAGES';
		$pack_xml = array();
		if(is_array($pack_dim) && count($pack_dim))
		{
			foreach ($pack_dim as $package)
			{
				/** REMINDER: WEIGHT AND DIMENSIONS HAVE ALREADY BEEN CONVERTED */
				$packageArray['GroupPackageCount'] = 1;
				$packageArray['Weight'] = array(
					'Value' => $package['weight'],
					'Units' => ($this->_fedex_unit == 'LBS' ? 'LB' : 'KG')
				);                
				
				/** ONLY INSERT PACKAGE SIZE IN REQUEST IF PACKAGE IS CUSTOMIZED */
				if($this->_fedex_pack == 'YOUR_PACKAGING')
				{
					$packageArray['Dimensions'] = array(
						'Length' => $package['d'],
						'Width' => $package['w'],
						'Height' => $package['h'],
						'Units' => ($this->_fedex_unit == 'LBS' ? 'IN' : 'CM')
					);
				}
				
				array_push($pack_xml, $packageArray);
			}
		}

		$request['RequestedShipment']['RequestedPackageLineItems'] = $pack_xml;
		if ($iamount > 0)
			$request['RequestedShipment']['RequestedPackageLineItems'][0]['InsuredValue'] = array('Amount' => $iamount, 'Currency' => 'USD');
			
		$useSSL = (Configuration::get('PS_SSL_ENABLED') ? true : false);
		$protocol_content = (($useSSL) ? 'https://' : 'http://');
		$post_url = $protocol_content.Tools::getHttpHost().__PS_BASE_URI__.'modules/fedex/includes/requestSOAP.php';
			
		if(!$noRequest)
			return $this->curl_post($post_url, 'request='.base64_encode(serialize($request)));
		else
			return 'request='.base64_encode(serialize($request));
	}

	protected function FedexCurrencyConversion($iso)
	{
		$codes = array(
			'RDD' => 'DOP', // Dominican Peso
			'ECD' => 'XCD', // Caribbean Dollars
			'ARN' => 'ARS', // Argentina Peso
			'SID' => 'SGD', // Singapore Dollars
			'WON' => 'KRW', // South Korea Won
			'JAD' => 'JMD', // Jamaican Dollars
			'SFR' => 'CHF', // Swiss Francs
			'JYE' => 'JPY', // Japanese Yen
			'KUD' => 'KWD', // Kuwaiti Dinars
			'UKL' => 'GBP', // British Pounds
			'DHS' => 'AED', // UAE Dirhams
			'NMP' => 'MXN', // Mexican Pesos
			'UYP' => 'UYU', // Uruguay New Pesos
			'CHP' => 'CLP', // Chilean Pesos
			'NTD' => 'TWD', // New Taiwan Dollars
		);
		return isset($codes[$iso]) ? $codes[$iso] : $iso;
	}
}