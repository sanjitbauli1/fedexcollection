<?php
if(!in_array('Fedex', get_declared_classes()))
	require_once( _PS_MODULE_DIR_ . 'fedex/fedex.php');

class LabelPrinting extends Fedex 
{
	private $_requests = array();
	private $_post_response = '';
	private $_response = array('errors' => array(), 'success' => array());
	private $_tracking_numbers = array();
	private $_return_tracking_numbers = array();
	private $_trackingIdType = null;
	private $_return_trackingIdType = null;
	private $_error = false;

	private $_usps_post_url = "https://secure.shippingapis.com/ShippingAPI.dll";
	private $_fedex_path_to_wsdl;
	private $_fedex_soap_client;
	private $_fedex_label_infos = array();
	private $_fedex_label_packages = array();
	
	function __construct()
	{
		parent::__construct();
		$this->_fedex_path_to_wsdl = ($this->_fedex_mode == 'live' ? dirname(__FILE__)."/../includes/ShipService_v12.wsdl" : dirname(__FILE__)."/../includes/TestShipService_v12.wsdl");
		ini_set("soap.wsdl_cache_enabled", "0");
		$this->_fedex_soap_client = new SoapClient($this->_fedex_path_to_wsdl, array('trace' => 1)); // Refer to http://us3.php.net/manual/en/ref.soap.php for more information
		
		if(isset($_POST))
		{
			$this->prepareLabelInfo();
			$this->_fedex_label_packages = Tools::getValue('pack');
			
			$this->_response['errors'] = $this->validateRequiredInfos();
			$this->_response['success'] = array();
		}
	}
	
	public function getShippingLabel($id_order)
	{
		if(is_array($this->_response) && !count($this->_response['errors']))
		{
			foreach ($this->_fedex_label_packages as $package)
			{
				$this->_requests['standard'] = $this->buildLabelRequest($package);
				if($this->_fedex_label_infos['LabelReturn'])
					$this->_requests['return'] = $this->buildLabelRequest($package, true);

				foreach ($this->_requests as $label_type => $request)
				{
					if(!is_array($request)) //if request was not built correctly
					{
						$this->_error = array(0, is_string($request) ? $request : $this->l('Please change incorrect settings and try again.'));
						break;
					}

					try
					{
						$this->_post_response = $this->_fedex_soap_client->processShipment($request);  // FedEx web service invocation

						if ($this->_post_response->HighestSeverity != 'FAILURE' && $this->_post_response->HighestSeverity != 'ERROR')
						{
							//success
							/** CONVERT IMAGE TO BASE64 TO AVOID CHARACTER ERRORS */
							$this->_post_response->CompletedShipmentDetail->CompletedPackageDetails->Label->Parts->Image = base64_encode($this->_post_response->CompletedShipmentDetail->CompletedPackageDetails->Label->Parts->Image);
							$this->_response[] = $this->_post_response;
							if($label_type == 'standard')
							{
								$this->_trackingIdType = $this->_post_response->CompletedShipmentDetail->CompletedPackageDetails->TrackingIds->TrackingIdType; //the same for all packages, shipping method
								$this->_tracking_numbers[] = $this->_post_response->CompletedShipmentDetail->CompletedPackageDetails->TrackingIds->TrackingNumber;
							}
							else //return label
							{
								$this->_return_trackingIdType = $this->_post_response->CompletedShipmentDetail->CompletedPackageDetails->TrackingIds->TrackingIdType; //the same for all packages, shipping method
								$this->_return_tracking_numbers[] = $this->_post_response->CompletedShipmentDetail->CompletedPackageDetails->TrackingIds->TrackingNumber;
							}
						}
						else
						{
							if(is_array($this->_post_response->Notifications))
							{
								$error = '';
								foreach ($this->_post_response->Notifications as $notification)
								{
									$this->_error .= $notification->Code.': '.$notification->Message.'<br>';
								}
								$this->_error = array(0, $this->_error);
							}
							else
								$this->_error = array(0, @$this->_post_response->Notifications->Code.': '.@$this->_post_response->Notifications->Message);
							break;
						}
					} catch (SoapFault $exception) {
						return array(0, $exception);
					}
				}
				if($this->_error)
					break;
			}
		}
		else
		{
			$this->_response[0] = 0;
			return $this->_response;    
		}

		//if some request failed but before it were successful requests, we need to cancel all successful requests
		if($this->_error)
		{
			if($this->_trackingIdType)
				$this->deleteShipment($this->_tracking_numbers, $id_order, $this->_trackingIdType);
			if($this->_return_trackingIdType)
				$this->deleteShipment($this->_return_tracking_numbers, $id_order, $this->_return_trackingIdType);


			return $this->_error;
		}

		//if success
		if((int)$this->_fedex_label_order_status > 0)
		{
			$history = new OrderHistory();
			$history->id_order = (int)$id_order;
			$history->changeIdOrderState($this->_fedex_label_order_status, $id_order);
			$history->addWithemail();
		}

		//saving tracking number
		$order = new Order($id_order);
		$order->shipping_number = implode(', ', $this->_tracking_numbers);
		$order->update();
		
		$id_order_carrier = Db::getInstance()->getValue('
			SELECT `id_order_carrier`
			FROM `'._DB_PREFIX_.'order_carrier`
			WHERE `id_order` = '.(int)$order->id);
		if ($id_order_carrier)
		{
			$order_carrier = new OrderCarrier($id_order_carrier);
			$order_carrier->tracking_number = implode(', ', $this->_tracking_numbers);
			$order_carrier->update();
		}
		//sending email to customer
		if ($order->shipping_number)
		{
			global $_LANGMAIL;
			$customer = new Customer((int)($order->id_customer));
			$carrier = new Carrier((int)($order->id_carrier));
			if (!Validate::isLoadedObject($customer) OR !Validate::isLoadedObject($carrier))
				die(Tools::displayError());
			$templateVars = array(
				'{followup}' => str_replace('@', $order->shipping_number, $carrier->url),
				'{firstname}' => $customer->firstname,
				'{lastname}' => $customer->lastname,
				'{id_order}' => (int)($order->id),
				'{order_name}' => (int)($order->id)
			);
	
			if($this->getPSV() == 1.6)
				$templateVars['{order_name}'] = $order->getUniqReference();
			
			@Mail::Send((int)$order->id_lang, 'in_transit', Mail::l('Package in transit', (int)$order->id_lang), $templateVars,
				$customer->email, $customer->firstname.' '.$customer->lastname, NULL, NULL, NULL, NULL,
				_PS_MAIL_DIR_, true);
		}
		$this->_tracking_numbers = serialize($this->_tracking_numbers);
		if($this->_return_tracking_numbers)
			$this->_return_tracking_numbers = serialize($this->_return_tracking_numbers);
		Db::getInstance()->execute('
			UPDATE `'._DB_PREFIX_.'fe_fedex_labels_info`
			SET `tracking_id_type` = \''.pSQL($this->_trackingIdType).'\', `tracking_numbers` = \''.$this->_tracking_numbers.'\'
			'.($this->_return_tracking_numbers ? ', `return_tracking_numbers` = \''.$this->_return_tracking_numbers.'\'' : '').'
			'.($this->_return_trackingIdType ? ', `return_tracking_id_type` = \''.$this->_return_trackingIdType.'\'' : '').'
			WHERE `id_order` = '.(int)$id_order.'
		');

		return array(1, $this->_response);
	}

	protected function prepareLabelInfo()
	{
		$this->_fedex_label_infos = array();

		$this->_fedex_label_infos['ServiceType'] = Tools::getValue('shipping_type');
		$this->_fedex_label_infos['ImageType'] = Tools::getValue('label_format');
		$this->_fedex_label_infos['LabelStockType'] = Tools::getValue('label_stock_type');
		$this->_fedex_label_infos['LabelReturn'] = Tools::isSubmit('label_return');
		$this->_fedex_label_infos['ReturnShippingType'] = Tools::getValue('label_return_shipping_method');
		$this->_fedex_label_infos['Currency'] = Tools::getValue('currency');

		$this->_fedex_label_infos['Shipper']['CompanyName'] = htmlspecialchars(trim(Tools::getValue('shop_name')));
		$this->_fedex_label_infos['Shipper']['PersonName'] = htmlspecialchars(trim(Tools::getValue('attention_name')));
		$this->_fedex_label_infos['Shipper']['PhoneNumber'] = htmlspecialchars(trim(Tools::getValue('phone_number')));
		$address1 = htmlspecialchars(trim(Tools::getValue('address1')));
		$address2 = htmlspecialchars(trim(Tools::getValue('address2')));
		$this->_fedex_label_infos['Shipper']['StreetLines'] = array($address1, $address2);
		$this->_fedex_label_infos['Shipper']['City'] = htmlspecialchars(trim(Tools::getValue('shop_city')));
		$shop_country = new Country(trim(Tools::getValue('shop_country')));
		$this->_fedex_label_infos['Shipper']['CountryCode'] = $shop_country->iso_code;
		$shop_state = new State(trim(Tools::getValue('shop_state')));
		$this->_fedex_label_infos['Shipper']['StateOrProvinceCode'] = Country::containsStates($shop_country->id) ? $shop_state->iso_code : '';
		$this->_fedex_label_infos['Shipper']['PostalCode'] = htmlspecialchars(trim(Tools::getValue('shop_postal')));
		
		$order = new Order(Tools::getValue('id_order'));
		$address = new Address($order->id_address_delivery);
		$customerState = $address->id_state ? (new State($address->id_state)) : NULL;
		$customerCountry = new Country($address->id_country);

		$this->_fedex_label_infos['Recipient']['PersonName'] = htmlspecialchars(Tools::getValue('shipto_attention_name', ''));
		$shipto_company = htmlspecialchars(Tools::getValue('shipto_company', ''));
		$this->_fedex_label_infos['Recipient']['CompanyName'] = ($shipto_company == '' && $this->_fedex_destin_resident == 0? $this->_fedex_label_infos['Recipient']['PersonName'] : $shipto_company);
		$this->_fedex_label_infos['Recipient']['PhoneNumber'] = htmlspecialchars(Tools::getValue('shipto_phone', ''));
		$shipto_address1 = htmlspecialchars(Tools::getValue('shipto_address1', ''));
		$shipto_address2 = htmlspecialchars(Tools::getValue('shipto_address2', ''));
		$this->_fedex_label_infos['Recipient']['StreetLines'] = array($shipto_address1, $shipto_address2);
		$this->_fedex_label_infos['Recipient']['City'] = htmlspecialchars(Tools::getValue('shipto_city', ''));
		$shipto_state = Tools::getValue('shipto_state') != '' ? new State(Tools::getValue('shipto_state')) : (is_object($customerState) ? $customerState : NULL);
		$shipto_country = Tools::getValue('shipto_country') != '' ? new Country(Tools::getValue('shipto_country')) : $customerCountry;
		$this->_fedex_label_infos['Recipient']['StateOrProvinceCode'] = Country::containsStates($shipto_country->id) ? ($shipto_state ? htmlspecialchars($shipto_state->iso_code) : '') : '';
		$this->_fedex_label_infos['Recipient']['CountryCode'] = htmlspecialchars($shipto_country->iso_code);
		$this->_fedex_label_infos['Recipient']['PostalCode'] = htmlspecialchars(Tools::getValue('shipto_postcode', ''));

		return $this->_fedex_label_infos;
	}
	
	/*
	* Validate label settings, and package information
	* 
	* @return array(
	*   [] => array(
	*       'Description' => Error description,
	*       'HelpContext' => 0 (when Help Context returns 0, it is not an USPS default error. In this case "[#{CODE}] {Error description}" does not appear )
	*   ),
	* );
	*/
	protected function validateRequiredInfos()
	{
		$return = array();
		$isINTL = false;
		if($this->_fedex_label_infos['Shipper']['CountryCode'] != $this->_fedex_label_infos['Recipient']['CountryCode'])
			$isINTL = true;
		
		/* VALIDATE LABEL SETTINGS */
		
		/* Verify Shipping Type, if there is something selected */
		if(!strlen($this->_fedex_label_infos['ServiceType']) || $this->_fedex_label_infos['ServiceType'] == 'none'){
			$return[] = array(
				'Description' => Tools::displayError('Shipping Type is a required field.'),
				'HelpContext' => 0 
			);
		}
		
		/* Verify Image Type, if there is something selected */
		if(!strlen($this->_fedex_label_infos['ImageType'])){
			$return[] = array(
				'Description' => Tools::displayError('Label Format is a required field.'),
				'HelpContext' => 0 
			);
		}
		
		/* Verify Label Stock Type, if there is something selected */
		if(!strlen($this->_fedex_label_infos['LabelStockType'])){
			$return[] = array(
				'Description' => Tools::displayError('Label Stock Type is a required field.'),
				'HelpContext' => 0 
			);
		}
		
		/* Verify Currency, if there is something selected */
		if(!strlen($this->_fedex_label_infos['Currency'])){
			$return[] = array(
				'Description' => Tools::displayError('Currency is a required field.'),
				'HelpContext' => 0 
			);
		}
		
		/* VALIDATE REQUIRED CUSTOMER INFOS */
		
		/* Verify Name, if there is something wrote */
		if(!strlen($this->_fedex_label_infos['Recipient']['PersonName'])){
			$return[] = array(
				'Description' => Tools::displayError('Customer Name is a required field.'),
				'HelpContext' => 0 
			);
		}
		
		/* Verify Address, if there is something wrote */
		if(!strlen($this->_fedex_label_infos['Recipient']['StreetLines'][0])){
			$return[] = array(
				'Description' => Tools::displayError('Customer Address Line 1 is a required field.'),
				'HelpContext' => 0 
			);
		}
		
		/* Verify City, if there is something wrote */
		if(!strlen($this->_fedex_label_infos['Recipient']['City'])){
			$return[] = array(
				'Description' => Tools::displayError('Customer City is a required field.'),
				'HelpContext' => 0 
			);
		}
		
		/* Verify Country, if there is something selected */
		if(!strlen($this->_fedex_label_infos['Recipient']['CountryCode'])){
			$return[] = array(
				'Description' => Tools::displayError('Customer Country is a required field.'),
				'HelpContext' => 0 
			);
		}
		
		$country = new Country($this->_fedex_label_infos['Recipient']['CountryCode']);
		
		/* Verify ZIP Code, if there is something wrote. For non international shipping, if is a valid format */
		if(!strlen($this->_fedex_label_infos['Recipient']['PostalCode'])){
			$return[] = array(
				'Description' => Tools::displayError('Customer Zip Code is a required field.'),
				'HelpContext' => 0 
			);
		}elseif($country->iso_code == 'US' && !preg_match('/^[0-9]{5}$/', $this->_fedex_label_infos['Recipient']['PostalCode'])){
			$return[] = array(
				'Description' => Tools::displayError('Customer Zip Code format is incorrect. (Ex: 20770)'),
				'HelpContext' => 0 
			);
		}
		
		/* VALIDATE SENDER INFORMATION */  
		
		/* Verify Name, if there is something wrote */
		if(!strlen($this->_fedex_label_infos['Shipper']['PersonName'])){
			$return[] = array(
				'Description' => Tools::displayError('Your Name is a required field.'),
				'HelpContext' => 0 
			);
		}
		
		/* Verify Address, if there is something wrote */
		if(!strlen($this->_fedex_label_infos['Shipper']['StreetLines'][0])){
			$return[] = array(
				'Description' => Tools::displayError('Your Address Line 1 is a required field.'),
				'HelpContext' => 0 
			);
		}
		
		/* Verify City, if there is something wrote */
		if(!strlen($this->_fedex_label_infos['Shipper']['City'])){
			$return[] = array(
				'Description' => Tools::displayError('Your City is a required field.'),
				'HelpContext' => 0 
			);
		}
		
		/* Verify Country, if there is something selected */
		if(!strlen($this->_fedex_label_infos['Shipper']['CountryCode'])){
			$return[] = array(
				'Description' => Tools::displayError('Your Country is a required field.'),
				'HelpContext' => 0 
			);
		}
		
		$country = new Country($this->_fedex_label_infos['Shipper']['CountryCode']);
		
		/* Verify ZIP Code, if there is something wrote. For non international shipping, if is a valid format */
		if(!strlen($this->_fedex_label_infos['Shipper']['PostalCode'])){
			$return[] = array(
				'Description' => Tools::displayError('Your Zip Code is a required field.'),
				'HelpContext' => 0 
			);
		}elseif($country->iso_code == 'US' && !preg_match('/^[0-9]{5}$/', $this->_fedex_label_infos['Shipper']['PostalCode'])){
			$return[] = array(
				'Description' => Tools::displayError('Your Zip Code format is incorrect. (Ex: 20770)'),
				'HelpContext' => 0 
			);
		}
		
		/* VALIDATE PACKAGES */
		if(is_array($this->_fedex_label_packages) && count($this->_fedex_label_packages))
		{
			foreach ($this->_fedex_label_packages as $key => $package){
				
				$key++;
				
				/* Verify Package Width, if there is something wrote, format and if greater than 0 */
				if(isset($package['w']) && strlen($package['w']) && !is_numeric($package['w'])){
					$return[] = array(
						'Description' => Tools::displayError('Width of the package '). $key .Tools::displayError(' need to be numeric.'),
						'HelpContext' => 0 
					);
				}  
				
				/* Verify Package Height, if there is something wrote, format and if greater than 0 */
				if(isset($package['h']) && strlen($package['h']) && !is_numeric($package['h'])){
					$return[] = array(
						'Description' => Tools::displayError('Height of the package '). $key .Tools::displayError(' need to be numeric.'),
						'HelpContext' => 0 
					);
				}
				
				/* Verify Package Depth, if there is something wrote, format and if greater than 0 */
				if(isset($package['d']) && strlen($package['d']) && !is_numeric($package['d'])){
					$return[] = array(
						'Description' => Tools::displayError('Depth of the package '). $key .Tools::displayError(' need to be numeric.'),
						'HelpContext' => 0 
					);
				} 
				
				/* Verify Package Weight, if there is something wrote, format and if greater than 0 */
				if(!strlen($package['weight'])){
					$return[] = array(
						'Description' => Tools::displayError('Field Weight of the package '). $key .Tools::displayError(' is required.'),
						'HelpContext' => 0 
					);
				}elseif(!is_numeric($package['weight'])){
					$return[] = array(
						'Description' => Tools::displayError('Wight of the package '). $key .Tools::displayError(' need to be numeric.'),
						'HelpContext' => 0 
					);
				}elseif($package['weight'] <= 0){
					$return[] = array(
						'Description' => Tools::displayError('Weight of the package '). $key .Tools::displayError(' need to be greater than 0.'),
						'HelpContext' => 0 
					);
				}
				
				/* Verify Package Insured Value, if there is something wrote, format and if greater than 0 */
				if(isset($package['insurance']) && $package['insurance'] < 0){
					$return[] = array(
						'Description' => Tools::displayError('Insured Value of the package '). $key .Tools::displayError(' need to be greater than 0.'),
						'HelpContext' => 0 
					);
				}elseif(strlen($package['insurance']) && !is_numeric($package['insurance'])){
					$return[] = array(
						'Description' => Tools::displayError('Insured Value of the package '). $key .Tools::displayError(' need to be numeric.'),
						'HelpContext' => 0 
					);
				}
				
				/* Verify Package COD value, if there is something selected, must be > 0*/
				if(isset($package['cod_code']) && $package['cod_code'] != '' && $package['cod_amount'] < 0){
					$return[] = array(
						'Description' => Tools::displayError('COD Amount of the package '). $key .Tools::displayError(' need to be greater than 0.'),
						'HelpContext' => 0 
					);
				}elseif(strlen($package['cod_code']) && $package['cod_code'] != '' && !is_numeric($package['cod_amount'])){
					$return[] = array(
						'Description' => Tools::displayError('COD Amount of the package '). $key .Tools::displayError(' need to be numeric.'),
						'HelpContext' => 0 
					);
				}
				 
				/* INTERNATIONAL VERIFICATION */
				if($isINTL || $this->_requiresINTLOptions($this->_fedex_label_infos['Shipper']['CountryCode'], $this->_fedex_label_infos['Recipient']['CountryCode']))
				{
					$productsNotInBox = 0;
					foreach($package['products'] as $keyProd => $productInfo){
						$keyProd++;
						
						if(strlen($productInfo['Description']) && $productInfo['Quantity'] <= 0){
							$return[] = array(
								'Description' => Tools::displayError('Quantity of the product '). $keyProd . Tools::displayError(' of the package '). $key .Tools::displayError(' need to be greater than 0, please verify.'),
								'HelpContext' => 0 
							);
						}
						
						if(strlen($productInfo['Description']) && $productInfo['Value'] <= 0){
							$return[] = array(
								'Description' => Tools::displayError('Unit Value of the product '). $keyProd . Tools::displayError(' of the package '). $key .Tools::displayError(' need to be greater than 0, please verify.'),
								'HelpContext' => 0 
							);
						}
						
						if(strlen($productInfo['Description']) && !strlen($productInfo['CountryofManufacture'])){
							$return[] = array(
								'Description' => Tools::displayError('Country of Manufacture of the product '). $keyProd . Tools::displayError(' of the package '). $key .Tools::displayError(' is required, please verify.'),
								'HelpContext' => 0 
							);
						}
						
						if(!strlen($productInfo['Description'])){
							$productsNotInBox++;
						}
					} 
				
					if($productsNotInBox == count($package['products'])){
						$return[] = array(
							'Description' => Tools::displayError('You must insert the products information of the package '). $key .'.',
							'HelpContext' => 0 
						);
					}
					
					if(isset($package['ShippersLoadandCount']) && !strlen($package['ShippersLoadandCount']))
					{
						$return[] = array(
							'Description' => Tools::displayError('You must insert the Shippers Load and Count information of the package '). $key .'.',
							'HelpContext' => 0 
						);
					}
					elseif(!is_numeric($package['ShippersLoadandCount'])) 
					{
						$return[] = array(
							'Description' => Tools::displayError('Shippers Load and Count information of the package '). $key .' '. Tools::displayError('must be numeric'),
							'HelpContext' => 0 
						);
					} 
					elseif($package['ShippersLoadandCount'] < 0) 
					{
						$return[] = array(
							'Description' => Tools::displayError('Shippers Load and Count information of the package '). $key .' '. Tools::displayError('must be greater than 0'),
							'HelpContext' => 0 
						);
					} 
					
					if($this->_fedex_label_infos['LabelReturn'] && $package['customs_options_type'] == 'OTHER' && !strlen($package['customs_options_description']))
					{
						$return[] = array(
							'Description' => Tools::displayError('Customs Options Description information of the package '). $key .' '. Tools::displayError('is required'),
							'HelpContext' => 0 
						);
					}
				} 
			}
		}
		else
		{
			$return[] = array(
				'Description' => Tools::displayError('You must insert at least one package.'),
				'HelpContext' => 0 
			);
		}
		
		return $return;
	}
	
	protected function buildLabelRequest($package, $return_label = false)
	{
		$isINTL = false;
		if($this->_fedex_label_infos['Shipper']['CountryCode'] != $this->_fedex_label_infos['Recipient']['CountryCode'])
			$isINTL = true;
		
		$request = array();
		$request['WebAuthenticationDetail'] = array(
			'UserCredential' => array(
				'Key' => $this->_fedex_key,
				'Password' => $this->_fedex_pass,
			)
		);
		$request['ClientDetail'] = array(
			'AccountNumber' => $this->_fedex_account,
			'MeterNumber' => $this->_fedex_meter,
		);
		$request['TransactionDetail'] = array('CustomerTransactionId' => '*** Express Domestic Shipping Request v12 using PHP ***');
		$request['Version'] = array(
			'ServiceId' => 'ship',
			'Major' => '12',
			'Intermediate' => '1',
			'Minor' => '0'
		);

		$request['RequestedShipment'] = array(
			'ShipTimestamp' => date('c'),
			'DropoffType' => 'REGULAR_PICKUP', // valid values REGULAR_PICKUP, REQUEST_COURIER, DROP_BOX, BUSINESS_SERVICE_CENTER and STATION
			'ServiceType' => $this->_fedex_label_infos['ServiceType'],
			'PackagingType' => $package['type'],
			'TotalWeight' => array(
				'Value' => ($this->_fedex_unit == 'LBS' ? $this->getWeightInLb($package['weight']) : $this->getWeightInKg($package['weight'])), 
				'Units' => ($this->_fedex_unit == 'LBS' ? 'LB' : 'KG')
			),
			'Shipper' => array(
				'Contact' => array(
					'PersonName' => $this->_fedex_label_infos['Shipper']['PersonName'],
					'CompanyName' => $this->_fedex_label_infos['Shipper']['CompanyName'],
					'PhoneNumber' => $this->_fedex_label_infos['Shipper']['PhoneNumber'],
				),
				'Address' => array(
					'StreetLines' => $this->_fedex_label_infos['Shipper']['StreetLines'],
					'City' => $this->_fedex_label_infos['Shipper']['City'],
					'StateOrProvinceCode' => $this->_fedex_label_infos['Shipper']['StateOrProvinceCode'],
					'PostalCode' => $this->_fedex_label_infos['Shipper']['PostalCode'],
					'CountryCode' => $this->_fedex_label_infos['Shipper']['CountryCode'],
					'Residential' => (($this->_fedex_origin_resident == 1 || $this->_fedex_label_infos['Shipper']['CompanyName'] == "") ? 1 : 0),
				)
			),
			'Recipient' => array(
				'Contact' => array(
					'PersonName' => $this->_fedex_label_infos['Recipient']['PersonName'],
					'CompanyName' => $this->_fedex_label_infos['Recipient']['CompanyName'],
					'PhoneNumber' => $this->_fedex_label_infos['Recipient']['PhoneNumber']),
				'Address' => array(
					'StreetLines' => $this->_fedex_label_infos['Recipient']['StreetLines'],
					'City' => $this->_fedex_label_infos['Recipient']['City'],
					'StateOrProvinceCode' => $this->_fedex_label_infos['Recipient']['StateOrProvinceCode'],
					'PostalCode' => $this->_fedex_label_infos['Recipient']['PostalCode'],
					'CountryCode' => $this->_fedex_label_infos['Recipient']['CountryCode'],
					'Residential' => (($this->_fedex_destin_resident == 1 || $this->_fedex_label_infos['Recipient']['CompanyName'] == "") ? 1 : 0),
				)
			),
			'ShippingChargesPayment' => array(
				'PaymentType' => 'SENDER',
				'Payor' => array(
					'ResponsibleParty' => array(
						'AccountNumber' => $this->_fedex_account,
						'Contact' => null,
						'Address' => array('CountryCode' => $this->_fedex_label_infos['Shipper']['CountryCode']),
					)
				)
			),
			'LabelSpecification' => array(
				'LabelFormatType' => 'COMMON2D', // valid values COMMON2D, LABEL_DATA_ONLY
				'ImageType' => $this->_fedex_label_infos['ImageType'],  // valid values DPL, EPL2, PDF, ZPLII and PNG
				'LabelStockType' => $this->_fedex_label_infos['LabelStockType'],
			),
			'RateRequestTypes' => array('ACCOUNT'), // valid values ACCOUNT and LIST
			'PackageCount' => 1,
			'RequestedPackageLineItems' => array(
				'0' => array(
					'SequenceNumber'=>1,
					'GroupPackageCount'=>1,
					'Weight' => array(
						'Value' => ($this->_fedex_unit == 'LBS' ? $this->getWeightInLb($package['weight']) : $this->getWeightInKg($package['weight'])),
						'Units' => ($this->_fedex_unit == 'LBS' ? 'LB' : 'KG')
					),
					'Dimensions' => array(
						'Length' => ($this->_fedex_unit == 'LBS' ? $this->convertDimensionToIn($package['d']) : $this->convertDimensionToCm($package['d'])),
						'Width' => ($this->_fedex_unit == 'LBS' ? $this->convertDimensionToIn($package['w']) : $this->convertDimensionToCm($package['w'])),
						'Height' => ($this->_fedex_unit == 'LBS' ? $this->convertDimensionToIn($package['h']) : $this->convertDimensionToCm($package['h'])),
						'Units' => ($this->_fedex_unit == 'LBS' ? 'IN' : 'CM')
					)
				)
			)
		);

		if($package['insurance'] > 0)
			$request['RequestedShipment']['RequestedPackageLineItems']['0']['InsuredValue'] = array('Amount' => $package['insurance'], 'Currency' => $this->_fedex_label_infos['Currency']);

		if($return_label)
		{
			$request['RequestedShipment']['ServiceType'] = $this->_fedex_label_infos['ReturnShippingType'];
			$request['RequestedShipment']['SpecialServicesRequested']['SpecialServiceTypes'][] = 'RETURN_SHIPMENT';
			$request['RequestedShipment']['SpecialServicesRequested']['ReturnShipmentDetail'] = array(
				'ReturnType' => 'PRINT_RETURN_LABEL'
			);
		}
		if(strlen($package['cod_code']) AND (int)$package['cod_amount'] > 0)
		{
			$request['RequestedShipment']['SpecialServicesRequested']['SpecialServiceTypes'][] = 'COD';
			$request['RequestedShipment']['SpecialServicesRequested']['CodDetail'] = array(
				'CodCollectionAmount' => array('Currency' => $this->_fedex_label_infos['Currency'], 'Amount' => $package['cod_amount']),
				'CollectionType' => $package['cod_code'],
			);
		}

		$totalCustomsValue = 0;
		$Commodities = array();
		
		if($isINTL || $this->_requiresINTLOptions($this->_fedex_label_infos['Shipper']['CountryCode'], $this->_fedex_label_infos['Recipient']['CountryCode'])) //if it is an international shipment
		{            
			if(is_array($package['products']) && count($package['products']))
			{
				foreach($package['products'] as $keyProd => $productInfo)
				{
					$productInfo['Weight'] = round($productInfo['Weight'] * $productInfo['Quantity'], 3);
					$totalCustomsValue += $productInfo['Value'] * $productInfo['Quantity'];
					
					$Commodities[] = array(
						'NumberOfPieces' => 1,
						'CountryOfManufacture' => $productInfo['CountryofManufacture'],
						'Quantity' => $productInfo['Quantity'],
						'QuantityUnits' => 'PIECES',
						'Weight' => array(
							'Units' => ($this->_fedex_unit == 'LBS' ? 'LB' : 'KG'),
							'Value' => ($this->_fedex_unit == 'LBS' ? $this->getWeightInLb($productInfo['Weight']) : $this->getWeightInKg($productInfo['Weight'])), 
						),
						'UnitPrice' => array(
							'Currency' => $this->_fedex_label_infos['Currency'],
							'Amount' => $productInfo['Value'],
						),
						'Description' => $productInfo['Description'],
					);
				}
			}   

			$request['RequestedShipment']['CustomsClearanceDetail'] = array(
				'DutiesPayment' => array(
					'PaymentType' => $package['DutyPayer'],
				),
				'CustomsValue' => array(
					'Currency' => $this->_fedex_label_infos['Currency'],
					'Amount' => number_format($totalCustomsValue, 2, '.', '')
				),
				'CommercialInvoice' => array(
					'Purpose' => $package['ShipmentPurpose']
				),
				'Commodities' => $Commodities
			);       
			
			/** IF PAYER IS SENDER */
			if($package['DutyPayer'] == 'SENDER')
			{
				$package['DutyPayerAccount'] = $this->_fedex_account;
				$package['DutyPayerCountry'] = $this->_fedex_label_infos['Shipper']['CountryCode'];
			}
			/** CONVERT COUNTRY ID TO ISO CODE */ 
			elseif(is_numeric($package['DutyPayerCountry']))
				$package['DutyPayerCountry'] = Country::getIsoById((int) $package['DutyPayerCountry']);
			
			/** INSERT DUTY PAYOR INFORMATION */
			if($package['DutyPayerAccount'])
			{
				$request['RequestedShipment']['CustomsClearanceDetail']['DutiesPayment']['Payor'] = array(
					'ResponsibleParty' => array(
						'AccountNumber' => $package['DutyPayerAccount'],
						'Contact' => null,
						'Address' => array('CountryCode' => $package['DutyPayerCountry']),
					)
				);
			}    
			
			$request['RequestedShipment']['ExpressFreightDetail'] = array(
				'ShippersLoadAndCount' => $package['ShippersLoadandCount'],
				'BookingConfirmationNumber' => $package['BookingConfirmationNumber'],
			);

			if($return_label)
			{
				$request['RequestedShipment']['CustomsClearanceDetail']['CustomsOptions']['Type'] = isset($package['customs_options_type']) ? $package['customs_options_type'] : '';
				if(isset($package['customs_options_type']) AND $package['customs_options_type'] == 'OTHER')
					$request['RequestedShipment']['CustomsClearanceDetail']['CustomsOptions']['Description'] = 'FOLLOWING_REPAIR';
			}
		}
		
		return $request;
	}
	
	public function buildDeleteShipmentRequest($tracking_number, $tracking_id_type)
	{                                                    
		$request = array();
		$request['WebAuthenticationDetail'] = array(
			'UserCredential' => array(
				'Key' => $this->_fedex_key,
				'Password' => $this->_fedex_pass,
			)
		);
		$request['ClientDetail'] = array(
			'AccountNumber' => $this->_fedex_account,
			'MeterNumber' => $this->_fedex_meter,
		);
		$request['TransactionDetail'] = array('CustomerTransactionId' => 'Delete Domestic Express Shipment');
		$request['Version'] = array(
			'ServiceId' => 'ship',
			'Major' => '12',
			'Intermediate' => '1',
			'Minor' => '0'
		);
		$request['ShipTimestamp'] = date('c');
		$request['TrackingId'] = array(
			'TrackingIdType' => $tracking_id_type,
			'TrackingNumber' => $tracking_number,
		);
		$request['DeletionControl'] = 'DELETE_ALL_PACKAGES';

		return $request;
	}

	// void shipment
	public function deleteShipment($tracking_numbers, $id_order, $tracking_id_type)
	{
		$success = true;
		$error_message = '';
		if(is_array($tracking_numbers) AND sizeof($tracking_numbers))
		{
			foreach ($tracking_numbers as $tracking_number)
			{
				$this->_requests = $this->buildDeleteShipmentRequest($tracking_number, $tracking_id_type);
				$this->_post_response = $this->_fedex_soap_client->deleteShipment($this->_requests);
				if ($this->_post_response->HighestSeverity != 'SUCCESS' AND $this->_post_response->Notifications->Code != '8159') //8159 - already deleted
				{
					$success = false;
					$error_message = (string)$this->_post_response->Notifications->Message;
				}
			}
		}

		return array($success, $error_message);
	}
	
	public function _requiresINTLOptions($origin, $destination)
	{
		/** Countries ISO CODE that requires INTL Options */
		$requiresINTLOptions = array('IN');
		if($origin == $destination && in_array($origin, $requiresINTLOptions) === true && in_array($destination, $requiresINTLOptions) === true)
			return true;
		
		return false;		
	}
}
