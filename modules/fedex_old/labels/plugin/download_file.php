<?php
include_once(dirname(__FILE__) . '/../../../../config/config.inc.php');
include_once(dirname(__FILE__) . '/../../../../init.php');
include_once(_PS_MODULE_DIR_.'fedex/fedex.php');

$fedex = new Fedex();

$fileInfo = explode('/', $_GET['file']);
$file_name = $fileInfo[count($fileInfo)-1];
$id_order = $fileInfo[count($fileInfo)-2];

if(substr($file_name, -4, 4) == '.pdf' || substr($file_name, -4, 4) == '.xml'){
	header('location: '.$_GET['file']);
	die;
}      
/** NOT IMPLEMENTED YET (REMOVE 1 == 2 TO IMPLEMENT)*/
elseif(1 == 2 && substr($file_name, -4, 4) == '.zpl' && $fedex->_fedex_printing_method > 0 && !Tools::getValue('force_donwload'))
{
	echo '                            
		<script type="text/javascript" src="'._MODULE_DIR_.'fedex/labels/plugin/js/jquery-1.10.2.js"></script>
		<script type="text/javascript" src="'._MODULE_DIR_.'fedex/labels/plugin/js/html2canvas.js"></script>
		<script type="text/javascript" src="'._MODULE_DIR_.'fedex/labels/plugin/js/jquery.plugin.html2canvas.js"></script>
		<script type="text/javascript" src="'._MODULE_DIR_.'fedex/labels/plugin/js/deployJava.js"></script>
		<script type="text/javascript">
			/**
			* Optionally used to deploy multiple versions of the applet for mixed
			* environments.  Oracle uses document.write(), which puts the applet at the
			* top of the page, bumping all HTML content down.
			*/
			deployQZ();
			
			/**
			* Deploys different versions of the applet depending on Java version.
			* Useful for removing warning dialogs for Java 6.  This function is optional
			* however, if used, should replace the <applet> method.  Needed to address 
			* MANIFEST.MF TrustedLibrary=true discrepency between JRE6 and JRE7.
			*/
			function deployQZ() {
				var attributes = {id: "qz", code:\'qz.PrintApplet.class\', 
					archive:\'qz-print.jar\', width:1, height:1};
				var parameters = {jnlp_href: \'qz-print_jnlp.jnlp\', 
					cache_option:\'plugin\', disable_logging:\'false\', 
					initial_focus:\'false\'};
				if (deployJava.versionCheck("1.7+") == true) {}
				else if (deployJava.versionCheck("1.6+") == true) {
					attributes[\'archive\'] = \'jre6/qz-print.jar\';
					parameters[\'jnlp_href\'] = \'jre6/qz-print_jnlp.jnlp\';
				}
				deployJava.runApplet(attributes, parameters, \'1.5\');
			}
			
			/**
			* Automatically gets called when applet has loaded.
			*/
			function qzReady() {
				/** NOT NECESSARY */
			}
			
			/**
			* Returns whether or not the applet is not ready to print.
			* Displays an alert if not ready.
			*/
			function notReady() {
				// If applet is not loaded, display an error
				if (!isLoaded()) {
					return true;
				}
				// If a printer hasn\'t been selected, display a message.
				else if (!qz.getPrinter()) {
					alert(\'Please select a printer first by using the "Detect Printer" button.\');
					return true;
				}
				return false;
			}
			
			/**
			* Returns is the applet is not loaded properly
			*/
			function isLoaded() {
				if (!qz) {
					alert(\'Error:\n\n\tPrint plugin is NOT loaded!\');
					return false;
				} else {
					try {
						if (!qz.isActive()) {
							alert(\'Error:\n\n\tPrint plugin is loaded but NOT active!\');
							return false;
						}
					} catch (err) {
						alert(\'Error:\n\n\tPrint plugin is NOT loaded properly!\');
						return false;
					}
				}
				return true;
			}
			
			/**
			* Automatically gets called when "qz.print()" is finished.
			*/
			function qzDonePrinting() {
				// Alert error, if any
				if (qz.getException()) {
					alert(\'Error printing:\n\n\t\' + qz.getException().getLocalizedMessage());
					qz.clearException();
					return; 
				}
				
				// Alert success message
				alert(\'Successfully sent print data to "\' + qz.getPrinter() + \'" queue.\');
			}
			
			/***************************************************************************
			* Prototype function for finding the "default printer" on the system
			* Usage:
			*    qz.findPrinter();
			*    window[\'qzDoneFinding\'] = function() { alert(qz.getPrinter()); };
			***************************************************************************/
			function useDefaultPrinter() {
				if (isLoaded()) {
					// Searches for default printer
					qz.findPrinter();
					
					// Automatically gets called when "qz.findPrinter()" is finished.
					window[\'qzDoneFinding\'] = function() {
						// Alert the printer name to user
						var printer = qz.getPrinter();
						alert(printer !== null ? \'Default printer found: "\' + printer + \'"\':
							\'Default printer \' + \'not found\');
							
						'.($fedex->_fedex_printing_method == 1 ? 'if(printer !== null){ printZPL(); }' : '').'
						
						// Remove reference to this function
						window[\'qzDoneFinding\'] = null;
					};
				}
			}
			
			/***************************************************************************
			* Prototype function for finding the closest match to a printer name.
			* Usage:
			*    qz.findPrinter(\'zebra\');
			*    window[\'qzDoneFinding\'] = function() { alert(qz.getPrinter()); };
			***************************************************************************/
			function findPrinter(name) {
				// Get printer name from input box
				var p = document.getElementById(\'printer\');
				if (name) {
					p.value = name;
				}
				
				if (isLoaded()) {
					// Searches for locally installed printer with specified name
					qz.findPrinter(p.value);
					
					// Automatically gets called when "qz.findPrinter()" is finished.
					window[\'qzDoneFinding\'] = function() {
						var p = document.getElementById(\'printer\');
						var printer = qz.getPrinter();
						
						// Alert the printer name to user
						alert(printer !== null ? \'Printer found: "\' + printer + 
							\'" after searching for "\' + p.value + \'"\' : \'Printer "\' + 
							p.value + \'" not found.\');
							
						'.($fedex->_fedex_printing_method == 1 ? 'if(printer !== null){ printZPL(); }' : '').'
						
						// Remove reference to this function
						window[\'qzDoneFinding\'] = null;
						
						
					};
				}
			}
			
			/***************************************************************************
			* Prototype function for listing all printers attached to the system
			* Usage:
			*    qz.findPrinter(\'\\{dummy_text\\}\');
			*    window[\'qzDoneFinding\'] = function() { alert(qz.getPrinters()); };
			***************************************************************************/
			function findPrinters() {
				if (isLoaded()) {
					// Searches for a locally installed printer with a bogus name
					qz.findPrinter(\'\\{bogus_printer\\}\');
					
					// Automatically gets called when "qz.findPrinter()" is finished.
					window[\'qzDoneFinding\'] = function() {
						// Get the CSV listing of attached printers
						var printers = qz.getPrinters().split(\',\');
						for (i in printers) {
							alert(printers[i] ? printers[i] : \'Unknown\');      
						}
						
						// Remove reference to this function
						window[\'qzDoneFinding\'] = null;
					};
				}
			}
		
			/***************************************************************************
			* Prototype function for printing raw ZPL commands
			* Usage:
			*    qz.append(\'^XA\n^FO50,50^ADN,36,20^FDHello World!\n^FS\n^XZ\n\');
			*    qz.print();
			***************************************************************************/     
			function printZPL() {
				if (notReady()) { return; }
				 
				// Send characters/raw commands to qz using "append"
				// This example is for ZPL.  Please adapt to your printer language
				// Hint:  Carriage Return = \r, New Line = \n, Escape Double Quotes= \"
	';
	
			$content = file_get_contents(_PS_MODULE_DIR_.'fedex/labels/'.$id_order.'/'.$file_name);
			$lines = explode("\n", $content);
			
			foreach($lines as $line)
			{
				echo '
					qz.append(\''.$line.'\n\');
				';
			}
	
	echo '		
				qz.print();		
				/*		
				// Automatically gets called when "qz.appendImage()" is finished.
				window[\'qzDoneAppending\'] = function() {					
					// Tell the apple to print.
					
					// Remove any reference to this function
					window[\'qzDoneAppending\'] = null;
				};
				*/
			}
		</script>
		
		<h1 style="margin: 0 0 20px 0;">'.$fedex->l('Print', 'download_file').' '.substr($file_name, -4, 4).' '.$fedex->l('Shipping Label', 'download_file').'</h1>
	';
	
	if($fedex->_fedex_printing_method == 1)
	{
		echo '
			<script type="text/javascript">
				$(document).ready(function(){
					'.($fedex->_fedex_printer_name == 'default' ? 'useDefaultPrinter();' : 'findPrinter();').'
					
					$("#showOptions").click(function(){
						if($("#printingOptions").css("display") == "none")
						{
							$("#printingOptions").fadeIn();
							$(this).html("'.$fedex->l('Hide printing options', 'download_file').'");
						}    
						else
						{
							$("#printingOptions").fadeOut();
							$(this).html("'.$fedex->l('Show printing options', 'download_file').'");
						}
					});
				});                
			</script>
			<p style="color: blue;">'.$fedex->l('Printing shipping label automatically...', 'download_file').'</p>
			
			<h3 style="color: #000; cursor: pointer; margin: 30px 0 0 0;" id="showOptions">'.$fedex->l('Show printing options', 'download_file').'</h3>     
		';  
	}
	
	echo '
		<div id="printingOptions" style="'.($fedex->_fedex_printing_method == 1 ? 'display: none;' : '').' border: 1px solid;float: left;padding: 10px;width: 340px;">
			<div style="float: left; width: 100%; margin: 5px 0;">
				<label for="printer">Printer Name:</label>
				<input type="text" id="printer" name="printer" value="'.($fedex->_fedex_printer_name != 'default' ? $fedex->_fedex_printer_name : '').'" />
				'.($fedex->_fedex_printer_name != 'default' ? '' : '<br /> ('.$fedex->l('Currently using system default printer', 'download_file').')').'
			</div>
			
			<div style="float: left; width: 100%; margin: 5px 0;">
				<input type="button" onclick="findPrinter()" value="Detect Printer By Name">
				<input type="button" onclick="useDefaultPrinter()" value="Use Default Printer">
				<input type="button" onclick="findPrinters()" value="List All Printers">
			</div>
			
			<div style="float: left; width: 100%; margin: 25px 0; text-align: center;">
				<input type="button" onClick="printZPL()" value="Print Shipping Label" />
			</div>
		</div>
	';
	
	echo '
		<h4 style="float: left; width: 100%; cursor: pointer;" onclick="window.location = \''.$_SERVER['REQUEST_URI'].'&force_donwload=1\'">'.$fedex->l('Click to download', 'download_file').' '.substr($file_name, -4, 4).' '.$fedex->l('file', 'download_file').'</h4>
	';
}
else
{
	header('Content-Description: File Transfer');
	header('Content-Type: application/octet-stream');
	header('Content-Disposition: attachment; filename="'.basename($file_name).'"');
	header('Content-Transfer-Encoding: Binary');

	ob_clean();
	flush(); 
	readfile(_PS_MODULE_DIR_.'fedex/labels/'.$id_order.'/'.$file_name);

	exit; 
}
?>