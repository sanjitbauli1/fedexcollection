<?php

include_once(dirname(__FILE__) . '/../../config/config.inc.php');
include_once(dirname(__FILE__) . '/../../init.php');
include_once(dirname(__FILE__) . '/classes/RateAvailableServices.php');
include_once(dirname(__FILE__) . '/JSON.php');

$log = false;
$sti = microtime();
$ps_version  = floatval(substr(_PS_VERSION_,0,3));
$fedex = new Fedex();
$is_cart = Tools::getValue('fedex_is_cart');
$qty = max((int)Tools::getValue('qty'),1);

$fedex->updateCartWithNewCarrier();

$fedex->saveLog('fedex_log1.txt', "Starting $sti ".print_r($_POST,true), $log);

// Get Address and zone
$address = $fedex->getPreviewAddress($log);

if (isset($_POST['id_product']))
	$product = new Product($_POST['id_product']);
$id_product_attribute = Tools::getValue('id_product_attribute','0');
$product_weight = $product->weight;
// Add combination weight impact
if ($id_product_attribute != 0)
	$product_weight += Db::getInstance()->getValue('SELECT `weight`	FROM `'._DB_PREFIX_.'product_attribute`	WHERE `id_product_attribute` = '.(int)($id_product_attribute));
$is_downloadable = ProductDownload::getIdFromIdProduct($_POST['id_product']);
if ($is_downloadable)
	$json = array("fedex_rate_tpl"=> $fedex->hookAjaxPreview($rates, $address['dest_zip'], $address['dest_state'], $address['dest_country'], true, $is_cart));
else
{
	$context = $fedex->getContext();
	$currency = new Currency($context->currency->id);
	$rates = $fedex->getAllRates($address['id_zone'], $is_cart, $cart, $product_weight, $address['dest_zip'], $address['dest_state'], $address['dest_country'], $currency, $product, $id_product_attribute, $qty, $address['dest_city']);

	$json = array("fedex_rate_tpl"=> $fedex->hookAjaxPreview($rates, $address['dest_zip'], $address['dest_state'], $address['dest_country'], false, $is_cart, $address['dest_city']));
}
if (!function_exists('json_decode') )
{
	$j = new JSON();
	print $j->serialize($fedex->array2object($json));
}
else
	print json_encode($json);

$fedex->saveLog('fedex_log1.txt', "5) = ".(microtime() - $sti), $log);