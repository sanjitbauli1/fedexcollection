<?php

require_once( _PS_MODULE_DIR_ . 'fedex/classes/init.php');

Class Fedex extends PrestoChangeoCarrierModule
{
	protected  $_html = '';
	public $id_carrier;
	public $_fedex_key;
	public $_fedex_pass;
	public $_fedex_account;
	public $_fedex_meter;
	public $_fedex_dropoff;
	public $_fedex_pack;
	public $_fedex_boxes;
	public $_fedex_width;
	public $_fedex_height;
	public $_fedex_depth;
	public $_fedex_weight;
	public $_fedex_origin_resident;
	public $_fedex_destin_resident;
	public $_fedex_origin_zip;
	public $_fedex_origin_city;
	public $_fedex_origin_country;
	public $_fedex_mode;
	public $_fedex_reload_carriers;
	public $_fedex_packages;
	public $_fedex_package_size;
	public $_fedex_packages_per_box;
	public $_fedex_override_address;
	public $_fedex_debug_mode;
	public $_fedex_xml_log;
	public $_fedex_address_display;
	public $_fedex_unit;
	public $_fedex_log_filename;
	/* SHIPPER LABEL PRINTING PARAMS */
	public $_fedex_shipper_shop_name;
	public $_fedex_shipper_attention_name;
	public $_fedex_shipper_phone;
	public $_fedex_shipper_addr1;
	public $_fedex_shipper_addr2;
	public $_fedex_shipper_city;
	public $_fedex_shipper_country;
	public $_fedex_shipper_state;
	public $_fedex_shipper_postcode;
	/* GLOBAL LABEL PRINTING PARAMS */
	public $_usps_enable_labels;
	public $_fedex_label_order_status;
	public $_fedex_carbon;
	public $_fedex_label_margin;
	public $_fedex_label_format;
	public $_fedex_label_stock_type;
	public $_fedex_random;

	protected $_full_version = 15300;

	function __construct()
	{
		$this->name = 'fedex';
		$this->tab = 'shipping_logistics';
		$this->author = 'Presto-Changeo';
		$this->version = '1.5.3';

		parent::__construct(); // The parent construct is required for translations
		$this->_refreshProperties();
		
		if ((float)substr(_PS_VERSION_,0,3) >= 1.6)
			$this->bootstrap = true;

		$this->displayName = $this->l('Fedex Shipping');
		$this->description = $this->l('Get real time fedex shipping rates');
		if (!$this->_fedex_key || !$this->_fedex_pass || !$this->_fedex_account || !$this->_fedex_meter)
			$this->warning = $this->l('You must enter your Fedex account info');
		if ($this->upgradeCheck('FDX'))
			$this->warning = $this->l('We have released a new version of the module,') .' '.$this->l('request an upgrade at ').' https://www.presto-changeo.com/en/contact_us';
	}

	function install()
	{
		if (!parent::install() ||
		!$this->registerHook('productActions') ||
		!$this->registerHook('updateCarrier'))
			return false;
		if (!$this->registerHook('adminOrder'))
			return false;

		$ps_version_array = explode('.', _PS_VERSION_);
		$ps_version_id = (int)($ps_version_array[0].$ps_version_array[1].substr($ps_version_array[2],0, 1));
		if ($ps_version_id < 155)
			$this->checkHookCartShippingPreview();

		if (!$this->registerHook('cartShippingPreview'))
			return false;
		Configuration::updateValue('FEDEX_INSTALL','inline');
		$country = new Country(Configuration::get('PS_COUNTRY_DEFAULT'));
		Configuration::updateValue('FEDEX_ORIGIN_COUNTRY', $country->iso_code);
		Configuration::updateValue('FEDEX_ORIGIN_ZIP', Configuration::get('PS_SHOP_CODE'));
		Configuration::updateValue('FEDEX_ORIGIN_CITY', Configuration::get('PS_SHOP_CITY'));
		Configuration::updateValue('FEDEX_BOXES', serialize(array()));
		Configuration::updateValue('FEDEX_DECIMAL_UPDATE','1');
		Configuration::updateValue('FEDEX_FS_UPDATE','1');
		Configuration::updateValue('FEDEX_WIDTH', 0);
		Configuration::updateValue('FEDEX_OVERRIDE_ADDRESS', 1);
		Configuration::updateValue('FEDEX_HEIGHT', 0);
		Configuration::updateValue('FEDEX_DEPTH', 0);
		Configuration::updateValue('FEDEX_WEIGHT', 100);
		Configuration::updateValue('FEDEX_PACKAGES_PER_BOX', 0);
		Configuration::updateValue('FEDEX_PACKAGES', 'multiple');
		Configuration::updateValue('FEDEX_PACKAGE_SIZE', 'fixed');
		Configuration::updateValue('FEDEX_ADDRESS_DISPLAY', serialize(array('country' => 1, 'state' => 0, 'city' => 0, 'zip' => 1)));
		Configuration::updateValue('FEDEX_UNIT', 'LBS');
		$query = 'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'fe_fedex_method` (
			 `id_fedex_method` int(11) NOT NULL AUTO_INCREMENT,
			 `id_carrier` int(11) NOT NULL,
			 `method` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
			 `free_shipping` decimal(9,2) NOT NULL,
			 `free_shipping_product` TEXT NOT NULL,
			 `free_shipping_category` TEXT NOT NULL,
			 `free_shipping_manufacturer` TEXT NOT NULL,
			 `free_shipping_supplier` TEXT NOT NULL,
			 `extra_shipping_type` int(1) NOT NULL,
			 `extra_shipping_amount` decimal(9,2) NOT NULL,
			 `insurance_minimum` int(11) NOT NULL,
			 `insurance_type` int(1) NOT NULL,
			 `insurance_amount` decimal(9,2) NOT NULL,
			 PRIMARY KEY (`id_fedex_method`),
			 KEY `id_carrier` (`id_carrier`),
			 KEY `method` (`method`)
			 ) ENGINE='._MYSQL_ENGINE_.'  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;';
		Db::getInstance()->execute($query);

		$query = 'DROP TABLE IF EXISTS `'._DB_PREFIX_.'fe_fedex_rate_cache`;';
		Db::getInstance()->execute($query);
		$query = 'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'fe_fedex_rate_cache` (
			`id_fedex_rate` int(11) NOT NULL AUTO_INCREMENT,
			`id_carrier` int(11) NOT NULL,
			`origin_zip` varchar(11) NOT NULL,
			`origin_country` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
			`dest_zip` varchar(11) NOT NULL,
			`dest_country` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
			`dest_resident` tinyint(1) NOT NULL,
			`method` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
			`insurance` int(11) NOT NULL,
			`dropoff` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
			`packing` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
			`packages` decimal(17,2) NOT NULL,
			`weight` decimal(17,2) NOT NULL,
			`rate_code` int(2) NOT NULL,
			`rate` decimal(17,2) NOT NULL,
			`quote_date` int(11) NOT NULL,
			PRIMARY KEY (`id_fedex_rate`),
			KEY `origin_zip` (`origin_zip`,`origin_country`,`dest_zip`,`dest_country`)
			) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;';
		Db::getInstance()->execute($query);

		$query = '
			CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'fe_fedex_package_rate_cache` (
			`id_package` int(11) NOT NULL AUTO_INCREMENT,
			`id_fedex_rate` int(11) NOT NULL,
			`weight` decimal(17,2) NOT NULL,
			`width` decimal(17,2) NOT NULL,
			`height` decimal(17,2) NOT NULL,
			`depth` decimal(17,2) NOT NULL,
			PRIMARY KEY (`id_package`),
			KEY `weight` (`weight`,`width`,`height`,`depth`)
			) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;';
		Db::getInstance()->execute($query);

		$query = '
			CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'fe_fedex_hash_cache` (
			`id_hash` int(11) NOT NULL AUTO_INCREMENT,
			`id_fedex_rate` int(11) NOT NULL,
			`hash` varchar(40) NOT NULL,
			`hash_date` int(11) NOT NULL,
			PRIMARY KEY (`id_hash`),
			KEY `hash` (`hash`)
			) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;';
		Db::getInstance()->execute($query);

		$query = 'DROP TABLE IF EXISTS `'._DB_PREFIX_.'fe_fedex_invalid_dest`;';
		Db::getInstance()->execute($query);
		$query = '
			CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'fe_fedex_invalid_dest` (
				`id_invalid` int(11) NOT NULL AUTO_INCREMENT,
				`method` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
				`zip` varchar(11) NOT NULL,
				`country` varchar(2) NOT NULL,
				`state` varchar(2) NOT NULL,
				`ondate` int(11) NOT NULL,
				PRIMARY KEY (`id_invalid`),
				KEY `zip` (`zip`,`country`)
			) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;';
		Db::getInstance()->execute($query);

		$query = 'DROP TABLE IF EXISTS `'._DB_PREFIX_.'fe_fedex_labels_info`;';
		Db::getInstance()->execute($query);
		Db::getInstance()->execute('
			CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'fe_fedex_labels_info` (
				`id_label_info` int(11) NOT NULL AUTO_INCREMENT,
				`id_order` int(11) NOT NULL,
				`info` TEXT NOT NULL,
				`tracking_id_type` TINYTEXT NOT NULL,
				`tracking_numbers` TEXT NOT NULL,
				`return_tracking_id_type` TINYTEXT NOT NULL,
				`return_tracking_numbers` TEXT NOT NULL,
				PRIMARY KEY (`id_label_info`)
			) ENGINE = '._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
		');

		return true;
	}

	function uninstall()
	{
		if (!parent::uninstall())
			return false;
		return true;
	}

	protected function _refreshProperties()
	{
		$this->_fedex_key = Configuration::get('FEDEX_KEY');
		$this->_fedex_pass = Configuration::get('FEDEX_PASS');
		$this->_fedex_account = Configuration::get('FEDEX_ACCOUNT');
		$this->_fedex_meter = Configuration::get('FEDEX_METER');
		$this->_fedex_dropoff = Configuration::get('FEDEX_DROPOFF');
		$this->_fedex_pack = Configuration::get('FEDEX_PACK');
		$temp_box = Configuration::get('FEDEX_BOXES');
		$this->_fedex_boxes = strlen($temp_box) > 5?unserialize($temp_box):array();

		// Make sure the boxes are ordered from small to large.
		$this->sortBoxes();

		$this->_fedex_width = Configuration::get('FEDEX_WIDTH');
		$this->_fedex_height = floatval(Configuration::get('FEDEX_HEIGHT'));
		$this->_fedex_depth = floatval(Configuration::get('FEDEX_DEPTH'));
		$this->_fedex_weight = floatval(Configuration::get('FEDEX_WEIGHT'));
		$this->_fedex_origin_resident = Configuration::get('FEDEX_ORIGIN_RESIDENT');
		$this->_fedex_destin_resident = Configuration::get('FEDEX_DESTIN_RESIDENT');
		$this->_fedex_unit = Configuration::get('FEDEX_UNIT');
		$this->_fedex_origin_zip = Configuration::get('FEDEX_ORIGIN_ZIP');
		$this->_fedex_origin_city = Configuration::get('FEDEX_ORIGIN_CITY');
		$this->_fedex_origin_country = Configuration::get('FEDEX_ORIGIN_COUNTRY');
		$this->_fedex_mode = Configuration::get('FEDEX_MODE');
		$this->_fedex_reload_carriers = Configuration::get('FEDEX_RELOAD_CARRIERS');
		$this->_fedex_packages = Configuration::get('FEDEX_PACKAGES');
		$this->_fedex_package_size = Configuration::get('FEDEX_PACKAGE_SIZE');
		$this->_fedex_packages_per_box = (int)Configuration::get('FEDEX_PACKAGES_PER_BOX');
		$this->_fedex_override_address = (int)Configuration::get('FEDEX_OVERRIDE_ADDRESS');
		$this->_fedex_debug_mode = (int)Configuration::get('FEDEX_DEBUG_MODE');
		$this->_fedex_xml_log = (int)Configuration::get('FEDEX_XML_LOG');
		$tmp_display = Configuration::get('FEDEX_ADDRESS_DISPLAY');
		if (strlen($tmp_display) > 6)
			$this->_fedex_address_display = unserialize($tmp_display);
		else
		{
				$this->_fedex_address_display = array('country' => 1, 'state' => 0, 'city' => 0, 'zip' => 1);
				Configuration::updateValue('FEDEX_ADDRESS_DISPLAY', serialize($this->_fedex_address_display));
		}

		$this->_last_updated = Configuration::get('PRESTO_CHANGEO_UC');

		if ($this->_fedex_reload_carriers)
		{
			$query = 'SELECT * FROM `'._DB_PREFIX_.'fe_fedex_method` GROUP BY id_carrier';
			$mresult = Db::getInstance()->executeS($query);
			foreach ($mresult AS $mrow)
			{
				$query = 'SELECT cz.id_zone, rw.id_range_weight, cz.id_zone, d.price FROM `'._DB_PREFIX_.'range_weight` rw, `'._DB_PREFIX_.'carrier_zone` cz LEFT JOIN `'._DB_PREFIX_.'delivery` d ON cz.id_carrier = d.id_carrier AND d.id_zone = cz.id_zone WHERE cz.id_carrier = '.$mrow['id_carrier'].' AND cz.id_carrier = rw.id_carrier';
				$result = Db::getInstance()->ExecuteS($query);
				foreach ($result as $row)
				{
					if ($row['price'] == "" || $row['price'] == "NULL")
					{
						$query = 'INSERT INTO `'._DB_PREFIX_.'delivery` (id_carrier, id_range_weight, id_zone, price) VALUES("'.$mrow['id_carrier'].'", "'.$row['id_range_weight'].'", "'.$row['id_zone'].'","0")';
						Db::getInstance()->Execute($query);
					}
				}
			}
			Configuration::updateValue('FEDEX_RELOAD_CARRIERS','');
		}

		/** LABEL PRINTING: SHIPPER PARAMS */
		$this->_fedex_shipper_shop_name = (Configuration::get('FEDEX_SHIPPER_SHOP_NAME') ? Configuration::get('FEDEX_SHIPPER_SHOP_NAME') : Configuration::get('PS_SHOP_NAME'));
		$this->_fedex_shipper_attention_name = Configuration::get('FEDEX_SHIPPER_ATTENTION_NAME');
		$this->_fedex_shipper_phone = (Configuration::get('FEDEX_SHIPPER_PHONE') ? Configuration::get('FEDEX_SHIPPER_PHONE') : Configuration::get('PS_SHOP_PHONE'));
		$this->_fedex_shipper_addr1 = (Configuration::get('FEDEX_SHIPPER_ADDR1') ? Configuration::get('FEDEX_SHIPPER_ADDR1') : Configuration::get('PS_SHOP_ADDR1'));
		$this->_fedex_shipper_addr2 = (Configuration::get('FEDEX_SHIPPER_ADDR2') ? Configuration::get('FEDEX_SHIPPER_ADDR2') : Configuration::get('PS_SHOP_ADDR2'));
		$this->_fedex_shipper_city = (Configuration::get('FEDEX_SHIPPER_CITY') ? Configuration::get('FEDEX_SHIPPER_CITY') : Configuration::get('PS_SHOP_CITY'));
		$this->_fedex_shipper_country = (Configuration::get('FEDEX_SHIPPER_COUNTRY') ? Configuration::get('FEDEX_SHIPPER_COUNTRY') : Configuration::get('FEDEX_ORIGIN_COUNTRY'));
		$this->_fedex_shipper_state = (Configuration::get('FEDEX_SHIPPER_STATE') ? Configuration::get('FEDEX_SHIPPER_STATE') : Configuration::get('FEDEX_ORIGIN_STATE'));
		$this->_fedex_shipper_postcode = (Configuration::get('FEDEX_SHIPPER_POSTCODE') ? Configuration::get('FEDEX_SHIPPER_POSTCODE') : Configuration::get('PS_SHOP_CODE'));
		/** LABEL PRINTING: GLOBAL PARAMS */
		$this->_fedex_enable_labels = Configuration::get('FEDEX_ENABLE_LABELS');
		$this->_fedex_label_order_status = Configuration::get('FEDEX_LABEL_ORDER_STATUS');
		$this->_fedex_label_margin = Configuration::get('FEDEX_LABEL_MARGIN');
		$this->_fedex_label_format = Configuration::get('FEDEX_LABEL_FORMAT');
		$this->_fedex_label_stock_type = Configuration::get('FEDEX_LABEL_STOCK_TYPE', 'PAPER_8.5X11_TOP_HALF_LABEL');
		$this->_fedex_random = Configuration::get('FEDEX_RANDOM');
		
		if ($this->_fedex_random == '')
		{
			$this->_fedex_random = md5(mt_rand().time());
			Configuration::updateValue('FEDEX_RANDOM', $this->_fedex_random);
		}
		$this->_fedex_log_filename = "fedex_xml_log_".$this->_fedex_random.".txt";
	}

	public function getContent()
	{
		$this->_postProcess();
		$this->_displayForm();

		return $this->_html;
	}

	protected function _displayForm()
	{
		$ps_version  = $this->getPSV();
		$this->checkHookCartShippingPreview();
		$this->applyUpdates();
		$verified = $this->checkFedexSettings();
		$zones = Zone::getZones(true);
		$carriers = $this->getCarriers();
		$weightUnit = strtolower(Configuration::get('PS_WEIGHT_UNIT'));
		$currency = new Currency(Configuration::get('PS_CURRENCY_DEFAULT'));
		$dropoff_types = $this->getDropoffTypes();
		$ship_meth = $this->getShippingMethods();
		$package_types = $this->getPackageTypes();
		$countries = Country::getCountries($this->context->language->id, true); 

		$this->_html .= '<link type="text/css" rel="stylesheet" href="'.$this->_path.'css/tooltipster.css" />';
		$this->_html .= '<script type="text/javascript" src="'.$this->_path.'js/jquery.tooltipster.min.js"></script>';
		
		if($ps_version == 1.6)
		{
			$this->_html .= '
			<style>
			sup {
				color: #CC0000;
				font-weight: bold;
			}
			</style> 
			';
		}
		
		$this->_html .= '
			<link type="text/css" rel="stylesheet" href="'.$this->_path.'css/admin.css" /> 
			<script type="text/javascript">
				var invalid_account = "'.$this->l('You must enter a valid Fedex Account Number').'";
				var invalid_key = "'.$this->l('You must enter a valid Fedex Authentication Key').'";
				var invalid_meter = "'.$this->l('You must enter a valid Fedex Meter Number').'";
				var invalid_pass = "'.$this->l('You must enter a valid Fedex Password').'";
				var invalid_zip = "'.$this->l('You must enter a valid Zipcode, if there are no Zipcodes in your country, enter 00000').'";
				var module_path = "'.(isset($this->context->shop->virtual_uri) && $this->context->shop->virtual_uri != '' ? substr($this->context->shop->virtual_uri,0, -1) : '').$this->_path.'";
			</script>
			<script type="text/javascript" src="'.$this->_path.'js/back_office.js"></script>
		';
		
		$this->_html .= ($ps_version >= 1.5 ? '<div style="'.($ps_version < 1.6 ? 'width:900px;margin:auto' : '' ).'">' : '').
		$this->getModuleRecommendations('FDX').
		'<h2 style="clear:both;padding-top:5px;">'.$this->displayName.' '.$this->version.'</h2>'.
		'<b>'.$this->l('For any technical questions, or problems with the module, please contact us using our').' <a href="https://www.presto-changeo.com/en/contact_us" target="_index"><u>'.$this->l('Contact Form').'</u></b></a><br /><br />';
		
		if ($url = $this->upgradeCheck('FDX'))
		{
			$this->_html .=
			($ps_version >= 1.6?'<div class="panel">':
			'<fieldset class="width3" style="background-color:#FFFAC6;width:900px;">').
			($ps_version < 1.6 ? '<legend>': '<h3>').
			'<img src="'.$this->_path.'logo.gif" style="'.($this->getPSV() == 1.6 ? 'margin: 0 10px 0 0;' : '').'" /> '.$this->l('New Version Available').
			($ps_version < 1.6 ? '</legend>': '</h3>').
			$this->l('We have released a new version of the module. For a list of new features, improvements and bug fixes, view the ').'<a href="'.$url.'#change" target="_index"><b><u>'.$this->l('Change Log').'</b></u></a> '.$this->l('on our site.').'
			<br />'.
			$this->l('For real-time alerts about module updates, be sure to join us on our') .' <a href="http://www.facebook.com/pages/Presto-Changeo/333091712684" target="_index"><u><b>Facebook</b></u></a> / <a href="http://twitter.com/prestochangeo1" target="_index"><u><b>Twitter</b></u></a> '.$this->l('pages').'.
			<br /><br />'.
			$this->l('Please').' <a href="https://www.presto-changeo.com/en/contact_us" target="_index"><b><u>'.$this->l('contact us').'</u></b></a> '.$this->l('to request an upgrade to the latest version.').
			($ps_version < 1.6 ? '</fieldset>': '</div>').'
			<br />';
		}

		$this->_html .= '
			<form action="'.$_SERVER['REQUEST_URI'].'" name="fedex_form" id="fedex_form" method="post">
				'.($ps_version >= 1.6?'<div class="panel">':
				'<fieldset class="width3" style="width:900px;">').
				($ps_version < 1.6 ? '<legend>': '<h3>').'
					<img src="'.$this->_path.'logo.gif" style="'.($this->getPSV() == 1.6 ? 'margin: 0 10px 0 0;' : '').'" /> '.$this->l('Installation Instructions').' (<a href="'.$_SERVER['REQUEST_URI'].'&fedex_shi='.Configuration::get('FEDEX_INSTALL').'" style="color:blue;text-decoration:underline">'.(Configuration::get('FEDEX_INSTALL')=="inline"?"Collapse":"Expand").'</a>))
				'.($ps_version < 1.6 ? '</legend>': '</h3>').'

					<div id="fedex_install" style="padding-left:10px;display:'.Configuration::get('FEDEX_INSTALL').'">
						<table width="900" style="font-size:13px;">
				';
		if ($weightUnit != "lb" && $weightUnit != "lbs" && $weightUnit != "kg" && $weightUnit != "oz" && $weightUnit != "g")
		{
				$this->_html .= '
							<tr height="31">
								<td align="left">
									<li style="margin-left:10px"><b style="color:red">'.$this->l('The Module doesn\'t recognize your weight unit, please use lb, kg, oz or g').'.</b></li>
								</td>
							</tr>
				';
		}
		
		$this->_html .= '
							<tr height="40">
								<td align="left">
									<b>'.$this->l('1) To enable Block Cart Preview, the following changes need to be made:').'</b><br/>
									<p style="margin-left:10px;margin-top:0;">
								';
				if($ps_version < 1.5)
				{
					$modified_file = @file(dirname(__FILE__).'/override_1.4/classes/FrontController.php');
					$server_file = @file(dirname(__FILE__).'/../../override/classes/FrontController.php');
					if (sizeof($server_file) <= 1 || !$this->overrideCheck($modified_file, $server_file))
						$this->_html .= '
								<br />'.$this->l('Copy').'&nbsp; <b>/modules/fedex/override_1.4/classes/FrontController.php</b> &nbsp;'.$this->l('to').'&nbsp; <b>/override/classes</b>
								<br />
								'.$this->l('If a file with the exact name already exists (not one starting with _ ), then copy line #33 from our modified file to your file (place it inside the preProcess() function');
					else
						$this->_html .= '
								<br /><b style="color:green">FrontController.php '.$this->l('is installed correctly').'</b>';

					$this->_html .= '
								<br />
								<br />
								'.$this->l('Edit').'&nbsp; <b>'.(file_exists(_PS_ROOT_DIR_.'/themes/'._THEME_NAME_.'/modules/blockcart/blockcart.tpl')?'/themes/'._THEME_NAME_.'/modules/blockcart/blockcart.tpl':'/modules/blockcart/blockcart.tpl').'</b> '.$this->l('and add the following line where you want the button to appear (typically after the last').' &lt;/p&gt; '.$this->l('in the file').'.
								<br />
								<br />
								{if isset($HOOK_CART_SHIPPING_PREVIEW)}
								<br />
								&nbsp;&nbsp;&nbsp;&nbsp;{$HOOK_CART_SHIPPING_PREVIEW}
								<br />
								{/if}
								<br />
								<br />
								'.$this->l('Make sure to clear the smarty cache (turn on force recompile in Preferences->Performance)');
				}
				else
				{
					if($ps_version == 1.5)
						$overridePath = 'override_1.5';
					elseif($ps_version == 1.6)
						$overridePath = 'override_1.6';
					
					$modified_file = @file(dirname(__FILE__).'/'.$overridePath.'/classes/Carrier.php');
					$server_file = @file(dirname(__FILE__).'/../../override/classes/Carrier.php');
					
					if (sizeof($server_file) <= 1 || !$this->overrideCheck($modified_file, $server_file))
					$this->_html .= '
								<br />'.$this->l('Copy').'&nbsp; <b>/modules/fedex/'.$overridePath.'/classes/Carrier.php</b> &nbsp;'.$this->l('to').'&nbsp; <b>/override/classes</b>
								<br />
								'.$this->l('If a file with the exact name already exists, then copy lines #24-27 from our modified file to your file.');
					else
						$this->_html .= '
								<br /><b style="color:green">Carrier.php '.$this->l('is installed correctly').'</b>';

					$this->_html .= '
								<br />
								<br />
								'.$this->l('Edit').'&nbsp; <b>'.(file_exists(_PS_ROOT_DIR_.'/themes/'._THEME_NAME_.'/modules/blockcart/blockcart.tpl')?'/themes/'._THEME_NAME_.'/modules/blockcart/blockcart.tpl':'/modules/blockcart/blockcart.tpl').'</b> '.$this->l('and add the following line where you want the button to appear (typically after the last').' &lt;/p&gt; '.$this->l('in the file').'.
								<br />
								<br />
								{hook h = "cartShippingPreview"}
								<br />
								<br />
								'.$this->l('Make sure to clear the smarty cache (turn on force recompile in Preferences->Performance)');
			}
			
			$this->_html .= '
								</p>
								<br />
								<br />
								<b>'.$this->l('2) Follow these steps to get your FedEx API information (required below).').'</b>
								<p style="margin-left:10px">
									1) '.$this->l('Visit \'Registration for FedEx Web Services Production Access\' here').' <a href="http://www.fedex.com/us/developer/" target="_index">https://www.fedex.com/wpor/web/jsp/commonTC.jsp</a> '.$this->l('Enter your login information if you are not already signed in.').'
									<br />
									2) '.$this->l('Select "No," "FedEx Web Services for Shipping," and "Consultant" for the 3rd option').'.
									<br />
									3) '.$this->l('Complete the application, save the Authentication Key you get at the end, and you will receive an email with your Password and Meter Number').'.
								</p>
								<br />
								<br />
								<b>'.$this->l('3) To configure this module, complete each of the 6 sections below. As you successfully enter the requested information, each section will turn from red to green once you press "Update" to signify that the section is complete.').'</b>
								<p style="margin-left:10px">

								</p>
							</td>
						</tr>
					</table>
				</div>   
			'.($ps_version < 1.6 ? '</fieldset>': '</div>').'
		</form>
		<br />

		<form method="post" name="settings_form" id="settings_form">
			'.($ps_version >= 1.6?'<div class="panel">':
			'<fieldset class="inner_fieldset" style="width:900px;">').
				($ps_version < 1.6 ? '<legend>': '<h3>').'
					<img src="'.$this->_path.'logo.gif" style="'.($this->getPSV() == 1.6 ? 'margin: 0 10px 0 0;' : '').'" /> '.$this->l('Fedex Account Setting').'
				'.($ps_version < 1.6 ? '</legend>': '</h3>').'
				
				'.($ps_version >= 1.6?'<div class="panel">':
				'<fieldset class="inner_fieldset" style="width:900px;">').
					($ps_version < 1.6 ? '<legend class="'.($verified ? 'validated' : 'not_validated').'">': '<h3 class="'.($verified ? 'validated' : 'not_validated').'">').'
						<img src="'.$this->_path.'logo.gif" style="'.($this->getPSV() == 1.6 ? 'margin: 0 10px 0 0;' : '').'" /> '.$this->l('Step 1: Account Info').'
					'.($ps_version < 1.6 ? '</legend>': '</h3>').'
				
				<table width="100%" cellpadding="0" cellspacing="0"> 
					<tr height="31">
						<td align="right" width="20%">
							'.$this->l('Account Number').':&nbsp;
						</td>
						<td align="left" width="25%">
							<input type="text" name="fedex_account" style="width:190px;float: left;margin: 0 10px 0 0;" id="fedex_account" value="'.Tools::getValue('fedex_account', $this->_fedex_account).'" />
						</td>
						<td align="right" width="20%">
							'.$this->l('Meter Number').':&nbsp;
						</td>
						<td align="left">
							<input type="text" name="fedex_meter" style="width:190px;float: left;margin: 0 10px 0 0;" id="fedex_meter" value="'.Tools::getValue('fedex_meter', $this->_fedex_meter).'" />
						</td>
					</tr>
					<tr height="31">
						<td align="right" width="20%">
							'.$this->l('Authentication Key').':&nbsp;
						</td>
						<td align="left" width="25%">
							<input type="text" name="fedex_key" style="width:190px;float: left;margin: 0 10px 0 0;" id="fedex_key" value="'.Tools::getValue('fedex_key', $this->_fedex_key).'" />
						</td>
						<td align="right" width="20%">
							'.$this->l('Password').':&nbsp;
						</td>
						<td align="left">
							<input type="text" name="fedex_pass" style="width:190px;float: left;margin: 0 10px 0 0;" id="fedex_pass" value="'.Tools::getValue('fedex_pass', $this->_fedex_pass).'" />
						</td>
					</tr>
					<tr height="31">
						<td align="right" width="20%">
							'.$this->l('Origin Country').':&nbsp;
						</td>
						<td align="left" width="25%">
							<select name="fedex_origin_country" style="'.($this->getPSV() < 1.6 ? 'width:200px;padding: 1px;' : 'width:190px;').'float: left;margin: 0 10px 0 0;">';
								foreach ($countries as $country)
									$this->_html .= '<option value="'.$country['iso_code'].'" '.(Tools::getValue('fedex_origin_country', $this->_fedex_origin_country) == $country['iso_code']?"selected":"").'>'.$country['name'].'</option>';
								$this->_html .= '
							</select>
						</td>
						<td align="right" width="20%">
							'.$this->l('Origin Zipcode').':&nbsp;
						</td>
						<td align="left">
							<input type="text" name="fedex_origin_zip" style="width:190px;float: left;margin: 0 10px 0 0;" id="fedex_origin_zip" value="'.Tools::getValue('fedex_origin_zip', $this->_fedex_origin_zip).'" />
									
							<span class="info_tooltip" title="
								'.$this->l('Zipcode where you ship your packages from.').'
							"></span>					
						</td>
					</tr>
					<tr height="31">
						<td align="right" width="20%">
							'.$this->l('Origin City').':&nbsp;
						</td>
						<td colspan="3" align="left">
							<input type="text" name="fedex_origin_city" style="width:190px;float: left;margin: 0 10px 0 0;" id="fedex_origin_city" value="'.Tools::getValue('fedex_origin_city', $this->_fedex_origin_city).'" />

							<span class="info_tooltip" title="
								'.$this->l('City where you ship your packages from.').'
							"></span>        
						</td>
					</tr>
					<tr height="31">
						<td align="right" width="20%">
							'.$this->l('Account Mode').':&nbsp;
						</td>
						<td align="left" width="25%">
							<select name="fedex_mode" style="'.($this->getPSV() < 1.6 ? 'width:200px;padding: 1px;' : 'width:190px;').'float: left;margin: 0 10px 0 0;">
								<option value="test" '.(Tools::getValue('fedex_mode', $this->_fedex_mode) != "live"?"selected":"").'>'.$this->l('Test').'</option>
								<option value="live" '.(Tools::getValue('fedex_mode', $this->_fedex_mode) == "live"?"selected":"").'>'.$this->l('Live').'</option>
							</select>
						</td>
						<td colspan="2" align="center">';
						if(Tools::getValue('fedex_mode', $this->_fedex_mode) == "live")
						{
							if($verified == 1)
								$this->_html .= '<b style="vertical-align: middle; color: green;">'.$this->l('Account Setting Verified').'</b>';
							elseif(!$verified && ($this->_fedex_account || $this->_fedex_pass || $this->_fedex_meter || $this->_fedex_key))
								$this->_html .= '<b style="vertical-align: middle; color: red;">'.$this->l('Incorrect Account Setting').'</b>';
						}
						$this->_html .= '
						</td>
					</tr> 
					<tr height="31">
						<td colspan="4" style="text-align:center;">
							<input onclick="return validate_as()" type="submit" value="'.$this->l('Update').'" name="updateSettings" class="'.($this->getPSV() == 1.6 ? 'btn btn-default' : 'button').'" style="width: 100px;" />
						</td>
					</tr>
				</table>
			'.($ps_version < 1.6 ? '</fieldset>': '</div>').'
			
			'.($ps_version >= 1.6?'<div class="panel">':
			'<fieldset class="inner_fieldset" style="width:900px;">').
				($ps_version < 1.6 ? '<legend class="validated">': '<h3 class="validated">').'
					<img src="'.$this->_path.'logo.gif" style="'.($this->getPSV() == 1.6 ? 'margin: 0 10px 0 0;' : '').'" /> '.$this->l('Step 2: Shipping Preferences').'
				'.($ps_version < 1.6 ? '</legend>': '</h3>').'
					
				<table width="95%" cellpadding="0" cellspacing="0">
					<tr height="31">
						<td align="right" width="20%">
							'.$this->l('Units').':&nbsp;
						</td>
						<td align="left" width="25%">
							<select name="fedex_unit" id="fedex_unit" style="'.($this->getPSV() < 1.6 ? 'width:200px;padding: 1px;' : 'width:190px;').'float: left;margin: 0 10px 0 0;">
								<option value="LBS" '.(Tools::getValue('fedex_unit', $this->_fedex_unit) == "LBS"?"selected":"").'>'.$this->l('LBS / IN').'</option>
								<option value="KGS" '.(Tools::getValue('fedex_unit', $this->_fedex_unit) == "KGS"?"selected":"").'>'.$this->l('KGS / CM').'</option>
							</select>
						</td>
						<td align="right" width="20%">
							'.$this->l('Origin Type').':&nbsp;
						</td>
						<td align="left">
							<select name="fedex_origin_resident" style="'.($this->getPSV() < 1.6 ? 'width:200px;padding: 1px;' : 'width:190px;').'float: left;margin: 0 10px 0 0;">
								<option value="0" '.(Tools::getValue('fedex_origin_resident', $this->_fedex_origin_resident) != "1"?"selected":"").'>'.$this->l('Commercial').'</option>
								<option value="1" '.(Tools::getValue('fedex_origin_resident', $this->_fedex_origin_resident) == "1"?"selected":"").'>'.$this->l('Residential').'</option>
							</select>
						</td>
					</tr>
					<tr height="31">
						<td align="right" width="20%">
							'.$this->l('Dropoff Point').':&nbsp;
						</td>
						<td align="left" width="25%">
							<select name="fedex_dropoff" id="fedex_dropoff" style="'.($this->getPSV() < 1.6 ? 'width:200px;padding: 1px;' : 'width:190px;').'float: left;margin: 0 10px 0 0;">';
							foreach ($dropoff_types as $code => $type) {
								$this->_html .= '<option value="'.$code.'" '.(Tools::getValue('fedex_dropoff', $this->_fedex_dropoff) == $code ? "selected" : "").'>'.$type.'</option>';
							}
							$this->_html .= '
							</select>
						</td>
						<td align="right" width="20%">
							'.$this->l('Destination Type').':&nbsp;
						</td>
						<td align="left">
							<select name="fedex_destin_resident" style="'.($this->getPSV() < 1.6 ? 'width:200px;padding: 1px;' : 'width:190px;').'float: left;margin: 0 10px 0 0;">
								<option value="0" '.(Tools::getValue('fedex_destin_resident', $this->_fedex_destin_resident) == "0"?"selected":"").'>'.$this->l('Commercial').'</option>
								<option value="1" '.(Tools::getValue('fedex_destin_resident', $this->_fedex_destin_resident) != "0" && Tools::getValue('fedex_destin_resident', $this->_fedex_destin_resident) != "2"?"selected":"").'>'.$this->l('Residential').'</option>
								<option value="2" '.(Tools::getValue('fedex_destin_resident', $this->_fedex_destin_resident) == "2"?"selected":"").'>'.$this->l('Automatic').'</option>
							</select>
							
							<span class="info_tooltip" title="
								'.$this->l('Select the destination type you generally ship to. If you choose Automatic, the module will default to Residential unless a company name has been entered, in which case Commercial will be selected. Also note that FedEx "Ground" is only used for Commercial, and FedEx "Ground Home Delivery" is used for Residential, so if you select Automatic for Destination Type, make sure both Ground methods are added in Step 5 below.').'
							"></span>    
						</td>
					</tr>
				</table>
			'.($ps_version < 1.6 ? '</fieldset>': '</div>');
			
			if (($this->_fedex_package_size == "product" && !sizeof($this->_fedex_boxes)))
				$packaging_validated = false;
			elseif ($this->_fedex_weight < 0) 
				$packaging_validated = false;
			else
				$packaging_validated = true; 
				
			$this->_html .= 
			($ps_version >= 1.6?'<div class="panel">':
			'<fieldset class="inner_fieldset" style="width:900px;">').
				($ps_version < 1.6 ? '<legend  class="'.($packaging_validated ? 'validated' : 'not_validated').'">': '<h3  class="'.($packaging_validated ? 'validated' : 'not_validated').'">').'
					<img src="'.$this->_path.'logo.gif" style="'.($this->getPSV() == 1.6 ? 'margin: 0 10px 0 0;' : '').'" /> '.$this->l('Step 3: Packaging').'
				'.($ps_version < 1.6 ? '</legend>': '</h3>').'

				<p>'.$this->l('The default and most basic shipping configuration is by weight only: select "Multiple Boxes", "Fixed Size", leave "Width/Height/Depth" as zero, enter a "Max Weight" and leave "Maximum product per box" as zero.').'</p>
				<p style="padding-bottom:15px;">'.$this->l('Or, to let this module determine the number of boxes needed based on product dimensions, select "Multiple Boxes" and "Product Dimensions" and add each box size you use, one at a time. Be sure all of your products have width/height/depth defined.').'</p>
				<b style="float: left;">'.$this->l('Packaging Type').':&nbsp;</b>
				<select name="fedex_pack" id="fedex_pack" style="'.($this->getPSV() < 1.6 ? 'width:200px;padding: 1px;' : 'width:190px;').'float: left;margin: 0 10px 0 0;">';
					foreach ($package_types as $code => $type) 
					{
						$this->_html .= '<option value="'.$code.'" '.(Tools::getValue('fedex_pack', $this->_fedex_pack) == $code ? "selected" : "").'>'.$type.'</option>';
					}
					$this->_html .= '
				</select>
				
				<span class="info_tooltip" title="
					'.$this->l('Size/type of the package which you generally use for FedEx shipments. \'Your Packaging\' requires that you enter box size information (When selected, additional fields appear below).').'
				"></span>  

				<br /><br />
				<div id="fedex_my_pack" style="display:'.($this->_fedex_pack == "" || $this->_fedex_pack == "YOUR_PACKAGING"?"block":"none").'">
				<table width="100%" class="packaging_table" style="margin-left:20px;">
					<tr height="31">
						<td align="left" width="35%">
								<span style="vertical-align:middle;margin-right: 40px;font-weight:bold;">'.$this->l('Number of Boxes').':</span>

						</td>
						<td align="left" width="15%">
							<input type="radio" name="fedex_packages" id="fedex_packages_single" onclick="show_package_size();" value="single" '.(Tools::getValue('fedex_packages', $this->_fedex_packages) == "single"?'checked':'').' />
							
						   <label for="fedex_packages_single" style="float:none;display:inline;font-weight:normal;vertical-align:middle;">'.$this->l('Single box').'</label>
							<span class="info_tooltip" title="
								'.$this->l('Select \'Single box\' if you always ship each order in only one box.').'
							"></span>  
						</td>
						<td align="left" width="5%">
							'.$this->l('or').'
						</td>
						<td align="left">
							<input type="radio" name="fedex_packages" id="fedex_packages_multiple" onclick="show_package_size();" value="multiple" '.(Tools::getValue('fedex_packages', $this->_fedex_packages) == "multiple"?'checked':'').' />
							
							<label for="fedex_packages_multiple" style="float:none;display:inline;font-weight:normal;vertical-align:middle;">'.$this->l('Multiple boxes').'</label>
							<span class="info_tooltip" title="
								'.$this->l('Select \'Multiple boxes\' if your shipments sometimes requires the use of more than one box. If you choose Multiple boxes, you can then choose between \'Fixed Size\' and \'Product Dimensions\'.').'
							"></span>  
						</td>
					</tr>
					<tr height="31">
							<td align="left">
							<span style="vertical-align:middle;margin-right: 40px;font-weight:bold;">'.$this->l('Calculate # of Products Per Box By').':</span>
						</td>
						<td align="left">
							<input type="radio" name="fedex_package_size" id="fedex_package_size_fixed" onclick="show_package_size();" value="fixed" '.(Tools::getValue('fedex_package_size', $this->_fedex_package_size) == "fixed"?'checked':'').' />
							
							<label for="fedex_package_size_fixed" style="float:none;display:inline;font-weight:normal;vertical-align:middle;">'.$this->l('Fixed Size').'</label>
							<span class="info_tooltip" title="
								'.$this->l('Select \'Fixed size\' if you use only one box size. You will be able to select number of products per box, or use maximum weight per box.').'<br />'.$this->l('The number of boxes needed will be determined by weight or max products per box (Requires entering only weight for each product).').'
							"></span>  
						</td>
							<td align="left">
							'.$this->l('or').'
						</td>
						<td align="left">
							<input type="radio" name="fedex_package_size" id="fedex_package_size_product" onclick="show_package_size(\'\');" value="product" '.(Tools::getValue('fedex_package_size', $this->_fedex_package_size) == "product"?'checked':'').' />
							
							<label for="fedex_package_size_product" style="float:none;display:inline;font-weight:normal;vertical-align:middle;">'.$this->l('Product Dimensions').'</label>	
							<span class="info_tooltip" title="
								'.$this->l('Select \'Product dimensions\' if you use more than one size box. The boxes needed for each order will be determined by the box dimentions as well as product dimension (Requires entering dimension for each product).').'<br />'.$this->l('The module will automatically detect the number of products per box. It will attempt to use the smallest / lowest number of boxes based on the cart contents. Be sure all products have dimensions entered.').'
							"></span>  
						</td>
					</tr>
					<tr height="31">
						<td align="left" colspan="4">
							<div id="fixed_box" style="display:none;">
								<b>'.$this->l('Box Dimensions').' ('.Configuration::get('PS_WEIGHT_UNIT').'/'.Configuration::get('PS_DIMENSION_UNIT').')</b>:<br/>
								
								<div style="float: left; margin: 5px 2% 15px 0;">
									'.($this->getPSV() != 1.6 ? '&nbsp;&nbsp;' : '').'<small>'.$this->l('Width').':</small>
									'.($this->getPSV() == 1.6 ? '<br />' : '').'
									<input type="text" name="fedex_width" class="needValidate" style="'.($this->getPSV() == 1.6 ? 'float: left; width: 50px; margin: 0 5px 0 0;' : 'width:20px;').'" id="fedex_width" value="'.Tools::getValue('fedex_width', $this->_fedex_width, 10).'" />&nbsp;
										
									<span class="info_tooltip" title="
										'.$this->l('Enter the Width of a box which you will use for shipping (Value must be greater than 0). You can enter multiple boxes.').'
									"></span>
								</div>
								<div style="float: left; margin: 5px 2% 15px 0;">
									'.($this->getPSV() != 1.6 ? '&nbsp;&nbsp;' : '').'<small>'.$this->l('Height').':</small>
									'.($this->getPSV() == 1.6 ? '<br />' : '').'
									<input type="text" name="fedex_height" class="needValidate" style="'.($this->getPSV() == 1.6 ? 'float: left; width: 50px; margin: 0 5px 0 0;' : 'width:20px;').'" id="fedex_height" value="'.Tools::getValue('fedex_height', $this->_fedex_height, 10).'" />&nbsp;
									
									<span class="info_tooltip" title="
										'.$this->l('Enter the Height of a box which you will use for shipping (Value must be greater than 0). You can enter multiple boxes.').'
									"></span>
								</div>
								<div style="float: left; margin: 5px 2% 15px 0;">
									'.($this->getPSV() != 1.6 ? '&nbsp;&nbsp;' : '').'<small>'.$this->l('Depth / Length').':</small>
									'.($this->getPSV() == 1.6 ? '<br />' : '').'
									<input type="text" name="fedex_depth" class="needValidate" style="'.($this->getPSV() == 1.6 ? 'float: left; width: 50px; margin: 0 5px 0 0;' : 'width:20px;').'" id="fedex_depth" value="'.Tools::getValue('fedex_depth', $this->_fedex_depth, 10).'" />
									
									<span class="info_tooltip" title="
										'.$this->l('Enter the Depth/Length of a box which you will use for shipping (Value must be greater than 0). You can enter multiple boxes.').'
									"></span>
								</div>
								<div style="float: left; margin: 5px 2% 15px 0;">
									'.($this->getPSV() != 1.6 ? '&nbsp;&nbsp;' : '').'<small>'.$this->l('Max Weight').':</small>
									'.($this->getPSV() == 1.6 ? '<br />' : '').'
									<input type="text" name="fedex_weight" class="needValidate"  style="'.($this->getPSV() == 1.6 ? 'float: left; width: 60px; margin: 0 5px 0 0;' : 'width:26px;').'" id="fedex_weight" value="'.Tools::getValue('fedex_weight', $this->_fedex_weight, 150).'" /> '.$this->l('(Must be greater than 0)').'
										
									<span class="info_tooltip" title="
										'.$this->l('Enter the maximum Weight of a box which you will use for shipping (Value must be greater than 0). You can enter multiple boxes.').'
									"></span>
								</div>
							</div>
							
							<div id="fixed_box_multiple" style="margin-top:8px;display:none;clear:left;'.($this->getPSV() == 1.6 ? 'float:left;' : '').'">
								'.($this->getPSV() != 1.6 ? '&nbsp;&nbsp;' : '<span style="float: left; margin: 0 5px 0 0;">').$this->l('Maximum products per box: ').($this->getPSV() == 1.6 ? '</span>' : '').'
								<input type="text" name="fedex_packages_per_box" style="'.($this->getPSV() == 1.6 ? 'float: left; width: 50px; margin: 0 5px 0 0;' : 'width:30px;').'" id="fedex_packages_per_box" value="'.Tools::getValue('fedex_packages_per_box', $this->_fedex_packages_per_box).'" />
								
								<span class="info_tooltip" title="
									'.$this->l('Enter the maximum number of product that can fit in a box which you will use for shipping (Enter 0 to determine by Weight)').'
								"></span>
							</div>
							<div id="fixed_multi_box" style="display:none">
								<b style="float: left;  margin: 0 0 10px 0;">'.$this->l('Box Dimension').' ('.Configuration::get('PS_WEIGHT_UNIT').'/'.Configuration::get('PS_DIMENSION_UNIT').'):</b>
								&nbsp;'.$this->l('Add the dimensions for each of the boxes you use to ship your products, click the').' <img src="'.$this->_path.'img/add.gif" style="cursor:pointer;vertical-align:middle;" /> '.$this->l('to add a new box.').'
									
								<span class="info_tooltip" title="
									'.$this->l('Click the').' <img src=\''.$this->_path.'img/add.gif\' style=\'cursor:pointer;vertical-align:middle;\' /> '.$this->l('to add; once a box was added you can assign it a name (optional, for internal organization purposes).').'
								"></span>  

								<div style="float: left; margin: 5px 2% 15px 0; clear:both">
									'.($this->getPSV() != 1.6 ? '&nbsp;&nbsp;' : '').'<small>'.$this->l('Width').'</small>
									'.($this->getPSV() == 1.6 ? '<br />' : '').'
									<input type="text" name="fedex_box_width" value="10" class="needValidate" style="'.($this->getPSV() == 1.6 ? 'float: left; width: 50px; margin: 0 5px 0 0;' : 'width:20px;').'" id="fedex_box_width" />
									'.($this->getPSV() != 1.6 ? '&nbsp;' : '').'
									
									<span class="info_tooltip" title="
										'.$this->l('Enter the Width of a box which you will use for shipping (Value must be greater than 0). You can enter multiple boxes.').'
									"></span>
								</div>
								<div style="float: left; margin: 5px 2% 15px 0; ">
									'.($this->getPSV() != 1.6 ? '&nbsp;&nbsp;' : '').'<small>'.$this->l('Height').'</small>
									'.($this->getPSV() == 1.6 ? '<br />' : '').'
									<input type="text" name="fedex_box_height" value="10" class="needValidate" style="'.($this->getPSV() == 1.6 ? 'float: left; width: 50px; margin: 0 5px 0 0;' : 'width:20px;').'" id="fedex_box_height" />
									'.($this->getPSV() != 1.6 ? '&nbsp;' : '').'
									
									<span class="info_tooltip" title="
										'.$this->l('Enter the Height of a box which you will use for shipping (Value must be greater than 0). You can enter multiple boxes.').'
									"></span>
								</div>
								<div style="float: left; margin: 5px 2% 15px 0; ">
									'.($this->getPSV() != 1.6 ? '&nbsp;&nbsp;' : '').'<small>'.$this->l('Depth / Length').'</small>
									'.($this->getPSV() == 1.6 ? '<br />' : '').'
									<input type="text" name="fedex_box_depth" value="10" class="needValidate" style="'.($this->getPSV() == 1.6 ? 'float: left; width: 50px; margin: 0 5px 0 0;' : 'width:20px;').'" id="fedex_box_depth" />
									
									<span class="info_tooltip" title="
										'.$this->l('Enter the Depth/Length of a box which you will use for shipping (Value must be greater than 0). You can enter multiple boxes.').'
									"></span>
								</div>
								<div style="float: left; margin: 5px 2% 15px 0; ">
									'.($this->getPSV() != 1.6 ? '&nbsp;&nbsp;' : '').'<small>'.$this->l('Max Weight').'</small>
									'.($this->getPSV() == 1.6 ? '<br />' : '').'
									<input type="text" name="fedex_box_weight" value="150" class="needValidate" style="'.($this->getPSV() == 1.6 ? 'float: left; width: 60px; margin: 0 5px 0 0;' : 'width:26px;').'" id="fedex_box_weight" /> '.$this->l('(Must be greater than 0)').'

									<span class="info_tooltip" title="
										'.$this->l('Enter the maximum Weight of a box which you will use for shipping (Value must be greater than 0). You can enter multiple boxes.').'
									"></span>
								</div>
								
								<div style="float: left; margin: 5px 2% 15px 0; ">
									'.($this->getPSV() != 1.6 ? '&nbsp;&nbsp;&nbsp;&nbsp;' : '').'
									<img src="'.$this->_path.'img/add.gif" id="edit_box" style="cursor:pointer;vertical-align:middle;'.($this->getPSV() == 1.6 ? 'margin: 20px 0 0 15px;' : '').'" onclick="add_box(\'\')" />
								</div>
								<div id="fedex_boxes" style="clear:both"></div>
							</div>
							<div id="product_box" style="display:none;margin-top:6px;clear:both;">
								<table width="100%">
								<tr height="31">
									<td align="left">
										<li style="left-margin:10px">
										<i style="color:red">'.$this->l('The detection algorithm does its best to estimate the number of boxes needed to fit all the products, it may over estimate').'.</i>
										</li>
										<li style="left-margin:10px">
										<i style="color:green">'.$this->l('If the product dimension are bigger than the "Box Dimensions" above, a new box the size of the product will be added').'.</i>
										</li>
									</td>
								</tr>
								<tr height="31">
									<td align="left">
										'.($this->getPSV() == 1.6 ? '<span style="float: left; margin: 0 5px 0 0;">' : '').'
											'.$this->l('Debug Mode').':
										'.($this->getPSV() == 1.6 ? '</span>' : '').'
										<select name="fedex_debug_mode" style="width:60px; '.($this->getPSV() == 1.6 ? 'float: left; margin: 0 5px 0 0;' : '').'">
											<option value="0"'.($this->_fedex_debug_mode != 1?' selected':'').'>'.$this->l('Off').'</option>
											<option value="1"'.($this->_fedex_debug_mode == 1?' selected':'').'>'.$this->l('On').'</option>
										</select>
										
										<span class="info_tooltip" title="
											'.$this->l('Will show the number of boxes used and their size / weight (when not getting rates from the cache)').'.
										"></span>
									</td>
								</tr>
								</table>
							</div>
						</td>
					</tr>
				</table>
				</div>
			'.($ps_version < 1.6 ? '</fieldset>': '</div>').'
			
			'.($ps_version >= 1.6?'<div class="panel">':
			'<fieldset class="inner_fieldset" style="width:900px;">').
				($ps_version < 1.6 ? '<legend class="validated">': '<h3 class="validated">').'
					<img src="'.$this->_path.'logo.gif" style="'.($this->getPSV() == 1.6 ? 'margin: 0 10px 0 0;' : '').'" /> '.$this->l('Step 4: Module Settings').'
				'.($ps_version < 1.6 ? '</legend>': '</h3>').'

				<table width="100%" cellpadding="0" cellspacing="0">
					<tr height="31">
					<td align="left">
							'.$this->l('"Shipping Rates" Buttons').':&nbsp;
						</td>
					<td align="left">
							<select name="ssr" style="'.($this->getPSV() == 1.6 ? ' float: left;width: 90px;margin: 0 5px 0 0;' : '').'">
								<option '.(Configuration::get('FEDEX_SHIPPING_RATES_BUTTON') == 'show' ? 'selected="selected"' : '').' value="show">'.$this->l('Show').'</option>
								<option '.(Configuration::get('FEDEX_SHIPPING_RATES_BUTTON') == 'hide' ? 'selected="selected"' : '').' value="hide">'.$this->l('Hide').'</option>
							</select>
							
							<span class="info_tooltip" title="
								'.$this->l('* When using more than one of our shipping modules, only one should be set as \'Show\'').'
								<br /><br />
								<img src=\''.$this->_path.'img/shipping_preview.jpg\' alt=\'?\' />
							"></span>  
						</td>
					</tr>
					<tr height="31">
						<td align="left">
							'.$this->l('Shipping Rate Address').':&nbsp;
						</td>
						<td align="left">
							<input type="checkbox" id="fedex_country_display" name="fedex_country_display" value="1" '.(Tools::getValue('fedex_country_display', $this->_fedex_address_display['country']) == '1'?'checked="checked"':'').' />
							'.$this->l('Country').'&nbsp;&nbsp;&nbsp;&nbsp;

							<input type="checkbox" id="fedex_state_display" name="fedex_state_display" value="1" '.(Tools::getValue('fedex_state_display', $this->_fedex_address_display['state']) == '1'?'checked="checked"':'').' />
							'.$this->l('State').'&nbsp;&nbsp;&nbsp;&nbsp;

							<input type="checkbox" id="fedex_city_display" name="fedex_city_display" value="1" '.(Tools::getValue('fedex_city_display', $this->_fedex_address_display['city']) == '1'?'checked="checked"':'').' />
							'.$this->l('City').'&nbsp;&nbsp;&nbsp;&nbsp;

							<input type="checkbox" id="fedex_zip_display" name="fedex_zip_display" value="1" '.(Tools::getValue('fedex_zip_display', $this->_fedex_address_display['zip']) == '1'?'checked="checked"':'').' />
							'.$this->l('Zip').'&nbsp;&nbsp;&nbsp;&nbsp;
							
							<span class="info_tooltip" title="
								'.$this->l('Select the fields to display in the cart preview. City is only required for International shipping.').'
							"></span>  
						</td>
					</tr>
					<tr height="31">
						<td width="180">
							'.$this->l('Address Override').':&nbsp;
						</td>
						<td align="left">
							<input id="fedex_override_address" type="checkbox" name="fedex_override_address" value="1" '.($this->_fedex_override_address == 1?'checked':'').' />
							<label for="fedex_override_address" style="float:none;font-weight:normal;display:inline;">'.$this->l('Update the customer\'s account address with the shipping preview address').'</label>
						</td>
					</tr>
					<tr height="31">
						<td align="left">
							'.$this->l('XML Log').':&nbsp;
						</td>
						<td align="left">
							<input id="fedex_xml_log" type="checkbox" name="fedex_xml_log" value="1" '.(Tools::getValue('fedex_xml_log', $this->_fedex_xml_log) == '1'?'checked="checked"':'').' />
							<label for="fedex_xml_log" style="float:none;font-weight:normal;display:inline;">'.$this->l('Create a log of the request and response from FEDEX').'</label>
							
							<span class="info_tooltip" title="
								'.$this->l('The XML log will show the request and response from Fedex, you can use it to see exactly which shipping options are being returned, and refer to it if you think the rates you are getting back are different that the rates you get on Fedex.com. The log will only show new requests that are not already cached (clear the cache if needed).').'
								<br /><br />
								'.$this->l('The log is saved in').'
								<br />
								'.__PS_BASE_URI__.'modules/fedex/logs/'.$this->_fedex_log_filename.'
							"></span>  
							
							'.(file_exists(dirname(__FILE__).'/logs/'.$this->_fedex_log_filename)?'(<a href="'.__PS_BASE_URI__.'modules/fedex/logs/'.$this->_fedex_log_filename.'" target="_index" style="color:blue;">View Log</a>)':'').'
						</td>
					</tr>
					<tr height="31">
						<td colspan="5" align="left">
							<input onclick="return confirm(\''.$this->l('Are you sure you want to delete the cache').'?\');" type="submit" value="'.$this->l('Delete Rate Cache').'" name="deleteCache" class="'.($this->getPSV() == 1.6 ? 'btn btn-default' : 'button').'" />
						</td>
					</tr>
				</table>
			'.($ps_version < 1.6 ? '</fieldset>': '</div>').'
			
			<div style="text-align:center;">
				<input onclick="return validate_as()" type="submit" value="'.$this->l('Update').'" name="updateSettings" class="'.($this->getPSV() == 1.6 ? 'btn btn-default' : 'button').'" style="width: 100px;" />
			</div>
			
		'.($ps_version < 1.6 ? '</fieldset>': '</div>').'
		
		<br />
		
		'.($ps_version >= 1.6?'<div class="panel">':
		'<fieldset class="inner_fieldset" style="width:900px;">').
			($ps_version < 1.6 ? '<legend>': '<h3>').'
				<img src="'.$this->_path.'logo.gif" style="'.($this->getPSV() == 1.6 ? 'margin: 0 10px 0 0;' : '').'" /> '.$this->l('Shipping Options (Carriers)').'
			'.($ps_version < 1.6 ? '</legend>': '</h3>').'
			
			'.($ps_version >= 1.6?'<div class="panel">':
			'<fieldset class="inner_fieldset" style="width:900px;">').
				($ps_version < 1.6 ? '<legend class="'.(sizeof($carriers) ? 'validated' : 'not_validated').'">': '<h3 class="'.(sizeof($carriers) ? 'validated' : 'not_validated').'">').'
					<img src="'.$this->_path.'logo.gif" style="'.($this->getPSV() == 1.6 ? 'margin: 0 10px 0 0;' : '').'" /> '.$this->l('Step 5: Add Carriers -').' <span style="font-weight:normal;">'.$this->l('Add at least one FedEx shipping method here to ensure your shipping option(s) appear in the front office.').'</span>
				'.($ps_version < 1.6 ? '</legend>': '</h3>').'
				
				<table border="0" width="100%">
					<tr height="31">
						<td align="left" colspan="10">
							<p style="color:red;font-weight:normal">
								'.$this->l('Note: before adding carriers, be sure that your PrestaShop zones are setup correctly.').'
							
								<span class="info_tooltip" title="
									'.$this->l('Any Country and State that you ship to must be linked to the same zone as the carrier you add in this module. You can check this from').' '.($ps_version < 1.5?$this->l('Shipping'):$this->l('Localization')).$this->l('->Countries / States').'
								"></span>  
							</p>
						</td>
					</tr>
					<tr height="31">
						<td align="left" style="width:15%;">
							'.$this->l('Name').':&nbsp;<sup>*</sup>
						</td>
						<td align="left" style="width:30%;">
							<input type="text" name="fedex_carrier_name" style="width:190px;'.($this->getPSV() == 1.6 ? 'margin: 0 5px 0 0;float: left;' : '').'" id="fedex_carrier_name" value="" />
							
							<span class="info_tooltip" title="
								'.$this->l('Assign a Name to the carrier (the Fedex shipping method), for example \'Fedex Ground\'.').'
								<br >
								'.$this->l('Use PrestaShop\'s Shipping > Carriers to edit tax rules, zones or delete a carrier.').'
							"></span>  
						</td>
						<td align="left" style="width:1%;">
						</td>
						<td align="left" style="width:16%;">
							'.$this->l('Free Shipping From').':&nbsp;
						</td>
						<td align="left" style="width:38%;">
							'.($this->getPSV() == 1.6 ? '<span style="float: left; margin: 3px 3px 0 0;">' : '').
								$currency->sign.
							($this->getPSV() == 1.6 ? '</span>' : '').'
							<input type="text" name="fedex_free_shipping" style="width:40px;'.($this->getPSV() == 1.6 ? 'margin: 0 5px 0 0;float: left;' : '').'" id="fedex_free_shipping" value="0" />
							
							<span class="info_tooltip" title="
								'.$this->l('The shipping method will be free when the cart subtotal is above this amount. Leave as zero if you do not want to offer free shipping. You will be able to add free shipping by Product/Category/Manufacturer/Supplier below after you add the carrier.').'
							"></span>  
						</td>
					</tr>
					<tr height="31">
						<td align="left">
							'.$this->l('Transit Time').':&nbsp;<sup>*</sup>
						</td>
						<td align="left">
							<input type="text" name="fedex_transit_time" style="width:190px;'.($this->getPSV() == 1.6 ? 'margin: 0 5px 0 0;float: left;' : '').'" id="fedex_transit_time" value="" />
							
							<span class="info_tooltip" title="
								'.$this->l('Appears in the checkout page next to the carrier name. As an example you could enter \'3-5 Days\'').'
							"></span>  
						</td>
						<td>
						</td>
						<td align="left">
							'.$this->l('Extra Charge').':&nbsp;
						</td>
						<td align="left">
							<select name="fedex_extra_charge" id="fedex_extra_charge" style="width: 140px;'.($this->getPSV() == 1.6 ? 'margin: 0 5px 0 0;float: left;' : '').'" onchange="if ($(\'#fedex_extra_charge\').val() == \'0\') { $(\'#fedex_extra_amount\').fadeOut(1200); $(\'#fedex_extra_sign\').html(\'\');} else if ($(\'#fedex_extra_charge\').val() != \'2\'){$(\'#fedex_extra_amount\').fadeIn(1200);$(\'#fedex_extra_sign\').html(\'%\');}  else {$(\'#fedex_extra_amount\').fadeIn(1200);$(\'#fedex_extra_sign\').html(\''.$currency->sign.'\');}">
								<option value="0">'.$this->l('None').'</option>
								<option value="1">'.$this->l('% of Order Total').'</option>
								<option value="2">'.$this->l('Fixed Amount').'</option>
								<option value="3">'.$this->l('% of Shipping Total').'</option>
							</select>
							
							<span class="info_tooltip" title="
								'.$this->l('Add a shipping cost price markup (optional) to this shipping method. You will be able to edit this later below.').'
							"></span>  

							&nbsp;&nbsp;
							<span id="fedex_extra_sign" style="'.($this->getPSV() == 1.6 ? 'float: left; margin: 3px 3px 0 0;' : '').'"></span>
							<input type="text" name="fedex_extra_amount" style="padding: 0 5px;'.($this->getPSV() == 1.6 ? 'float: left;width:45px;margin: 0 3px 0 0;' : 'width:30px;').' display: none;" id="fedex_extra_amount" value="" />
						</td>
					</tr>
					<tr height="31">
					<td align="left">
							'.$this->l('Type').':&nbsp;
						</td>
						<td align="left">
							<select name="fedex_method" style="'.($this->getPSV() < 1.6 ? 'width:200px;padding: 1px;' : 'width:190px;').'float: left;margin: 0 10px 0 0;">';
								foreach ($ship_meth as $id => $meth) {
									$this->_html .= '<option value="'.$id.'">'.$meth.'</option>';
								}
							$this->_html .= '</select>
						</td>
						<td>
						</td>
						<td align="left">
							'.$this->l('Insurance').':&nbsp;
						</td>
						<td align="left">
							<select name="fedex_insurance_charge" style="width: 140px;'.($this->getPSV() == 1.6 ? 'margin: 0 5px 0 0;float: left;' : '').'" id="fedex_insurance_charge" onchange="if ($(\'#fedex_insurance_charge\').val() == \'0\') { $(\'.fedex_insurance\').fadeOut(1200); } else if ($(\'#fedex_insurance_charge\').val() == \'1\') { $(\'.fedex_insurance\').fadeOut(100);$(\'.fedex_insurance_whole\').fadeIn(1200); } else { $(\'.fedex_insurance\').fadeIn(1200);}">
								<option value="0">'.$this->l('None').'</option>
								<option value="1">'.$this->l('Wholesale Price').'</option>
								<option value="2">'.$this->l('% of Retail Price').'</option>
							</select>
							
							<span class="info_tooltip" title="
								'.$this->l('Add insurance (optional) to this shipping method. You will be able to edit this later below.').'
							"></span>  
							
							&nbsp;&nbsp;
							<span style="display:none" class="fedex_insurance">
								'.($this->getPSV() == 1.6 ? '<span style="float: left; margin: 3px 3px 0 0;">' : '').'
									%
								'.($this->getPSV() == 1.6 ? '</span>' : '').'
								<input type="text" name="fedex_insurance_amount"  style="padding: 0 5px;'.($this->getPSV() == 1.6 ? 'float: left;width:45px;margin: 0 3px 0 0;' : 'width:30px;').'" id="fedex_insurance_amount" value="" />
							</span>
						</td>
						<td align="left" style="width:160px;">
							<span style="display:none; '.($this->getPSV() == 1.6 ? 'width: 40px; float: left;' : '').'" class="fedex_insurance fedex_insurance_whole">'.$this->l('Min').': '.$currency->sign.'</span>
						</td>
						<td align="left">
							<span style="display:none;" class="fedex_insurance fedex_insurance_whole"><input type="text" name="fedex_minimum_insurance_amount" style="width:30px;" id="fedex_minimum_insurance_amount" value="" /></span>
						</td>
					</tr>
					<tr height="31">
						<td align="left">
							'.$this->l('Zone').':&nbsp;
						</td>
						<td align="left">
							<select id="fedex_zone" name="fedex_zone" style="'.($this->getPSV() < 1.6 ? 'width:200px;padding: 1px;' : 'width:190px;').'margin: 0 10px 0 0;float: left;">';
							foreach($zones AS $zone) {
								$this->_html .= '<option value="' . $zone['id_zone'] . '">' . $zone['name'] . '</option>';
							}
							$this->_html .= '
							</select>
							
							<span class="info_tooltip" title="
								'.$this->l('Not all shipping methods are available for all delivery addresses; if an option is not offered by a carrier for a particular address, it will be automatically disabled.').'
							"></span>  
						</td>
					</tr>
					<tr height="31">
						<td align="center" colspan="4">
							<input style="'.($this->getPSV() == 1.6 ? 'margin: 20px 0;' : 'margin-bottom: 20px;').'" onclick="if ($(\'#fedex_carrier_name\').val() == \'\') { alert(\''.$this->l('You must enter a name for the shipping method').'\');return false;};'.($verified != 1?'alert(\''.$this->l('Your account information is incorrect, please make sure you enter the correct information before adding new shipping options').'\');return false;':'').'" type="submit" value="'.$this->l('Add New Shipping Option (Carrier)').'" name="addMethod" class="'.($this->getPSV() == 1.6 ? 'btn btn-default' : 'button').'" />
						</td>
					</tr>
					</table>';
					if (is_array($carriers) && sizeof($carriers) > 0)
					{
						$this->_html .= '
							<table cellspacing="0" cellpadding="0" border="0" class="table widthfull">
							<tr height="31">
								<th>'.$this->l('Name').'</th>
								<th>'.$this->l('Type').'</th>
								<th>'.$this->l('Zone').'</th>
								<th>
									'.$this->l('FS').'
									
									<span class="info_tooltip" title="
										'.$this->l('You can make this shipping method free based on your catalog, any products that are added will not be included in the shipping calculation. Enter the Product ID (\'P\'), Category ID (\'C\'), Manufacturer ID (\'M\') and/or Supplier ID (\'S\') you wish to apply free shipping to. Separate multiple entires with a comma (no spaces).').'
									"></span>  
								</th>
								<th>
									'.$this->l('FS Product').'    
									
									<span class="info_tooltip" title="
										'.$this->l('You can make this shipping method free based on your catalog, any products that are added will not be included in the shipping calculation. Enter the Product ID (\'P\'), Category ID (\'C\'), Manufacturer ID (\'M\') and/or Supplier ID (\'S\') you wish to apply free shipping to. Separate multiple entires with a comma (no spaces).').'
									"></span>  
								</th>
								<th>
									'.$this->l('Extra / Insurance').'
									
									<span class="info_tooltip" title="
										'.$this->l('Use \'Add\' to apply a shipping price markup, and \'Insure\' to add insurance, to orders using this method.').'
									"></span>  
								</th>
							</tr> ';
						$irow = 0;
						foreach ($carriers as $carrier)
						{
							$this->_html .= '<tr '.($irow++ % 2 ? 'class="alt_row"' : '').' height="31">
									<td align="left">
										'.$carrier['name'].'
										<input type="hidden" name="id_'.$irow.'" value="'.$carrier['id_carrier'].'" />
									</td>
									<td align="left"><small>'.(isset($ship_meth[$carrier['method']]) ? $ship_meth[$carrier['method']] : $this->l('Invalid')).'</small></td>
									<td align="left">';
							foreach($zones AS $zone)
								if ($zone['id_zone'] == $carrier['id_zone'])
									$this->_html .= $zone['name'];
							$this->_html .= '</td>
									<td align="left"  width="'.($this->getPSV() == 1.6 ? '70' : '60').'">
										'.($this->getPSV() == 1.6 ? '<span style="float: left; margin: 3px 3px 0 0;">' : '').
											$currency->sign.
										($this->getPSV() == 1.6 ? '</span>' : '').'
										<input type="text" name="fedex_free_shipping_'.$irow.'" style="'.($this->getPSV() == 1.6 ? 'float: left; width:40px; padding: 0 5px;' : 'width:30px;').'" id="fedex_free_shipping_'.$irow.'" value="'.$carrier['free_shipping'].'" />
									</td>
									<td align="left" width="130">
										'.($this->getPSV() == 1.6 ? '<span style="float: left; width: 15px; margin: 3px 0 0 0;">' : '').$this->l('P').($this->getPSV() == 1.6 ? '</span>' : '').' 
										<input type="text" name="fedex_free_shipping_product_'.$irow.'" style="'.($this->getPSV() == 1.6 ? 'width:90px; padding: 0 5px; margin: 0 0 5px 0;' : 'width:80px; margin: 5px 0;').'" id="fedex_free_shipping_product_'.$irow.'" value="'.$carrier['free_shipping_product'].'" />
										'.($this->getPSV() != 1.6 ? '<br />' : '').'
										'.($this->getPSV() == 1.6 ? '<span style="float: left; width: 15px; margin: 3px 0 0 0;">' : '').$this->l('C').($this->getPSV() == 1.6 ? '</span>' : '').' 
										<input type="text" name="fedex_free_shipping_category_'.$irow.'" style="'.($this->getPSV() == 1.6 ? 'width:90px; padding: 0 5px; margin: 0 0 5px 0;' : 'width:80px; margin: 5px 0;').'" id="fedex_free_shipping_category_'.$irow.'" value="'.$carrier['free_shipping_category'].'" />
										'.($this->getPSV() != 1.6 ? '<br />' : '').'
										'.($this->getPSV() == 1.6 ? '<span style="float: left; width: 15px; margin: 3px 0 0 0;">' : '').$this->l('M').($this->getPSV() == 1.6 ? '</span>' : '').' 
										<input type="text" name="fedex_free_shipping_manufacturer_'.$irow.'" style="'.($this->getPSV() == 1.6 ? 'width:90px; padding: 0 5px; margin: 0 0 5px 0;' : 'width:80px; margin: 5px 0;').'" id="fedex_free_shipping_manufacturer_'.$irow.'" value="'.$carrier['free_shipping_manufacturer'].'" />
										'.($this->getPSV() != 1.6 ? '<br />' : '').'
										'.($this->getPSV() == 1.6 ? '<span style="float: left; width: 15px; margin: 3px 0 0 0;">' : '').$this->l('S').($this->getPSV() == 1.6 ? '</span>' : '').' 
										<input type="text" name="fedex_free_shipping_supplier_'.$irow.'" style="'.($this->getPSV() == 1.6 ? 'width:90px; padding: 0 5px; margin: 0 0 5px 0;' : 'width:80px; margin: 5px 0;').'" id="fedex_free_shipping_supplier_'.$irow.'" value="'.$carrier['free_shipping_supplier'].'" />
									</td>
									<td align="left">
										<table width="100%">
										<tr height="31">
											<td align="left">
												'.$this->l('Add').':
											</td>
											<td align="left" colspan="4">
												<select name="fedex_extra_charge_'.$irow.'" id="fedex_extra_charge_'.$irow.'" style="width: 130px;'.($this->getPSV() == 1.6 ? 'float: left;margin: 0 3px 0 0;' : '').'" onchange="if ($(\'#fedex_extra_charge_'.$irow.'\').val() == \'0\') { $(\'#fedex_extra_charge_container_'.$irow.'\').fadeOut(1200); $(\'#fedex_extra_sign_'.$irow.'\').html(\'\');} else if ($(\'#fedex_extra_charge_'.$irow.'\').val() != \'2\'){$(\'#fedex_extra_charge_container_'.$irow.'\').fadeIn(1200);$(\'#fedex_extra_sign_'.$irow.'\').html(\'%\');}  else {$(\'#fedex_extra_charge_container_'.$irow.'\').fadeIn(1200);$(\'#fedex_extra_sign_'.$irow.'\').html(\''.$currency->sign.'\');}">
													<option value="0" '.($carrier['extra_shipping_type'] == 0?'selected':'').'>'.$this->l('None').'</option>
													<option value="1" '.($carrier['extra_shipping_type'] == 1?'selected':'').'>'.$this->l('% of Order Total').'</option>
													<option value="2" '.($carrier['extra_shipping_type'] == 2?'selected':'').'>'.$this->l('Fixed Amount').'</option>
													<option value="3" '.($carrier['extra_shipping_type'] == 3?'selected':'').'>'.$this->l('% of Shipping Total').'</option>
												</select>
												
												<span id="fedex_extra_charge_container_'.$irow.'" style="display: '.($carrier['extra_shipping_type'] == 0?'none':'').'">&nbsp;
													<span id="fedex_extra_sign_'.$irow.'" style="'.($this->getPSV() == 1.6 ? 'float: left; margin: 3px 3px 0 0;' : '').'">
														'.($carrier['extra_shipping_type'] != 2?'%':$currency->sign).'
													</span>
													<input type="text" name="fedex_extra_amount_'.$irow.'" style="padding: 0 5px;'.($this->getPSV() == 1.6 ? 'float: left;width:45px;' : 'width:30px;').'" id="fedex_extra_amount_'.$irow.'" value="'.$carrier['extra_shipping_amount'].'" />
												</span>
											</td>
										</tr>
										<tr height="31">
											<td align="left">
												'.$this->l('Insure').':
											</td>
											<td align="left">
												<select name="fedex_insurance_charge_'.$irow.'" style="width: 130px;'.($this->getPSV() == 1.6 ? 'float: left;' : '').'" id="fedex_insurance_charge_'.$irow.'" onchange="if ($(\'#fedex_insurance_charge_'.$irow.'\').val() == \'0\') { $(\'.fedex_insurance_'.$irow.'\').fadeOut(1200); } else if ($(\'#fedex_insurance_charge_'.$irow.'\').val() == \'1\') { $(\'.fedex_insurance_'.$irow.'\').fadeOut(100);$(\'.fedex_insurance_whole_'.$irow.'\').fadeIn(1200); } else { $(\'.fedex_insurance_'.$irow.'\').fadeIn(1200);}">
													<option value="0" '.($carrier['insurance_type'] == 0?'selected':'').'>'.$this->l('None').'</option>
													<option value="1" '.($carrier['insurance_type'] == 1?'selected':'').'>'.$this->l('Wholesale Price').'</option>
													<option value="2" '.($carrier['insurance_type'] == 2?'selected':'').'>'.$this->l('% of Retail Price').'</option>
												</select>
											</td>
											<td align="left">
												<span style="'.($this->getPSV() == 1.6 ? 'margin: 4px 5px 0 0; float: left;' : 'vertical-align:middle;').'display:'.($carrier['insurance_type'] != 2?'none':'block').';" class="fedex_insurance_'.$irow.'">
													%
												</span> 
												<input type="text" class="fedex_insurance_'.$irow.'" name="fedex_insurance_amount_'.$irow.'" style="padding: 0 5px;'.($this->getPSV() == 1.6 ? 'float: left;width:45px;' : 'width:30px;').' display:'.($carrier['insurance_type'] != 2?'none':'block').';" id="fedex_insurance_amount_'.$irow.'" value="'.($carrier['insurance_amount']).'" />
											</td>
											<td align="left">
												<span class="fedex_insurance_'.$irow.' fedex_insurance_whole_'.$irow.'" style="'.($carrier['insurance_type'] == 0?'display:none':'').'">'.$this->l('Min').': '.$currency->sign.'</span>
											</td>
											<td align="left">
												<input type="text" class="fedex_insurance_'.$irow.' fedex_insurance_whole_'.$irow.'" name="fedex_minimum_insurance_amount_'.$irow.'" style="width:30px;display:'.($carrier['insurance_type'] == 0?'none':'block').';" id="fedex_minimum_insurance_amount_'.$irow.'" value="'.$carrier['insurance_minimum'].'" />
											</td>
										</tr>
										</table>
									</td>
								</tr>';
						}
						$this->_html .= '
								<tr height="31">
									<td align="center" colspan="15">
										<input type="submit" value="'.$this->l('Update').'" name="updateShipping" class="'.($this->getPSV() == 1.6 ? 'btn btn-default' : 'button').'" style="margin: 10px 0;" />
									</td>
								</tr>
							</table>';
						}
					$this->_html .= '
				'.($ps_version < 1.6 ? '</fieldset>': '</div>').'
			'.($ps_version < 1.6 ? '</fieldset>': '</div>');

			/** LABEL PRINTING */
			$countries = Country::getCountries($this->context->language->id, true);
			$states = array();
			$country = Country::getByIso($this->_fedex_shipper_country);
			$states = isset($countries[$country]['states'])?$countries[$country]['states']:array();
			$order_statuses = OrderState::getOrderStates($this->context->language->id);

			if(!$this->_fedex_shipper_shop_name || 
			   !$this->_fedex_shipper_attention_name || 
			   !$this->_fedex_shipper_phone || 
			   !$this->_fedex_shipper_addr1 || 
			   !$this->_fedex_shipper_city || 
			   !$this->_fedex_shipper_country || 
			   (sizeof($states) > 0 && !$this->_fedex_shipper_state) || 
			   !$this->_fedex_shipper_postcode
			){ 
				$label_validated = false; 
			}
			else
				$label_validated = true;
				
			$this->_html .= '
			<br/>
			
		
		'.($ps_version >= 1.6?'<div class="panel">':
		'<fieldset class="inner_fieldset" style="width:900px;">').
			($ps_version < 1.6 ? '<legend>': '<h3>').'
				<img src="'.$this->_path.'logo.gif" style="'.($this->getPSV() == 1.6 ? 'margin: 0 10px 0 0;' : '').'" /> '.$this->l('Label Printing').'
			'.($ps_version < 1.6 ? '</legend>': '</h3>').'
			
			'.($ps_version >= 1.6?'<div class="panel">':
			'<fieldset class="inner_fieldset" style="width:900px;">').
				($ps_version < 1.6 ? '<legend class="'.($label_validated ? 'validated' : 'not_validated').'">': '<h3 class="'.($label_validated ? 'validated' : 'not_validated').'">').'
					<img src="'.$this->_path.'logo.gif" style="'.($this->getPSV() == 1.6 ? 'margin: 0 10px 0 0;' : '').'" /> '.$this->l('Step 6: Enable Label Printing in your FedEx Account').'
				'.($ps_version < 1.6 ? '</legend>': '</h3>').'
				
					<p><b>'.$this->l('FedEx requires you print and provide test labels for their approval before you can start printing labels.').'</b></p>
					<p><b>'.$this->l('Follow the steps below to complete the process.').'</b></p>
					<p>'.$this->l('1) Contact FedEx and request to activate label printing for your account. Email (websupport@fedex.com) or call (1-800-GO-FEDEX in the US; International:').' <a href="http://www.fedex.com/us/customersupport/call/" target="_index">http://www.fedex.com/us/customersupport/call/</a>)
					<p><b>'.$this->l('Wait for an email containing the Label Evaluation Process, which includes instructions on how to proceed.').'</b></p>
					<br />'.$this->l('2) Read the Label Evaluation Process email from FedEx carefully!').'
					<br />'.$this->l('3) Complete the \'WIS Label Cover Sheet\' attached to the email from FedEx.').'
					<p><b>'.$this->l('* To obtain Production credentials, use:').'</b> <a href="https://www.fedex.com/wpor/web/jsp/drclinks.jsp?links=wss/production.html" target="_index">https://www.fedex.com/wpor/web/jsp/drclinks.jsp?links=wss/production.html</a>
					<br /><b>'.$this->l('* To obtain Test credentials, use:').'</b> <a href="https://www.fedex.com/wpor/web/jsp/drclinks.jsp?links=wss/develop.html" target="_index">https://www.fedex.com/wpor/web/jsp/drclinks.jsp?links=wss/develop.html</a></p>
					<p><b>'.$this->l('How to print a test label for each shipping method you\'ll use from your Prestashop store').'</b></p>
					<p>'.$this->l('4) Create a new test order on your site using your current (active) FedEx account.').'
					<br />'.$this->l('5) Replace your current FedEx account info with your Test account info. (top of this page)').'
					<p style="color:red;">'.$this->l('Note: Once you enter the test account info you will see an "Incorrect Account Settings" alert which you can safely ignore.').'</p>
					<p>'.$this->l('6) Complete the form below with your shipping info.').'
					<br />'.$this->l('7) Go to the new order you created (in step 4) and generate one label using each shipping method you would use. Print each label.').'</p>
					<p><b>'.$this->l('Separate labels into two categories: 1) FedEx Express and 2) FedEx Ground & Home Delivery').'</b></p>
					<p>'.$this->l('8) According to FedEx\'s email, scan each Express label at 600dpi and send with the completed \'WIS Label Cover Sheet\' to label@corp.ds.fedex.com.').'
					<br />'.$this->l('9) For Ground & Home Delivery methods, scan each label at 600dpi and send, with the completed \'WIS Label Cover Sheet\' to groundlabel@fedex.com.').'</p>
					<p><b>'.$this->l('You\'re done! (Don\'t forget to re-enter your Live account settings above when finished testing!) If your labels are approved, FedEx will contact you within three business days that you can begin printing labels. If they are not approved, FedEx will contact you with info on how to fix the issues.').'</b></p>
					<br />
					<br />
					
					<hr style="width: 100%;height: 1px;background-color: #CCCED7;margin: 5px 0; float: left;" />
					
					<div style="margin: 5px 0; float:left; width: 100%;">
						<label for="fedex_enable_labels_fedex" style="width:110px;font-weight:normal;text-align:left; float:left;">'.$this->l('Enable Labels').': <sup>*</sup></label>
						<label style="float: none; width: 100%"><input type="radio" name="fedex_enable_labels" id="fedex_enable_labels_fedex" value="fedex" '.($this->_fedex_enable_labels == 'fedex' ? 'checked="checked"' : '').' /> '.$this->l('Only for orders placed using Fedex').'</label>
						<br />
						<label style="float: none; width: 100%"><input type="radio" name="fedex_enable_labels" id="fedex_enable_labels_all" value="all" '.($this->_fedex_enable_labels == 'all' ? 'checked="checked"' : '').' /> '.$this->l('For all orders').'</label>
					</div>
					
					<div style="margin: 5px 0; float:left; width: 100%;">
						<label for="label_order_status" style="width:110px;font-weight:normal;text-align:left; float:left;">'.$this->l('Order Status:').'</label>
						<select name="label_order_status" style="'.($this->getPSV() < 1.6 ? 'width:200px;padding: 1px;' : 'width:190px;').'float: left;margin: 0 10px 0 0;">
							<option value="0">'.$this->l('Do not change order status').'</option>
			'; 
						if(is_array($order_statuses) && count($order_statuses))
						{
							foreach ($order_statuses as $status)
							{
								$this->_html .= '<option value="'.$status['id_order_state'].'" '.($this->_fedex_label_order_status == $status['id_order_state']?"selected":"").'>'.$status['name'].'</option>';
							}
						}
						
			$this->_html .= '
						</select> ('.$this->l('After printing a label, order status will be changed to your selection').')
					</div>
					
					<hr style="width: 100%;height: 1px;background-color: #CCCED7;margin: 5px 0; float: left;" />
					
					<p><b>'.$this->l('Default shipper information').'</b> '.$this->l('(Can be changed on each order)').'</p>
					
					<div style="margin: 5px 0; float:left; width: 100%;">
						<label for="shipper_shop_name" style="width:110px;font-weight:normal;text-align:left; float:left;">'.$this->l('Shop Name').': <sup>*</sup></label>
						<input type="text" name="shipper_shop_name" id="shipper_shop_name" value="'.$this->_fedex_shipper_shop_name.'" style="width:190px;float: left;margin: 0 10px 0 0;">
					</div>

					<div style="margin: 5px 0; float:left; width: 100%;">
						<label for="shipper_attention_name" style="width:110px;font-weight:normal;text-align:left; float:left;">'.$this->l('Your Name').': <sup>*</sup></label>
						<input type="text" name="shipper_attention_name" id="shipper_attention_name" value="'.$this->_fedex_shipper_attention_name.'" style="width:190px;float: left;margin: 0 10px 0 0;">
					</div>

					<div style="margin: 5px 0; float:left; width: 100%;">
						<label for="shipper_phone" style="width:110px;font-weight:normal;text-align:left; float:left;">'.$this->l('Phone Number').': <sup>*</sup></label>
						<input type="text" name="shipper_phone" id="shipper_phone" value="'.$this->_fedex_shipper_phone.'" style="width:190px;float: left;margin: 0 10px 0 0;">
					</div>

					<div style="margin: 5px 0; float:left; width: 100%;">
						<label for="shipper_address1" style="width:110px;font-weight:normal;text-align:left; float:left;">'.$this->l('Address Line 1').': <sup>*</sup></label>
						<input type="text" name="shipper_address1" id="shipper_address1" value="'.$this->_fedex_shipper_addr1.'" style="width:190px;float: left;margin: 0 10px 0 0;">
					</div>

					<div style="margin: 5px 0; float:left; width: 100%;">
						<label for="shipper_address2" style="width:110px;font-weight:normal;text-align:left; float:left;">'.$this->l('Address Line 2').':</label>
						<input type="text" name="shipper_address2" id="shipper_address2" value="'.$this->_fedex_shipper_addr2.'" style="width:190px;float: left;margin: 0 10px 0 0;">
					</div>

					<div style="margin: 5px 0; float:left; width: 100%;">
						<label for="shipper_city" style="width:110px;font-weight:normal;text-align:left; float:left;">'.$this->l('Shop City').': <sup>*</sup></label>
						<input type="text" name="shipper_city" id="shipper_city" value="'.$this->_fedex_shipper_city.'" style="width:190px;float: left;margin: 0 10px 0 0;">
					</div>

					<div style="margin: 5px 0; float:left; width: 100%;">
						<label style="width:110px;font-weight:normal;text-align:left; float:left;">'.$this->l('Shop Country').': <sup>*</sup></label>
						<select name="shipper_country" style="'.($this->getPSV() < 1.6 ? 'width:200px;padding: 1px;' : 'width:190px;').'float: left;margin: 0 10px 0 0;">
							<option value="0">-- '.$this->l('Select Country').' --</option>    
			';
						if(is_array($countries) && count($countries))
						{
							foreach ($countries as $country)
							{
								$this->_html .= '<option value="'.$country['iso_code'].'" '.($this->_fedex_shipper_country == $country['iso_code']?"selected":"").'>'.$country['name'].'</option>';
							}
						}
						
			$this->_html .= '
						</select>
					</div>

					<div style="margin: 5px 0; float:left; width: 100%;">
						<label style="width:110px;font-weight:normal;text-align:left; float:left;">'.$this->l('Shop State').': '.(is_array($states) && count($states) ? '<sup>*</sup>' : '').'</label>
						<select name="shipper_state" style="'.($this->getPSV() < 1.6 ? 'width:200px;padding: 1px;' : 'width:190px;').'float: left;margin: 0 10px 0 0;">
							<option value="0">-- '.$this->l('Select State').' --</option>
			';
						if(is_array($states) && count($states))
						{
							foreach ($states as $state)
							{
								$this->_html .= '<option value="'.$state['iso_code'].'" '.($this->_fedex_shipper_state == $state['iso_code']?"selected":"").'>'.$state['name'].'</option>';
							}
						}
						
			$this->_html .= '
						</select>
					</div>

					<div style="margin: 5px 0; float:left; width: 100%;">
						<label for="shipper_postcode" style="width:110px;font-weight:normal;text-align:left; float:left;">'.$this->l('Shop Zipcode').': <sup>*</sup></label>
						<input type="text" name="shipper_postcode" id="shipper_postcode" value="'.$this->_fedex_shipper_postcode.'" style="width:190px;float: left;margin: 0 10px 0 0;">
					</div> 
		
					<hr style="width: 100%;height: 1px;background-color: #CCCED7;margin: 5px 0; float: left;" />
					
					<p><b>'.$this->l('Global shipment label settings').'</b> '.$this->l('(Can be changed on each order)').'</p>
					
					<div style="margin: 5px 0; float:left; width: 100%;">
						<label for="label_margin" style="width:110px;font-weight:normal;text-align:left; float:left;">'.$this->l('Label Margin:').'</label>
						<input type="text" name="label_margin" id="label_margin" size="3" value="'.$this->_fedex_label_margin.'" style="width:60px;float: left;margin: 0 10px 0 0;"> px
					</div>

					<div style="margin: 5px 0; float:left; width: 100%;">
						<label for="label_format" style="width:110px;font-weight:normal;text-align:left; float:left;">'.$this->l('Label Format:').'</label>
						<select name="label_format" style="'.($this->getPSV() < 1.6 ? 'width:200px;padding: 1px;' : 'width:190px;').'float: left;margin: 0 10px 0 0;">
							<option value="PNG" '.($this->_fedex_label_format == 'PNG' ? 'selected' : '').'>PNG</option>
							<option value="PDF" '.($this->_fedex_label_format == 'PDF' ? 'selected' : '').'>PDF</option>
							<option value="DPL" '.($this->_fedex_label_format == 'DPL' ? 'selected' : '').'>DPL</option>
							<option value="EPL2" '.($this->_fedex_label_format == 'EPL2' ? 'selected' : '').'>EPL2</option>
							<option value="ZPLII" '.($this->_fedex_label_format == 'ZPLII' ? 'selected' : '').'>ZPLII</option>
						</select>
					</div>

					<div style="margin: 5px 0; float:left; width: 100%;">
						<label for="label_stock_type" style="width:110px;font-weight:normal;text-align:left; float:left;">'.$this->l('Label Stock Type:').'</label>
						<select name="label_stock_type" style="'.($this->getPSV() < 1.6 ? 'width:200px;padding: 1px;' : 'width:190px;').'float: left;margin: 0 10px 0 0;">
							<optgroup label="'.$this->l('For PNG and PDF').'">
								<option value="PAPER_4X6" '.($this->_fedex_label_stock_type == 'PAPER_4X6' ? 'selected' : '').'>PAPER_4X6</option>
								<option value="PAPER_4X8" '.($this->_fedex_label_stock_type == 'PAPER_4X8' ? 'selected' : '').'>PAPER_4X8</option>
								<option value="PAPER_4X9" '.($this->_fedex_label_stock_type == 'PAPER_4X9' ? 'selected' : '').'>PAPER_4X9</option>
								<option value="PAPER_7X4.75" '.($this->_fedex_label_stock_type == 'PAPER_7X4.75' ? 'selected' : '').'>PAPER_7X4.75</option>
								<option value="PAPER_8.5X11_BOTTOM_HALF_LABEL" '.($this->_fedex_label_stock_type == 'PAPER_8.5X11_BOTTOM_HALF_LABEL' ? 'selected' : '').'>PAPER_8.5X11_BOTTOM_HALF_LABEL</option>
								<option value="PAPER_8.5X11_TOP_HALF_LABEL" '.($this->_fedex_label_stock_type == 'PAPER_8.5X11_TOP_HALF_LABEL' ? 'selected' : '').'>PAPER_8.5X11_TOP_HALF_LABEL</option>
								<option value="PAPER_LETTER" '.($this->_fedex_label_stock_type == 'PAPER_LETTER' ? 'selected' : '').'>PAPER_LETTER</option>
							</optgroup>
							<optgroup label="'.$this->l('For Thermal Labels').'">
								<option value="STOCK_4X6" '.($this->_fedex_label_stock_type == 'STOCK_4X6' ? 'selected' : '').'>STOCK_4X6</option>
								<option value="STOCK_4X6.75_LEADING_DOC_TAB" '.($this->_fedex_label_stock_type == 'STOCK_4X6.75_LEADING_DOC_TAB' ? 'selected' : '').'>STOCK_4X6.75_LEADING_DOC_TAB</option>
								<option value="STOCK_4X6.75_TRAILING_DOC_TAB" '.($this->_fedex_label_stock_type == 'STOCK_4X6.75_TRAILING_DOC_TAB' ? 'selected' : '').'>STOCK_4X6.75_TRAILING_DOC_TAB</option>
								<option value="STOCK_4X8" '.($this->_fedex_label_stock_type == 'STOCK_4X8' ? 'selected' : '').'>STOCK_4X8</option>
								<option value="STOCK_4X9_LEADING_DOC_TAB" '.($this->_fedex_label_stock_type == 'STOCK_4X9_LEADING_DOC_TAB' ? 'selected' : '').'>STOCK_4X9_LEADING_DOC_TAB</option>
								<option value="STOCK_4X9_TRAILING_DOC_TAB" '.($this->_fedex_label_stock_type == 'STOCK_4X9_TRAILING_DOC_TAB' ? 'selected' : '').'>STOCK_4X9_TRAILING_DOC_TAB</option>
							</optgroup>
						</select>
					</div>

					<div style="margin: 10px 0;text-align: center;width: 100%;">
						<input type="submit" value="'.$this->l('Update').'" name="updateShipperInfo" class="'.($this->getPSV() == 1.6 ? 'btn btn-default' : 'button').'" />
					</div>
				'.($ps_version < 1.6 ? '</fieldset>': '</div>').'
			'.($ps_version < 1.6 ? '</fieldset>': '</div>').'
		</form>'.($ps_version >= 1.5?'</div>':'');
	}

	protected function _postProcess()
	{
		$languages = Language::getLanguages();
		$result = Db::getInstance()->ExecuteS('SHOW TABLES');
		$existing_tables = array();
		foreach ($result AS $row)
			foreach ($row AS $key => $table)
				array_push($existing_tables, $table);

		//hide or display installation instructions
		if (Tools::getValue('fedex_shi') != "")
		{
			if (Tools::getValue('fedex_shi') == "inline")
				Configuration::updateValue('FEDEX_INSTALL',"none");
			else
				Configuration::updateValue('FEDEX_INSTALL',"inline");
		}

		if (Tools::isSubmit('updateSettings'))
		{
			if (Tools::getValue('fedex_pack') != $this->_fedex_pack)
			{
				$query = 'TRUNCATE `'._DB_PREFIX_.'fe_fedex_invalid_dest`';
				Db::getInstance()->Execute($query);
			}
			if (Tools::getValue('fedex_origin_resident') != $this->_fedex_origin_resident || Tools::getValue('fedex_destin_resident') != $this->_fedex_destin_resident)
			{
				$query = 'TRUNCATE `'._DB_PREFIX_.'fe_fedex_rate_cache`';
				Db::getInstance()->Execute($query);
				$query = 'TRUNCATE `'._DB_PREFIX_.'fe_fedex_package_rate_cache`';
				Db::getInstance()->Execute($query);
				$query = 'TRUNCATE `'._DB_PREFIX_.'fe_fedex_invalid_dest`';
				Db::getInstance()->Execute($query);
			}
			if(!Configuration::updateValue('FEDEX_KEY', Tools::getValue('fedex_key'))
				|| !Configuration::updateValue('FEDEX_PASS', Tools::getValue('fedex_pass'))
				|| !Configuration::updateValue('FEDEX_ACCOUNT', Tools::getValue('fedex_account'))
				|| !Configuration::updateValue('FEDEX_METER', Tools::getValue('fedex_meter'))
				|| !Configuration::updateValue('FEDEX_DROPOFF', Tools::getValue('fedex_dropoff'))
				|| !Configuration::updateValue('FEDEX_PACK', Tools::getValue('fedex_pack'))
				|| !Configuration::updateValue('FEDEX_WIDTH', Tools::getValue('fedex_width'))
				|| !Configuration::updateValue('FEDEX_HEIGHT', Tools::getValue('fedex_height'))
				|| !Configuration::updateValue('FEDEX_DEPTH', Tools::getValue('fedex_depth'))
				|| !Configuration::updateValue('FEDEX_WEIGHT', Tools::getValue('fedex_weight'))
				|| !Configuration::updateValue('FEDEX_ORIGIN_RESIDENT', Tools::getValue('fedex_origin_resident'))
				|| !Configuration::updateValue('FEDEX_DESTIN_RESIDENT', Tools::getValue('fedex_destin_resident'))
				|| !Configuration::updateValue('FEDEX_ORIGIN_ZIP', Tools::getValue('fedex_origin_zip'))
				|| !Configuration::updateValue('FEDEX_ORIGIN_CITY', Tools::getValue('fedex_origin_city'))
				|| !Configuration::updateValue('FEDEX_ORIGIN_COUNTRY', Tools::getValue('fedex_origin_country'))
				|| !Configuration::updateValue('FEDEX_TYPE', Tools::getValue('fedex_type'))
				|| !Configuration::updateValue('FEDEX_PACKAGES', Tools::getValue('fedex_packages'))
				|| !Configuration::updateValue('FEDEX_PACKAGE_SIZE', Tools::getValue('fedex_package_size'))
				|| !Configuration::updateValue('FEDEX_PACKAGES_PER_BOX', Tools::getValue('fedex_packages_per_box'))
				|| !Configuration::updateValue('FEDEX_OVERRIDE_ADDRESS', Tools::getValue('fedex_override_address'))
				|| !Configuration::updateValue('FEDEX_DEBUG_MODE', Tools::getValue('fedex_debug_mode'))
				|| !Configuration::updateValue('FEDEX_XML_LOG', Tools::getValue('fedex_xml_log'))
				|| !Configuration::updateValue('FEDEX_ADDRESS_DISPLAY', serialize(array('country' => Tools::getValue('fedex_country_display'), 'state' => Tools::getValue('fedex_state_display'), 'city' => Tools::getValue('fedex_city_display'), 'zip' => Tools::getValue('fedex_zip_display'))))
				|| !Configuration::updateValue('FEDEX_MODE', Tools::getValue('fedex_mode'))
				|| !Configuration::updateValue('FEDEX_UNIT', Tools::getValue('fedex_unit'))
			){
				if($this->getPSV() == 1.6)
					$this->_html .= $this->displayError($this->l('Cannot update settings'));
				else
					$this->_html .= '<div class="alert error">'.$this->l('Cannot update settings').'</div>';
			}
			else
			{
				if($this->getPSV() == 1.6)
					$this->_html .= $this->displayConfirmation($this->l('Settings updated'));
				else
					$this->_html .= '<div class="conf confirm"><img src="../img/admin/ok.gif" alt="'.$this->l('Confirmation').'" />'.$this->l('Settings updated').'</div>';
			}
		}

		if (Tools::getValue('ssr') == 'show') //show shipping rates button
		{
			$this->registerHook($this->getPSV() >= 1.5 ? 'displayProductButtons' : 'productActions');
			$this->registerHook('cartShippingPreview');
			Configuration::updateValue('FEDEX_SHIPPING_RATES_BUTTON', 'show');
		}
		elseif(Tools::getValue('ssr') == 'hide') //hide shipping rates button
		{
			$this->unregisterHook(($this->getPSV() >= 1.5 ? Hook::getIdByName('displayProductButtons') : Hook::get('productActions')));
			$this->unregisterHook(($this->getPSV() >= 1.5 ? Hook::getIdByName('cartShippingPreview') : Hook::get('cartShippingPreview')));
			Configuration::updateValue('FEDEX_SHIPPING_RATES_BUTTON', 'hide');
		}

		if (Tools::isSubmit('addMethod'))
		{
			$query = 'INSERT INTO `'._DB_PREFIX_.'carrier` (name, url, active, is_module, shipping_external, need_range, shipping_method, external_module_name'.($this->getPSV() >= 1.5?',id_reference':'').') VALUES("'.(Tools::getValue('fedex_carrier_name') != ""?Tools::getValue('fedex_carrier_name'):" ").'", "http://www.fedex.com/Tracking?tracknumber_list=@",  "1","1","1","1","1","fedex"'.($this->getPSV() >= 1.5?',1':'').')';
			Db::getInstance()->Execute($query);
			$id_carrier = Db::getInstance()->Insert_ID();
			if ($this->getPSV() >= 1.5)
				Db::getInstance()->execute('UPDATE '._DB_PREFIX_.'carrier set id_reference = '.$id_carrier.' WHERE id_carrier = '.$id_carrier);
			if (in_array(_DB_PREFIX_."carrier_group",$existing_tables))
			{
				$id_groups = Db::getInstance()->executeS('SELECT id_group FROM '._DB_PREFIX_.'group GROUP BY id_group');
				foreach ($id_groups as $group)
				{
					$query  = 'INSERT INTO `'._DB_PREFIX_.'carrier_group` (id_carrier, id_group) VALUES("'.$id_carrier.'", "'.$group['id_group'].'")';
					Db::getInstance()->Execute($query);
				}
			}

            if($this->getPSV() >= 1.5)
            {
                /** $this->context->shop is not accurate */
                $id_shop_group = Shop::getContextShopGroupID(true);
                $id_shop = Shop::getContextShopID(true);
                
                /** if shop group is selected */
                if(!$id_shop && $id_shop_group)
                    $shops = Shop::getShops(false, $id_shop_group);
                /** if all shops is selected */
                elseif(!$id_shop && !$id_shop_group)
                    $shops = Shop::getShops(false);
                /** if a shop is selected */
                else
                {
                    $shops = array(
                        0 => array(
                            'id_shop' => $id_shop
                        )
                    );
                }

                if(is_array($shops) && count($shops))
                {
                    foreach($shops as $shop)
                    {
                        if (in_array(_DB_PREFIX_."carrier_tax_rules_group_shop",$existing_tables))
                        {
                            $query  = 'INSERT INTO `'._DB_PREFIX_.'carrier_tax_rules_group_shop` (id_carrier, id_tax_rules_group, id_shop) VALUES("'.$id_carrier.'", "0","'.$shop['id_shop'].'")';
                            Db::getInstance()->Execute($query);
                        }
                        
                        if (in_array(_DB_PREFIX_."carrier_shop",$existing_tables))
                        {
                            $query  = 'INSERT INTO `'._DB_PREFIX_.'carrier_shop` (id_carrier, id_shop) VALUES("'.$id_carrier.'", "'.$shop['id_shop'].'")';
                            Db::getInstance()->Execute($query);
                        }
                        
                        /** insert carrier language for the store */
                        foreach ($languages as $language)
                        {
                            $query  = 'INSERT INTO `'._DB_PREFIX_.'carrier_lang` (id_carrier, id_shop, id_lang, delay) VALUES("'.(int)$id_carrier.'", "'.$shop['id_shop'].'", "'.(int)$language['id_lang'].'", "'.(Tools::getValue('fedex_transit_time') != "" ? Tools::getValue('fedex_transit_time') : " ").'")';
                            Db::getInstance()->Execute($query);
                        }
                    }
                }
                /** FATAL ERROR? */
                else
                    die('fatal error: could not create carrier, reload the page and try again');
            }
            else
            {
                /** insert carrier language for the store */
                foreach ($languages as $language)
                {
                    $query  = 'INSERT INTO `'._DB_PREFIX_.'carrier_lang` (id_carrier, id_lang, delay) VALUES("'.(int)$id_carrier.'", "'.(int)$language['id_lang'].'", "'.(Tools::getValue('fedex_transit_time') != "" ? Tools::getValue('fedex_transit_time') : " ").'")';
                    Db::getInstance()->Execute($query);
                }
            }

			$query  = 'INSERT INTO `'._DB_PREFIX_.'fe_fedex_method` (id_carrier, method, free_shipping, extra_shipping_type, extra_shipping_amount, insurance_minimum, insurance_type, insurance_amount, free_shipping_product, free_shipping_category, free_shipping_manufacturer, free_shipping_supplier) VALUES
				("'.(int)$id_carrier.'","'.pSQL(Tools::getValue('fedex_method')).'","'.floatval(Tools::getValue('fedex_free_shipping')).'",
				"'.floatval(Tools::getValue('fedex_extra_charge')).'","'.floatval(Tools::getValue('fedex_extra_amount')).'",
				"'.floatval(Tools::getValue('fedex_minimum_insurance_amount')).'","'.floatval(Tools::getValue('fedex_insurance_charge')).'",
				"'.floatval(Tools::getValue('fedex_insurance_amount')).'","","","","")';
			Db::getInstance()->Execute($query);

			$fedex_img = "default.jpg";
			if (strstr(strtolower(Tools::getValue('fedex_method')), 'ground') !== false)
				$fedex_img = "ground.jpg";
			else if (strstr(strtolower(Tools::getValue('fedex_method')), 'priority') !== false || strstr(strtolower(Tools::getValue('fedex_method')), 'overnight') !== false || strstr(strtolower(Tools::getValue('fedex_method')), 'express') !== false)
				$fedex_img = "express.jpg";
			copy(dirname(__FILE__).'/img/'.$fedex_img, _PS_SHIP_IMG_DIR_.intval($id_carrier).'.jpg');
			copy(dirname(__FILE__).'/img/'.$fedex_img, _PS_TMP_IMG_DIR_.'carrier_mini_'.intval($id_carrier).'.jpg');

			$query = 'INSERT INTO `'._DB_PREFIX_.'carrier_zone` (id_carrier, id_zone) VALUES("'.$id_carrier.'", "'.Tools::getValue('fedex_zone').'")';
			Db::getInstance()->Execute($query);

			$query = 'INSERT INTO `'._DB_PREFIX_.'range_weight` (id_carrier, delimiter1, delimiter2) VALUES("'.$id_carrier.'", "0.00", "100000.00")';
			Db::getInstance()->Execute($query);
			$id_range_weight = Db::getInstance()->Insert_ID();

			$query = 'INSERT INTO `'._DB_PREFIX_.'delivery` (id_carrier, id_range_weight, id_zone, price) VALUES("'.$id_carrier.'", "'.$id_range_weight.'", "'.Tools::getValue('fedex_zone').'","0")';
			Db::getInstance()->Execute($query);
			
			if($this->getPSV() == 1.6)
				$this->_html .= $this->displayConfirmation($this->l('Carrier created successfully'));
			else
				$this->_html .= '<div class="conf confirm"><img src="../img/admin/ok.gif" alt="'.$this->l('Confirmation').'" />'.$this->l('Carrier created successfully').'</div>';

		}

		if (Tools::isSubmit('updateShipping') || Tools::isSubmit('deleteCache') || Tools::isSubmit('updateSettings'))
		{
			$query = 'TRUNCATE `'._DB_PREFIX_.'fe_fedex_rate_cache`';
			Db::getInstance()->Execute($query);
			$query = 'TRUNCATE `'._DB_PREFIX_.'fe_fedex_hash_cache`';
			Db::getInstance()->Execute($query);
			$query = 'TRUNCATE `'._DB_PREFIX_.'fe_fedex_package_rate_cache`';
			Db::getInstance()->Execute($query);
			$query = 'TRUNCATE `'._DB_PREFIX_.'fe_fedex_invalid_dest`';
			Db::getInstance()->Execute($query);

			//delete logs:
			$files = glob(dirname(__FILE__).'/logs/*'); // get all file names
			foreach($files as $file){ // iterate files
				if(is_file($file) AND !strstr($file, 'index.php'))
					unlink($file); // delete file
			}
			
			if($this->getPSV() == 1.6 && Tools::isSubmit('deleteCache'))
				$this->_html .= $this->displayConfirmation($this->l('Shipping cache deleted successfully'));
			elseif(Tools::isSubmit('deleteCache'))
				$this->_html .= '<div class="conf confirm"><img src="../img/admin/ok.gif" alt="'.$this->l('Confirmation').'" />'.$this->l('Shipping cache deleted successfully').'</div>';

		}

		if (Tools::isSubmit('updateShipping'))
		{
			$i = 1;
			while (isset($_POST['id_'.$i]) && $_POST['id_'.$i])
			{
				$query = 'UPDATE `'._DB_PREFIX_.'fe_fedex_method` SET free_shipping = "'.$_POST['fedex_free_shipping_'.$i].'",free_shipping_product = "'.$_POST['fedex_free_shipping_product_'.$i].'",
						free_shipping_category = "'.$_POST['fedex_free_shipping_category_'.$i].'",free_shipping_manufacturer = "'.$_POST['fedex_free_shipping_manufacturer_'.$i].'",
						free_shipping_supplier = "'.$_POST['fedex_free_shipping_supplier_'.$i].'",extra_shipping_type = "'.$_POST['fedex_extra_charge_'.$i].'", extra_shipping_amount = "'.$_POST['fedex_extra_amount_'.$i].'",
						insurance_minimum = "'.(float)$_POST['fedex_minimum_insurance_amount_'.$i].'", insurance_type = "'.$_POST['fedex_insurance_charge_'.$i].'",
						insurance_amount = "'.(float)$_POST['fedex_insurance_amount_'.$i].'" WHERE id_carrier = "'.(int)$_POST['id_'.$i].'"';
				Db::getInstance()->Execute($query);
				$i++;
			}
			
			if($this->getPSV() == 1.6)
				$this->_html .= $this->displayConfirmation($this->l('Settings updated'));
			else
				$this->_html .= '<div class="conf confirm"><img src="../img/admin/ok.gif" alt="'.$this->l('Confirmation').'" />'.$this->l('Settings updated').'</div>';

		}

		if(Tools::isSubmit('updateShipperInfo'))
		{
			Configuration::updateValue('FEDEX_SHIPPER_SHOP_NAME', Tools::getValue('shipper_shop_name'));
			Configuration::updateValue('FEDEX_SHIPPER_ATTENTION_NAME', Tools::getValue('shipper_attention_name'));
			Configuration::updateValue('FEDEX_SHIPPER_PHONE', Tools::getValue('shipper_phone'));
			Configuration::updateValue('FEDEX_SHIPPER_ADDR1', Tools::getValue('shipper_address1'));
			Configuration::updateValue('FEDEX_SHIPPER_ADDR2', Tools::getValue('shipper_address2'));
			Configuration::updateValue('FEDEX_SHIPPER_CITY', Tools::getValue('shipper_city'));
			Configuration::updateValue('FEDEX_SHIPPER_COUNTRY', Tools::getValue('shipper_country'));
			Configuration::updateValue('FEDEX_SHIPPER_STATE', Tools::getValue('shipper_state'));
			Configuration::updateValue('FEDEX_SHIPPER_POSTCODE', Tools::getValue('shipper_postcode'));
			
			Configuration::updateValue('FEDEX_ENABLE_LABELS', Tools::getValue('fedex_enable_labels'));
			Configuration::updateValue('FEDEX_LABEL_ORDER_STATUS', Tools::getValue('label_order_status')); 
			Configuration::updateValue('FEDEX_LABEL_MARGIN', Tools::getValue('label_margin'));
			Configuration::updateValue('FEDEX_LABEL_FORMAT', Tools::getValue('label_format'));
			Configuration::updateValue('FEDEX_LABEL_STOCK_TYPE', Tools::getValue('label_stock_type'));
			
			if($this->getPSV() == 1.6)
				$this->_html .= $this->displayConfirmation($this->l('Settings updated'));
			else
				$this->_html .= '<div class="conf confirm"><img src="../img/admin/ok.gif" alt="'.$this->l('Confirmation').'" />'.$this->l('Settings updated').'</div>';
		}

		$this->_refreshProperties();
	}

	protected function checkFedexSettings()
	{           
		$log = false;
		require_once(dirname(__FILE__).'/classes/fedex-common.php');
		if (!class_exists("SoapClient",false))
		{
			print '<b style="color:red">'.$this->l('SOAP is not installed on your server, contact your host and ask them to install it.').'</b>';
			exit;
		}

		$path_to_wsdl = ($this->_fedex_mode == 'live' ? _PS_MODULE_DIR_.'fedex/includes/RateService_v13.wsdl' : _PS_MODULE_DIR_.'fedex/includes/TestRateService_v13.wsdl');
		$key = $this->_fedex_key;
		$password = $this->_fedex_pass;
		$shipAccount = $this->_fedex_account;
		$meter = $this->_fedex_meter;
		$billAccount = $this->_fedex_account;
		ini_set("soap.wsdl_cache_enabled", "0");

		$client = new SoapClient($path_to_wsdl, array('trace' => 1)); // Refer to http://us3.php.net/manual/en/ref.soap.php for more information
		$request['WebAuthenticationDetail'] = array('UserCredential' =>	array('Key' => $key, 'Password' => $password));
		$request['ClientDetail'] = array('AccountNumber' => $shipAccount, 'MeterNumber' => $meter);
		$request['TransactionDetail'] = array('CustomerTransactionId' => ' *** Rate Available Services Request v13 using PHP ***');
		$request['Version'] = array('ServiceId' => 'crs', 'Major' => '13', 'Intermediate' => '0', 'Minor' => '0');
		$request['ReturnTransitAndCommit'] = true;
		$request['RequestedShipment']['DropoffType'] = $this->_fedex_dropoff; // valid values REGULAR_PICKUP, REQUEST_COURIER, ...
		$request['RequestedShipment']['ShipTimestamp'] = date('c');
		$request['RequestedShipment']['PackagingType'] = $this->_fedex_pack;
		$request['RequestedShipment']['Shipper'] = array(
			'Address' => array(
				'StreetLines' => array(''), // Origin details
				'City' => $this->_fedex_origin_city,
				'StateOrProvinceCode' => '',
				'PostalCode' => $this->_fedex_origin_zip,
				'CountryCode' => $this->_fedex_origin_country
			)
		);
		$request['RequestedShipment']['Recipient'] = array(
			'Address' => array (
				'StreetLines' => array(''), // Destination details
				'City' => $this->_fedex_origin_city,
				'Residential' => false,
				'PostalCode' => $this->_fedex_origin_zip,
				'CountryCode' => $this->_fedex_origin_country
			)
		);
		$request['RequestedShipment']['ShippingChargesPayment'] = array(
			'PaymentType' => 'SENDER',
			'Payor' => array('AccountNumber' => $billAccount, // Replace 'XXX' with payor's account number
				'CountryCode' => $this->_fedex_origin_country
			)
		);
		$request['RequestedShipment']['RateRequestTypes'] = 'ACCOUNT';
		$request['RequestedShipment']['RateRequestTypes'] = 'LIST';
		$request['RequestedShipment']['PackageCount'] = '1';
		$request['RequestedShipment']['PackageDetail'] = 'INDIVIDUAL_PACKAGES';
		$request['RequestedShipment']['RequestedPackageLineItems'] = array(
			'0' => array(
				'GroupPackageCount' => 1,
				'Weight' => array(
					'Value' => 1,
					'Units' => 'LB'
				),
				'Dimensions' => array(
					'Length' => $this->_fedex_depth,
					'Width' => $this->_fedex_width,
					'Height' => $this->_fedex_height,
					'Units' => 'IN'
				)
			)
		);
        
        if(@file_exists(dirname(__FILE__).'/logs/'.$this->name.'_error_report.html'))
            @unlink(dirname(__FILE__).'/logs/'.$this->name.'_error_report.html');

		try
		{
			$response = $client->getRates($request);  

			if ($response->HighestSeverity == 'FAILURE' || $response->HighestSeverity == 'ERROR')
			{
				$error_message = '<pre>['.date('m/d/Y H:i:s').'] '.$this->l('An error occured while trying to validate your account').'<br><br>';
				$error_message .= '// ------------------------ '.$this->l('Following is our module\'s request').':'.'<br>';
				$error_message .= print_r($request, true).'<br><br>';
				
				$error_message .= '// ------------------------ '.$this->l('Following is Carrier\'s message(s)').':'.'<br>';
				/** IF RETURNED MORE THAN 1 NOTIFICATION */            
				if(is_array($response->Notifications) && count($response->Notifications))
				{
					foreach($response->Notifications as $not)
					{
						$error_message .= '['.$not->Severity.' #'.$not->Code.'] '.$not->Message.'<br><br>';
					}
				}
				else
					$error_message .= '['.$response->Notifications->Severity.' #'.$response->Notifications->Code.'] '.$response->Notifications->Message.'<br><br>';
				
				$error_message .= '// ------------------------ '.$this->l('Following is Carrier\'s full response').':'.'<br>';
				$error_message .= print_r($response, true).'<br><br></pre>';
				
				$this->saveLog($this->name.'_error_report.html', $error_message, true);
				
				if ($this->getPSV() == 1.6)
					$this->_html .= $this->displayError($this->l('An error occured while trying to validate your account').' (<a href="'.__PS_BASE_URI__.'modules/'.$this->name.'/logs/'.$this->name.'_error_report.html" target="_blank">'.$this->l('click here to see full report').'</a>)');
				else
					$this->_html .= '<div class="alert error">'.$this->l('An error occured while trying to validate your account').' (<a href="'.__PS_BASE_URI__.'modules/'.$this->name.'/logs/'.$this->name.'_error_report.html" target="_blank">'.$this->l('click here to see full report').'</a>)'.'</div>';
				
				return 0;
			}
			else
				return 1;
		}
		catch (SoapFault $exception)
		{
			$error_message = '['.date('m/d/Y H:i:s').'] '.$this->l('An error occured while trying to validate your account').'<br><br>';
			$error_message .= '// ------------------------ '.$this->l('Error message').':'.'<br>';
			$error_message .= $exception->faultstring.'<br><br>';
			
			$this->saveLog($this->name.'_error_report.html', $error_message, true);
				
			if ($this->getPSV() == 1.6)
				$this->_html .= $this->displayError($this->l('An error occured while trying to validate your account').' (<a href="'.__PS_BASE_URI__.'modules/'.$this->name.'/logs/'.$this->name.'_error_report.html" target="_blank">'.$this->l('click here to see full report').'</a>)');
			else
				$this->_html .= '<div class="alert error">'.$this->l('An error occured while trying to validate your account').' (<a href="'.__PS_BASE_URI__.'modules/'.$this->name.'/logs/'.$this->name.'_error_report.html" target="_blank">'.$this->l('click here to see full report').'</a>)'.'</div>';
				
			return 0;
		}
	}

	public function getHash($id_carrier, $products, $id_product, $id_product_attribute, $qty, $dest_country, $dest_state, $dest_zip, $get_rate = false)
	{
		$log = false;

		/** ERROR: http://screencast.com/t/MCybwgzy1WC
		$this->saveLog('1h_log.txt', "1) $id_carrier, $products, $id_product, $qty, $get_rate\n\r", $log);
		*/ $this->saveLog('1h_log.txt', "1) $id_carrier, $id_product, $qty, $get_rate\n\r", $log);

		$fs_arr = Db::getInstance()->getRow('SELECT free_shipping_product, free_shipping_category, free_shipping_manufacturer, free_shipping_supplier FROM '._DB_PREFIX_.'fe_fedex_method fdm, '._DB_PREFIX_.'carrier c WHERE fdm.id_carrier = "'.$id_carrier.'" AND fdm.id_carrier = c.id_carrier AND c.active = 1 AND c.deleted = 0');
		if (!is_array($fs_arr) || sizeof($fs_arr) == 0)
			return 0;

		$origin_country = $this->_fedex_origin_country;
		$origin_zip = $this->_fedex_origin_zip;
		$origin_resident = $this->_fedex_origin_resident;
		$dest_resident = 1;
		if ($this->context->cart->id_address_delivery > 0)
		{
			$address = new Address($this->context->cart->id_address_delivery);
			if ($this->_fedex_destin_resident == "2" && $address->company != "")
				$dest_resident = 0;
		}
		$hash = "$id_carrier, $origin_country, $origin_zip, $origin_resident, $dest_country, $dest_zip, $dest_resident,";
		$is_fs = false;

		if ($id_product > 0)
		{
			if ($this->is_free_ship_product($id_product, $fs_arr))
				$is_fs = true;
			$hash .= " $id_product, $id_product_attribute, ".(int)$qty;
		}
		else
		{
			$is_fs = true;
			foreach ($products as $product)
			{
				if (!$this->is_free_ship_product($product['id_product'], $fs_arr))
					$is_fs = false;
				$hash .= " ".$product['id_product'].", ".$product['id_product_attribute'].", ".$product['quantity'];
			}
		}

		$md5 = md5($hash);

		$this->saveLog('1h_log.txt', "2) hash ($md5) $hash\n\r", $log);

		if ($get_rate)
		{
			if ($is_fs)
				return 0;

			$query = 'SELECT drc.rate,drc.packages, dm.* FROM `'._DB_PREFIX_.'fe_fedex_hash_cache` dhc, `'._DB_PREFIX_.'fe_fedex_rate_cache` drc, `'._DB_PREFIX_.'fe_fedex_method` dm WHERE dhc.hash = "'.$md5.'" AND dhc.id_fedex_rate = drc.id_fedex_rate AND hash_date > '.(time() - 86400).' AND drc.id_carrier = dm.id_carrier LIMIT 1';
			$res = Db::getInstance()->executeS($query);

			$this->saveLog('1h_log.txt', "3) $query \n\r", $log);

			if (is_array($res) && sizeof($res) == 1)
			{
				$this->saveLog('1h_log.txt', print_r($res,true)."\n\r"."getOrderTotal \n\r", $log);
				$orderTotal = $this->getOrderTotal($id_product, NULL, $qty);
				$this->saveLog('1h_log.txt', "$query\n\norderTotal ($id_product) $orderTotal\n\r", $log);
				$base_rate = $res[0]['rate'];
				if ($res[0]['extra_shipping_type'] == 2)
					$base_rate += $res[0]['extra_shipping_amount'];
				elseif ($res[0]['extra_shipping_type'] == 1)
					$base_rate += $res[0]['extra_shipping_amount'] * $orderTotal / 100;
				elseif ($res[0]['extra_shipping_type'] == 3)
					$base_rate += $res[0]['extra_shipping_amount'] * $base_rate / 100;
				$ret_amount =  number_format($base_rate,2,".","");
				if ($res[0]['free_shipping'] > 0 && $res[0]['free_shipping'] <= $orderTotal)
					$ret_amount = 0;

				$this->saveLog('1h_log.txt', "returning $ret_amount \n\r", $log);

				return $ret_amount;
			}
			else
			{
				$this->saveLog('1h_log.txt', "4) Not found in cache\n\r", $log);
				return false;
			}
		}

		return $md5;
	}

	protected function is_box_exception($id)
	{
		foreach ($this->_fedex_boxes as $box)
			if (isset($box[5]) && in_array($id, explode(",",$box[5])))
				return $box[5];
		return false;
	}

	protected function get_boxes_no_exception()
	{
		$no_exceptions = array();
		foreach ($this->_fedex_boxes as $box)
			if (!isset($box[5]) || $box[5] == "")
				$no_exceptions[] = $box;
		return $no_exceptions;
	}


	public function getShippingMethods()
	{
		$ship_meth = array(
			'FEDEX_GROUND' => $this->l('Ground'),
			'FEDEX_1_DAY_FREIGHT' => $this->l('1 Day Freight'),
			'FEDEX_2_DAY' => $this->l('2 Day'),
			'FEDEX_2_DAY_AM' => $this->l('2 Day AM'),
			'FEDEX_2_DAY_FREIGHT' => $this->l('2 Day Freight'),
			'FEDEX_3_DAY_FREIGHT' => $this->l('3 Day Freight'),
			'FEDEX_EXPRESS_SAVER' => $this->l('Express Saver'),
			'FEDEX_FIRST_FREIGHT' => $this->l('First Freight'),
			'FEDEX_FREIGHT_ECONOMY' => $this->l('Freight Economy'),
			'FEDEX_FREIGHT_PRIORITY' => $this->l('Freight Priority'),
			'FIRST_OVERNIGHT' => $this->l('First Overnight'),
			'GROUND_HOME_DELIVERY' => $this->l('Ground Home Delivery'),
			'PRIORITY_OVERNIGHT' => $this->l('Priority Overnight'),
			'SMART_POST' => $this->l('SmartPost'),
			'STANDARD_OVERNIGHT' => $this->l('Standard Overnight'),
			'EUROPE_FIRST_INTERNATIONAL_PRIORITYl' => $this->l('Europe First Priority'),
			'INTERNATIONAL_ECONOMY' => $this->l('International Economy'),
			'INTERNATIONAL_ECONOMY_FREIGHT' => $this->l('International Economy Freight'),
			'INTERNATIONAL_FIRST' => $this->l('International First'),
			'INTERNATIONAL_PRIORITY' => $this->l('International Priority'),
			'INTERNATIONAL_PRIORITY_FREIGHT' => $this->l('International Priority Freight'),
		);

		return $ship_meth;
	}

	public function getPackageTypes()
	{
		$types = array(
			'YOUR_PACKAGING' => $this->l('Your Packaging'),
			'FEDEX_BOX' => $this->l('FedEx Box'),
			'FEDEX_PAK' => $this->l('FedEx Pak'),
			'FEDEX_TUBE' => $this->l('FedEx Tube'),
			'FEDEX_ENVELOPE' => $this->l('FedEx Envelope'),
			'FEDEX_10KG_BOX' => $this->l('FedEx 10KG Box'),
			'FEDEX_25KG_BOX' => $this->l('FedEx 25KG Box'),
		);

		return $types;
	}

	public function getDimensionsByType($type = false)
	{
		return array('w' => '0.1', 'h' => '0.1', 'd' => '0.1');
	}


	public function getDropoffTypes()
	{
		$types = array(
			'BUSINESS_SERVICE_CENTER' => $this->l('Business Service Center'),
			'DROP_BOX' => $this->l('Drop Box'),
			'REQUEST_COURIER' => $this->l('Request Courier'),
			'REGULAR_PICKUP' => $this->l('Regularly Scheduled Pickup'),
			'STATION' => $this->l('Station'),
		);

		return $types;
	}

	public function getMaxWeightPackageTypes()
	{
		$max_weight = false;
		if ($this->_fedex_pack == "FEDEX_10KG_BOX")
		{
			if ($this->_fedex_unit == 'LBS')
				$max_weight = 22;
			else
				$max_weight = 10;
		}
		else if ($this->_fedex_pack == "FEDEX_25KG_BOX")
		{
			if ($this->_fedex_unit == 'LBS')
				$max_weight = 55;
			else
				$max_weight = 25;
		}

		return $max_weight;
	}

	protected function applyUpdates()
	{
		$this->registerHook('adminOrder');
		if (Configuration::get('FEDEX_DECIMAL_UPDATE') != 1)
		{
			Db::getInstance()->execute('ALTER TABLE  `'._DB_PREFIX_.'fe_fedex_method` CHANGE  `extra_shipping_amount`  `extra_shipping_amount` DECIMAL( 6, 2 ) NOT NULL');
			Db::getInstance()->execute('ALTER TABLE  `'._DB_PREFIX_.'fe_fedex_method` CHANGE  `insurance_amount`  `insurance_amount` DECIMAL( 6, 2 ) NOT NULL');
			Configuration::updateValue('FEDEX_DECIMAL_UPDATE','1');
		}

		if (Configuration::get('FEDEX_FS_UPDATE') != 1)
		{
			$cols = Db::getInstance()->ExecuteS('describe '._DB_PREFIX_.'fe_fedex_method');
			foreach ($cols AS $col)
				if ($col['Field'] == "free_shipping_category")
				{
					Configuration::updateValue('FEDEX_FS_UPDATE','1');
					return;
				}			
			Db::getInstance()->execute('ALTER TABLE  `'._DB_PREFIX_.'fe_fedex_method` ADD  `free_shipping_product` TEXT NOT NULL AFTER  `free_shipping` ,
											ADD  `free_shipping_category` TEXT NOT NULL AFTER  `free_shipping_product` ,
											ADD  `free_shipping_manufacturer` TEXT NOT NULL AFTER  `free_shipping_category` ,
											ADD  `free_shipping_supplier` TEXT NOT NULL AFTER  `free_shipping_manufacturer`');
			Configuration::updateValue('FEDEX_FS_UPDATE','1');
		}

		//labels update
		Db::getInstance()->execute('
			CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'fe_fedex_labels_info` (
				`id_label_info` int(11) NOT NULL AUTO_INCREMENT,
				`id_order` int(11) NOT NULL,
				`info` TEXT NOT NULL,
				`tracking_id_type` TINYTEXT NOT NULL,
				`tracking_numbers` TEXT NOT NULL,
				`return_tracking_id_type` TINYTEXT NOT NULL,
				`return_tracking_numbers` TEXT NOT NULL,
				PRIMARY KEY (`id_label_info`)
			) ENGINE = '._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
		');

		if(Configuration::get('FEDEX_ID_REFERENCE_UPDATE') != 1 AND $this->getPSV() >= 1.5)
		{
			Db::getInstance()->execute('UPDATE `'._DB_PREFIX_.'carrier` SET id_reference = id_carrier WHERE external_module_name = "'.$this->name.'"');
			Configuration::updateValue('FEDEX_ID_REFERENCE_UPDATE','1');
		}
		
		if(!Configuration::get('FEDEX_ENABLE_LABELS'))
			Configuration::updateValue('FEDEX_ENABLE_LABELS', 'fedex');
			
		/** VERIFY IF ALL TABLES ARE CREATED PROPERLY */
		$tables = $this->_getTables();
		foreach($tables as $table => $query)
		{
			$showTable = Db::getInstance()->ExecuteS('SHOW TABLES LIKE "'.$table.'"'); 
			if(is_array($showTable) && !count($showTable))
			{
				/** CLEAN RATE CACHE */
				Db::getInstance()->Execute('TRUNCATE `'._DB_PREFIX_.'fe_fedex_rate_cache`');
				Db::getInstance()->Execute('TRUNCATE `'._DB_PREFIX_.'fe_fedex_hash_cache`');
				Db::getInstance()->Execute('TRUNCATE `'._DB_PREFIX_.'fe_fedex_package_rate_cache`');
				Db::getInstance()->Execute('TRUNCATE `'._DB_PREFIX_.'fe_fedex_invalid_dest`');
				/** CREATE TABLE */
				Db::getInstance()->Execute($query);
			}	
		}
		
		$serverIP = ($_SERVER['SERVER_ADDR'] ? $_SERVER['SERVER_ADDR'] : $_SERVER['LOCAL_ADDR']);
		if(!Configuration::get('PS_SHOP_ENABLE'))
		{
			if(strlen(Configuration::get('PS_MAINTENANCE_IP')))
			{
				$isAdded = false;
				$PS_MAINTENANCE_IP = explode(',', Configuration::get('PS_MAINTENANCE_IP'));
				if(is_array($PS_MAINTENANCE_IP) && count($PS_MAINTENANCE_IP))
				{
					foreach($PS_MAINTENANCE_IP as $ipAddress)
					{
						if($ipAddress == $serverIP)
						{
							$isAdded = true;
							break;
						}
					}
					
					if(!$isAdded)
						$PS_MAINTENANCE_IP[] = $serverIP;    
				}
				elseif($PS_MAINTENANCE_IP == $serverIP)
					$PS_MAINTENANCE_IP = array($serverIP);
			}
			else
				$PS_MAINTENANCE_IP = array($serverIP);
				
			$PS_MAINTENANCE_IP = implode(',', $PS_MAINTENANCE_IP);
			Configuration::updateValue('PS_MAINTENANCE_IP', $PS_MAINTENANCE_IP);
		}
        
        $this->_refreshProperties();
        $this->verifyAndCleanConfigurationTable();
	}

	protected function sortBoxes()
	{
		if (sizeof($this->_fedex_boxes) > 0)
		{
			foreach($this->_fedex_boxes as $box)
			{
				$tot = round($box[0] * $box[1] * $box[2]);
				while (isset($arr_tmp[$tot]))
					$tot++;
				$box[0] = round($box[0],2);
				$box[1] = round($box[1],2);
				$box[2] = round($box[2],2);
				$arr_tmp[$tot] = $box;
			}
			if (!isset($arr_tmp) || !is_array($arr_tmp))
				$arr_tmp = array();
			ksort($arr_tmp);
			$sorted = array();
			foreach ($arr_tmp as $box)
				$sorted[] = $box;
			$this->_fedex_boxes = $sorted;
		}
	}
	
	public function hookAdminOrder($params)
	{
		$this->applyUpdates();

		$html = '';
		$ps_version  = floatval(substr(_PS_VERSION_,0,3));

		$order = new Order($params['id_order']);
		$carrier = new Carrier($order->id_carrier);
		
		$ShippingModules = $this->_getPrestoChangeoShippingModulesForOrder($carrier);
		
		/** SAVE/LOAD PREDEFINED CARRIER */
		if(Tools::getValue('shipping_label_carrier'))
		{
			$shipping_label_carrier = Tools::getValue('shipping_label_carrier'); 
			$predefined = @unserialize(Configuration::get('PC_SHIPPING_CARRIERS'));
			$predefined[Tools::getValue('id_order')] = $shipping_label_carrier;
			Configuration::updateValue('PC_SHIPPING_CARRIERS', serialize($predefined)); 
		}
		else
		{                                           
			$predefined = @unserialize(Configuration::get('PC_SHIPPING_CARRIERS')); 
			if(isset($predefined[Tools::getValue('id_order')]))
				$shipping_label_carrier = $predefined[Tools::getValue('id_order')];
			else
				$shipping_label_carrier = false;
		}
		/***/
		
		$package_types = $this->getPackageTypes();
		$countries = Country::getCountries($this->context->cookie->id_lang, true);

		if(!$shipping_label_carrier && is_array($ShippingModules) && count($ShippingModules) > 1 && !defined('PC_SHIPPING_LABEL_LOADED'))
		{
			define('PC_SHIPPING_LABEL_LOADED', true);
			
			$html .= '
				<br/>
				<form action="'.$_SERVER['REQUEST_URI'].'" method="POST" id="LabelPrintingForm" target="_self">
			';
				
			if($ps_version == 1.6)
			{
				$html .= '
				<div class="row">
					<div class="col-lg-12">
						<!-- Shipping Label -->
						<div class="panel">
							<h3><span style="float:left;">'.$this->l('Generate shipping label').'</h3>
				';
			}
			else
			{
				$html .= '
					<fieldset>
						<legend>'.$this->l('Generate shipping label').'</legend>                
				';

			}
			
			/** INSERT CARRIER SELECTION */
			$html .= '     
						<p>
							<label for="shipping_label_carrier" style="float: left; width: auto; '.($this->getPSV() == 1.6 ? 'line-height: 27px;' : '').'">'.$this->l('Select Carrier').':</label> 
							<select name="shipping_label_carrier" id="shipping_label_carrier" style="float: left; width: 190px; margin: 0 1%;">
			';
			
			foreach($ShippingModules as $shipping_module)
			{
				if($shipping_module == 'usps' || $shipping_module == 'ups' || $shipping_module == 'dhl')
					$shipping_module_name = strtoupper($shipping_module);
				elseif($shipping_module == 'fedex')
					$shipping_module_name = 'FedEx';
				elseif($shipping_module == 'canadapost')
					$shipping_module_name = 'Canada Post';
				
				$html .= '
							<option value="'.$shipping_module.'" '.($shipping_label_carrier == $shipping_module ? 'selected="selected"' : '').'>'.$shipping_module_name.'</option>
				';
			}

			$html .= '
							</select>
							
							<input type="submit" value="'.$this->l('Show Settings').'" id="selectShippingLabelCarrier" name="selectShippingLabelCarrier" class="'.($ps_version == 1.6 ? 'btn btn-default' : 'button').'" />
						</p>
			';
			
			if($ps_version == 1.6)
			{
				$html .= '
						</div>
					</div>
				</div>
				';
			}
			else
			{
				$html .= '      
					</fieldset>
				';

			} 

			$html .= '
				</form>
				<br/>
			';
		}
		elseif($shipping_label_carrier == $this->name || (is_array($ShippingModules) && count($ShippingModules) == 1 && $ShippingModules[0] == $this->name))
		{    
			/* VERIFY IF THERE IS A LABEL PREVIOUSLY PRIINTED 
			* 
			*  if(yes){ @return true; }
			*  else{ @return false; }
			*/
			$files = glob(dirname(__FILE__).'/labels/'.$params['id_order'].'/*');
			$hasPrintedLabel = false;
			if(is_array($files) && count($files) > 0)
			{
				foreach ($files as $file)
				{
					$file = explode('/', $file);
					$file = $file[count($file)-1];
					if($file != 'index.php'){
						$hasPrintedLabel = true;
					}
				}
			}   
			
			$html .= '
				<br/>
			';
			
			if($ps_version == 1.6)
			{
				$html .= '
				<div class="row">
					<div class="col-lg-12">
						<!-- FedEx Shipping Label -->
						<div class="panel">
							<h3><span style="float:left;">'.$this->l('Generate FedEx shipping label').' ('.($ps_version == 1.6 ? '</span>' : '').'<a href="#" id="expandLabelForm" class="expand" style="color:blue;text-decoration:underline;'.($ps_version == 1.6 ? 'display: inherit;float: left;' : '').'">'.$this->l('Expand').'</a>)</h3>
				';
			}
			else
			{
				$html .= '
					<fieldset>
						<legend>'.$this->l('Generate FedEx shipping label').' (<a href="#" id="expandLabelForm" class="expand" style="color:blue;text-decoration:underline;">'.$this->l('Expand').'</a>)</legend>                
				';
			}
			
			/** INSERT CARRIER SELECTION */
			if(is_array($ShippingModules) && count($ShippingModules) > 1)
			{
				$html .= '
						<form action="'.$_SERVER['REQUEST_URI'].'" method="POST" id="LabelPrintingForm" target="_self">
							<p>
								<label for="shipping_label_carrier" style="float: left; width: auto; '.($this->getPSV() == 1.6 ? 'line-height: 27px;' : '').'">'.$this->l('Select Carrier').':</label> 
								<select name="shipping_label_carrier" id="shipping_label_carrier" style="float: left; width: 190px; margin: 0 1%;">
				';
				
				foreach($ShippingModules as $shipping_module)
				{
					if($shipping_module == 'usps' || $shipping_module == 'ups' || $shipping_module == 'dhl')
						$shipping_module_name = strtoupper($shipping_module);
					elseif($shipping_module == 'fedex')
						$shipping_module_name = 'FedEx';
					elseif($shipping_module == 'canadapost')
						$shipping_module_name = 'Canada Post';
					
					$html .= '
								<option value="'.$shipping_module.'" '.($shipping_label_carrier == $shipping_module ? 'selected="selected"' : '').'>'.$shipping_module_name.'</option>
					';
				}

				$html .= '
								</select>
								
								<input type="submit" value="'.$this->l('Show Settings').'" id="selectShippingLabelCarrier" name="selectShippingLabelCarrier" class="'.($ps_version == 1.6 ? 'btn btn-default' : 'button').'" />
							</p>
						</form>
				';
			}
			/***/
			
			$html .= '
					<form action="'.$this->_path.'display_label.php?shippingLabel=1&id_order='.$params['id_order'].'" method="POST" id="labelForm" target="_blank">
						<div id="labelFormContent"></div>
					</form>
			';
				
			if($ps_version == 1.6)
			{
				$html .= '
						</div>
					</div>
				</div>
				';
			}
			else
			{                
				$html .= '
					</fieldset>
				';
			}
			
			$html .= '
				<br/>

				<script type="text/javascript">
					var psv = '.$this->getPSV().';
					var international_methods = new Array("INTERNATIONAL_ECONOMY", "INTERNATIONAL_PRIORITY", "INTERNATIONAL_FIRST", "INTERNATIONAL_ECONOMY_FREIGHT", "EUROPE_FIRST_INTERNATIONAL_PRIORITY");
					$(document).ready(function() {
						$("#labelForm input, #labelForm select, #labelForm textarea").live("change", function(){
							saveLabelInfo();
						});
						
						$("#expandLabelForm").bind("click", function(){
							if($(this).hasClass("expand")) {
								$.ajax({
									type: "POST",
									url: "'.(isset($this->context->shop->virtual_uri) ? substr($this->context->shop->virtual_uri,0, -1) : '').$this->_path.'ajaxLabelSettings.php",
									async: false,
									cache: false,
									data: {fedex_random: "'.$this->_fedex_random.'", load: 1, id_order: '.$params['id_order'].'},
									success: function(html) {
										$("#labelFormContent").html(html);
										$("#expandLabelForm").text("'.$this->l('Collapse').'").removeClass("expand").addClass("collapse");
										if(psv < 1.5)
											$("#labelForm").parent("div").css("float", "none").next("div").css("margin-left", "0");
										showDimensions();
									}
								});
							}
							else {
								$("#labelFormContent").html("");
								$("#expandLabelForm").text("'.$this->l('Expand').'").removeClass("collapse").addClass("expand");
								if(psv < 1.5)
									$("#labelForm").parent("div").css("float", "left").next("div").css("margin-left", "40px");
							}
							saveLabelInfo();
							return false;
						})'.(!$hasPrintedLabel || Tools::getValue('shipping_label_carrier') ? '.trigger("click")' : '').';

						$("#fedex_pack").live("change", function(){
							showDimensions();
						});

						$("#add_pack").live("click", function(){
							var pack_type = $("#fedex_pack").val();
							var pack_width = $("#fedex_width").val();
							var pack_height = $("#fedex_height").val();
							var pack_depth = $("#fedex_depth").val();
							var pack_weight = $("#fedex_weight").val();
							var pack_insurance = $("#fedex_insurance").val();
							var pack_cod_code = $("#fedex_cod_type").val();
							var pack_cod_amount = $("#fedex_cod_amount").val();
							var label_return = $("#label_return").is(":checked");

							//validation
							if(pack_weight <= 0)
							{
								alert("'.$this->l('Please enter a weight greater than 0.').'");
								return false;
							}

							var id = $("#packages_list .package_item").length;
							var pack_html = \'\
							<p class="package_item" style="float: left; width: 100%; \' + (id > 0 ? "margin: 20px 0 10px 0;border-top: 1px solid #cccccc;padding: 16px 0 0 0;" : "") + \'">\
								\
								<span style="float:left;margin: 0 2% 0 0;">\
									'.$this->l('Package Type').': <sup>*</sup>\
									<br />\
									<select class="pack_type_select" name="pack[\' + id + \'][type]" id="fedex_pack\' + id + \'" style="width:110px;">';
									if(is_array($package_types) && count($package_types))
									{
										foreach ($package_types as $code => $package_name)
										{
											$html .= '<option value="'.$code.'" \' + (pack_type == "'.$code.'" ? \'selected="selected"\' : "") + \'>'.$package_name.'</option>';
										}
									}
					$html .= '\
									</select>\
								</span>\
								\
								<span style="float:left;margin: 0 2% 0 0;">\
									'.$this->l('Width').':\
									<br />\
									<input type="text" class="pack_dimensions" id="pack_width_\' + id + \'" name="pack[\' + id + \'][w]" style="width:40px;\' + (pack_type != "YOUR_PACKAGING" ? "display:none;" : "") + \'" value="\' + pack_width + \'">\
								</span>\
								\
								<span style="float:left;margin: 0 2% 0 0;">\
									'.$this->l('Height').':\
									<br />\
									<input type="text" class="pack_dimensions" id="pack_height_\' + id + \'" name="pack[\' + id + \'][h]" style="width:40px;\' + (pack_type != "YOUR_PACKAGING" ? "display:none;" : "") + \'" value="\' + pack_height + \'">\
								</span>\
								\
								<span style="float:left;margin: 0 2% 0 0;">\
									'.$this->l('Depth').':\
									<br />\
									<input type="text" class="pack_dimensions" id="pack_depth_\' + id + \'" name="pack[\' + id + \'][d]" style="width:40px;\' + (pack_type != "YOUR_PACKAGING" ? "display:none;" : "") + \'" value="\' + pack_depth + \'">\
								</span>\
								\
								<span style="float:left;margin: 0 2% 0 0;">\
									'.$this->l('Weight').': <sup>*</sup>\
									<br />\
									<input type="text" id="pack_weight_\' + id + \'" name="pack[\' + id + \'][weight]" style="width:55px;" value="\' + pack_weight + \'" class="package_weight">\
								</span>\
								\
								<span style="float:left;margin: 0 2% 0 0;">\
									'.$this->l('Insured Value').':\
									<br />\
									<input type="text" id="pack_insurance_\' + id + \'" name="pack[\' + id + \'][insurance]" style="width:85px;" value="\' + pack_insurance + \'" class="package_insurance">\
								</span>\
								\
								<span style="float:left;margin: 0 2% 0 0;">\
									'.$this->l('COD Collection Type').':\
									<br />\
									<select name="pack[\' + id + \'][cod_code]" id="pack_cod_code_\' + id + \'" style="width:105px;">\
										<option></option>\
										<option value="ANY" \' + (pack_cod_code == "ANY" ? "selected" : "") + \' >'.$this->l('Any').'</options>\
										<option value="CASH" \' + (pack_cod_code == "CASH" ? "selected" : "") + \' >'.$this->l('Cash').'</options>\
										<option value="COMPANY_CHECK" \' + (pack_cod_code == "COMPANY_CHECK" ? "selected" : "") + \' >'.$this->l('Company Check').'</options>\
										<option value="GUARANTEED_FUNDS" \' + (pack_cod_code == "GUARANTEED_FUNDS" ? "selected" : "") + \' >'.$this->l('Guaranteed Funds').'</options>\
										<option value="PERSONAL_CHECK" \' + (pack_cod_code == "PERSONAL_CHECK" ? "selected" : "") + \' >'.$this->l('Personal Check').'</options>\
									</select>\
								</span>\
								\
								<span style="float:left;margin: 0 2% 0 0;">\
									'.$this->l('COD Amount').':\
									<br />\
									<input type="text" id="pack_cod_amount_\' + id + \'" name="pack[\' + id + \'][cod_amount]" style="width:85px;" value="\' + pack_cod_amount + \'" class="package_cod_amount">\
								</span>\
								\
								<span style="float:left;margin: 18px 1% 0 0;">\
									<input type="button" class="'.($this->getPSV() == 1.6 ? 'btn btn-default' : 'button').' show_intl_settings" rel="\' + id + \'" value="'.$this->l('Hide Intl settings').'" />\
								</span>\
								\
								<span style="float:left;margin: 20px 0 0 0;">\
									<img src="'._MODULE_DIR_.'fedex/img/delete.gif" id="pack_delete_\' + id + \'" class="pack_delete" style="cursor:pointer;">  \
								</span>\
							</p>\
							\
							<h3 id="internationalSettings" class="international_settings_\' + id + \'">\
								<span style="float: left;">'.$this->l('Product(s)').' - </span><span style="font-weight:normal;float: left;">'.$this->l('Number of product(s) in the Box').':</span>\
								<input type="text" name="pack[\' + id + \'][productsPerBox]" id="pack[\' + id + \']_productsPerBox" value="1" style="width: 40px; float: left; margin: 3px;" onblur="location.reload();" />\
							</h3>\
							\
							<div class="international_settings_\' + id + \'" id="internationalSettings" style="float: left; width: 100%;">\
								<div id="pack[\' + id + \']_products" style="float: left; width: 100%">\
										\
									<p style="float: left; width: 100%" id="pack[\' + id + \']_productIdentification0">\
										<span style="font-weight:bold;float:left;width:100%;">'.$this->l('Product ').'1: </span>    \
									</p>\
									\
									<p id="pack[\' + id + \']_product0" style="float: left; width: 100%">\
									\
										<span style="float:left;margin: 0 2% 0 0;">\
											'.$this->l('Description').': <sup>**</sup>\
											<br />\
											<input name="pack[\' + id + \'][products][0][Description]" id="pack[\' + id + \']_product0_Description" type="text" value="" maxlength="56" style="width: 130px; padding: 0 3px; float:left; margin: 0 5px 0 0;" />\
										\
											<span class="info_tooltip" title="\
												'.$this->l('Complete and accurate description of this commodity.').'\
											"></span> \
										</span>\
										\
										<span style="float:left;margin: 0 2% 0 0;">\
											'.$this->l('Quantity').': <sup>**</sup>\
											<br />\
											<input name="pack[\' + id + \'][products][0][Quantity]" id="pack[\' + id + \']_product0_Quantity" type="text" value="0" style="width: 25px; padding: 0 3px; float: left;" />\
											\
											<span class="info_tooltip" title="\
												'.$this->l('Total number of pieces of this commodity.').'\
											"></span> \
										</span>\
										\
										<span style="float:left;margin: 0 2% 0 0;">\
											'.$this->l('Unit Value').': <sup>**</sup>\
											<br />\
											<input name="pack[\' + id + \'][products][0][Value]" id="pack[\' + id + \']_product0_Value" type="text" value="0.00" style="width: 45px; padding: 0 3px; float: left;" />\
											\
											<span class="info_tooltip" title="\
												'.$this->l('Value of each unit in Quantity.').'\
											"></span> \
										</span>\
										\
										<span style="float:left;margin: 0 2% 0 0;">\
											'.$this->l('Unit Weight').': <sup>**</sup>\
											<br />\
											<input name="pack[\' + id + \'][products][0][Weight]" id="pack[\' + id + \']_product0_Weight" type="text" value="0" style="width: 45px; padding: 0 3px; float: left;" />\
											\
											<span class="info_tooltip" title="\
												'.$this->l('Weight of each unit in Quantity.').'\
											"></span> \
										</span>\
										\
										<span style="float:left;margin: 0 2% 0 0;">\
											'.$this->l('Country of Manufacture').': <sup>**</sup>\
											<br />\
											<input name="pack[\' + id + \'][products][0][CountryofManufacture]" id="pack[\' + id + \']_product0_CountryofManufacture" type="text" value="" style="width: 130px; padding: 0 3px; float: left;" />\
											\
											<span class="info_tooltip" title="\
												'.$this->l('Country code where commodity contents were produced or manufactured in their final form.').'\
											"></span> \
										</span>\
									</p>\
								</div>\
							</div>\
							\
							<div style="float: left; width: 100%;" class="international_settings_\' + id + \' package_item" id="internationalSettings">\
								<h3>'.$this->l('Box Information').'</h3>\
								\
								<div style="margin: 5px 0;float: left; width: 100%">\
									<label for="pack[\' + id + \']_DutyPayer" style="float:left;width:220px;font-weight:normal;">'.$this->l('Duty Payer').': <sup>**</sup></label>\
									<select name="pack[\' + id + \'][DutyPayer]" id="pack[\' + id + \']_DutyPayer" style="width: 130px; padding: 0 3px; float:left; margin: 0 5px 0 0;">\
										<option value="SENDER" selected="selected">'.$this->l('Sender').'</option>\
										<option value="RECIPIENT">'.$this->l('Recipient').'</option>\
										<option value="THIRD_PARTY">'.$this->l('Recipient').'</option>  \
									</select>\
									\
									<span class="info_tooltip" title="\
										'.$this->l('Define who is responsible for the products importation taxes').'. '.$this->l('A FedEx Account Number is required for Third Party Payer').'.\
									"></span> \
								</div>  \
								\
								<div style="margin: 5px 0;float: left; width: 100%">\
									<label for="pack[\' + id + \']_DutyPayerAccount" style="float:left;width:220px;font-weight:normal;">'.$this->l('Duty Payer FedEx Account').':</label>\
									<input name="pack[\' + id + \'][DutyPayerAccount]" id="pack[\' + id + \']_DutyPayerAccount" type="text" value="" style="width: 130px; padding: 0 3px; float:left; margin: 0 5px 0 0;" />\
									\
									<span class="info_tooltip" title="\
										'.$this->l('The FedEx account number associated with the duty payment').'. '.$this->l('If payer is Sender, your FedEx Account number will be used').'.\
									"></span> \
								</div>   \
								\
								<div style="margin: 5px 0;float: left; width: 100%">\
									<label for="pack[\' + id + \']_DutyPayerCountry" style="float:left;width:220px;font-weight:normal;">'.$this->l('Duty Payer Country').':</label>\
									<select name="pack[\' + id + \'][DutyPayerCountry]" id="pack[\' + id + \']_DutyPayerCountry" style="width: 130px; padding: 0 3px; float:left; margin: 0 5px 0 0;">';
										foreach ($countries as $country)
										{
											$html .= '<option value="'.$country['id_country'].'">'.$country['name'].'</option>';
										}
										$html .= '\
									</select>\
								</div>  \
								\
								<div style="margin: 5px 0;float: left; width: 100%">\
									<label for="pack[\' + id + \']_ShippersLoadandCount" style="float:left;width:220px;font-weight:normal;">'.$this->l('Shippers Load and Count').': <sup>**</sup></label>\
									<input name="pack[\' + id + \'][ShippersLoadandCount]" id="pack[\' + id + \']_ShippersLoadandCount" type="text" value="" style="width: 130px; padding: 0 3px; float:left; margin: 0 5px 0 0;" />\
									\
									<span class="info_tooltip" title="\
										'.$this->l('Total shipment pieces. E.g. 3 boxes and 3 pallets of 100 pieces each = Shippers Load and Count of 303. Applicable to International Priority Freight and International Economy Freight. Values must be in the range of 1 - 99999.').'\
									"></span> \
								</div>   \
								\
								<div style="margin: 5px 0;float: left; width: 100%">\
									<label for="pack[\' + id + \']_BookingConfirmationNumber" style="float:left;width:220px;font-weight:normal;">'.$this->l('Booking Confirmation Number').':</label>\
									<input name="pack[\' + id + \'][BookingConfirmationNumber]" id="pack[\' + id + \']_BookingConfirmationNumber" type="text" value="" style="width: 130px; padding: 0 3px; float:left; margin: 0 5px 0 0;" />\
									\
									<span class="info_tooltip" title="\
										'.$this->l('An advance booking number is required for FedEx International Priority Freight. When you call 1.800.332.0807 to book your freight shipment, you will receive a booking number. This booking number can be included in the Ship request and prints on the shipping label.').'\
									"></span> \
								</div>    \
								\
								<div style="margin: 5px 0;float: left; width: 100%; \' + (!label_return ? "display:none;" : "") + \'" class="customs_options_type">\
									<label for="pack_customs_options_type_\' + id + \'" style="float:left;width:220px;font-weight:normal;">'.$this->l('Customs Options Type').': <sup>**</sup></label>\
									<select id="pack_customs_options_type_\' + id + \'" name="pack[\' + id + \'][customs_options_type]" style="width: 130px; padding: 0 3px; float:left; margin: 0 5px 0 0;" class="package_customs_options_type intl_field">';
										foreach ($this->getCustomsOptionsTypes() as $code => $type)
										{
											$html .= '<option value="'.$code.'">'.$type.'</option>';
										}
									$html .= '\
									</select>\
									\
									<span class="info_tooltip" title="\
										'.$this->l('Details the return reason used for clearance processing of international dutiable outbound and international dutiable return shipments.').'\
									"></span> \
									\
									<span style="font-size:11px;position:relative;top:-1px;">'.$this->l('(Return label only)').'</span>\
								</div> \
								\
								<div style="margin: 5px 0;float: left; width: 100%; display:none;" class="customs_options_type">\
									<label for="pack_customs_options_description_\' + id + \'" style="float:left;width:220px;font-weight:normal;">'.$this->l('Customs Options Description').': <sup>**</sup></label>\
									<input type="text" id="pack_customs_options_description_\' + id + \'" name="pack[\' + id + \'][customs_options_description]" style="width: 130px; padding: 0 3px; float:left; margin: 0 5px 0 0;" value="" class="package_customs_options_description">\
									\
									<span class="info_tooltip" title="\
										'.$this->l('Specifies additional description about customs options.').'\
									"></span> \
									\
									<span style="font-size:11px;position:relative;top:-1px;">'.$this->l('(Return label only)').'</span>\
								</div> \
							</div>\
							\';
							
							$("#packages_list #package_list_body").append(pack_html);
							saveLabelInfo();
						});

						$(".package_customs_options_type").live("change", function(){
							if($(this).val() == "OTHER")
								$(this).parents().next().show();
							else
								$(this).parents().next().hide();
						});

						$(".pack_type_select").live("change", function(){
							if($(this).val() == "YOUR_PACKAGING")
							{
								$(this).parents().next().find(".pack_dimensions").fadeIn();
								$(this).parents().next().next().find(".pack_dimensions").fadeIn();
								$(this).parents().next().next().next().find(".pack_dimensions").fadeIn();
							}
							else
								$(this).parents().next().find(".pack_dimensions").fadeOut();
								$(this).parents().next().next().find(".pack_dimensions").fadeOut();
								$(this).parents().next().next().next().find(".pack_dimensions").fadeOut();
						});

						$("img[class=\'pack_delete\']").live("click", function(){ 
							/** REMOVE BOX INFORMATION LINE  */
							$(this).parents("p").next().next().next().remove();
							/** REMOVE BOX PRODUCTS INFORMATION LINE  */
							$(this).parents("p").next().next().remove();                     
							/** REMOVE H3 LINE */                       
							$(this).parents("p").next().remove();
							/** REMOVE PACKAGE LINE */                      
							$(this).parents("p").remove();                       
							saveLabelInfo();
						}); 

						$("#shipping_type, #label_return_shipping_method, #label_return").live("change", function(){
							if(in_array($("#shipping_type").val(), international_methods) || (in_array($("#label_return_shipping_method").val(), international_methods) && $("#label_return").is(":checked"))) {
								$("div[id=\'internationalSettings\'], h3[id=\'internationalSettings\']").slideDown();
								$(".show_intl_settings").val("'.$this->l('Hide Intl Settings').'");
							}
							else {
								$("div[id=\'internationalSettings\'], h3[id=\'internationalSettings\']").slideUp();
								$(".show_intl_settings").val("'.$this->l('Show Intl Settings').'");
							}
							if($("#label_return").is(":checked")) {
								$(".customs_options_type").show();
								
								$(".package_customs_options_type").each(function(){
									if($(this).val() == "OTHER")
										$(this).parent().next().show();
									else
										$(this).parent().next().hide();
								});
							}
							else
								$(".customs_options_type, .customs_options_description").hide();
						});

						$("#labelForm").live("submit", function(){
							if(in_array($("#shipping_type").val(), international_methods) || (in_array($("#label_return_shipping_method").val(), international_methods) && $("#label_return").is(":checked"))) {
								/*check if international settings are not filled*/
								var intl_error = false;
								$(".intl_field").each(function(){
									if(!$(this).val()) {
										intl_error = true;
									}
								});
								if(intl_error) {
									alert("'.$this->l('Please fill international settings.').'");
									$("tr.international_settings").slideDown();
									$(".show_intl_settings").val("'.$this->l('Hide Intl Settings').'");
									return false;
								}
							}

							var errorsCount = 0;
							$(".package_weight:visible").each(function(){
								if($(this).val() <= 0)
								{
									//if this error is first
									if(errorsCount < 1)
										alert("'.$this->l('Please enter a weight greater than 0.').'");
									$(this).css("outline","1px solid red");
									setTimeout(function(){
										$(".package_weight").css("outline","");
									}, 1500);
									errorsCount++;
								}
							});
							if(errorsCount > 0) 
								return false;

							var labelFormat = $("#label_format").val();
							var formdata = $("#labelForm").serialize() + "&id_employee='.$this->context->cookie->id_employee.'&fedex_random='.$this->_fedex_random.'&shippingLabel=1&id_order='.(int)$params['id_order'].'&ajax=1";
							$("#labels_links").html("");
							$("#labelLoader").show();
							$("#void_response").html("");
							var generateReturnLabel = $("#label_return").is(":checked");
							
							$.ajax({
								type: "POST",
								url: "'.$this->_path.'display_label.php",
								async: true,
								cache: false,
								data: formdata,
								success: function(json) {
									json = $.parseJSON(json);
									
									if(json.labels != 0)
									{
										$("#generateLabel").attr("disabled","disabled");
										$("#voidShipment").removeAttr("disabled");
										if(generateReturnLabel) 
										{
											$("#voidReturnShipment").removeAttr("disabled");
											$("#label_return").attr("disabled","disabled");
										}
										
										var arr = json.labels.split(",");
										var length = arr.length;
										
										for (var i = 0; i < length; i++) {
										  window.open("'._MODULE_DIR_.$this->name.'/labels/plugin/download_file.php?file=" + arr[i], "_blank");
										}
										
										setTimeout(function(){ location.reload(); }, 3000);
									}
										
									$("#labelLoader").hide();
									$("#errorsLabelGeneration").html(json.html);
									
									if(json.labels != 0){
										$.ajax({
											type: "POST",
											url: "'.$this->_path.'display_label.php",
											async: true,
											cache: false,
											data: {id_employee: "'.$this->context->cookie->id_employee.'", fedex_random: "'.$this->_fedex_random.'", "id_order": '.(int)$params['id_order'].', "show_existing": 1},
											success: function(data) {
												$("#previous_labels_list").html(data);
											}
										});
									}
								}
							});

							return false;
						});

						$("#delete_labels").live("click", function(){
							if(confirm("'.$this->l('Are you sure you want to delete the labels from your server? It will not cancel of affect the shipment').'")) {
								$.ajax({
									type: "POST",
									url: "'.$this->_path.'display_label.php",
									async: true,
									cache: false,
									data: {id_employee: "'.$this->context->cookie->id_employee.'", fedex_random: "'.$this->_fedex_random.'", "id_order": '.(int)$params['id_order'].', "delete_labels": 1},
									success: function(html) {
										$("#previous_labels_list").html("");
										$("#labels_links").html("");
									}
								});
							}
						});

						$("#voidShipment, #voidReturnShipment").live("click", function(){
							if(confirm("'.$this->l('Are you sure you want to void this shipment?').'")) {
								var void_return = ($(this).attr("id") == "voidReturnShipment" ? 1 : 0);
								if(void_return)
									$("#voidLoader1").show();
								else
									$("#voidLoader").show();
								$.ajax({
									type: "POST",
									url: "'.$this->_path.'display_label.php",
									async: true,
									cache: false,
									data: {id_employee: "'.$this->context->cookie->id_employee.'", fedex_random: "'.$this->_fedex_random.'", "id_order": '.(int)$params['id_order'].', "void_shipment": 1, "void_return": void_return},
									success: function(data) {
										data = $.parseJSON(data);
										$("#void_response").html(data.text);
										if(void_return)
											$("#voidLoader1").hide();
										else
											$("#voidLoader").hide();
										if(data.status == 1 && !void_return) {
											document.getElementById("voidShipment").setAttribute("disabled","disabled");
											$("#generateLabel").removeAttr("disabled");
											$("#label_return").removeAttr("disabled");
										}
										else if(void_return == 1) {
											document.getElementById("voidReturnShipment").setAttribute("disabled","disabled");
											$("#label_return").removeAttr("disabled");
										}
									}
								});
							}
						});

						$("input[class=\''.($this->getPSV() == 1.6 ? 'btn btn-default' : 'button').' show_intl_settings\']").live("click", function(){
							var settings_div = $("div[class=\'international_settings_" + $(this).attr("rel") + "\'], h3[class=\'international_settings_" + $(this).attr("rel") + "\'], div[class=\'international_settings_" + $(this).attr("rel") + " package_item\']");
							
							if(settings_div.is(":visible")) {
								settings_div.hide();
								$(this).val("'.$this->l('Show Intl Settings').'");
							}
							else {
								settings_div.show();
								$(this).val("'.$this->l('Hide Intl Settings').'");
							}
							
							return false;
						});

						$("#shop_country, #shipto_country").live("change", function(){
							var id_country = $(this).val();
							var type = $(this).attr("id");
							$.ajax({
								type: "POST",
								url: "'.$this->_path.'ajaxLabelSettings.php",
								data: {get_states: 1, id_country: id_country, id_order: '.$params['id_order'].', fedex_random: "'.$this->_fedex_random.'", type: type},
								success: function(html) {
									if(type == "shop_country")
										$("#shop_state").html(html);
									else if(type == "shipto_country")
										$("#shipto_state").html(html);
								}
							});
						});
					});

					function showDimensions()
					{
						var pack_type = $("#fedex_pack").val();

						if(pack_type == "YOUR_PACKAGING")
							$("#dimensions *").fadeIn();
						else {
							$("#dimensions *").not(".always_show, .very_always_show").fadeOut();
							$("#dimensions .always_show, #dimensions .very_always_show").fadeIn();
						}
					}

					function saveLabelInfo() {
						if ($("#labelForm").serialize() == "")
							return;
						var data = $("#labelForm").serialize() + "&fedex_random='.$this->_fedex_random.'&save=1&id_order='.$params['id_order'].'";                    
						$.ajax({
							type: "POST",
							url: "'.$this->_path.'ajaxLabelSettings.php",
							async: false,
							cache: false,
							data: data,
							success: function(html) {
								//console.log(html);
							}
						});
					} 

					function in_array(needle, haystack, strict) {	// Checks if a value exists in an array
						var found = false, key, strict = !!strict;
						for (key in haystack) {
							if ((strict && haystack[key] === needle) || (!strict && haystack[key] == needle)) {
								found = true;
								break;
							}
						}

						return found;
					}


				</script>
			';
			return $html;
		}
	}
	
	public function getFedexCurrencies()
	{
		$currencies = array(
			"USD" => "U.S. Dollar",
			"EUR" => "Euro",
			"ANG" => "Antilles Guilder",
			"ARN" => "Argentinian Peso",
			"AUD" => "Australian Dollar",
			"AWG" => "Aruban Guilder",
			"BBD" => "Barbados Dollar",
			"BHD" => "Bahraini Dinar",
			"BMD" => "Bermuda Dollar",
			"BND" => "Brunei Dollar",
			"BRL" => "Brazilian Real",
			"BSD" => "Bahamian Dollars",
			"CAD" => "Canadian Dollar",
			"CHP" => "Chilean Peso",
			"CID" => "Cayman Dollar",
			"CID" => "Cayman Dollars",
			"CNY" => "Chinese Renminbi",
			"COP" => "Colombian Peso",
			"CRC" => "Costa Rican Colon",
			"CZK" => "Czech Republic Koruny",
			"DHS" => "United Arab Emirates Dirham",
			"DKK" => "Danish Krone",
			"ECD" => "E. Caribbean Dollar",
			"EGP" => "Egyptian Pound",
			"HKD" => "Hong Kong Dollar",
			"HUF" => "Hungarian Forint",
			"ILS" => "Israeli Shekel",
			"INR" => "Indian Rupee",
			"JAD" => "Jamaican Dollar",
			"JYE" => "Japanese Yen",
			"KES" => "Kenyan Schilling",
			"KUD" => "Kuwaiti Dinar",
			"KZT" => "Kazachstan Tenge",
			"LTL" => "Lithuanian Litas",
			"LVL" => "Latvian Lats",
			"LYD" => "Libyan Dinar",
			"MOP" => "Macau Patacas",
			"MYR" => "Malaysian Ringgits",
			"NMP" => "New Mexican Peso",
			"NOK" => "Norwegian Krone",
			"NTD" => "New Taiwan Dollar",
			"NZD" => "New Zealand Dollar",
			"PAB" => "Panama Balboa",
			"PHP" => "Philippine Peso",
			"PKR" => "Pakistan Rupee",
			"PLN" => "Polish Zloty",
			"RDD" => "Dominican Peso",
			"RPA" => "Indonesian Rupiah",
			"RUR" => "Russian Rouble",
			"SAR" => "Saudi Arabian Riyal",
			"SBD" => "Solomon Island Dollar",
			"SEK" => "Swedish Krona",
			"SFR" => "Swiss Francs",
			"SID" => "Singapore Dollar",
			"THB" => "Thailand Baht",
			"TOP" => "Tonga Pa'anga",
			"TRY" => "New Turkish Lira",
			"TTD" => "Trinidad & Tobago Dollar",
			"UGX" => "Uganda Schilling",
			"UKL" => "UK Pounds Sterling",
			"UYP" => "Uruguay New Peso",
			"VEF" => "Venezuela Bolivar Fuerte",
			"WON" => "South Korean Won",
			"WST" => "Western Samoa Tala",
			"ZAR" => "South African Rand",
		);

		return $currencies;
	}

	public function getCustomsOptionsTypes()
	{
		$types = array(
			'COURTESY_RETURN_LABEL' => $this->l('Courtesy return label'),
			'EXHIBITION_TRADE_SHOW' => $this->l('Exhibition trade show'),
			'FAULTY_ITEM' => $this->l('Faulty item'),
			'FOLLOWING_REPAIR' => $this->l('Following repair'),
			'FOR_REPAIR' => $this->l('For repair'),
			'ITEM_FOR_LOAN' => $this->l('Item for loan'),
			'OTHER' => $this->l('Other'),
			'REJECTED' => $this->l('Rejected'),
			'REPLACEMENT' => $this->l('Replacement'),
			'TRIAL' => $this->l('Trial'),
		);

		return $types;
	}
	
	private function _getTables()
	{
		return array(
			_DB_PREFIX_.'fe_fedex_hash_cache' =>
			'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'fe_fedex_hash_cache` (
			  `id_hash` int(11) NOT NULL AUTO_INCREMENT,
			  `id_fedex_rate` int(11) NOT NULL,
			  `hash` varchar(40) NOT NULL,
			  `hash_date` int(11) NOT NULL,
			  PRIMARY KEY (`id_hash`),
			  KEY `hash` (`hash`)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;',
			
			_DB_PREFIX_.'fe_fedex_invalid_dest' =>
			'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'fe_fedex_invalid_dest` (
			  `id_invalid` int(11) NOT NULL AUTO_INCREMENT,
			  `method` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
			  `zip` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
			  `country` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
			  `state` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
			  `ondate` int(11) NOT NULL,
			  PRIMARY KEY (`id_invalid`),
			  KEY `zip` (`zip`,`country`)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;',
			
			_DB_PREFIX_.'fe_fedex_labels_info' =>
			'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'fe_fedex_labels_info` (
			  `id_label_info` int(11) NOT NULL AUTO_INCREMENT,
			  `id_order` int(11) NOT NULL,
			  `info` text COLLATE utf8_unicode_ci NOT NULL,
			  `tracking_id_type` tinytext COLLATE utf8_unicode_ci NOT NULL,
			  `tracking_numbers` text COLLATE utf8_unicode_ci NOT NULL,
			  `return_tracking_id_type` tinytext COLLATE utf8_unicode_ci NOT NULL,
			  `return_tracking_numbers` text COLLATE utf8_unicode_ci NOT NULL,
			  PRIMARY KEY (`id_label_info`)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;',
			
			_DB_PREFIX_.'fe_fedex_method' =>
			'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'fe_fedex_method` (
			  `id_fedex_method` int(11) NOT NULL AUTO_INCREMENT,
			  `id_carrier` int(11) NOT NULL,
			  `method` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
			  `free_shipping` decimal(9,2) NOT NULL,
			  `free_shipping_product` text COLLATE utf8_unicode_ci NOT NULL,
			  `free_shipping_category` text COLLATE utf8_unicode_ci NOT NULL,
			  `free_shipping_manufacturer` text COLLATE utf8_unicode_ci NOT NULL,
			  `free_shipping_supplier` text COLLATE utf8_unicode_ci NOT NULL,
			  `extra_shipping_type` int(1) NOT NULL,
			  `extra_shipping_amount` decimal(9,2) NOT NULL,
			  `insurance_minimum` int(11) NOT NULL,
			  `insurance_type` int(1) NOT NULL,
			  `insurance_amount` decimal(9,2) NOT NULL,
			  PRIMARY KEY (`id_fedex_method`),
			  KEY `id_carrier` (`id_carrier`),
			  KEY `method` (`method`)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;',
			
			_DB_PREFIX_.'fe_fedex_package_rate_cache' =>
			'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'fe_fedex_package_rate_cache` (
			  `id_package` int(11) NOT NULL AUTO_INCREMENT,
			  `id_fedex_rate` int(11) NOT NULL,
			  `weight` decimal(17,2) NOT NULL,
			  `width` decimal(17,2) NOT NULL,
			  `height` decimal(17,2) NOT NULL,
			  `depth` decimal(17,2) NOT NULL,
			  PRIMARY KEY (`id_package`),
			  KEY `weight` (`weight`,`width`,`height`,`depth`)
			) ENGINE=InnoDB DEFAULT CHARSET=latin1;',
			
			_DB_PREFIX_.'fe_fedex_rate_cache' =>
			'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'fe_fedex_rate_cache` (
			  `id_fedex_rate` int(11) NOT NULL AUTO_INCREMENT,
			  `id_carrier` int(11) NOT NULL,
			  `origin_zip` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
			  `origin_country` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
			  `dest_zip` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
			  `dest_country` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
			  `dest_resident` tinyint(1) NOT NULL,
			  `method` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
			  `insurance` int(11) NOT NULL,
			  `dropoff` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
			  `packing` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
			  `packages` decimal(17,2) NOT NULL,
			  `weight` decimal(17,2) NOT NULL,
			  `rate_code` int(2) NOT NULL,
			  `rate` decimal(17,2) NOT NULL,
			  `quote_date` int(11) NOT NULL,
			  PRIMARY KEY (`id_fedex_rate`),
			  KEY `origin_zip` (`origin_zip`,`origin_country`,`dest_zip`,`dest_country`)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;',
			
		);
	}
    
    private function verifyAndCleanConfigurationTable()
    {  
        $groupedConfigurations = array();
        $configurationsToDelete = array();
        
        $configurationFields = array(
            '"FEDEX_KEY"',
            '"FEDEX_PASS"',
            '"FEDEX_ACCOUNT"',
            '"FEDEX_METER"',
            '"FEDEX_DROPOFF"',
            '"FEDEX_PACK"',
            '"FEDEX_WIDTH"',
            '"FEDEX_HEIGHT"',
            '"FEDEX_DEPTH"',
            '"FEDEX_WEIGHT"',
            '"FEDEX_ORIGIN_RESIDENT"',
            '"FEDEX_DESTIN_RESIDENT"',
            '"FEDEX_ORIGIN_ZIP"',
            '"FEDEX_ORIGIN_CITY"',
            '"FEDEX_ORIGIN_COUNTRY"',
            '"FEDEX_TYPE"',
            '"FEDEX_PACKAGES"',
            '"FEDEX_PACKAGE_SIZE"',
            '"FEDEX_PACKAGES_PER_BOX"',
            '"FEDEX_OVERRIDE_ADDRESS"',
            '"FEDEX_DEBUG_MODE"',
            '"FEDEX_XML_LOG"',
            '"FEDEX_ADDRESS_DISPLAY"',
            '"FEDEX_MODE"',
            '"FEDEX_UNIT"',
            '"FEDEX_SHIPPING_RATES_BUTTON"',
            '"FEDEX_SHIPPER_SHOP_NAME"',
            '"FEDEX_SHIPPER_ATTENTION_NAME"',
            '"FEDEX_SHIPPER_PHONE"',
            '"FEDEX_SHIPPER_ADDR1"',
            '"FEDEX_SHIPPER_ADDR2"',
            '"FEDEX_SHIPPER_CITY"',
            '"FEDEX_SHIPPER_COUNTRY"',
            '"FEDEX_SHIPPER_STATE"',
            '"FEDEX_SHIPPER_POSTCODE"',
            '"FEDEX_ENABLE_LABELS"',
            '"FEDEX_LABEL_ORDER_STATUS"',
            '"FEDEX_LABEL_MARGIN"',
            '"FEDEX_LABEL_FORMAT"',
            '"FEDEX_LABEL_STOCK_TYPE"',
        );
        
        $query = 'SELECT `name`, `id_configuration`, `id_shop` FROM `'._DB_PREFIX_.'configuration` WHERE `name` IN ('.implode(',', $configurationFields).') ORDER BY `id_shop` ASC';
        $configurations = Db::getInstance()->ExecuteS($query);
        
        if(is_array($configurations) && count($configurations))
        {
            foreach($configurations as $configuration)
            {
                $groupedConfigurations[$configuration['name'].($configuration['id_shop'] ? '_'.$configuration['id_shop'] : '')][] = $configuration['id_configuration'];
            }
        }
        
        if(is_array($groupedConfigurations) && count($groupedConfigurations))
        {
            foreach($groupedConfigurations as $groupedConfiguration)
            {
                /** IF NOT DUPLICATED */
                if(is_array($groupedConfiguration) && count($groupedConfiguration) <= 1)
                    continue;
                
                foreach($groupedConfiguration as $configurationKey => $id_configuration)
                {
                    /** SKIP FIRST CONFIGURATION FOUND */
                    if($configurationKey == 0)
                        continue;
                    
                    $configurationsToDelete[] = $id_configuration;
                }
            }
        }
        
        if(is_array($configurationsToDelete) && count($configurationsToDelete))
        {
            $query = 'DELETE FROM `'._DB_PREFIX_.'configuration` WHERE `id_configuration` IN ('.implode(',', $configurationsToDelete).')';
            $deleted = Db::getInstance()->Execute($query);
        }
        
        return;
    }
	
	public function getPSV()
	{
		return parent::getPSV();
	}
}