function fedex_preview_update_state(is_cart, id_state)
{
	$('select#fedex_dest_state'+is_cart+' option:not(:first-child)').remove();
	var fedex_states = fedex_countries[$('select#fedex_dest_country'+is_cart).val()];
	if(typeof(fedex_states) != 'undefined')
	{
		$(fedex_states).each(function (key, item){
			$('select#fedex_dest_state'+is_cart).append('<option value="'+item.id+'" '+(id_state == item.id?'selected':'')+'>'+item.name+'</option>');
		});
		$(".fedex_id_state"+is_cart).fadeIn('slow');
	}
	else
		$(".fedex_id_state"+is_cart).fadeOut('slow');
}