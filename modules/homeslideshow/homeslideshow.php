<?php

class Homeslideshow extends Module
{
	/** @var max image size */
	protected $maxImageSize = 307200;

	function __construct()
	{
		$this->name = 'homeslideshow';
		$this->tab = 'Tools';
		$this->version = '1.4';
		
		/* The parent construct is required for translations */
		parent::__construct();
		
		$this->page = basename(__FILE__, '.php');
		$this->displayName = $this->l('Home Slideshow');
		$this->description = $this->l('A slideshow module for your homepage');
		$this->modulepath=_MODULE_DIR_.$this->name;
	}

	function install()
	{
		if (!parent::install())
			return false;
			$this->installDb();
		// Trunk file if already exists with contents
		/*
		if (!$fd = @fopen(dirname(__FILE__).'/homeslideshow.xml', 'w'))
			return false;
		@fclose($fd);
		*/
		return $this->registerHook('home');
	}
  public function uninstall()	{
	
	Db::getInstance()->ExecuteS('DROP TABLE ' ._DB_PREFIX_ .'slideshow');
	}
  public function installDb()
  {
	  
    Db::getInstance()->ExecuteS('
    CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'slideshow` (
	 `id` int(4) NOT NULL AUTO_INCREMENT,
	 `imagePath` varchar(200) NOT NULL,
	 `title` varchar(500) NOT NULL,
	 `url` varchar(255) NOT NULL,
	 `order` int(4) NOT NULL,
	 PRIMARY KEY (`id`)
	) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;');
   
    return true;
  }

	function putContent($xml_data, $key, $field, $forbidden, $section)
	{
		foreach ($forbidden AS $line)
			if ($key == $line)
				return 0;
		if (!eregi('^'.$section.'_', $key))
			return 0;
		$key = eregi_replace('^'.$section.'_', '', $key);
		//$field = pSQL($field);
		$field = htmlspecialchars($field);
		if (!$field)
			return 0;
		return ("\n".'		<'.$key.'>'.$field.'</'.$key.'>');
	}

	function getContent()
	{
		/* display the module name */
		$this->_html = '<h2>'.$this->displayName.'</h2>';


        //deleting the data into the table
		if(isset($_GET['action']) && $_GET['action']=="delete")
		 {
			 $sql="delete from `"._DB_PREFIX_."slideshow` where id='".$_GET['id']."'";
			 if(Db::getInstance()->Execute($sql))
			   $this->_html.= $this->displayConfirmation("image deleted succesfully");
			 else
			   $errors[]="image not deleted ";
			 
		
		 }
		/* update the homeslideshow xml */
		if (isset($_POST['submitUpdate']))
		{
	 $imagePath=$_FILES["imagePath"]["name"];
	 $newfilename=time().".".$type[1];
	 $order=$_POST['order'];
	 $title=$_POST['title'];
	 $url=$_POST['url'];
	 $type=explode(".",$imagePath);
	
	 if(isset($_POST['hidden_id']) && $_POST['hidden_id']!='')//edit
	  {
		  if(isset($imagePath) && $imagePath!='')
		  {
			  if(move_uploaded_file($_FILES["imagePath"]["tmp_name"], dirname(__FILE__)."/images/" . $newfilename))
			   {
					$sql="update `"._DB_PREFIX_."slideshow` set `imagePath`='".$newfilename."',`title`='".$title."', `url`='".$url."', `order`='".$order."' where id='".$_POST['hidden_id']."'";
				$this->_html.= $this->displayConfirmation("Updated Successfully");
			   }
			   else
			   $errors[]= "File not uploaded. updation failed";
		  }
		  else
		  {
		  $sql="update `"._DB_PREFIX_."slideshow` set `title`='".$title."', `url`='".$url."', `order`='".$order."' where id='".$_POST['hidden_id']."'";
		  }
			
			if(Db::getInstance()->Execute($sql))
			   $this->_html.= $this->displayConfirmation("data updated succesfully");
			else
			   $errors[]= "data updation failed";

		
		 //die($newfilename);
		 
	  }
	  else//insert
	   {
		 if(move_uploaded_file($_FILES["imagePath"]["tmp_name"], dirname(__FILE__)."/images/". $newfilename))
		   {
			$sql="INSERT INTO `"._DB_PREFIX_."slideshow` (`id`, `imagePath`, `title`, `url`, `order`) VALUES (NULL, '".$newfilename."', '".$title."', '".$url."', '".$order."')";
			if(Db::getInstance()->Execute($sql) )
			   $this->_html.= $this->displayConfirmation("data inserted succesfully");
			/*else
			  $errors.= "data insertion failed";*/
		    }
		 else
		  {
			  $errors[]= "data insertion failed";
		  }
		 
       }      
	   
	   if ($errors)
			echo $this->displayError(implode('</ br>',$errors));
 
			
		}
       
		/* display the homeslideshow's form */
		$this->_displayForm();
		return $this->_html;

		
	}

	private function _displayForm()
	{
		global $cookie;
		//editing the data into the table
		if(isset($_GET['action']) && $_GET['action']=="edit")
		 {
			 $formdata=$this->getImageById($_GET['id']);
			  
		 }
		?>
<fieldset>
<legend>Slide Show List</legend>
<?php
   // getting the list of all the iamges
   $sql="select * from "._DB_PREFIX_."slideshow order by id";
   $result=mysql_query($sql);
   if(mysql_num_rows($result)>0)
   {
   ?>
<table class="table" width="100%">
    <tr>
            <th>Id</th>
            <th>Image</th>
            <th>Title</th>
            <th>Url</th>
            <th>Order</th>
            <th>Edit</th>
            <th>Delete</th>
    </tr>
<?php 

   while ($row=mysql_fetch_array($result))
    {
?>
<tr>
            <td><?php echo $row['id'] ;?></td>
            <td><img src="<?php echo $this->modulepath.'/images/'.$row['imagePath']; ?>" height="100" /></td>
            <td><?php echo $row['title'] ;?></td>
            <td><?php echo $row['url'] ;?></td>
            <td><?php echo $row['order'] ; ?></td>
            <td><a href="index.php?tab=AdminModules&configure=homeslideshow&action=edit&token=<?=$_REQUEST['token'];?>&id=<?php echo $row['id'];?>" > edit</a></td>
            <td><a href="index.php?tab=AdminModules&configure=homeslideshow&action=delete&token=<?=$_REQUEST['token'];?>&id=<?php echo $row['id'];?>" > delete</a></td>
    </tr>
    <?php
	}
   }
   else
   echo 'No data Inserted.';
	?>
</table>
</fieldset>

<fieldset>
<legend><?=$_GET['action']=='edit'?'Edit':'Add New';?></legend>
<form action="index.php?tab=AdminModules&configure=homeslideshow&token=<?=$_REQUEST['token'];?>" method="post" name="form1" id="form1" enctype="multipart/form-data">
<input type="hidden" name="hidden_id" name="hidden_id" value="<?php echo $formdata['id'];?>"  />
<label style="width:140px">Image</label>
   <?php if($formdata['imagePath']!=''){?> <div class="margin-form" style="padding-left: 160px;"><img src="<?=$this->modulepath.'/images/'.$formdata['imagePath'];?>" height="100" /></div><?php }?>
    <div class="margin-form" style="padding-left: 160px;"><input type="file" name="imagePath"  id="imagePath" /></div>
    
    <label style="width:140px">Title</label>
    <div class="margin-form" style="padding-left: 160px;"><input type="text" name="title" id="title" value="<?php echo $formdata['title'];?>" /></div>
    
    <label style="width:140px">Url</label>
    <div class="margin-form" style="padding-left: 160px;"><input type="text" name="url" id="url" value="<?php echo $formdata['url'];?>" /></div>
    
<label style="width:140px">Order</label>
    <div class="margin-form" style="padding-left: 160px;"><input type="text" name="order" id="order" value="<?php echo $formdata['order'];?>" /></div>
  <input type="submit" name="submitUpdate" id="submitUpdate" />
  </form>
</fieldset>
<?php
	}
	
	
	public function getImageById($id)
	{
		$result=Db::getInstance()->getRow("select * from "._DB_PREFIX_."slideshow where id='".$id."'");
        return $result;
	}
   public	function getImages()
	{
        $result=Db::getInstance()->ExecuteS("select * from "._DB_PREFIX_."slideshow order by `order` desc");
        return $result;
	}


	function hookHome($params)
	{

				global $cookie, $smarty;
				$smarty->assign(array(
					'this_path' => $this->_path,
					'slideimages'=> $this->getImages()
				));
				return $this->display(__FILE__, 'homeslideshow.tpl');

		return false;
	}
	
 public function hooktop($param)
  {
		global $smarty;
		
    return $this->hookHome($param);
  }

}
