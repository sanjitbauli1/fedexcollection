<!-- Module Homeslideshow -->
{literal}



<script type="text/javascript" src="http://jqueryjs.googlecode.com/files/jquery-1.2.6.pack.js"></script>

<script type="text/javascript">

$(document).ready(function(){

  var currentPosition = 0;

  var slideWidth = 665;

  var slides = $('.slide');

  var numberOfSlides = slides.length;



  // Remove scrollbar in JS

  $('#slidesContainer').css('overflow', 'hidden');



  // Wrap all .slides with #slideInner div

  slides

    .wrapAll('<div id="slideInner"></div>')

    // Float left to display horizontally, readjust .slides width

	.css({

      'float' : 'left',

	  'margin': '0',

      'width' : slideWidth,
    });



  // Set #slideInner width equal to total width of all slides

  $('#slideInner').css('width', slideWidth * numberOfSlides);



  // Insert controls in the DOM

  $('#slideshow')

    .prepend('<span class="control" id="leftControl" style="z-index:1000; left:4px">Clicking moves left</span>')

    .append('<span class="control" id="rightControl">Clicking moves right</span>');



  // Hide left arrow control on first load

  manageControls(currentPosition);



  // Create event listeners for .controls clicks

  $('.control')

    .bind('click', function(){

    // Determine new position

	currentPosition = ($(this).attr('id')=='rightControl') ? currentPosition+1 : currentPosition-1;

    

	// Hide / show controls

    manageControls(currentPosition);

    // Move slideInner using margin-left

    $('#slideInner').animate({

      'marginLeft' : slideWidth*(-currentPosition)

    });

  });



  // manageControls: Hides and Shows controls depending on currentPosition

  function manageControls(position){

    // Hide left arrow if position is first slide

	if(position==0){ $('#leftControl').hide() } else{ $('#leftControl').show() }

	// Hide right arrow if position is last slide

    if(position==numberOfSlides-1){ $('#rightControl').hide() } else{ $('#rightControl').show() }

  }	

});

</script>

<!--[if lte IE 6]>

<script type="text/javascript" src="supersleight-min.js"></script>

<![endif]-->

{/literal}
  <div id="welcomeHero">
			
		<div id="homepage-gab1">
      <div id="offerbloc-principal">
        <div id="flashcontent">
        <script type="text/javascript" src="http://code.jquery.com/jquery-1.4.min.js"></script>
        <script src="js/allMinified-jQueryFiles.js" type="text/javascript"></script>
          <script type="text/javascript"> var path_for_js = "null";  </script>
          <link rel="stylesheet" type="text/css" href="css/hp.css">
          <script type="text/javascript" src="js/javascript_hp.js"></script>
          <script type="text/javascript" src="js/hppreload.js"></script>
          <script type="text/javascript" src="js/TweenMax.min.js"></script>
          <div id="MAINHP01">
            <div id="preload"><img src="images/preloadback.jpg" ></div>
            <div id="wait"><img src="images/loading.gif" alt="chargement..."></div>
            <div id="block">
              <div id="hps_main">
                <div id="mainscreen"> <img id="hp1" src="images/screen1.jpg"> <img id="hp2" src="images/screen2.jpg"> <img id="hp3" src="images/screen3.jpg"> <img id="hp4" src="images/screen4.jpg"> <img id="hp5" src="images/screen5.jpg">
                  <button type="button" id="yr-bigbtn"> </button>
                  <button type="button" id="bottomhp-button"></button>
                  <button type="button" id="mainmenu-button5"></button>
                  <button type="button" id="mainmenu-button4"></button>
                  <button type="button" id="mainmenu-button3"></button>
                  <button type="button" id="mainmenu-button2"></button>
                  <button type="button" id="mainmenu-button1"></button>
                </div>
                <div id="hp1_adds"> <img id="logomaq" src="images/log1.png">
                  <div id="addblock"> <img id="backg" src="images/acc1.jpg"> <img id="arrow_menuhp1" src="images/arrow_hp.png"> </div>
                </div>
                <div id="hp2_adds">
                  <div id="addblock2"> <img id="backg2" src="images/acc2.jpg"> <img id="arrow_menuhp2" src="images/arrow_hp.png"> </div>
                  <img id="logomaq2" src="images/log2.png"> </div>
                <div id="hp3_adds">
                  <div id="addblock3"> <img id="backg3" src="images/acc3.jpg"> <img id="arrow_menuhp3" src="images/arrow_hp.png"> </div>
                  <img id="logomaq3" src="images/log3.png"> </div>
                <div id="hp4_adds">
                  <div id="addblock4"> <img id="backg4" src="images/acc4.jpg"> <img id="arrow_menuhp4" src="images/arrow_hp.png"> </div>
                  <img id="logomaq4" src="images/log4.png"> </div>
                <div id="hp5_adds">
                  <div id="addblock5"> <img id="backg5" src="images/acc5.jpg"> <img id="arrow_menuhp5" src="images/arrow_hp.png"> </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
				
		
		
		<div class="clear"></div>
		
	</div>

		<!--<img src="{$img_dir}slider.gif" alt="slider" /> -->
        </div>
  