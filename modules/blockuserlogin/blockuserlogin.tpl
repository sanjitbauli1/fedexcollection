{*

*}

<!-- Block user login module -->
{if !$logged && $page_name!='authentication'}
<div id="block_user" class="block">
	<h4>{l s='Company Store' mod='blockuserlogin'}</h4>
    <div class="block_content">
    {include file="$tpl_dir./errors.tpl"}
    
    {if !$logged && $page_name!='authentication'}
    <form action="{$base_dir}authentication.php" method="post" id="login_form" class="std">
		<fieldset>
			<h3>Welcome and Log In</h3>
			<p class="text">
				<!--<label for="email">User ID</label>-->
                
				<span><label>E-mail Address:</label><input type="text" id="email" name="email" value="User ID" onclick="javascript: this.value=''" onblur="javascript: if(this.value=='') this.value=' User ID'" class="account_input" autocomplete="off"></span>
                <div class="clear"></div>
			</p>
			<p class="text">
				<!--<label for="passwd">Password</label>-->
				<span><label>Password:</label><input type="password" id="passwd" name="passwd" value="Password" onclick="javascript: this.value=''" onblur="javascript: if(this.value=='') this.value=' Password'" class="account_input" autocomplete="off"></span></p>
                <div class="clear"></div>
			
            <!--<p class="text">
            	<label for="passwd">I want to</label>
                <span><select name=""><option>1</option><option>1</option></select></span>
            </p>-->
			<p class="submit">
				<a href="{$base_dir}password.php">Forgot your password?</a><input type="hidden" class="hidden" name="back" value="my-account.php">				<input type="submit" id="SubmitLogin" name="SubmitLogin" class="button" value="Login">
			</p>
            <div class="clear"></div>
			<!--<p class="lost_password"><a href="{$base_dir}password.php">Forgot your password?</a></p>-->
            <div id="new-customer">
					<!--<a href="{$base_dir}authentication.php" class="blk" id="register-now">Register now</a>-->
					<h3><a href="{$base_dir}authentication.php">New Customer?</a></h3>
                      <div>
                    <a href="#">Employee</a>
                    <a href="#">Consumer</a>
                    <a href="#" style="padding:0">Contractor &amp; GSP's</a>
                </div>
                    <div class="clear"></div>
                  
				</div>
		</fieldset>
	</form>
    {else}
 
    
		<ul id="block_user_login">
	  		<li>
	  			<img src="{$base_dir}modules/blockuserlogin/login.png"/>
            	{l s='Welcome' mod='blockuserlogin'},
				{if $logged}
				
				{else}
				<a href="{$base_dir_ssl}my-account.php">{l s='Log in' mod='blockuserlogin'}</a>
				{/if}
			</li>
        	
  			<!--<li id="your_account">
  				<img src="{$base_dir}modules/blockuserlogin/user.png"/>
				<a href="{$base_dir_ssl}my-account.php" title="{l s='Your Account' mod='blockuserlogin'}">{l s='Your Account' mod='blockuserlogin'}</a>
        	</li>-->
		</ul>
        {/if}
    </div>
       
</div>
{/if}
{if !$logged}
{literal}
<style>
#left_column .block,#featured-products_block_center{ display:none}
#left_column #block_user{ display:block;}

</style>
{/literal}
{/if}



<!-- /Block user login module -->