<?php
/*
 * This is a replacement for the blockuserinfo module.
 * It is intended to run in the side tabs, but Header and Top are also hooked. 
 *
 * Dave Wesson 26 March 2011
*/
class BlockUserLogin extends Module
{
	public function __construct()
	{
		$this->name = 'blockuserlogin';
		$this->tab = 'front_office_features';
		$this->author = 'Dave Wesson';
		$this->version = 1.0;

		parent::__construct();
		
		$this->displayName = $this->l('User login block');
		$this->description = $this->l('Adds a side column block that allows the customer to log in');
		
	}

	public function install()
	{
		if (!parent::install())
			return false;
		if (!$this->registerHook('leftColumn'))
			return false;
		if (!$this->registerHook('header'))
			return false;
		return true;
	}

	/**
	* Returns module content for header
	*
	* @param array $params Parameters
	* @return string Content
	*/

    function hookLeftColumn($params)
	{
        global $smarty, $cookie;
		if(isset($_REQUEST['error'])){
			$errors[]='Please Login to proceed.';
		}
		
		$smarty->assign(array(
			'logged' => $cookie->isLogged(),
			'customerName' => ($cookie->logged ? $cookie->customer_firstname.' '.$cookie->customer_lastname : false),
			'firstName' => ($cookie->logged ? $cookie->customer_firstname : false),
			'lastName' => ($cookie->logged ? $cookie->customer_lastname : false),
			'errors' =>$errors
		));
		return $this->display(__FILE__, 'blockuserlogin.tpl');
	}

	function hookRightColumn($params)
	{
		return $this->hookLeftColumn($params);
	}
	function hookHeader($params)
	{
		  global $smarty, $cookie;
		$allow_pages=array('index','my-account','password','authentication','cms','contact-form');
		if(!$cookie->isLogged() && !in_array($smarty->tpl_vars['page_name']->value,$allow_pages)){
		$errors[] = Tools::displayError('authentication failed');
		$smarty->assign('errors',$errors);
	Tools::redirect(__PS_BASE_URI__.'authentication.php');
		}
		
		//return $this->hookLeftColumn($params);
	}
	function hookTop($params)
	{
		return $this->hookLeftColumn($params);
	}


function loginUser(){
	global $smarty,$cookie;
	if (Tools::isSubmit('SubmitLogin'))
{
$passwd = trim(Tools::getValue('passwd'));
$email = trim(Tools::getValue('email'));
if (empty($email))
  $errors[] = Tools::displayError('e-mail address is required');
elseif (!Validate::isEmail($email))
  $errors[] = Tools::displayError('invalid e-mail address');
elseif (empty($passwd))
  $errors[] = Tools::displayError('password is required');
elseif (Tools::strlen($passwd) > 32)
  $errors[] = Tools::displayError('password is too long');
elseif (!Validate::isPasswd($passwd))
  $errors[] = Tools::displayError('invalid password');
else
{
  $customer = new Customer();
  $authentication = $customer->getByEmail(trim($email), trim($passwd));
  /* Handle brute force attacks */
  sleep(1);
  if (!$authentication OR !$customer->id)
   $errors[] = Tools::displayError('authentication failed');
   else
  {
   $cookie->id_customer = intval($customer->id);
   $cookie->customer_lastname = $customer->lastname;
   $cookie->customer_firstname = $customer->firstname;
   $cookie->logged = 1;
   $cookie->passwd = $customer->passwd;
   $cookie->email = $customer->email;
   if (Configuration::get('PS_CART_FOLLOWING') AND (empty($cookie->id_cart) OR Cart::getNbProducts($cookie->id_cart) == 0))
    $cookie->id_cart = intval(Cart::lastNoneOrderedCart(intval($customer->id)));
   Module::hookExec('authentication');
   if ($back = Tools::getValue('back'))
    Tools::redirect($back);
   Tools::redirect('my-account.php');
  }

		}
	 }
	}
}

?>