<?php
/*
  Copyright (C) 2011-2012 SC Minic Studio S.R.L, office@minic.ro

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
 */
  
include_once('../../config/config.inc.php');
include_once('../../init.php');
include_once('minicslider.php');

$minicSlider = new MinicSlider();

if (!Tools::isSubmit('secure_key') || Tools::getValue('secure_key') != $minicSlider->secure_key || !Tools::getValue('action'))
	die(1);

$success = false;

if (Tools::getValue('action') == 'updateOrder' && Tools::getValue('slides')){  
	parse_str(Tools::getValue('slides'), $slides);	
	$i = 0; 	

	foreach ($slides['order'] as $key => $slide){		
		$i++;		
		$id = split('h', $slide);			

		if(Db::getInstance()->Execute('UPDATE `'._DB_PREFIX_.'minic_slider` SET order_id = '.$i.' WHERE slide_id = '.$id[0])){     
			$success = true;    
		}else{     
			$success = false;    
		}	
	}
}
if($success){
	echo '{"success" : "true"}';
}else{  
	echo '{"success" : "false"}';
}

?>