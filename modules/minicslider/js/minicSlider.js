/*
  Copyright (C) 2011-2012 minic studio, levykee@gmail.com

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
 */

function GetBrowser(){$.browser.chrome=/chrome/.test(navigator.userAgent.toLowerCase());if($.browser.chrome)return"chrome";if($.browser.mozilla)return"mozilla";if($.browser.opera)return"opera";if($.browser.safari)return"safari";if($.browser.msie)return"ie"}jQuery(document).ready(function(a){a("#donate").click(function(b){b.preventDefault();a("#paypal").submit()});var b=a("#feedbackVersion").val();a.getJSON("http://module.minic.ro/slider/feed.php?callback=?",function(c){if(c.update.version!=b)a("#sliderBanner").empty().html(c.update.content);else if(c.news.news=="true")a("#sliderBanner").empty().html(c.news.content);else return false});a(".ghost-text").ghostText();a(".tooltip").tipsy({fade:true,gravity:"w",offset:20});a(".image-chooser").click(function(){a("#image-chooser").trigger("click")});a(".slider_navigation a").click(function(b){b.preventDefault();a(a(this).attr("href")).toggleClass("active").slideToggle();if(a(a(this).attr("href")).hasClass("active"))a.scrollTo(a(a(this).attr("href")),500)});a(".hidden-div legend").click(function(){a(this).parent("fieldset").removeClass("active").slideToggle()});a(".slides_holder").width((900+15)*a(".languages").size());a(".slides_holder").animate({"margin-left":(-900-15)*a(".navigation.active").index()+"px"});a(".slide_header").click(function(){a(this).toggleClass("active").parent().children(".slide_body").slideToggle();a(this).children(".arrow").toggleClass("down")});a("a.navigation").click(function(b){b.preventDefault();a(".navigation.active").removeClass("active");a(this).addClass("active");var c=a(this).attr("href");var d=a(c).index();a(".slides_holder").animate({"margin-left":(-900-15)*d+"px"})});a(".cb-enable").click(function(){a(this).parent().children(".selected").removeClass("selected");a(this).addClass("selected");a(this).parent().children("input").attr("checked",true).val(1)});a(".cb-disable").click(function(){a(this).parent().children(".selected").removeClass("selected");a(this).addClass("selected");a(this).parent().children("input").attr("checked",true).val(0)});a("#add").click(function(){return!a("#select1 option:selected").remove().appendTo("#select2")});a("#remove").click(function(){return!a("#select2 option:selected").remove().appendTo("#select1")});a("#submitOptions").click(function(){a("#slider_options").find("option").each(function(){a(this).attr("selected","selected")})});a("input#delete-slide").click(function(b){if(a(this).hasClass("disabled")){b.preventDefault();a(this).removeClass("disabled");a(".confirmation_window").fadeIn(200)}});a("a.confirm-delete").click(function(b){b.preventDefault();a("input#delete-slide").trigger("click")});a("a.deny-delete").click(function(b){b.preventDefault();a("input#delete-slide").addClass("disabled");a(".confirmation_window").fadeOut(300)});var c=GetBrowser();if(c=="chrome")a(".image-chooser").addClass("chrome");else a(".image-chooser").addClass("not-chrome")});