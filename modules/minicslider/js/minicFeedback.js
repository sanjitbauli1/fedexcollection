/*
  Copyright (C) 2011-2012 minic studio, levykee@gmail.com

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
 */
 
function showResponse(a,b){$(".active_response").hide().removeClass("active_response");a.addClass("active_response");a.children("p").html(b);a.show().delay(1e4).fadeOut(function(){$(this).removeClass("active_response")})}jQuery(document).ready(function(a){var b;var c;var d;var e=a("#fixed_conf");var f=a("#fixed_error");var g="";a("#submitBug").click(function(g){g.preventDefault();b=a("#bug");c=b.find(".conf").text();if(a("#bugName").val()==""||a("#bugMessage").val()==""){c=b.find(".empty").text();showResponse(f,c);return false}a.getJSON("http://www.module.minic.ro/slider/process.php?"+"name="+a("#bugName").val()+"&email="+a("#bugEmail").val()+"&site="+a("#bugSite").val()+"&message="+a("#bugMessage").val()+"&psversion="+a("#bugPsVersion").val()+"&version="+a("#bugVersion").val()+"&server="+a("#bugServer").val()+"&php="+a("#bugPhp").val()+"&mysql="+a("#bugMysql").val()+"&theme="+a("#bugTheme").val()+"&userinfo="+a("#bugUserInfo").val()+"&action=bug&callback=?",function(a){}).success(function(a){d=a;if(d.error=="true"){c=b.find(".error").text();showResponse(f,c);return false}}).error(function(){c=b.find(".error").text();showResponse(f,c)}).complete(function(){showResponse(e,c)})});a("#submitFeedback").click(function(h){h.preventDefault();b=a("#feedback");c=b.find(".conf").text();if(a("#feedbackMessage").val()==""||a("#feedbackName").val()==""){c=b.find(".empty").text();showResponse(f,c);return false}if(a("#feedbackEmailAgree").is(":checked"))g=a("#feedbackEmail").val();a.getJSON("http://www.module.minic.ro/slider/process.php?"+"name="+a("#feedbackName").val()+"&email="+g+"&site="+a("#feedbackDomain").val()+"&message="+a("#feedbackMessage").val()+"&psversion="+a("#feedbackPsVersion").val()+"&version="+a("#feedbackVersion").val()+"&action=feedback&callback=?",function(a){}).success(function(a){d=a;if(d.error=="true"){c=b.find(".error").text();showResponse(f,c)}else showResponse(e,c)}).error(function(){}).complete(function(){})});a(".response .close").click(function(){a(this).parent(".response").hide().removeClass("active_response")})});
