{*
  Copyright (C) 2011-2012 SC Minic Studio S.R.L, office@minic.ro

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
 *}
 
<fieldset id="bug" class="hidden-div">
    <legend>{l s='Bug report'}</legend>
    <form id="formBug" class="" method="post">
        <div id="bugInfoCont" class="container">
            <div>
                <label>{l s='Your name'}:</label>
                <input id="bugName" class="name" type="text" name="name" value="{$slider.info.name}" size="50" />
            </div>
            <div>
                <label>{l s='Your email'}:</label>
                <input id="bugEmail" class="" type="text" name="email" size="50" value="{$slider.info.email}" />
            </div>
            <div>
                <label>{l s='Site address'}:</label>
                <input id="bugSite" class="" type="text" name="domain" size="50" value="{$slider.info.domain}" />
            </div>
            <div>
                <label>{l s='Message'}:</label>
                <textarea id="bugMessage" class="message" name="bugMessage" rows="10" cols="49"></textarea>
            </div>
        </div>
        <div class="comments"> 
            <h3>{l s='Notes'}</h3>
            <p>{l s='Please describe the bug with as much detail as it`s possible, so we can understand the problem easier.'}</p>
            <h3>{l s="Important!"}</h3>
            <p>{l s='By clicking to the "Send" button you agree, that we will get some basic information.'}</p>
            <ul>
                <li>{l s='Your shop`s name'}: <b>{$slider.info.name}</b></li>
                <li>{l s='Your shops e-mail address. IMPORTANT: we just need in case of communicaton!'}: <b>{$slider.info.email}</b></li>
                <li>{l s='Your shops domain'}: <b>{$slider.info.domain}</b></li>
                <li>{l s='prestashop version'}: <b>{$slider.info.psVersion}</b></li>
                <li>{l s='module version'}: <b>{$slider.info.version}</b></li>
                <li>{l s='server version'}: <b>{$slider.info.server}</b></li>
                <li>{l s='php version'}: <b>{$slider.info.php}</b></li>
                <li>{l s='mysql version'}: <b>{$slider.info.mysql}</b></li>
                <li>{l s='theme name'}: <b>{$slider.info.theme}</b></li>
                <li>{l s='browser version'}: <b>{$slider.info.userInfo}</b></li>
            </ul>
        </div>
        <div class="button_cont">
            <input id="submitBug" type="submit" name="submitBug" value="Send" class="button green" />
            <input id="bugPsVersion" type="hidden" value="{$slider.info.psVersion}" name="psversion" />
            <input id="bugVersion" type="hidden" value="{$slider.info.version}" name="version" />
            <input id="bugServer" type="hidden" value="{$slider.info.server}" name="server" />
            <input id="bugPhp" type="hidden" value="{$slider.info.php}" name="php" />
            <input id="bugMysql" type="hidden" value="{$slider.info.mysql}" name="mysql" />
            <input id="bugTheme" type="hidden" value="{$slider.info.theme}" name="theme" />
            <input id="bugUserInfo" type="hidden" value="{$slider.info.userInfo}" name="userInfo" />
        </div>
    </form>
    <div class="response" style="display:none;">
        <span class="conf">{l s='Message sent successfull! Thank you for your time.'}</span>
        <span class="error">{l s='Something went wrong, please try again later.'}</span>
        <span class="empty">{l s='Name and Message is required!'}</span>
    </div>
</fieldset>