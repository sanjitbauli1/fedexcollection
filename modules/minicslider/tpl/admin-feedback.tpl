{*
  Copyright (C) 2011-2012 SC Minic Studio S.R.L, office@minic.ro

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
 *}
 
<fieldset id="feedback" class="hidden-div">
    <legend>{l s='Feedback'}</legend>
	<form id="formFeed" class="" method="post">
        <div id="feedInfoCont" class="container">
    		<div>
    			<label>{l s='Your name'}:</label>
    			<input id="feedbackName" class="name" type="text" name="name" value="{$slider.info.name}" size="50" />
    		</div>
    		<div>
    			<label>{l s='Your email'}:</label>
    			<input id="feedbackEmail" class="" type="text" name="email" value="{$slider.info.email}" size="50" />
                <input id="feedbackEmailAgree" type="checkbox" checked="checked" value="1" />
    		</div>
    		<div>
    			<label>{l s='Site address'}:</label>
    			<input id="feedbackDomain" class="" type="text" name="domain" value="http://{$slider.info.domain}" size="50" />
    		</div>
            <div>
                <label>{l s='Message'}:</label>
                <textarea id="feedbackMessage" class="message" name="feedbackMessage" rows="10" cols="49"></textarea>
            </div>
        </div>
        <div class="comments"> 
            <h3>{l s='Notes'}</h3>
            <p>{l s='Feel free to give us a feedback about our work (we really like to hear few words) or write down your idea / request and if we think its good we`ll consider to implement into future versions.'}</p>
            <h3>{l s='Important!'}</h3>
            <p>{l s='By clicking to the "Send" button you agree, that we will get some basic information. If you don`t want to send your e-mail address uncheck the checkbox.'}</p>
            <ul>
                <li>{l s='Your shop`s name'}: <b>{$slider.info.name}</b></li>
                <li>{l s='Your shops e-mail address. IMPORTANT: we just need in case of communicaton!'}: <b>{$slider.info.email}</b></li>
                <li>{l s='Your shops domain'}: <b>{$slider.info.domain}</b></li>
                <li>{l s='prestashop version'}: <b>{$slider.info.psVersion}</b></li>
                <li>{l s='module version'}: <b>{$slider.info.version}</b></li>
            </ul>
        </div>
        <div class="button_cont">
            <input id="submitFeedback" type="submit" name="submitFeedback" value="Send" class="button green" />
            <input id="feedbackPsVersion" type="hidden" value="{$slider.info.psVersion}" name="psversion" />
            <input id="feedbackVersion" type="hidden" value="{$slider.info.version}" name="version" />
        </div>
	</form>
    <div class="response" style="display:none;">
        <span class="conf">{l s='Message sent successfull! Thank you for your time.'}</span>
        <span class="error">{l s='Something went wrong, please try again later.'}</span>
        <span class="empty">{l s='Name and Message is required!'}</span>
    </div>
</fieldset>