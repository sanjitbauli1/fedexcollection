{*
  Copyright (C) 2011-2012 SC Minic Studio S.R.L, office@minic.ro

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
 *}
 
<fieldset id="new_slide" class="hidden-div">
    <legend class="asd">{l s='Add New Slide'}</legend> 
    <form method="post" action="{$slider.postAction}" enctype="multipart/form-data">        
        <div id="new">
            <div id="new-slide">
                <div class="title input">
                    <label>{l s='Title'}</label>
                    <input type="text" name="title" size="41" class="ghost-text tooltip" value="{l s='The title of the slide'}" title="{l s='This will be the title on the slide.'}" /> 
                </div>
                <div class="url input">
                    <label>{l s='Url'}</label>
                    <input type="text" name="url" size="41" class="ghost-text tooltip" value="{l s='Link of the slide'}" title="{l s='ex. http://myshop.com/promotions'}" />           
                </div>
                <div class="target">
                    <label>{l s='Blank target'}</label>
                    <input type="checkbox" name="target" class="tooltip" value="1" title="{l s='Check this if you want to open the link in new window.'}" />
                </div>
                <div class="image input">
                    <label>{l s='Image'}</label>
                    <input type="file" name="image" size="29" id="image-chooser" class="tooltip" title="{l s='Choose an image, only .jpg, .png, .gif are allowed.'}" />
                </div>
                <div class="imageName input">
                    <label>{l s='Image name'}</label>
                    <input type="text" name="imageName" size="41" class="ghost-text tooltip" value="{l s='Image name'}" title="{l s='Optional! The name of the uploaded image without extension.'}" />           
                </div>
                {if $slider.options.single == 1}
                    <div class="language">
                        <label>{l s='Language'}</label>
                        <select name="language" class="tooltip" title="{l s='The language of the slide.'}">
                            {foreach from=$slider.lang.all item=lang}
                                <option value="{$lang.id_lang}" {if $lang.id_lang == $slider.lang.default.id_lang}selected="selected"{/if}>{$lang.name}</option>
                            {/foreach}
                        </select>
                    </div>
                {/if}
                <div class="alt input">
                    <label>{l s='Image alt'}</label>
                    <input type="text" name="alt" size="41" class="ghost-text tooltip" value="{l s='An alternate text for the image'}" title="{l s='The image alt, alternate text for the image'}" />
                </div>
                <div class="caption"> 
                    <label>{l s='Caption'}</label>
                    <textarea type="text" name="caption" cols=40 rows=6 class="ghost-text tooltip" title="{l s='Be carefull, too long text isnt good and FULL HTML is allowed.'}">{l s='The slide text'}</textarea>                      
                </div>
            </div> 
        <div class="comments"> 
            <h3>{l s="Few important notes"}</h3>
            <p>{l s='The Nivo Slider is now'} <b><a href="http://nivo.dev7studios.com/2012/05/30/the-nivo-slider-is-responsive/" target="_blank">{l s='responsive'}</a></b>! This mean that the images will be resized! The best if your images is the same in size!</p>
            <p>{l s='If you dont want title and/or captions text than leave empty those fields.'}</p>
            <p>{l s='The images wont be resized automatically to the same size, you need to resize them manually!'}</p>
            <p>{l s='You can upload different sized images for different language.'}</p>
            <p>{l s='If you want you can upload the same image to different language. They will be renamed but its better if you give them a normal name.'}</p>
            <p>{l s='More than 8 image looks ugly if you use thumbnails in center column.'}</p>
        </div>
        <div class="button_cont">
            <input type="submit" name="submitNewSlide" value="{l s='Add Slide'}" class="green" />
            {if $slider.options.single == 0}
                <input type="hidden" name="language" value="{$slider.lang.default.id_lang}" />
            {/if}
         </div>      
	    </div>
    </form>
</fieldset>