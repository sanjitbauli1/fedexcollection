{*
  Copyright (C) 2011-2012 SC Minic Studio S.R.L, office@minic.ro

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
 *}

<!-- MINIC SLIDER -->	
{if $minicSlider.options.single == 0}
    {if $slides.$lang_iso|@count != 0}
</div>  
    </div>
        <div id="minic_slider" class="theme-default{if $minicSlider.options.thumbnail == 1 and $minicSlider.options.control != 0} controlnav-thumbs{/if}">   
            <div id="slider" class="nivoSlider" style="{if $minicSlider.options.width}width:{$minicSlider.options.width}px;{/if}{if $minicSlider.options.height}height:{$minicSlider.options.height}px{/if}{if $minicSlider.options.control != 1}margin-bottom:0;{/if}">
                {foreach from=$slides.$defaultLangIso item=image name=singleimage}
                    {if $image.url != ''}<a href="{$image.url}" {if $image.target == 1}target="_blank"{/if}>{/if}
                        <img src="{$minicSlider.path.images}{$image.image}" class="slider_image" 
                            {if $image.alt}alt="{$image.alt}"{/if}
                            {if $image.title != '' or $image.caption != ''}title="#htmlcaption_{$image.slide_id}"{/if} 
                            {if $minicSlider.options.thumbnail == 1}data-thumb="{$minicSlider.path.thumbs}{$image.image}"{/if}/>
                    {if $image.url != ''}</a>{/if}
                {/foreach}
            </div>
            {foreach from=$slides.$defaultLangIso item=caption name=singlecaption}
                {if $caption.title != '' or $caption.caption != ''}
                    <div id="htmlcaption_{$caption.slide_id}" class="nivo-html-caption">
                        <h3>{$caption.title}</h3>
                        <p>{$caption.caption}</p>
                    </div>
                {/if}
            {/foreach}
        </div>  
<div style="display:none;">
    <div>
    {/if}
{else}
    {if $slides.$lang_iso|@count != 0}
</div>  
    </div>
    <div id="minic_slider" class="theme-default{if $minicSlider.options.thumbnail == 1 and $minicSlider.options.control != 0} controlnav-thumbs{/if}">
        {foreach from=$slides key=iso item=slide}
            {if $iso == $lang_iso and $iso|@count != 0}
                <div id="slider" class="nivoSlider" style="{if $minicSlider.options.width}width:{$minicSlider.options.width}px;{/if}{if $minicSlider.options.height}height:{$minicSlider.options.height}px{/if}{if $minicSlider.options.control != 1}margin-bottom:0;{/if}">
                    {foreach from=$slide item=image name=images}
                        {if $image.url != ''}<a href="{$image.url}">{/if}
                            <img src="{$minicSlider.path.images}{$image.image}" class="slider_image" alt="{$image.alt}" 
                                {if $image.title != '' or $image.caption != ''}title="#htmlcaption_{$image.slide_id}"{/if} 
                                {if $minicSlider.options.thumbnail == 1}data-thumb="{$minicSlider.path.thumbs}{$image.image}"{/if}/>
                        {if $image.url != ''}</a>{/if}
                    {/foreach}
                </div>
                {foreach from=$slide item=caption name=captions}
                    {if $caption.title != '' or $caption.caption != ''}
                        <div id="htmlcaption_{$caption.slide_id}" class="nivo-html-caption">
                            <h3>{$caption.title}</h3>
                            <p>{$caption.caption}</p>
                        </div>
                    {/if}
                {/foreach}
            {/if}
        {/foreach}
    </div>
<div style="display:none;">
    <div>
    {/if}
{/if}    
<script type="text/javascript">
$(window).load(function() {
    $('#slider').nivoSlider({
        effect: '{if $minicSlider.options.current != ''}{$minicSlider.options.current}{else}random{/if}', 
        slices: {if $minicSlider.options.slices != ''}{$minicSlider.options.slices}{else}15{/if}, 
        boxCols: {if $minicSlider.options.slices != ''}{$minicSlider.options.cols}{else}8{/if}, 
        boxRows: {if $minicSlider.options.rows != ''}{$minicSlider.options.rows}{else}4{/if}, 
        animSpeed: {if $minicSlider.options.speed != ''}{$minicSlider.options.speed}{else}500{/if}, 
        pauseTime: {if $minicSlider.options.pause != ''}{$minicSlider.options.pause}{else}3000{/if}, 
        startSlide: {if $minicSlider.options.startSlide != ''}{$minicSlider.options.startSlide}{else}0{/if},
        directionNav: {if $minicSlider.options.buttons == 1}true{else}false{/if}, 
        controlNav: {if $minicSlider.options.control == 1}true{else}false{/if}, 
        controlNavThumbs: {if $minicSlider.options.thumbnail == 1}true{else}false{/if},
        pauseOnHover: {if $minicSlider.options.hover == 1}true{else}false{/if}, 
        manualAdvance: {if $minicSlider.options.manual == 1}true{else}false{/if}, 
        prevText: '{l s='Prev'}', 
        nextText: '{l s='Next'}', 
        randomStart: {if $minicSlider.options.random == 1}true{else}false{/if}
    });
});
</script>