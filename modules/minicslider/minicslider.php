<?php
/*
  Copyright (C) 2011-2012 SC Minic Studio S.R.L, office@minic.ro

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
 */
  
if (!defined('_PS_VERSION_'))
  exit;
class MinicSlider extends Module
{
	protected $maxImageSize = 1048576;

	public function __construct()
	    {
		    $this->name = 'minicslider';
		    $this->tab = 'advertising_marketing';
		    $this->version = '2.4.3';
		    $this->author = 'minic studio';
		    $this->need_instance = 1;
			$this->secure_key = Tools::encrypt($this->name);
	
		    parent::__construct();
	
		    $this->displayName = $this->l('minic slider - powered with Nivo jQuery Slider');
		    $this->description = $this->l('Powerfull image slider for advertising.');
	    }
	
	private function installDB()
		{
	
			Db::getInstance()->Execute('DROP TABLE IF EXISTS `'._DB_PREFIX_.'minic_slider`');
    		Db::getInstance()->Execute('DROP TABLE IF EXISTS `'._DB_PREFIX_.'minic_options`');
	
			if (!Db::getInstance()->Execute('
				CREATE TABLE `'._DB_PREFIX_.'minic_slider` (
					slide_id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
					lang_id INT NOT NULL,
					lang_iso VARCHAR(5),
					title VARCHAR(100),
					url VARCHAR(100),
					target INT NOT NULL DEFAULT 0,
					image VARCHAR(100),
					alt VARCHAR(100),
					caption VARCHAR(300),
					order_id INT NOT NULL,
					active INT NOT NULL DEFAULT 1
			    ) ENGINE = '._MYSQL_ENGINE_))
				return false;
	
			if (!Db::getInstance()->Execute('
				CREATE TABLE `'._DB_PREFIX_.'minic_options` (
					id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
					effect VARCHAR(200),
					current VARCHAR(200),
					slices INT NOT NULL DEFAULT 15,
					cols INT NOT NULL DEFAULT 8,
					rows INT NOT NULL DEFAULT 4,
					speed INT NOT NULL DEFAULT 500,
					pause INT NOT NULL DEFAULT 3000,
					manual INT NOT NULL DEFAULT 0,
					hover INT NOT NULL DEFAULT 1,
					buttons INT NOT NULL DEFAULT 1,
					control INT NOT NULL DEFAULT 1,
					thumbnail INT NOT NULL DEFAULT 0,
					random INT NOT NULL DEFAULT 0,
					start_slide INT NOT NULL DEFAULT 0,
					single INT NOT NULL DEFAULT 0,
					width INT NOT NULL DEFAULT 0,
					height INT NOT NULL DEFAULT 0
		        ) ENGINE = '._MYSQL_ENGINE_))
				return false;	
			return true;
		}
	
	private function insertOptions()
		{
			if (!Db::getInstance()->Execute('
				INSERT INTO `'._DB_PREFIX_.'minic_options` (
					`effect`
				) VALUES (
					"sliceDown,sliceDownLeft,sliceUp,sliceUpLeft,sliceUpDown,sliceUpDownLeft,fold,slideInRight,slideInLeft,boxRandom,boxRain,boxRainReverse,boxRainGrow,boxRainGrowReverse,fade");'))
				return false;
			return true;
		}
	
	public function install()
	    {
			if (parent::install() && $this->installDB() && $this->insertOptions() && $this->registerHook('top') && $this->registerHook('header') && $this->registerHook('backOfficeHeader') && Configuration::updateValue('PS_MINIC_SLIDER_FIRST', '1')){
				return true;
			}else{
				$this->uninstall();
				return false;
			}
		}
	
	public function uninstall()
		{
			$image = Db::getInstance()->ExecuteS('SELECT image FROM `'._DB_PREFIX_.'minic_slider`');
	
			foreach($image as $img){
				$this->_deleteImages($img['image']);
			}
	
			if (!Db::getInstance()->Execute('DROP TABLE IF EXISTS `'._DB_PREFIX_.'minic_slider`') OR
	    		!Db::getInstance()->Execute('DROP TABLE IF EXISTS `'._DB_PREFIX_.'minic_options`') OR
	    		!Configuration::deleteByName('PS_MINIC_SLIDER_FIRST') OR
				!parent::uninstall())
				return false;
			return true;	
		}		
	
	public function getContent()
		{
			if (Tools::isSubmit('submitOptions')){
				$this->_handleOptions();
			} elseif (Tools::isSubmit('submitNewSlide')){
				$this->_handleNewSlide();
			} elseif (Tools::isSubmit('editSlide')){
				$this->_handleEditSlide();
			} elseif (Tools::isSubmit('deleteSlide')) {
				$this->_handleDeleteSlide();
			}
			return $this->_displayForm();
		}
	
	private function _displayForm()
		{
			global $smarty, $cookie;
	
			$defaultLanguage = Language::getLanguage(Configuration::get('PS_LANG_DEFAULT'));
			$activeLanguages = Language::getLanguages(true);
			$allLanguages = Language::getLanguages(false);
			$options = Db::getInstance()->getRow('SELECT * FROM `'._DB_PREFIX_.'minic_options`');
			$slides = array();
			$firstStart = Configuration::get('PS_MINIC_SLIDER_FIRST');
	
			foreach($activeLanguages as $lang){
				$slides[$lang['iso_code']] = Db::getInstance()->ExecuteS('SELECT * FROM `'._DB_PREFIX_.'minic_slider` WHERE lang_id ='.$lang['id_lang'].' ORDER BY order_id ASC');
			}
	
			$smarty->assign('slider', array(
				'options' => array(
					'effect' => (!empty($options['effect'])) ? explode(',', $options['effect']) : NULL,
					'current' => (!empty($options['current'])) ? explode(',', $options['current']) : NULL,
					'slices' => $options['slices'],
					'cols' => $options['cols'],
					'rows' => $options['rows'],
					'speed' => $options['speed'],
					'pause' => $options['pause'],
					'manual' => $options['manual'],
					'hover' => $options['hover'],
					'buttons' => $options['buttons'],
					'control' => $options['control'],
					'thumbnail' => $options['thumbnail'],
					'random' => $options['random'],
					'startSlide' => $options['start_slide'],
					'opacity' => $options['opacity'],
					'single' => $options['single'],
					'width' => $options['width'],
					'height' => $options['height']
				),
				'slides' => $slides,
				'lang' => array(
					'default' => $defaultLanguage,
					'default_iso' => $defaultLanguage,
					'default_name' => $defaultLanguage,
					'all' => $activeLanguages,
					'lang_dir' => _THEME_LANG_DIR_,
					'user' => $cookie->id_lang
				),				
				'tpl' => array(
                	'options' => _PS_MODULE_DIR_.'minicslider/tpl/admin-options.tpl',
                	'new' => _PS_MODULE_DIR_.'minicslider/tpl/admin-new.tpl',
                	'slides' => _PS_MODULE_DIR_.'minicslider/tpl/admin-slides.tpl',
                	'feedback' => _PS_MODULE_DIR_.'minicslider/tpl/admin-feedback.tpl',
                	'bug' => _PS_MODULE_DIR_.'minicslider/tpl/admin-bug.tpl'
            	),
            	'info' => array(
            		'name' => Configuration::get('PS_SHOP_NAME'),
            		'domain' => Configuration::get('PS_SHOP_DOMAIN'),
            		'email' => Configuration::get('PS_SHOP_EMAIL'),
            		'version' => $this->version,
                	'psVersion' => _PS_VERSION_,
            		'server' => Tools::htmlentitiesUTF8($_SERVER['SERVER_SOFTWARE']),
            		'php' => Tools::htmlentitiesUTF8(phpversion()),
            		'mysql' => Tools::htmlentitiesUTF8(mysql_get_server_info()),
            		'theme' => _THEME_NAME_,
            		'userInfo' => Tools::htmlentitiesUTF8($_SERVER['HTTP_USER_AGENT'])
        		),
				'postAction' => 'index.php?tab=AdminModules&configure='.$this->name.'&token='.Tools::getAdminTokenLite('AdminModules').'&tab_module=advertising_marketing='.$this->name.'',
				'istallUrl' => 'http://www.module.minic.ro/slider/process.php?domain='.Configuration::get('PS_SHOP_DOMAIN').'&psversion='. _PS_VERSION_.'&version='.$this->version,
				'firstStart' => $firstStart
			));	
			
			if($firstStart == 1)
				Configuration::updateValue('PS_MINIC_SLIDER_FIRST', '0');

			return $this->display(__FILE__, 'tpl/admin.tpl');
		}
	
	private function _handleOptions()
		{
			global $smarty;
		
			$effect = implode(',', Tools::getValue('nivo_effect'));
			$current = '';
			if(Tools::getValue('nivo_current') != ''){
				$current = implode(',', Tools::getValue('nivo_current'));
			}
		
			if(!Db::getInstance()->Execute('
				UPDATE `'._DB_PREFIX_.'minic_options` SET 
					effect = "'.$effect.'",
					current = "'.$current.'",
					slices = "'.(int)Tools::getValue('slices').'",
					cols = "'.(int)Tools::getValue('cols').'",
					rows = "'.(int)Tools::getValue('rows').'",
					speed = "'.(int)Tools::getValue('speed').'",
					pause = "'.(int)Tools::getValue('pause').'",
					manual = "'.(int)Tools::getValue('manual').'",
					hover = "'.(int)Tools::getValue('hover').'",
					buttons = "'.(int)Tools::getValue('buttons').'",
					control = "'.(int)Tools::getValue('control').'",
					thumbnail = "'.(int)Tools::getValue('thumbnail').'",					
					random = "'.(int)Tools::getValue('random').'",
					start_slide = "'.(int)Tools::getValue('startSlide').'",
					single = "'.(int)Tools::getValue('single').'",
					width = "'.(int)Tools::getValue('sliderWidth').'",
					height = "'.(int)Tools::getValue('sliderHeight').'"
					')){
				$smarty->assign('error', $this->displayError($this->l('An error occurred while saving data. I`m sure this is a DataBase error.')));
				return false;
			}
		
			$smarty->assign('confirmation', $this->l('Options saved.'));		
		}
	
	private function _handleNewSlide()
		{
			global $smarty;
			
			if(Configuration::get('PS_MINIC_SLIDER_FIRST') == 1){
				Configuration::updateValue('PS_MINIC_SLIDER_FIRST', '1');
			}

			$languages = Language::getLanguages(false);		
			$langId = (int)Tools::getValue('language');
			$lang = Language::getLanguage($langId);
			$lastSlideID = Db::getInstance()->ExecuteS('SELECT slide_id, order_id FROM `'._DB_PREFIX_.'minic_slider` WHERE lang_id = '.$langId.' ORDER BY slide_id DESC LIMIT 1');
			$currentSlideID = ($lastSlideID) ? $lastSlideID[0]['slide_id']+1 : 1;
			$currentOrderID = ($lastSlideID) ? $lastSlideID[0]['order_id']+1 : 1 ;
		
			if(empty($_FILES['image']['name'])){
				$smarty->assign('error', $this->l('Image needed, please choose one.'));
				return false;
			}
			
			$image = $this->_resizer($_FILES['image'], Tools::getValue('imageName'));
		
			if(!$image)
				return false;
		
			$insert = Db::getInstance()->Execute('
				INSERT INTO `'._DB_PREFIX_.'minic_slider` ( 
					lang_id, lang_iso, title, url, target, image, alt, caption, order_id 
				) VALUES ( 
					"'.(int)Tools::getValue('language').'",
					"'.$lang['iso_code'].'",
					"'.Tools::getValue('title').'",
					"'.Tools::getValue('url').'",
					"'.(int)Tools::getValue('target').'",
					"'.$image.'",
					"'.Tools::getValue('alt').'",
					"'.$_POST['caption'].'",
					"'.$currentOrderID.'")
				');
		
			if(!$insert){
				$this->_deleteImages($image);
				$smarty->assign('error', $this->l('An error occured while saving data.'));	
				return false;	
			}	
		
			$smarty->assign('confirmation', $this->l('New slide added successfull.'));
		}
	private function _handleEditSlide()
		{
			global $smarty;
		
			$langIso = Tools::getValue('slideIso');
			$newImage = '';
		
			if(!empty($_FILES['newImage']['name'])){
				$image = $this->_resizer($_FILES['newImage']);
				if(empty($image))
					return false;
				$newImage = 'image = "'.$image.'",';
			}
		
			$update = Db::getInstance()->Execute('
				UPDATE `'._DB_PREFIX_.'minic_slider` SET 
					title = "'.Tools::getValue('title').'",
					url = "'.Tools::getValue('url').'",
					target = "'.(int)Tools::getValue('target').'",
					'.$newImage.'
					alt = "'.Tools::getValue('alt').'",
					caption = "'.$_POST['caption'].'",
					active = "'.(int)Tools::getValue('isActive').'"
				WHERE slide_id = '.(int)Tools::getValue('slideId'));
		
			if(!$update){
				$this->_deleteImages(Tools::getValue('image'));			
				$smarty->assign('error', $this->l('An error occured while saving data.'));	
				return false;			
			}
		
			if(!empty($_FILES['newImage']['name'])){
				$this->_deleteImages(Tools::getValue('oldImage'));
			}
		
			$smarty->assign('confirmation', $this->l('Saved succsessfull.'));
		}
	
	public function _handleDeleteSlide()
		{
			global $smarty;
		
			Db::getInstance()->delete(_DB_PREFIX_.'minic_slider', 'slide_id = '.Tools::getValue('slideId'));
		
			if(Db::getInstance()->Affected_Rows() == 1){
				Db::getInstance()->Execute('
					UPDATE `'._DB_PREFIX_.'minic_slider` 
					SET order_id = order_id-1 
					WHERE (
						order_id > '.Tools::getValue('orderId').' AND 
						lang_iso = "'.Tools::getValue('slideIso').'")
				');
		
				$this->_deleteImages(Tools::getValue('oldImage'));
				$smarty->assign('confirmation', $this->l('Deleted succsessfull.'));
			}else{
				$smarty->assign('error', $this->l('Cant delete image data from database.'));
			}
		}
	
	private function _resizer($image, $newName = NULL)
		{
			global $smarty;
		
			$path = $_SERVER['DOCUMENT_ROOT'].$this->_path.'/uploads/';
			$pathThumb = $_SERVER['DOCUMENT_ROOT'].$this->_path.'/uploads/thumbs/';
			$imageName = explode('.', str_replace(' ', '_', $image['name']));
			$name = $imageName[0].'.'.$imageName[1];
			if($newName)
				$name = $newName.'.'.$imageName[1];

			Configuration::set('PS_IMAGE_GENERATION_METHOD', 1);
		
			if(file_exists($path.$name) && $newName == NULL){
				$name = $imageName[0].date('-i-s').'.'.$imageName[1];
			}
			if (!isPicture($image)) {
				$smarty->assign('error', $this->l('Image format not recognized, allowed formats are: .gif, .jpg, .png'));
				return false;
			}else{
				if (checkImage($image, $this->maxImageSize)){
					$smarty->assign('error', $this->l('Image is to large: ').$image['size'].' kb '.$this->l('Maximum allowed: ').$this->maxImageSize.' kb');
					return false;
				}else{
					if (!$tmpName = tempnam(_PS_TMP_IMG_DIR_, 'PS') OR !move_uploaded_file($image['tmp_name'], $tmpName)){
						$smarty->assign('error', $this->l('An error occurred while moving files. Please check permissions.'));
						return false;
						unlink($tmpName);
					}else{
						if (!imageResize($tmpName, $pathThumb.'admin_'.$name,250)){
							$smarty->assign('error', $this->l('An error occurred while creating thumbnails.'));
							return false;
							unlink($tmpName);
						}else{
							if (!imageResize($tmpName, $pathThumb.'front_'.$name,50)){
								$smarty->assign('error', $this->l('An error occurred while creating thumbnails.'));
								return false;
								unlink($tmpName);
							}else{
								if (!rename($tmpName, $path.$name)){
									$smarty->assign('error', $this->l('An error occurred while renaming files.'));
									unlink($tmpName);
								}else{
									$image = $name;
									chmod($path.$name, 0644);
									return $image;
								}
							}
						}
					}
				}
			}
		}	
	
	private function _deleteImages($image)
		{
			global $smarty;
		
			$path = $_SERVER['DOCUMENT_ROOT'].$this->_path.'/uploads/';
			$pathThumb = $_SERVER['DOCUMENT_ROOT'].$this->_path.'/uploads/thumbs/';		
			
			if(file_exists($path.$image)){
				if(!unlink($path.$image) || !unlink($pathThumb.'admin_'.$image) || !unlink($pathThumb.'front_'.$image))
					$smarty->assign('error', $this->l('Cant delete images, please check permissions!'));			
			}else{
				$smarty->assign('error', $this->l('Image doesn`t exists!'));
			}
		}
	
	public function hookHeader()
		{
			Tools::addCSS($this->_path.'css/nivo-slider.css', 'all');
			Tools::addJS($this->_path.'js/jquery.nivo.slider.pack.js');
		}
	
	public function hookBackOfficeHeader()
		{		
			$html  = '<link type="text/css" rel="stylesheet" href="' . $this->_path . 'css/tipsy.css" />';	
			$html .= '<link type="text/css" rel="stylesheet" href="' . $this->_path . 'css/style.css" />';
			$html .= '<script type="text/javascript" src="' . $this->_path . 'js/ghostText.min.js"></script>';			
			$html .= '<script type="text/javascript" src="' . $this->_path . 'js/jquery.tipsy.js"></script>';		
			$html .= '<script type="text/javascript" src="' . $this->_path . 'js/jquery-ui-1.9.0.custom.min.js"></script>';
			$html .= '<script type="text/javascript" src="' .__PS_BASE_URI__. 'js/jquery/jquery.scrollTo-1.4.2-min.js"></script>';
			$html .= '<script type="text/javascript" src="' . $this->_path . 'js/minicFeedback.js"></script>';		
			$html .= '<script type="text/javascript" src="' . $this->_path . 'js/minicSlider.js"></script>';
			$html .= '<script type="text/javascript">
				$(document).ready(function() {
					$("ul.languages").sortable({
						opacity: 0.6,
						cursor: "move",
						update: function(event, ui) {
							var list = $(this);
							var number;
							var response;
							$.getJSON(
								"'._PS_BASE_URL_.__PS_BASE_URI__.'modules/'.$this->name.'/ajax_'.$this->name.'.php?action=updateOrder&secure_key='.$this->secure_key.'", 
								{slides: $(this).sortable("serialize")}, 
								function(response){
									if(response.success == "true"){
										showResponse($("#fixed_conf"), "'.$this->l('Saved successfull').'.");
										var i = 1;
										list.children("li").each(function(){
											number = i;
											if(i < 10){ 
												number = "0"+i; 
											}
											$(this).find(".order").text(number);
											i++;
										});
									}else{
										showResponse($("#fixed_error"), "'.$this->l('Something went wrong, please refresh the page and try again').'.");
									}
								}
							);
						}
					});					
				});
			</script>';
			
			if (Tools::getValue('tab') == 'AdminModules' && (Tools::getValue('configure') == $this->name || Tools::getValue('module_name') == $this->name))
				return $html;
		}	
 	
 	public function hookHome()
	 	{
			global $smarty;
	
			$defLanguages = Configuration::get('PS_LANG_DEFAULT');
			$activeLanguages = Language::getLanguages(true);
			$allLanguages = Language::getLanguages(false);
			$defLangIso = $allLanguages[$defLanguages-1]['iso_code'];
			$smarty->assign('defaultLangIso', $defLangIso);
			$options = Db::getInstance()->getRow('SELECT * FROM `'._DB_PREFIX_.'minic_options`');
			$width = array();
			$slides = array();
	
			if($options['single'] == 0){
				$slides[$defLangIso] = Db::getInstance()->ExecuteS('SELECT * FROM `'._DB_PREFIX_.'minic_slider` WHERE (lang_id ='.$defLanguages.' AND active = 1) ORDER BY order_id ASC');			
			}else{
				foreach ($activeLanguages as $lang) {
					$slides[$lang['iso_code']] = Db::getInstance()->ExecuteS('SELECT * FROM `'._DB_PREFIX_.'minic_slider` WHERE (lang_id ='.$lang['id_lang'].' AND active = 1) ORDER BY order_id ASC');	
				}
			}			
	
			$smarty->assign('slides', $slides);		
			$smarty->assign('minicSlider', array(
				'options' => array(
					'current' => $options['current'],
					'slices' => $options['slices'],
					'cols' => $options['cols'],
					'rows' => $options['rows'],
					'speed' => $options['speed'],
					'pause' => $options['pause'],
					'manual' => $options['manual'],
					'hover' => $options['hover'],
					'buttons' => $options['buttons'],
					'active' => $options['active'],
					'control' => $options['control'],
					'thumbnail' => $options['thumbnail'],
					'keyboard' => $options['keyboard'],
					'random' => $options['random'],
					'startSlide' => $options['start_slide'],
					'opacity' => $options['opacity'],
					'single' => $options['single'],
					'width' => $options['width'],
					'height' => $options['height']
				),
				'path' => array(
					'images' => $this->_path.'uploads/',
					'thumbs' => $this->_path.'uploads/thumbs/front_'
				)
			));
	 	
	 		return $this->display(__FILE__, 'tpl/front.tpl');
	 	}	
	
	public function hookLeftColumn()
		{
			return $this->hookHome();
		}
	
	public function hookRightColumn()
		{
			return $this->hookHome();
		}		

	public function hookTop()
		{
			global $smarty;
	
			$defLanguages = Configuration::get('PS_LANG_DEFAULT');
			$activeLanguages = Language::getLanguages(true);
			$allLanguages = Language::getLanguages(false);
			$defLangIso = $allLanguages[$defLanguages-1]['iso_code'];
			$smarty->assign('defaultLangIso', $defLangIso);
			$options = Db::getInstance()->getRow('SELECT * FROM `'._DB_PREFIX_.'minic_options`');
			$width = array();
			$slides = array();
	
			if($options['single'] == 0){
				$slides[$defLangIso] = Db::getInstance()->ExecuteS('SELECT * FROM `'._DB_PREFIX_.'minic_slider` WHERE (lang_id ='.$defLanguages.' AND active = 1) ORDER BY order_id ASC');			
			}else{
				foreach ($activeLanguages as $lang) {
					$slides[$lang['iso_code']] = Db::getInstance()->ExecuteS('SELECT * FROM `'._DB_PREFIX_.'minic_slider` WHERE (lang_id ='.$lang['id_lang'].' AND active = 1) ORDER BY order_id ASC');	
				}
			}			
	
			$smarty->assign('slides', $slides);		
			$smarty->assign('minicSlider', array(
				'options' => array(
					'current' => $options['current'],
					'slices' => $options['slices'],
					'cols' => $options['cols'],
					'rows' => $options['rows'],
					'speed' => $options['speed'],
					'pause' => $options['pause'],
					'manual' => $options['manual'],
					'hover' => $options['hover'],
					'buttons' => $options['buttons'],
					'active' => $options['active'],
					'control' => $options['control'],
					'thumbnail' => $options['thumbnail'],
					'keyboard' => $options['keyboard'],
					'random' => $options['random'],
					'startSlide' => $options['start_slide'],
					'opacity' => $options['opacity'],
					'single' => $options['single'],
					'width' => $options['width'],
					'height' => $options['height']
				),
				'path' => array(
					'images' => $this->_path.'uploads/',
					'thumbs' => $this->_path.'uploads/thumbs/front_'
				)
			));
	 	
	 		return $this->display(__FILE__, 'tpl/front-top.tpl');
		}
}

?>