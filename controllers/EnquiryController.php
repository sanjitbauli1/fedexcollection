<?php
/*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @version  Release: $Revision: 14006 $
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class EnquiryControllerCore extends FrontController
{
	public $php_self = 'enquiry-form.php';
	public $ssl = true;

	public function preProcess()
	{
		parent::preProcess();
		if (self::$cookie->isLogged())
		{
			self::$smarty->assign('isLogged', 1);
			$customer = new Customer((int)(self::$cookie->id_customer));
			//print_r($customer);
			self::$smarty->assign('customer', (array)$customer);
			if (!Validate::isLoadedObject($customer))
				die(Tools::displayError('Customer not found'));
			
			/*echo $customer_address = Db::getInstance()->ExecuteS('
				SELECT *
				FROM '._DB_PREFIX_.'address
				WHERE id_customer = '.(int)$customer->id.' ORDER BY date_add');
				self::$smarty->assign('address', (array)$customer_address);*/
			
			$products = array();
			$orders = array();
			$getOrders = Db::getInstance()->ExecuteS('
				SELECT id_order
				FROM '._DB_PREFIX_.'orders
				WHERE id_customer = '.(int)$customer->id.' ORDER BY date_add');
			foreach ($getOrders as $row)
			{
				$order = new Order($row['id_order']);
				$date = explode(' ', $order->date_add);
				$orders[$row['id_order']] = Tools::displayDate($date[0], self::$cookie->id_lang);
				$tmp = $order->getProducts();
				foreach ($tmp as $key => $val)
					$products[$val['product_id']] = $val['product_name'];
			}

			$orderList = '';
			foreach ($orders as $key => $val)
				$orderList .= '<option value="'.$key.'" '.((int)(Tools::getValue('id_order')) == $key ? 'selected' : '').' >'.$key.' -- '.$val.'</option>';
			$orderedProductList = '';

			foreach ($products as $key => $val)
				$orderedProductList .= '<option value="'.$key.'" '.((int)(Tools::getValue('id_product')) == $key ? 'selected' : '').' >'.$val.'</option>';
			self::$smarty->assign('orderList', $orderList);
			self::$smarty->assign('orderedProductList', $orderedProductList);
		}

		if (Tools::isSubmit('submitMessage'))
		{
			$fileAttachment = NULL;
			if (isset($_FILES['fileUpload']['name']) AND !empty($_FILES['fileUpload']['name']) AND !empty($_FILES['fileUpload']['tmp_name']))
			{
				$extension = array('.txt', '.rtf', '.doc', '.docx', '.pdf', '.zip', '.png', '.jpeg', '.gif', '.jpg');
				$filename = uniqid().substr($_FILES['fileUpload']['name'], -5);
				$fileAttachment['content'] = file_get_contents($_FILES['fileUpload']['tmp_name']);
				$fileAttachment['name'] = $_FILES['fileUpload']['name'];
				$fileAttachment['mime'] = $_FILES['fileUpload']['type'];
			}
			
			$first_name =  Tools::htmlentitiesUTF8(Tools::getValue('first_name'));
			self::$smarty->assign('first_name', $first_name);
			$last_name =  Tools::htmlentitiesUTF8(Tools::getValue('last_name'));
			self::$smarty->assign('last_name', $last_name);
			$tel =  Tools::htmlentitiesUTF8(Tools::getValue('tel'));
			self::$smarty->assign('tel', $tel);
			$delivery_date =  Tools::htmlentitiesUTF8(Tools::getValue('delivery_date'));
			self::$smarty->assign('delivery_date', $delivery_date);
			$delivery_address = Tools::htmlentitiesUTF8(Tools::getValue('delivery_address'));
			self::$smarty->assign('delivery_address', $delivery_address);
			$billing_address = Tools::htmlentitiesUTF8(Tools::getValue('billing_address'));
			self::$smarty->assign('billing_address', $billing_address);
			$quantity = Tools::htmlentitiesUTF8(Tools::getValue('quantity'));
			self::$smarty->assign('quantity', $quantity);
			$quality = Tools::htmlentitiesUTF8(Tools::getValue('quality'));
			self::$smarty->assign('quality', $quality);
			$message = Tools::htmlentitiesUTF8(Tools::getValue('message'));
			if(!($first_name=nl2br2($first_name)))
				$this->errors[] = Tools::displayError('First Name cannot be blank');
			elseif(!($last_name=nl2br2($last_name)))
				$this->errors[] = Tools::displayError('Last Name cannot be blank');
			elseif (!($from = trim(Tools::getValue('from'))) OR !Validate::isEmail($from))
				$this->errors[] = Tools::displayError('Invalid e-mail address');
			elseif(!($delivery_date=nl2br2($delivery_date)))
				$this->errors[] = Tools::displayError('Delivary Date cannot be blank');
			elseif(!($quantity=nl2br2($quantity)))
				$this->errors[] = Tools::displayError('Quantity cannot be blank');
			elseif (!($message = nl2br2($message)))
				$this->errors[] = Tools::displayError('Description cannot be blank');
			elseif (!Validate::isCleanHtml($message))
				$this->errors[] = Tools::displayError('Description message');
			elseif (!empty($_FILES['fileUpload']['name']) AND $_FILES['fileUpload']['error'] != 0)
				$this->errors[] = Tools::displayError('An error occurred during the file upload');
			elseif (!empty($_FILES['fileUpload']['name']) AND !in_array(substr($_FILES['fileUpload']['name'], -4), $extension) AND !in_array(substr($_FILES['fileUpload']['name'], -5), $extension))
				$this->errors[] = Tools::displayError('Bad file extension');
			else
			{
				//print_r($_POST);
				//die();
				
				if ((int)(self::$cookie->id_customer))
				{
					$customer = new Customer((int)(self::$cookie->id_customer));
					$CurrentUserEmail	=$from;
				}
				else
				{
					$CurrentUserEmail	=$from;
				}
				$id_contact = "2";
				$contact = new Contact($id_contact, self::$cookie->id_lang);
				if (!empty($CurrentUserEmail))
				{
					
					$body = "Your Phone Number = ".$tel."<br>Requested delivery date= ".$delivery_date."<br>Requested Delivery Address= ".$delivery_address."<br>Quantity=".$quantity."<br>Quality=".$quality."<br>Description=".$message."";
					if (Mail::Send((int)self::$cookie->id_lang, 'contact', Mail::l('Message from Enquiry form', (int)self::$cookie->id_lang), array('{email}' => $from, '{message}' => stripslashes($body), '{tel}' => $tel,'{delivery_date}'=> $delivery_date,'{billing_address}'=> $billing_address, '{delivery_address}' => $delivery_address,'{quantity}'=> $quantity,'{quality}'=> $quality), $contact->email, $contact->name, $from, $firstname.' '.$lastname, $fileAttachment,'',_PS_MAIL_DIR_,true)
						AND 
						Mail::Send((int)self::$cookie->id_lang, 'contact_form', Mail::l('Your message has been correctly sent', (int)self::$cookie->id_lang), array('{message}' => stripslashes($message)), $from))
						self::$smarty->assign('confirmation', 1);
					else
						$this->errors[] = Tools::displayError('An error occurred while sending message.');
				}
			}
		}
	}

	public function setMedia()
	{
		parent::setMedia();
		Tools::addCSS(_THEME_CSS_DIR_.'contact-form.css');
	}

	public function process()
	{
		parent::process();

		$email = Tools::safeOutput(Tools::getValue('from', ((isset(self::$cookie) AND isset(self::$cookie->email) AND Validate::isEmail(self::$cookie->email)) ? self::$cookie->email : '')));
		self::$smarty->assign(array(
			'errors' => $this->errors,
			'email' => $email,
			'fileupload' => Configuration::get('PS_CUSTOMER_SERVICE_FILE_UPLOAD')
		));


		if ($id_customer_thread = (int)Tools::getValue('id_customer_thread') AND $token = Tools::getValue('token'))
		{
			$customerThread = Db::getInstance()->getRow('
			SELECT cm.* FROM '._DB_PREFIX_.'customer_thread cm
			WHERE cm.id_customer_thread = '.(int)$id_customer_thread.' AND token = \''.pSQL($token).'\'');
			self::$smarty->assign('customerThread', $customerThread);
		}
		self::$smarty->assign(array('contacts' => Contact::getContacts((int)(self::$cookie->id_lang)),
		'message' => html_entity_decode(Tools::getValue('message'))
		));
	}

	public function displayContent()
	{
		$_POST = array_merge($_POST, $_GET);
		parent::displayContent();
		self::$smarty->display(_PS_THEME_DIR_.'enquiry-form.tpl');
	}
}