<?php /* Smarty version Smarty-3.0.7, created on 2015-06-30 14:03:41
         compiled from "/srv/data/web/vhosts/www.socatec.fr/htdocs/fedexcollection/modules/fedex/fedex-preview-json.tpl" */ ?>
<?php /*%%SmartyHeaderCode:15234159365592859d2dc891-47634414%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'b8863849b7d90d22813579bc13e160256ed4b431' => 
    array (
      0 => '/srv/data/web/vhosts/www.socatec.fr/htdocs/fedexcollection/modules/fedex/fedex-preview-json.tpl',
      1 => 1420675880,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '15234159365592859d2dc891-47634414',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php if (!is_callable('smarty_modifier_escape')) include '/srv/data/web/vhosts/www.socatec.fr/htdocs/fedexcollection/tools/smarty/plugins/modifier.escape.php';
?><script type="text/javascript" src="<?php echo $_smarty_tpl->getVariable('this_fedex_path')->value;?>
js/fedex-preview.js"></script>
<script type="text/javascript" src="<?php echo $_smarty_tpl->getVariable('this_fedex_path')->value;?>
js/statesManagement.js"></script>
<script type="text/javascript">
	//<![CDATA[
	fedex_countries = new Array();
	<?php  $_smarty_tpl->tpl_vars['country'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('fedex_countries')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['country']->key => $_smarty_tpl->tpl_vars['country']->value){
?>
		<?php if (isset($_smarty_tpl->tpl_vars['country']->value['states'])&&$_smarty_tpl->tpl_vars['country']->value['contains_states']){?>
			fedex_countries[<?php echo intval($_smarty_tpl->tpl_vars['country']->value['id_country']);?>
] = new Array();
			<?php  $_smarty_tpl->tpl_vars['state'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['country']->value['states']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['state']->key => $_smarty_tpl->tpl_vars['state']->value){
?>
				fedex_countries[<?php echo intval($_smarty_tpl->tpl_vars['country']->value['id_country']);?>
].push({'id' : '<?php echo $_smarty_tpl->tpl_vars['state']->value['id_state'];?>
', 'name' : '<?php echo smarty_modifier_escape($_smarty_tpl->tpl_vars['state']->value['name'],'htmlall','UTF-8');?>
'});
			<?php }} ?>
		<?php }?>
	<?php }} ?>
	
<?php if ($_smarty_tpl->getVariable('psVersion')->value==1.6){?>
	$(document).ready(function(){
		if(typeof uniform == 'function')
		{
			$("#fedex_dest_country<?php echo $_smarty_tpl->getVariable('fedex_is_cart')->value;?>
, #fedex_dest_state<?php echo $_smarty_tpl->getVariable('fedex_is_cart')->value;?>
, #fedex_dest_city<?php echo $_smarty_tpl->getVariable('fedex_is_cart')->value;?>
, #fedex_dest_zip<?php echo $_smarty_tpl->getVariable('fedex_is_cart')->value;?>
").uniform();
			
			$("#uniform-fedex_dest_country<?php echo $_smarty_tpl->getVariable('fedex_is_cart')->value;?>
, #uniform-fedex_dest_state<?php echo $_smarty_tpl->getVariable('fedex_is_cart')->value;?>
").css('width', '100%');
			$("#uniform-fedex_dest_country<?php echo $_smarty_tpl->getVariable('fedex_is_cart')->value;?>
, #uniform-fedex_dest_state<?php echo $_smarty_tpl->getVariable('fedex_is_cart')->value;?>
").children("span").css('width', '100%');
		}
	});
<?php }?>
</script>
<?php if ((!isset($_smarty_tpl->getVariable('fedex_dest_country',null,true,false)->value)||!$_smarty_tpl->getVariable('fedex_dest_country')->value)&&isset($_smarty_tpl->getVariable('fedex_default_country',null,true,false)->value)){?>
	<?php $_smarty_tpl->tpl_vars['fedex_dest_country'] = new Smarty_variable($_smarty_tpl->getVariable('fedex_default_country')->value, null, null);?>
<?php }?>       
	  
<?php $_smarty_tpl->tpl_vars["shippingDetails"] = new Smarty_variable('', null, null);?>
<?php if ($_smarty_tpl->getVariable('fedex_address_display')->value['country']&&isset($_smarty_tpl->getVariable('fedex_dest_country',null,true,false)->value)&&$_smarty_tpl->getVariable('fedex_dest_country')->value){?>
	<?php $_smarty_tpl->tpl_vars["shippingDetails"] = new Smarty_variable(($_smarty_tpl->getVariable('shippingDetails')->value)." ".($_smarty_tpl->getVariable('fedex_dest_country')->value), null, null);?> 
<?php }?>
<?php if ($_smarty_tpl->getVariable('fedex_address_display')->value['state']&&isset($_smarty_tpl->getVariable('fedex_dest_state_name',null,true,false)->value)&&$_smarty_tpl->getVariable('fedex_dest_state_name')->value){?>
	<?php if ($_smarty_tpl->getVariable('fedex_address_display')->value['country']&&isset($_smarty_tpl->getVariable('fedex_dest_country',null,true,false)->value)&&$_smarty_tpl->getVariable('fedex_dest_country')->value){?>
		<?php $_smarty_tpl->tpl_vars["shippingDetails"] = new Smarty_variable(($_smarty_tpl->getVariable('shippingDetails')->value).", ".($_smarty_tpl->getVariable('fedex_dest_state_name')->value), null, null);?>
	<?php }else{ ?>
		<?php $_smarty_tpl->tpl_vars["shippingDetails"] = new Smarty_variable(($_smarty_tpl->getVariable('shippingDetails')->value)." ".($_smarty_tpl->getVariable('fedex_dest_state_name')->value), null, null);?>
	<?php }?> 
<?php }?>
<?php if ($_smarty_tpl->getVariable('fedex_address_display')->value['city']&&isset($_smarty_tpl->getVariable('fedex_dest_city',null,true,false)->value)&&$_smarty_tpl->getVariable('fedex_dest_city')->value){?>
	<?php if (($_smarty_tpl->getVariable('fedex_address_display')->value['country']&&isset($_smarty_tpl->getVariable('fedex_dest_country',null,true,false)->value)&&$_smarty_tpl->getVariable('fedex_dest_country')->value)||($_smarty_tpl->getVariable('fedex_address_display')->value['state']&&isset($_smarty_tpl->getVariable('fedex_dest_state_name',null,true,false)->value)&&$_smarty_tpl->getVariable('fedex_dest_state_name')->value)){?>
		<?php $_smarty_tpl->tpl_vars["shippingDetails"] = new Smarty_variable(($_smarty_tpl->getVariable('shippingDetails')->value)." - ".($_smarty_tpl->getVariable('fedex_dest_city')->value), null, null);?>
	<?php }else{ ?>
		<?php $_smarty_tpl->tpl_vars["shippingDetails"] = new Smarty_variable(($_smarty_tpl->getVariable('shippingDetails')->value)." ".($_smarty_tpl->getVariable('fedex_dest_city')->value), null, null);?>
	<?php }?> 
<?php }?>
<?php if ($_smarty_tpl->getVariable('fedex_address_display')->value['zip']&&isset($_smarty_tpl->getVariable('fedex_dest_zip',null,true,false)->value)&&$_smarty_tpl->getVariable('fedex_dest_zip')->value){?>
	<?php if (($_smarty_tpl->getVariable('fedex_address_display')->value['country']&&isset($_smarty_tpl->getVariable('fedex_dest_country',null,true,false)->value)&&$_smarty_tpl->getVariable('fedex_dest_country')->value)||($_smarty_tpl->getVariable('fedex_address_display')->value['state']&&isset($_smarty_tpl->getVariable('fedex_dest_state_name',null,true,false)->value)&&$_smarty_tpl->getVariable('fedex_dest_state_name')->value)||($_smarty_tpl->getVariable('fedex_address_display')->value['city']&&isset($_smarty_tpl->getVariable('fedex_dest_city',null,true,false)->value)&&$_smarty_tpl->getVariable('fedex_dest_city')->value)){?>
		<?php $_smarty_tpl->tpl_vars["shippingDetails"] = new Smarty_variable(($_smarty_tpl->getVariable('shippingDetails')->value).", ".($_smarty_tpl->getVariable('fedex_dest_zip')->value), null, null);?>
	<?php }else{ ?>
		<?php $_smarty_tpl->tpl_vars["shippingDetails"] = new Smarty_variable(($_smarty_tpl->getVariable('shippingDetails')->value)." ".($_smarty_tpl->getVariable('fedex_dest_zip')->value), null, null);?>
	<?php }?> 
<?php }?>

<div class="fedex_preview_container<?php echo $_smarty_tpl->getVariable('fedex_is_cart')->value;?>
" <?php if ($_smarty_tpl->getVariable('psVersion')->value==1.6){?>style="margin:10px 0;float: none; <?php if ($_smarty_tpl->getVariable('fedex_is_cart')->value!=''){?>background-color: #f6f6f6; padding: 0 0 10px 0;<?php }?>"<?php }?>>
						  
	<div id="fedex_address<?php echo $_smarty_tpl->getVariable('fedex_is_cart')->value;?>
">
		<a href="javascript:void(0)" id="fedex_shipping_rates_button" class="fedex_hide_button">X</a>
		
		<span style="font-weight: bold;">
			<?php echo $_smarty_tpl->getVariable('shippingDetails')->value;?>

		</span>
		<span class="fedex_pointer" onclick="     
			$(this).hide();
			$('#fedex_rate_content<?php echo $_smarty_tpl->getVariable('fedex_is_cart')->value;?>
').css('display','none');
			$('#fedex_dest_change<?php echo $_smarty_tpl->getVariable('fedex_is_cart')->value;?>
').fadeIn(800);
		">(<?php echo smartyTranslate(array('s'=>'change','mod'=>'fedex'),$_smarty_tpl);?>
)</span>
	</div>

	<div id="fedex_dest_change<?php echo $_smarty_tpl->getVariable('fedex_is_cart')->value;?>
">
	
		<?php $_smarty_tpl->tpl_vars['inLine'] = new Smarty_variable(0, null, null);?>  
		<div id="fedex_line" <?php if ($_smarty_tpl->getVariable('psVersion')->value==1.6){?>style="padding: 0 5px;"<?php }?>>
			
			<?php if ($_smarty_tpl->getVariable('fedex_address_display')->value['country']){?>
				<?php $_smarty_tpl->tpl_vars['inLine'] = new Smarty_variable($_smarty_tpl->getVariable('inLine')->value+1, null, null);?>
					<span>
						<select onchange="fedex_preview_update_state('<?php echo $_smarty_tpl->getVariable('fedex_is_cart')->value;?>
', 0)" name="fedex_dest_country<?php echo $_smarty_tpl->getVariable('fedex_is_cart')->value;?>
" id="fedex_dest_country<?php echo $_smarty_tpl->getVariable('fedex_is_cart')->value;?>
">
							<?php  $_smarty_tpl->tpl_vars['fedex_country'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('fedex_countries')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['fedex_country']->key => $_smarty_tpl->tpl_vars['fedex_country']->value){
?>
								<option value="<?php echo $_smarty_tpl->tpl_vars['fedex_country']->value['id_country'];?>
" <?php if (isset($_smarty_tpl->getVariable('fedex_dest_country',null,true,false)->value)&&$_smarty_tpl->getVariable('fedex_dest_country')->value==$_smarty_tpl->tpl_vars['fedex_country']->value['iso_code']){?> selected="selected"<?php }?>><?php echo smarty_modifier_escape($_smarty_tpl->tpl_vars['fedex_country']->value['name'],'htmlall','UTF-8');?>
</option>
							<?php }} ?>
						</select>
					</span>
			<?php }?>
			
		<?php if ((!$_smarty_tpl->getVariable('fedex_is_cart')->value&&$_smarty_tpl->getVariable('inLine')->value==1)||($_smarty_tpl->getVariable('fedex_is_cart')->value&&$_smarty_tpl->getVariable('inLine')->value==1)){?>
			</div>
			<?php $_smarty_tpl->tpl_vars['inLine'] = new Smarty_variable(0, null, null);?>
			<div id="fedex_line" <?php if ($_smarty_tpl->getVariable('psVersion')->value==1.6){?>style="padding: 0 5px;"<?php }?>>
		<?php }?>
			
			<?php if ($_smarty_tpl->getVariable('fedex_address_display')->value['state']){?>
				<?php $_smarty_tpl->tpl_vars['inLine'] = new Smarty_variable($_smarty_tpl->getVariable('inLine')->value+1, null, null);?>
					<span>
						<select name="fedex_dest_state<?php echo $_smarty_tpl->getVariable('fedex_is_cart')->value;?>
" id="fedex_dest_state<?php echo $_smarty_tpl->getVariable('fedex_is_cart')->value;?>
">
							<option value="">-- <?php echo smartyTranslate(array('s'=>'State','mod'=>'fedex'),$_smarty_tpl);?>
 --</option>
						</select>
					</span>
			<?php }?>
				
		<?php if ((!$_smarty_tpl->getVariable('fedex_is_cart')->value&&$_smarty_tpl->getVariable('inLine')->value==1)||($_smarty_tpl->getVariable('fedex_is_cart')->value&&$_smarty_tpl->getVariable('inLine')->value==1)){?>
			</div>
			<?php $_smarty_tpl->tpl_vars['inLine'] = new Smarty_variable(0, null, null);?>
			<div id="fedex_line" <?php if ($_smarty_tpl->getVariable('psVersion')->value==1.6){?>style="padding: 0 5px;"<?php }?>>
		<?php }?>

			<?php if ($_smarty_tpl->getVariable('fedex_address_display')->value['city']){?>
				<?php $_smarty_tpl->tpl_vars['inLine'] = new Smarty_variable($_smarty_tpl->getVariable('inLine')->value+1, null, null);?>
					<span>
						<input placeholder="<?php echo smartyTranslate(array('s'=>'City Name','mod'=>'fedex'),$_smarty_tpl);?>
" type="text" name="fedex_dest_city<?php echo $_smarty_tpl->getVariable('fedex_is_cart')->value;?>
" id="fedex_dest_city<?php echo $_smarty_tpl->getVariable('fedex_is_cart')->value;?>
"
							<?php if (isset($_smarty_tpl->getVariable('fedex_dest_city',null,true,false)->value)&&$_smarty_tpl->getVariable('fedex_dest_city')->value!=''){?>
								value="<?php echo $_smarty_tpl->getVariable('fedex_dest_city')->value;?>
"
							<?php }elseif($_smarty_tpl->getVariable('fedex_is_cart')->value!=''){?>
								value="<?php echo smartyTranslate(array('s'=>'City','mod'=>'fedex'),$_smarty_tpl);?>
" 
								onclick="$(this).val('')"
							<?php }?> 
						/>
					</span>
			<?php }?>
				
		<?php if ((!$_smarty_tpl->getVariable('fedex_is_cart')->value&&$_smarty_tpl->getVariable('inLine')->value==1)||($_smarty_tpl->getVariable('fedex_is_cart')->value&&$_smarty_tpl->getVariable('inLine')->value==1)){?>
			</div>
			<?php $_smarty_tpl->tpl_vars['inLine'] = new Smarty_variable(0, null, null);?>
			<div id="fedex_line" <?php if ($_smarty_tpl->getVariable('psVersion')->value==1.6){?>style="padding: 0 5px;"<?php }?>>
		<?php }?>
				
			<?php if ($_smarty_tpl->getVariable('fedex_address_display')->value['zip']){?>
				<?php $_smarty_tpl->tpl_vars['inLine'] = new Smarty_variable($_smarty_tpl->getVariable('inLine')->value+1, null, null);?>
					<span>
						<input placeholder="<?php echo smartyTranslate(array('s'=>'Zip Code','mod'=>'fedex'),$_smarty_tpl);?>
" type="text" <?php if (!$_smarty_tpl->getVariable('fedex_address_display')->value['zip']){?>hide="1"<?php }?> name="fedex_dest_zip<?php echo $_smarty_tpl->getVariable('fedex_is_cart')->value;?>
" id="fedex_dest_zip<?php echo $_smarty_tpl->getVariable('fedex_is_cart')->value;?>
"
							<?php if (isset($_smarty_tpl->getVariable('fedex_dest_zip',null,true,false)->value)&&$_smarty_tpl->getVariable('fedex_dest_zip')->value!=''){?>
								value="<?php echo $_smarty_tpl->getVariable('fedex_dest_zip')->value;?>
"
							<?php }elseif($_smarty_tpl->getVariable('fedex_is_cart')->value!=''){?>
								value="<?php echo smartyTranslate(array('s'=>'Zip','mod'=>'fedex'),$_smarty_tpl);?>
" 
								onclick="$(this).val('')"
							<?php }?>
						/>
					</span>
			<?php }?>
			
		</div>                   

		<span class="submit_button">
			<a href="javascript:void(0)" id="fedex_submit_location<?php echo $_smarty_tpl->getVariable('fedex_is_cart')->value;?>
" onclick="fedex_get_rates(true,'<?php echo $_smarty_tpl->getVariable('fedex_is_cart')->value;?>
')" class="<?php if ($_smarty_tpl->getVariable('psVersion')->value==1.6){?>btn btn-default<?php }else{ ?>exclusive<?php }?>" style="<?php if ($_smarty_tpl->getVariable('fedex_is_cart')->value!=''){?>margin: auto;<?php }?> <?php if ($_smarty_tpl->getVariable('psVersion')->value==1.6){?>color: #333333;<?php }?>"><?php echo smartyTranslate(array('s'=>'Submit','mod'=>'fedex'),$_smarty_tpl);?>
</a>
		</span>
			
	</div>
	<div id="fedex_rate_content<?php echo $_smarty_tpl->getVariable('fedex_is_cart')->value;?>
" <?php if ($_smarty_tpl->getVariable('psVersion')->value==1.6){?>style="padding: 0 5px;"<?php }?>>
		<?php if ($_smarty_tpl->getVariable('fedex_is_cart')->value!=''&&$_smarty_tpl->getVariable('fedex_cart_products')->value==0){?>
			<div><b><?php echo smartyTranslate(array('s'=>'Your cart is empty!','mod'=>'fedex'),$_smarty_tpl);?>
</b></div>
		<?php }elseif($_smarty_tpl->getVariable('is_downloadable')->value>0){?>
			<div><?php echo smartyTranslate(array('s'=>'This product is available for download and does not require shipping','mod'=>'fedex'),$_smarty_tpl);?>
.</div>
		<?php }elseif(isset($_smarty_tpl->getVariable('fedex_not_available',null,true,false)->value)||count($_smarty_tpl->getVariable('fedex_product_rates')->value)==0){?>
			<div><?php echo smartyTranslate(array('s'=>'Shipping rates preview is not available in your area','mod'=>'fedex'),$_smarty_tpl);?>
.</div>
		<?php }else{ ?>
			<?php  $_smarty_tpl->tpl_vars['fedex_product_rate'] = new Smarty_Variable;
 $_smarty_tpl->tpl_vars['fedex_product_carrier'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('fedex_product_rates')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['fedex_product_rate']->key => $_smarty_tpl->tpl_vars['fedex_product_rate']->value){
 $_smarty_tpl->tpl_vars['fedex_product_carrier']->value = $_smarty_tpl->tpl_vars['fedex_product_rate']->key;
?>
			<div class="fedex_carrier_name<?php echo $_smarty_tpl->getVariable('fedex_is_cart')->value;?>
">
				<span class="fedex_carrier_radio">
					<input id="id_carrier_new_<?php echo $_smarty_tpl->tpl_vars['fedex_product_rate']->value[1];?>
<?php echo $_smarty_tpl->getVariable('fedex_is_cart')->value;?>
" name="id_carrier_new" class="carrier_selection_radio" type="radio" <?php if ($_smarty_tpl->getVariable('ps_carrier_default')->value==$_smarty_tpl->tpl_vars['fedex_product_rate']->value[1]){?>checked="checked"<?php }?> onclick="fedex_change_carrier(<?php echo $_smarty_tpl->tpl_vars['fedex_product_rate']->value[1];?>
, $(this))" />
					<label class="fedex_carrier_label" class="carrier_selection_name" for="id_carrier_new_<?php echo $_smarty_tpl->tpl_vars['fedex_product_rate']->value[1];?>
<?php echo $_smarty_tpl->getVariable('fedex_is_cart')->value;?>
"><?php echo $_smarty_tpl->tpl_vars['fedex_product_carrier']->value;?>
:</label>
				</span>
			</div>
			<div class="fedex_carrier_rate">&nbsp;&nbsp;<?php echo $_smarty_tpl->tpl_vars['fedex_product_rate']->value[0];?>
</div>
			<div class="fedex_carrier_buffer"></div>
			<?php }} ?>
		<?php }?>
	</div>
</div>

<script type="text/javascript">
	var fedex_cart_empty = "<?php echo smartyTranslate(array('s'=>'Your shopping cart is empty, add an item before selecting a shipping method','mod'=>'fedex'),$_smarty_tpl);?>
.";
	fedex_preview_update_state('<?php echo $_smarty_tpl->getVariable('fedex_is_cart')->value;?>
', <?php echo intval($_smarty_tpl->getVariable('fedex_dest_state')->value);?>
);         
</script>