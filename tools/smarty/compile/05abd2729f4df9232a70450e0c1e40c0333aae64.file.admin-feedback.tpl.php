<?php /* Smarty version Smarty-3.0.7, created on 2015-06-30 14:03:01
         compiled from "/srv/data/web/vhosts/www.socatec.fr/htdocs/fedexcollection/modules/minicslider/tpl/admin-feedback.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1599491921559285755a29f7-82428104%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '05abd2729f4df9232a70450e0c1e40c0333aae64' => 
    array (
      0 => '/srv/data/web/vhosts/www.socatec.fr/htdocs/fedexcollection/modules/minicslider/tpl/admin-feedback.tpl',
      1 => 1409232624,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1599491921559285755a29f7-82428104',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>

 
<fieldset id="feedback" class="hidden-div">
    <legend><?php echo smartyTranslate(array('s'=>'Feedback'),$_smarty_tpl);?>
</legend>
	<form id="formFeed" class="" method="post">
        <div id="feedInfoCont" class="container">
    		<div>
    			<label><?php echo smartyTranslate(array('s'=>'Your name'),$_smarty_tpl);?>
:</label>
    			<input id="feedbackName" class="name" type="text" name="name" value="<?php echo $_smarty_tpl->getVariable('slider')->value['info']['name'];?>
" size="50" />
    		</div>
    		<div>
    			<label><?php echo smartyTranslate(array('s'=>'Your email'),$_smarty_tpl);?>
:</label>
    			<input id="feedbackEmail" class="" type="text" name="email" value="<?php echo $_smarty_tpl->getVariable('slider')->value['info']['email'];?>
" size="50" />
                <input id="feedbackEmailAgree" type="checkbox" checked="checked" value="1" />
    		</div>
    		<div>
    			<label><?php echo smartyTranslate(array('s'=>'Site address'),$_smarty_tpl);?>
:</label>
    			<input id="feedbackDomain" class="" type="text" name="domain" value="http://<?php echo $_smarty_tpl->getVariable('slider')->value['info']['domain'];?>
" size="50" />
    		</div>
            <div>
                <label><?php echo smartyTranslate(array('s'=>'Message'),$_smarty_tpl);?>
:</label>
                <textarea id="feedbackMessage" class="message" name="feedbackMessage" rows="10" cols="49"></textarea>
            </div>
        </div>
        <div class="comments"> 
            <h3><?php echo smartyTranslate(array('s'=>'Notes'),$_smarty_tpl);?>
</h3>
            <p><?php echo smartyTranslate(array('s'=>'Feel free to give us a feedback about our work (we really like to hear few words) or write down your idea / request and if we think its good we`ll consider to implement into future versions.'),$_smarty_tpl);?>
</p>
            <h3><?php echo smartyTranslate(array('s'=>'Important!'),$_smarty_tpl);?>
</h3>
            <p><?php echo smartyTranslate(array('s'=>'By clicking to the "Send" button you agree, that we will get some basic information. If you don`t want to send your e-mail address uncheck the checkbox.'),$_smarty_tpl);?>
</p>
            <ul>
                <li><?php echo smartyTranslate(array('s'=>'Your shop`s name'),$_smarty_tpl);?>
: <b><?php echo $_smarty_tpl->getVariable('slider')->value['info']['name'];?>
</b></li>
                <li><?php echo smartyTranslate(array('s'=>'Your shops e-mail address. IMPORTANT: we just need in case of communicaton!'),$_smarty_tpl);?>
: <b><?php echo $_smarty_tpl->getVariable('slider')->value['info']['email'];?>
</b></li>
                <li><?php echo smartyTranslate(array('s'=>'Your shops domain'),$_smarty_tpl);?>
: <b><?php echo $_smarty_tpl->getVariable('slider')->value['info']['domain'];?>
</b></li>
                <li><?php echo smartyTranslate(array('s'=>'prestashop version'),$_smarty_tpl);?>
: <b><?php echo $_smarty_tpl->getVariable('slider')->value['info']['psVersion'];?>
</b></li>
                <li><?php echo smartyTranslate(array('s'=>'module version'),$_smarty_tpl);?>
: <b><?php echo $_smarty_tpl->getVariable('slider')->value['info']['version'];?>
</b></li>
            </ul>
        </div>
        <div class="button_cont">
            <input id="submitFeedback" type="submit" name="submitFeedback" value="Send" class="button green" />
            <input id="feedbackPsVersion" type="hidden" value="<?php echo $_smarty_tpl->getVariable('slider')->value['info']['psVersion'];?>
" name="psversion" />
            <input id="feedbackVersion" type="hidden" value="<?php echo $_smarty_tpl->getVariable('slider')->value['info']['version'];?>
" name="version" />
        </div>
	</form>
    <div class="response" style="display:none;">
        <span class="conf"><?php echo smartyTranslate(array('s'=>'Message sent successfull! Thank you for your time.'),$_smarty_tpl);?>
</span>
        <span class="error"><?php echo smartyTranslate(array('s'=>'Something went wrong, please try again later.'),$_smarty_tpl);?>
</span>
        <span class="empty"><?php echo smartyTranslate(array('s'=>'Name and Message is required!'),$_smarty_tpl);?>
</span>
    </div>
</fieldset>