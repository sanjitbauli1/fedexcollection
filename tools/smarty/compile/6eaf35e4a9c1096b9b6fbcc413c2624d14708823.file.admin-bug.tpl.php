<?php /* Smarty version Smarty-3.0.7, created on 2015-06-30 14:03:01
         compiled from "/srv/data/web/vhosts/www.socatec.fr/htdocs/fedexcollection/modules/minicslider/tpl/admin-bug.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1335613967559285756939c1-76424024%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '6eaf35e4a9c1096b9b6fbcc413c2624d14708823' => 
    array (
      0 => '/srv/data/web/vhosts/www.socatec.fr/htdocs/fedexcollection/modules/minicslider/tpl/admin-bug.tpl',
      1 => 1409232623,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1335613967559285756939c1-76424024',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>

 
<fieldset id="bug" class="hidden-div">
    <legend><?php echo smartyTranslate(array('s'=>'Bug report'),$_smarty_tpl);?>
</legend>
    <form id="formBug" class="" method="post">
        <div id="bugInfoCont" class="container">
            <div>
                <label><?php echo smartyTranslate(array('s'=>'Your name'),$_smarty_tpl);?>
:</label>
                <input id="bugName" class="name" type="text" name="name" value="<?php echo $_smarty_tpl->getVariable('slider')->value['info']['name'];?>
" size="50" />
            </div>
            <div>
                <label><?php echo smartyTranslate(array('s'=>'Your email'),$_smarty_tpl);?>
:</label>
                <input id="bugEmail" class="" type="text" name="email" size="50" value="<?php echo $_smarty_tpl->getVariable('slider')->value['info']['email'];?>
" />
            </div>
            <div>
                <label><?php echo smartyTranslate(array('s'=>'Site address'),$_smarty_tpl);?>
:</label>
                <input id="bugSite" class="" type="text" name="domain" size="50" value="<?php echo $_smarty_tpl->getVariable('slider')->value['info']['domain'];?>
" />
            </div>
            <div>
                <label><?php echo smartyTranslate(array('s'=>'Message'),$_smarty_tpl);?>
:</label>
                <textarea id="bugMessage" class="message" name="bugMessage" rows="10" cols="49"></textarea>
            </div>
        </div>
        <div class="comments"> 
            <h3><?php echo smartyTranslate(array('s'=>'Notes'),$_smarty_tpl);?>
</h3>
            <p><?php echo smartyTranslate(array('s'=>'Please describe the bug with as much detail as it`s possible, so we can understand the problem easier.'),$_smarty_tpl);?>
</p>
            <h3><?php echo smartyTranslate(array('s'=>"Important!"),$_smarty_tpl);?>
</h3>
            <p><?php echo smartyTranslate(array('s'=>'By clicking to the "Send" button you agree, that we will get some basic information.'),$_smarty_tpl);?>
</p>
            <ul>
                <li><?php echo smartyTranslate(array('s'=>'Your shop`s name'),$_smarty_tpl);?>
: <b><?php echo $_smarty_tpl->getVariable('slider')->value['info']['name'];?>
</b></li>
                <li><?php echo smartyTranslate(array('s'=>'Your shops e-mail address. IMPORTANT: we just need in case of communicaton!'),$_smarty_tpl);?>
: <b><?php echo $_smarty_tpl->getVariable('slider')->value['info']['email'];?>
</b></li>
                <li><?php echo smartyTranslate(array('s'=>'Your shops domain'),$_smarty_tpl);?>
: <b><?php echo $_smarty_tpl->getVariable('slider')->value['info']['domain'];?>
</b></li>
                <li><?php echo smartyTranslate(array('s'=>'prestashop version'),$_smarty_tpl);?>
: <b><?php echo $_smarty_tpl->getVariable('slider')->value['info']['psVersion'];?>
</b></li>
                <li><?php echo smartyTranslate(array('s'=>'module version'),$_smarty_tpl);?>
: <b><?php echo $_smarty_tpl->getVariable('slider')->value['info']['version'];?>
</b></li>
                <li><?php echo smartyTranslate(array('s'=>'server version'),$_smarty_tpl);?>
: <b><?php echo $_smarty_tpl->getVariable('slider')->value['info']['server'];?>
</b></li>
                <li><?php echo smartyTranslate(array('s'=>'php version'),$_smarty_tpl);?>
: <b><?php echo $_smarty_tpl->getVariable('slider')->value['info']['php'];?>
</b></li>
                <li><?php echo smartyTranslate(array('s'=>'mysql version'),$_smarty_tpl);?>
: <b><?php echo $_smarty_tpl->getVariable('slider')->value['info']['mysql'];?>
</b></li>
                <li><?php echo smartyTranslate(array('s'=>'theme name'),$_smarty_tpl);?>
: <b><?php echo $_smarty_tpl->getVariable('slider')->value['info']['theme'];?>
</b></li>
                <li><?php echo smartyTranslate(array('s'=>'browser version'),$_smarty_tpl);?>
: <b><?php echo $_smarty_tpl->getVariable('slider')->value['info']['userInfo'];?>
</b></li>
            </ul>
        </div>
        <div class="button_cont">
            <input id="submitBug" type="submit" name="submitBug" value="Send" class="button green" />
            <input id="bugPsVersion" type="hidden" value="<?php echo $_smarty_tpl->getVariable('slider')->value['info']['psVersion'];?>
" name="psversion" />
            <input id="bugVersion" type="hidden" value="<?php echo $_smarty_tpl->getVariable('slider')->value['info']['version'];?>
" name="version" />
            <input id="bugServer" type="hidden" value="<?php echo $_smarty_tpl->getVariable('slider')->value['info']['server'];?>
" name="server" />
            <input id="bugPhp" type="hidden" value="<?php echo $_smarty_tpl->getVariable('slider')->value['info']['php'];?>
" name="php" />
            <input id="bugMysql" type="hidden" value="<?php echo $_smarty_tpl->getVariable('slider')->value['info']['mysql'];?>
" name="mysql" />
            <input id="bugTheme" type="hidden" value="<?php echo $_smarty_tpl->getVariable('slider')->value['info']['theme'];?>
" name="theme" />
            <input id="bugUserInfo" type="hidden" value="<?php echo $_smarty_tpl->getVariable('slider')->value['info']['userInfo'];?>
" name="userInfo" />
        </div>
    </form>
    <div class="response" style="display:none;">
        <span class="conf"><?php echo smartyTranslate(array('s'=>'Message sent successfull! Thank you for your time.'),$_smarty_tpl);?>
</span>
        <span class="error"><?php echo smartyTranslate(array('s'=>'Something went wrong, please try again later.'),$_smarty_tpl);?>
</span>
        <span class="empty"><?php echo smartyTranslate(array('s'=>'Name and Message is required!'),$_smarty_tpl);?>
</span>
    </div>
</fieldset>