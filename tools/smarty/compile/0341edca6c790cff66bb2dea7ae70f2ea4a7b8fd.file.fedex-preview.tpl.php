<?php /* Smarty version Smarty-3.0.7, created on 2015-08-07 12:39:32
         compiled from "/Applications/MAMP/htdocs/fedexcollection/modules/fedex/fedex-preview.tpl" */ ?>
<?php /*%%SmartyHeaderCode:59818810055c48ae40fccd9-08725955%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '0341edca6c790cff66bb2dea7ae70f2ea4a7b8fd' => 
    array (
      0 => '/Applications/MAMP/htdocs/fedexcollection/modules/fedex/fedex-preview.tpl',
      1 => 1424153217,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '59818810055c48ae40fccd9-08725955',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php if (!is_callable('smarty_modifier_escape')) include '/Applications/MAMP/htdocs/fedexcollection/tools/smarty/plugins/modifier.escape.php';
?><!-- FEDEX SHIPPING RATE PREVIEW -->
<script type="text/javascript" src="<?php echo $_smarty_tpl->getVariable('this_fedex_path')->value;?>
js/fedex-preview.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo $_smarty_tpl->getVariable('this_fedex_path')->value;?>
css/fedex.css" />
<script type="text/javascript" src="<?php echo $_smarty_tpl->getVariable('this_fedex_path')->value;?>
js/statesManagement.js"></script>
<script type="text/javascript">
//<![CDATA[
	fedex_countries = new Array();
	<?php  $_smarty_tpl->tpl_vars['country'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('fedex_countries')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['country']->key => $_smarty_tpl->tpl_vars['country']->value){
?>  
		<?php if (isset($_smarty_tpl->tpl_vars['country']->value['states'])&&$_smarty_tpl->tpl_vars['country']->value['contains_states']){?>
			fedex_countries[<?php echo intval($_smarty_tpl->tpl_vars['country']->value['id_country']);?>
] = new Array();
			<?php  $_smarty_tpl->tpl_vars['state'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['country']->value['states']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['state']->key => $_smarty_tpl->tpl_vars['state']->value){
?>
				fedex_countries[<?php echo intval($_smarty_tpl->tpl_vars['country']->value['id_country']);?>
].push({'id' : '<?php echo $_smarty_tpl->tpl_vars['state']->value['id_state'];?>
', 'name' : '<?php echo smarty_modifier_escape($_smarty_tpl->tpl_vars['state']->value['name'],'htmlall','UTF-8');?>
'});
			<?php }} ?>
		<?php }?>
	<?php }} ?>
	var fedex_invalid_zip = "<?php echo smartyTranslate(array('s'=>'Invalid Zipcode, if your country does not use zipcodes, enter 11111','mod'=>'fedex','js'=>1),$_smarty_tpl);?>
";
	var fedex_please_wait = "<?php echo smartyTranslate(array('s'=>'Please Wait','mod'=>'fedex','js'=>1),$_smarty_tpl);?>
";
	var fedex_hide = "<?php echo smartyTranslate(array('s'=>'Hide','mod'=>'fedex','js'=>1),$_smarty_tpl);?>
";
	var fedex_show = "<?php echo smartyTranslate(array('s'=>'Shipping Rates','mod'=>'fedex','js'=>1),$_smarty_tpl);?>
";
	
<?php if ($_smarty_tpl->getVariable('psVersion')->value==1.6){?>
	$(document).ready(function(){
		if(typeof uniform == 'function')
		{
			$("#fedex_dest_country<?php echo $_smarty_tpl->getVariable('fedex_is_cart')->value;?>
, #fedex_dest_state<?php echo $_smarty_tpl->getVariable('fedex_is_cart')->value;?>
, #fedex_dest_city<?php echo $_smarty_tpl->getVariable('fedex_is_cart')->value;?>
, #fedex_dest_zip<?php echo $_smarty_tpl->getVariable('fedex_is_cart')->value;?>
").uniform();
			
			$("#uniform-fedex_dest_country<?php echo $_smarty_tpl->getVariable('fedex_is_cart')->value;?>
, #uniform-fedex_dest_state<?php echo $_smarty_tpl->getVariable('fedex_is_cart')->value;?>
").css('width', '100%');
			$("#uniform-fedex_dest_country<?php echo $_smarty_tpl->getVariable('fedex_is_cart')->value;?>
, #uniform-fedex_dest_state<?php echo $_smarty_tpl->getVariable('fedex_is_cart')->value;?>
").children("span").css('width', '100%');
		}
	});
<?php }?>
</script>
<?php if ((!isset($_smarty_tpl->getVariable('fedex_dest_country',null,true,false)->value)||!$_smarty_tpl->getVariable('fedex_dest_country')->value)&&isset($_smarty_tpl->getVariable('fedex_default_country',null,true,false)->value)){?>
	<?php $_smarty_tpl->tpl_vars['fedex_dest_country'] = new Smarty_variable($_smarty_tpl->getVariable('fedex_default_country')->value, null, null);?>
<?php }?>

<div class="fedex_preview_container<?php echo $_smarty_tpl->getVariable('fedex_is_cart')->value;?>
" <?php if ($_smarty_tpl->getVariable('psVersion')->value==1.6){?>style="margin:10px 0;float: none; <?php if ($_smarty_tpl->getVariable('fedex_is_cart')->value!=''){?>padding: 0 0 10px 0;<?php }?>"<?php }?>>
	<p class="buttons_bottom_block" style="padding: 5px;<?php if ($_smarty_tpl->getVariable('psVersion')->value==1.6&&$_smarty_tpl->getVariable('fedex_is_cart')->value!=''){?>margin: 0; text-align: center;<?php }?>">
		<a href="javascript:void(0)" id="fedex_shipping_rates_button<?php echo $_smarty_tpl->getVariable('fedex_is_cart')->value;?>
" onclick="
			<?php if ($_smarty_tpl->getVariable('fedex_get_rates')->value!=1){?>
				$('#fedex_shipping_rates<?php echo $_smarty_tpl->getVariable('fedex_is_cart')->value;?>
').fadeIn(800);
				fedex_city_display(0, '<?php echo $_smarty_tpl->getVariable('fedex_is_cart')->value;?>
');
				fedex_define_hide_button($(this), '<?php echo $_smarty_tpl->getVariable('fedex_is_cart')->value;?>
'); 
			<?php }else{ ?>
				fedex_get_rates(false,'<?php echo $_smarty_tpl->getVariable('fedex_is_cart')->value;?>
');
				fedex_city_display(0, '<?php echo $_smarty_tpl->getVariable('fedex_is_cart')->value;?>
');
			<?php }?>" class="<?php if ($_smarty_tpl->getVariable('psVersion')->value==1.6){?>btn btn-default<?php }else{ ?>exclusive<?php }?>" style="<?php if ($_smarty_tpl->getVariable('fedex_is_cart')->value!=''){?>margin: auto;<?php }?> <?php if ($_smarty_tpl->getVariable('psVersion')->value==1.6){?>color: #333333; width: 100%;<?php }?>"><?php echo smartyTranslate(array('s'=>'Shipping Rates','mod'=>'fedex'),$_smarty_tpl);?>
</a>
	</p>
	<div id="fedex_shipping_rates<?php echo $_smarty_tpl->getVariable('fedex_is_cart')->value;?>
" style="<?php if ($_smarty_tpl->getVariable('fedex_get_rates')->value!=1){?>display:none;<?php }?>">
	<?php if ($_smarty_tpl->getVariable('fedex_get_rates')->value!=1){?>
		<div id="fedex_address<?php echo $_smarty_tpl->getVariable('fedex_is_cart')->value;?>
">  
			<a href="javascript:void(0)" id="fedex_shipping_rates_button" class="fedex_hide_button">X</a>
			
			<span style="font-weight: bold;">
				<?php echo smartyTranslate(array('s'=>'Please','mod'=>'fedex'),$_smarty_tpl);?>

				<a href="<?php echo $_smarty_tpl->getVariable('base_dir')->value;?>
authentication.php" style="text-decoration: underline"><?php echo smartyTranslate(array('s'=>'Login','mod'=>'fedex'),$_smarty_tpl);?>
</a><?php echo smartyTranslate(array('s'=>', or enter your','mod'=>'fedex'),$_smarty_tpl);?>

			</span>
		</div>

		<div id="fedex_dest_change<?php echo $_smarty_tpl->getVariable('fedex_is_cart')->value;?>
" style="<?php if ($_smarty_tpl->getVariable('fedex_get_rates')->value!=1){?>display:block;<?php }?>">
		
			<?php $_smarty_tpl->tpl_vars['inLine'] = new Smarty_variable(0, null, null);?>  
			<div id="fedex_line" <?php if ($_smarty_tpl->getVariable('psVersion')->value==1.6){?>style="padding: 0 5px;"<?php }?>>
				
				<?php if ($_smarty_tpl->getVariable('fedex_address_display')->value['country']){?>
					<?php $_smarty_tpl->tpl_vars['inLine'] = new Smarty_variable($_smarty_tpl->getVariable('inLine')->value+1, null, null);?>
						<span>
							<select onchange="fedex_preview_update_state('<?php echo $_smarty_tpl->getVariable('fedex_is_cart')->value;?>
', 0)" name="fedex_dest_country<?php echo $_smarty_tpl->getVariable('fedex_is_cart')->value;?>
" id="fedex_dest_country<?php echo $_smarty_tpl->getVariable('fedex_is_cart')->value;?>
">
								<?php  $_smarty_tpl->tpl_vars['fedex_country'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('fedex_countries')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['fedex_country']->key => $_smarty_tpl->tpl_vars['fedex_country']->value){
?>
									<option value="<?php echo $_smarty_tpl->tpl_vars['fedex_country']->value['id_country'];?>
" <?php if (isset($_smarty_tpl->getVariable('fedex_dest_country',null,true,false)->value)&&$_smarty_tpl->getVariable('fedex_dest_country')->value==$_smarty_tpl->tpl_vars['fedex_country']->value['iso_code']){?> selected="selected"<?php }?>><?php echo smarty_modifier_escape($_smarty_tpl->tpl_vars['fedex_country']->value['name'],'htmlall','UTF-8');?>
</option>
								<?php }} ?>
							</select>
						</span>
				<?php }?>
				
			<?php if ((!$_smarty_tpl->getVariable('fedex_is_cart')->value&&$_smarty_tpl->getVariable('inLine')->value==1)||($_smarty_tpl->getVariable('fedex_is_cart')->value&&$_smarty_tpl->getVariable('inLine')->value==1)){?>
				</div>
				<?php $_smarty_tpl->tpl_vars['inLine'] = new Smarty_variable(0, null, null);?>
				<div id="fedex_line" <?php if ($_smarty_tpl->getVariable('psVersion')->value==1.6){?>style="padding: 0 5px;"<?php }?>>
			<?php }?>
				
				<?php if ($_smarty_tpl->getVariable('fedex_address_display')->value['state']){?>
					<?php $_smarty_tpl->tpl_vars['inLine'] = new Smarty_variable($_smarty_tpl->getVariable('inLine')->value+1, null, null);?>
						<span>
							<select name="fedex_dest_state<?php echo $_smarty_tpl->getVariable('fedex_is_cart')->value;?>
" id="fedex_dest_state<?php echo $_smarty_tpl->getVariable('fedex_is_cart')->value;?>
">
								<option value="">-- <?php echo smartyTranslate(array('s'=>'State','mod'=>'fedex'),$_smarty_tpl);?>
 --</option>
							</select>
						</span>
				<?php }?>
					
			<?php if ((!$_smarty_tpl->getVariable('fedex_is_cart')->value&&$_smarty_tpl->getVariable('inLine')->value==1)||($_smarty_tpl->getVariable('fedex_is_cart')->value&&$_smarty_tpl->getVariable('inLine')->value==1)){?>
				</div>
				<?php $_smarty_tpl->tpl_vars['inLine'] = new Smarty_variable(0, null, null);?>
				<div id="fedex_line" <?php if ($_smarty_tpl->getVariable('psVersion')->value==1.6){?>style="padding: 0 5px;"<?php }?>>
			<?php }?>

				<?php if ($_smarty_tpl->getVariable('fedex_address_display')->value['city']){?>
					<?php $_smarty_tpl->tpl_vars['inLine'] = new Smarty_variable($_smarty_tpl->getVariable('inLine')->value+1, null, null);?>
						<span>
							<input placeholder="<?php echo smartyTranslate(array('s'=>'City Name','mod'=>'fedex'),$_smarty_tpl);?>
" type="text" name="fedex_dest_city<?php echo $_smarty_tpl->getVariable('fedex_is_cart')->value;?>
" id="fedex_dest_city<?php echo $_smarty_tpl->getVariable('fedex_is_cart')->value;?>
"
								<?php if (isset($_smarty_tpl->getVariable('fedex_dest_city',null,true,false)->value)&&$_smarty_tpl->getVariable('fedex_dest_city')->value!=''){?>
									value="<?php echo $_smarty_tpl->getVariable('fedex_dest_city')->value;?>
"
								<?php }elseif($_smarty_tpl->getVariable('fedex_is_cart')->value!=''){?>
									value="<?php echo smartyTranslate(array('s'=>'City','mod'=>'fedex'),$_smarty_tpl);?>
" 
									onclick="$(this).val('')"
								<?php }?> 
							/>
						</span>
				<?php }?>
					
			<?php if ((!$_smarty_tpl->getVariable('fedex_is_cart')->value&&$_smarty_tpl->getVariable('inLine')->value==1)||($_smarty_tpl->getVariable('fedex_is_cart')->value&&$_smarty_tpl->getVariable('inLine')->value==1)){?>
				</div>
				<?php $_smarty_tpl->tpl_vars['inLine'] = new Smarty_variable(0, null, null);?>
				<div id="fedex_line" <?php if ($_smarty_tpl->getVariable('psVersion')->value==1.6){?>style="padding: 0 5px;"<?php }?>>
			<?php }?>
					
				<?php if ($_smarty_tpl->getVariable('fedex_address_display')->value['zip']){?>
					<?php $_smarty_tpl->tpl_vars['inLine'] = new Smarty_variable($_smarty_tpl->getVariable('inLine')->value+1, null, null);?>
						<span>
							<input placeholder="<?php echo smartyTranslate(array('s'=>'Zip Code','mod'=>'fedex'),$_smarty_tpl);?>
" type="text" <?php if (!$_smarty_tpl->getVariable('fedex_address_display')->value['zip']){?>hide="1"<?php }?> name="fedex_dest_zip<?php echo $_smarty_tpl->getVariable('fedex_is_cart')->value;?>
" id="fedex_dest_zip<?php echo $_smarty_tpl->getVariable('fedex_is_cart')->value;?>
"
								<?php if (isset($_smarty_tpl->getVariable('fedex_dest_zip',null,true,false)->value)&&$_smarty_tpl->getVariable('fedex_dest_zip')->value!=''){?>
									value="<?php echo $_smarty_tpl->getVariable('fedex_dest_zip')->value;?>
"
								<?php }elseif($_smarty_tpl->getVariable('fedex_is_cart')->value!=''){?>
									value="<?php echo smartyTranslate(array('s'=>'Zip','mod'=>'fedex'),$_smarty_tpl);?>
" 
									onclick="$(this).val('')"
								<?php }?>
							/>
						</span>
				<?php }?>
				
			</div>          

			<span class="submit_button">
				<a href="javascript:void(0)" id="fedex_submit_location<?php echo $_smarty_tpl->getVariable('fedex_is_cart')->value;?>
" onclick="fedex_get_rates(true,'<?php echo $_smarty_tpl->getVariable('fedex_is_cart')->value;?>
')" class="<?php if ($_smarty_tpl->getVariable('psVersion')->value==1.6){?>btn btn-default<?php }else{ ?>exclusive<?php }?>" style="<?php if ($_smarty_tpl->getVariable('fedex_is_cart')->value!=''){?>margin: auto;<?php }?> <?php if ($_smarty_tpl->getVariable('psVersion')->value==1.6){?>color: #333333;<?php }?>"><?php echo smartyTranslate(array('s'=>'Submit','mod'=>'fedex'),$_smarty_tpl);?>
</a>
			</span>
				
		</div>
	<?php }?>
	</div>
</div>
<script type="text/javascript">
fedex_preview_update_state('<?php echo $_smarty_tpl->getVariable('fedex_is_cart')->value;?>
', <?php if (isset($_smarty_tpl->getVariable('fedex_dest_state',null,true,false)->value)){?><?php echo intval($_smarty_tpl->getVariable('fedex_dest_state')->value);?>
<?php }else{ ?>0<?php }?>);
</script>
<!-- /FEDEX SHIPPNG RATE PREVIEW -->
