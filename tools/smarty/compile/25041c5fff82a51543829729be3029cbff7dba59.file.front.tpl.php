<?php /* Smarty version Smarty-3.0.7, created on 2015-06-30 14:23:04
         compiled from "/srv/data/web/vhosts/www.socatec.fr/htdocs/fedexcollection/modules/minicslider/tpl/front.tpl" */ ?>
<?php /*%%SmartyHeaderCode:32126160755928a2816b739-35771292%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '25041c5fff82a51543829729be3029cbff7dba59' => 
    array (
      0 => '/srv/data/web/vhosts/www.socatec.fr/htdocs/fedexcollection/modules/minicslider/tpl/front.tpl',
      1 => 1409232633,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '32126160755928a2816b739-35771292',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>


<!-- MINIC SLIDER -->	
<?php if ($_smarty_tpl->getVariable('minicSlider')->value['options']['single']==0){?>
    <?php if (count($_smarty_tpl->getVariable('slides')->value[$_smarty_tpl->getVariable('lang_iso')->value])!=0){?>
        <div id="minic_slider" class="theme-default<?php if ($_smarty_tpl->getVariable('minicSlider')->value['options']['thumbnail']==1&&$_smarty_tpl->getVariable('minicSlider')->value['options']['control']!=0){?> controlnav-thumbs<?php }?>">   
            <div id="slider" class="nivoSlider" style="<?php if ($_smarty_tpl->getVariable('minicSlider')->value['options']['width']){?>width:<?php echo $_smarty_tpl->getVariable('minicSlider')->value['options']['width'];?>
px;<?php }?><?php if ($_smarty_tpl->getVariable('minicSlider')->value['options']['control']!=1){?>margin-bottom:0;<?php }?>">
                <?php  $_smarty_tpl->tpl_vars['image'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('slides')->value[$_smarty_tpl->getVariable('defaultLangIso')->value]; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['image']->key => $_smarty_tpl->tpl_vars['image']->value){
?>
                    <?php if ($_smarty_tpl->tpl_vars['image']->value['url']!=''){?><a href="<?php echo $_smarty_tpl->tpl_vars['image']->value['url'];?>
" <?php if ($_smarty_tpl->tpl_vars['image']->value['target']==1){?>target="_blank"<?php }?>><?php }?>
                        <img src="<?php echo $_smarty_tpl->getVariable('minicSlider')->value['path']['images'];?>
<?php echo $_smarty_tpl->tpl_vars['image']->value['image'];?>
" class="slider_image" 
                            <?php if ($_smarty_tpl->tpl_vars['image']->value['alt']){?>alt="<?php echo $_smarty_tpl->tpl_vars['image']->value['alt'];?>
"<?php }?>
                            <?php if ($_smarty_tpl->tpl_vars['image']->value['title']!=''||$_smarty_tpl->tpl_vars['image']->value['caption']!=''){?>title="#htmlcaption_<?php echo $_smarty_tpl->tpl_vars['image']->value['slide_id'];?>
"<?php }?> 
                            <?php if ($_smarty_tpl->getVariable('minicSlider')->value['options']['thumbnail']==1){?>data-thumb="<?php echo $_smarty_tpl->getVariable('minicSlider')->value['path']['thumbs'];?>
<?php echo $_smarty_tpl->tpl_vars['image']->value['image'];?>
"<?php }?>/>
                    <?php if ($_smarty_tpl->tpl_vars['image']->value['url']!=''){?></a><?php }?>
                <?php }} ?>
            </div>
            <?php  $_smarty_tpl->tpl_vars['caption'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('slides')->value[$_smarty_tpl->getVariable('defaultLangIso')->value]; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['caption']->key => $_smarty_tpl->tpl_vars['caption']->value){
?>
                <?php if ($_smarty_tpl->tpl_vars['caption']->value['title']!=''||$_smarty_tpl->tpl_vars['caption']->value['caption']!=''){?>
                    <div id="htmlcaption_<?php echo $_smarty_tpl->tpl_vars['caption']->value['slide_id'];?>
" class="nivo-html-caption">
                        <h3><?php echo $_smarty_tpl->tpl_vars['caption']->value['title'];?>
</h3>
                        <p><?php echo $_smarty_tpl->tpl_vars['caption']->value['caption'];?>
</p>
                    </div>
                <?php }?>
            <?php }} ?>
        </div>
    <?php }?>                      
<?php }else{ ?>
    <?php if (count($_smarty_tpl->getVariable('slides')->value[$_smarty_tpl->getVariable('lang_iso')->value])!=0){?>
    <div id="minic_slider" class="theme-default<?php if ($_smarty_tpl->getVariable('minicSlider')->value['options']['thumbnail']==1&&$_smarty_tpl->getVariable('minicSlider')->value['options']['control']!=0){?> controlnav-thumbs<?php }?>">
        <?php  $_smarty_tpl->tpl_vars['slide'] = new Smarty_Variable;
 $_smarty_tpl->tpl_vars['iso'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('slides')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['slide']->key => $_smarty_tpl->tpl_vars['slide']->value){
 $_smarty_tpl->tpl_vars['iso']->value = $_smarty_tpl->tpl_vars['slide']->key;
?>
            <?php if ($_smarty_tpl->tpl_vars['iso']->value==$_smarty_tpl->getVariable('lang_iso')->value&&count($_smarty_tpl->tpl_vars['iso']->value)!=0){?>
                <div id="slider" class="nivoSlider" style="<?php if ($_smarty_tpl->getVariable('minicSlider')->value['options']['width']){?>width:<?php echo $_smarty_tpl->getVariable('minicSlider')->value['options']['width'];?>
px;<?php }?><?php if ($_smarty_tpl->getVariable('minicSlider')->value['options']['control']!=1){?>margin-bottom:0;<?php }?>">
                    <?php  $_smarty_tpl->tpl_vars['image'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['slide']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['image']->key => $_smarty_tpl->tpl_vars['image']->value){
?>
                        <?php if ($_smarty_tpl->tpl_vars['image']->value['url']!=''){?><a href="<?php echo $_smarty_tpl->tpl_vars['image']->value['url'];?>
"><?php }?>
                            <img src="<?php echo $_smarty_tpl->getVariable('minicSlider')->value['path']['images'];?>
<?php echo $_smarty_tpl->tpl_vars['image']->value['image'];?>
" class="slider_image" alt="<?php echo $_smarty_tpl->tpl_vars['image']->value['alt'];?>
" 
                                <?php if ($_smarty_tpl->tpl_vars['image']->value['title']!=''||$_smarty_tpl->tpl_vars['image']->value['caption']!=''){?>title="#htmlcaption_<?php echo $_smarty_tpl->tpl_vars['image']->value['slide_id'];?>
"<?php }?> 
                                <?php if ($_smarty_tpl->getVariable('minicSlider')->value['options']['thumbnail']==1){?>data-thumb="<?php echo $_smarty_tpl->getVariable('minicSlider')->value['path']['thumbs'];?>
<?php echo $_smarty_tpl->tpl_vars['image']->value['image'];?>
"<?php }?>/>
                        <?php if ($_smarty_tpl->tpl_vars['image']->value['url']!=''){?></a><?php }?>
                    <?php }} ?>
                </div>
                <?php  $_smarty_tpl->tpl_vars['caption'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['slide']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['caption']->key => $_smarty_tpl->tpl_vars['caption']->value){
?>
                    <?php if ($_smarty_tpl->tpl_vars['caption']->value['title']!=''||$_smarty_tpl->tpl_vars['caption']->value['caption']!=''){?>
                        <div id="htmlcaption_<?php echo $_smarty_tpl->tpl_vars['caption']->value['slide_id'];?>
" class="nivo-html-caption">
                            <h3><?php echo $_smarty_tpl->tpl_vars['caption']->value['title'];?>
</h3>
                            <p><?php echo $_smarty_tpl->tpl_vars['caption']->value['caption'];?>
</p>
                        </div>
                    <?php }?>
                <?php }} ?>
            <?php }?>
        <?php }} ?>
    </div>
    <?php }?>
<?php }?>    
<script type="text/javascript">
$(window).load(function() {
    $('#slider').nivoSlider({
        effect: '<?php if ($_smarty_tpl->getVariable('minicSlider')->value['options']['current']!=''){?><?php echo $_smarty_tpl->getVariable('minicSlider')->value['options']['current'];?>
<?php }else{ ?>random<?php }?>', 
        slices: <?php if ($_smarty_tpl->getVariable('minicSlider')->value['options']['slices']!=''){?><?php echo $_smarty_tpl->getVariable('minicSlider')->value['options']['slices'];?>
<?php }else{ ?>15<?php }?>, 
        boxCols: <?php if ($_smarty_tpl->getVariable('minicSlider')->value['options']['slices']!=''){?><?php echo $_smarty_tpl->getVariable('minicSlider')->value['options']['cols'];?>
<?php }else{ ?>8<?php }?>, 
        boxRows: <?php if ($_smarty_tpl->getVariable('minicSlider')->value['options']['rows']!=''){?><?php echo $_smarty_tpl->getVariable('minicSlider')->value['options']['rows'];?>
<?php }else{ ?>4<?php }?>, 
        animSpeed: <?php if ($_smarty_tpl->getVariable('minicSlider')->value['options']['speed']!=''){?><?php echo $_smarty_tpl->getVariable('minicSlider')->value['options']['speed'];?>
<?php }else{ ?>500<?php }?>, 
        pauseTime: <?php if ($_smarty_tpl->getVariable('minicSlider')->value['options']['pause']!=''){?><?php echo $_smarty_tpl->getVariable('minicSlider')->value['options']['pause'];?>
<?php }else{ ?>3000<?php }?>, 
        startSlide: <?php if ($_smarty_tpl->getVariable('minicSlider')->value['options']['startSlide']!=''){?><?php echo $_smarty_tpl->getVariable('minicSlider')->value['options']['startSlide'];?>
<?php }else{ ?>0<?php }?>,
        directionNav: <?php if ($_smarty_tpl->getVariable('minicSlider')->value['options']['buttons']==1){?>true<?php }else{ ?>false<?php }?>, 
        controlNav: <?php if ($_smarty_tpl->getVariable('minicSlider')->value['options']['control']==1){?>true<?php }else{ ?>false<?php }?>, 
        controlNavThumbs: <?php if ($_smarty_tpl->getVariable('minicSlider')->value['options']['thumbnail']==1){?>true<?php }else{ ?>false<?php }?>,
        pauseOnHover: <?php if ($_smarty_tpl->getVariable('minicSlider')->value['options']['hover']==1){?>true<?php }else{ ?>false<?php }?>, 
        manualAdvance: <?php if ($_smarty_tpl->getVariable('minicSlider')->value['options']['manual']==1){?>true<?php }else{ ?>false<?php }?>, 
        prevText: '<?php echo smartyTranslate(array('s'=>'Prev'),$_smarty_tpl);?>
', 
        nextText: '<?php echo smartyTranslate(array('s'=>'Next'),$_smarty_tpl);?>
', 
        randomStart: <?php if ($_smarty_tpl->getVariable('minicSlider')->value['options']['random']==1){?>true<?php }else{ ?>false<?php }?>
    });
});
</script>
