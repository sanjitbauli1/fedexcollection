<?php /* Smarty version Smarty-3.0.7, created on 2015-06-30 14:03:01
         compiled from "/srv/data/web/vhosts/www.socatec.fr/htdocs/fedexcollection/modules/minicslider/tpl/admin.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1772542253559285750ac389-62295692%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '18b298eda0663c3e2f2c49572e66eb0dc073231e' => 
    array (
      0 => '/srv/data/web/vhosts/www.socatec.fr/htdocs/fedexcollection/modules/minicslider/tpl/admin.tpl',
      1 => 1409232630,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1772542253559285750ac389-62295692',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>


<?php if ($_smarty_tpl->getVariable('slider')->value['firstStart']){?>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#sendInfoWindow').fadeIn();
            $('#sendInfo').click(function(event){
                $('#sendInfoWindow').fadeOut();
                $.getJSON("http://module.minic.ro/slider/process.php?domain=<?php echo $_smarty_tpl->getVariable('slider')->value['info']['domain'];?>
&psversion=<?php echo $_smarty_tpl->getVariable('slider')->value['info']['psVersion'];?>
&version=<?php echo $_smarty_tpl->getVariable('slider')->value['info']['version'];?>
&email="+$('#sendInfoEmail').val()+"&action=install&callback=?");
            });
        });
    </script>
<?php }?>

<?php if ($_smarty_tpl->getVariable('error')->value){?><div class="error"><?php echo $_smarty_tpl->getVariable('error')->value;?>
</div><?php }?>
<?php if ($_smarty_tpl->getVariable('confirmation')->value){?><div class="conf"><?php echo $_smarty_tpl->getVariable('confirmation')->value;?>
</div><?php }?>
<div id="fixed_conf" class="conf response" style="display:none;"><p></p><span class="close">Close</span></div>
<div id="fixed_error" class="error response" style="display:none;"><p></p><span class="close">Close</span></div>

<div class="slider_admin">
    <div id="slider-header">
        <div id="feedback_navigation" class="slider_navigation">
            <a href="#bug"><?php echo smartyTranslate(array('s'=>'Bug Report'),$_smarty_tpl);?>
</a>
            <a href="#feedback"><?php echo smartyTranslate(array('s'=>'Feedback'),$_smarty_tpl);?>
</a>
        </div>
        <div id="sliderBanner">
            <a href="#" id="donate">
                <img src="<?php echo $_smarty_tpl->getVariable('module_template_dir')->value;?>
img/donate.jpg">
            </a>
        </div>
        <div id="slider_navigation" class="slider_navigation">
            <a href="#new_slide" id="addNew-button"><?php echo smartyTranslate(array('s'=>'Add New'),$_smarty_tpl);?>
</a>
            <a href="#optionsForm" id="options-button"><?php echo smartyTranslate(array('s'=>'Options'),$_smarty_tpl);?>
</a>
        </div>
    </div>
    <?php $_template = new Smarty_Internal_Template(($_smarty_tpl->getVariable('slider')->value['tpl']['options']), $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php unset($_template);?>
    <?php $_template = new Smarty_Internal_Template(($_smarty_tpl->getVariable('slider')->value['tpl']['new']), $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php unset($_template);?>
    <?php $_template = new Smarty_Internal_Template(($_smarty_tpl->getVariable('slider')->value['tpl']['feedback']), $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php unset($_template);?>
    <?php $_template = new Smarty_Internal_Template(($_smarty_tpl->getVariable('slider')->value['tpl']['bug']), $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php unset($_template);?>
    <?php $_template = new Smarty_Internal_Template(($_smarty_tpl->getVariable('slider')->value['tpl']['slides']), $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php unset($_template);?>
    <div class="donations">
        <form id="paypal" target="_blank" action="https://www.paypal.com/cgi-bin/webscr" method="post">
            <input type="hidden" name="cmd" value="_s-xclick">
            <input type="hidden" name="encrypted" value="-----BEGIN PKCS7-----MIIHVwYJKoZIhvcNAQcEoIIHSDCCB0QCAQExggEwMIIBLAIBADCBlDCBjjELMAkGA1UEBhMCVVMxCzAJBgNVBAgTAkNBMRYwFAYDVQQHEw1Nb3VudGFpbiBWaWV3MRQwEgYDVQQKEwtQYXlQYWwgSW5jLjETMBEGA1UECxQKbGl2ZV9jZXJ0czERMA8GA1UEAxQIbGl2ZV9hcGkxHDAaBgkqhkiG9w0BCQEWDXJlQHBheXBhbC5jb20CAQAwDQYJKoZIhvcNAQEBBQAEgYBD0G6HMb/F/tXLaCWxIZp8lkBDuWFgoh6o84Kc4P9fHUKnP3tJDhAhK4ygSRSU11doi9HHfLGn4IqZnViMoLRfj1l347cs28aWcQmVt9FmmokzNdQJynDiMdA0BLM8sJBvs+lc2qcMcg61cK0AVTVAZbCapB4HVPX3uGz6s9ECpzELMAkGBSsOAwIaBQAwgdQGCSqGSIb3DQEHATAUBggqhkiG9w0DBwQItx5o23dLAFCAgbCEWVi44T78lcHHaWpYNUk+cBOuXSRXoqBnHTfQlmgySFLriah7hwH0ci6E8+68Y6JMgM85dRelxZGxBC95bVr8cngh82iCb66spCgf5O1GQ+C+MfHxYKeVESAECRAqq0M8ss4oys5eK3yjX+yB5Xqvy0cAuFH7uxmJLVA8aWvEZkO4RDK4PvVsf8q3GvAXDD83Cn1X9FRXJn8qhLMkbTI7LtEFM54F00+VwHP5ntKa9aCCA4cwggODMIIC7KADAgECAgEAMA0GCSqGSIb3DQEBBQUAMIGOMQswCQYDVQQGEwJVUzELMAkGA1UECBMCQ0ExFjAUBgNVBAcTDU1vdW50YWluIFZpZXcxFDASBgNVBAoTC1BheVBhbCBJbmMuMRMwEQYDVQQLFApsaXZlX2NlcnRzMREwDwYDVQQDFAhsaXZlX2FwaTEcMBoGCSqGSIb3DQEJARYNcmVAcGF5cGFsLmNvbTAeFw0wNDAyMTMxMDEzMTVaFw0zNTAyMTMxMDEzMTVaMIGOMQswCQYDVQQGEwJVUzELMAkGA1UECBMCQ0ExFjAUBgNVBAcTDU1vdW50YWluIFZpZXcxFDASBgNVBAoTC1BheVBhbCBJbmMuMRMwEQYDVQQLFApsaXZlX2NlcnRzMREwDwYDVQQDFAhsaXZlX2FwaTEcMBoGCSqGSIb3DQEJARYNcmVAcGF5cGFsLmNvbTCBnzANBgkqhkiG9w0BAQEFAAOBjQAwgYkCgYEAwUdO3fxEzEtcnI7ZKZL412XvZPugoni7i7D7prCe0AtaHTc97CYgm7NsAtJyxNLixmhLV8pyIEaiHXWAh8fPKW+R017+EmXrr9EaquPmsVvTywAAE1PMNOKqo2kl4Gxiz9zZqIajOm1fZGWcGS0f5JQ2kBqNbvbg2/Za+GJ/qwUCAwEAAaOB7jCB6zAdBgNVHQ4EFgQUlp98u8ZvF71ZP1LXChvsENZklGswgbsGA1UdIwSBszCBsIAUlp98u8ZvF71ZP1LXChvsENZklGuhgZSkgZEwgY4xCzAJBgNVBAYTAlVTMQswCQYDVQQIEwJDQTEWMBQGA1UEBxMNTW91bnRhaW4gVmlldzEUMBIGA1UEChMLUGF5UGFsIEluYy4xEzARBgNVBAsUCmxpdmVfY2VydHMxETAPBgNVBAMUCGxpdmVfYXBpMRwwGgYJKoZIhvcNAQkBFg1yZUBwYXlwYWwuY29tggEAMAwGA1UdEwQFMAMBAf8wDQYJKoZIhvcNAQEFBQADgYEAgV86VpqAWuXvX6Oro4qJ1tYVIT5DgWpE692Ag422H7yRIr/9j/iKG4Thia/Oflx4TdL+IFJBAyPK9v6zZNZtBgPBynXb048hsP16l2vi0k5Q2JKiPDsEfBhGI+HnxLXEaUWAcVfCsQFvd2A1sxRr67ip5y2wwBelUecP3AjJ+YcxggGaMIIBlgIBATCBlDCBjjELMAkGA1UEBhMCVVMxCzAJBgNVBAgTAkNBMRYwFAYDVQQHEw1Nb3VudGFpbiBWaWV3MRQwEgYDVQQKEwtQYXlQYWwgSW5jLjETMBEGA1UECxQKbGl2ZV9jZXJ0czERMA8GA1UEAxQIbGl2ZV9hcGkxHDAaBgkqhkiG9w0BCQEWDXJlQHBheXBhbC5jb20CAQAwCQYFKw4DAhoFAKBdMBgGCSqGSIb3DQEJAzELBgkqhkiG9w0BBwEwHAYJKoZIhvcNAQkFMQ8XDTEyMTAxMDEyNTE1MFowIwYJKoZIhvcNAQkEMRYEFKrImQ4JhhUSPlerIMYs7oUGAWrVMA0GCSqGSIb3DQEBAQUABIGAStumTFrW8GoOrteq1Bxbv8es/HI8X5BS7uOm6vnPsJIsQ2GLzUXSpgllcuBNRgxxkQNKtVPt7MGcjl1u86V9zx5KIJJVmxgA6bZOqLkdOiNOkMi/OZnqPnHphNuuEpvh1dTy7gQYnBqumOpfQ/jveQRcVutt9C6lHF4VwDXKj4g=-----END PKCS7-----
              ">
            <input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_donate_SM.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
            <img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
        </form>
    </div>      
    <div id="deleteWindow" class="confirmation_window">
        <div class="conf_header">
            <a href="#" class="close_confirm deny-delete">x</a>
            <h3><?php echo smartyTranslate(array('s'=>'Confirmation'),$_smarty_tpl);?>
</h3>
        </div>
        <div class="confirmation_content">
            <span class="warning_img"></span>
            <p><?php echo smartyTranslate(array('s'=>'Are you sure? If so than you have to know that the image and all of its data will be deleted! I suggest to use the active state switch, and turn it off.'),$_smarty_tpl);?>
</p>
        </div>
        <div class="button_box">
            <div class="confirm-b">
                <a href="#" class="confirm-delete"><?php echo smartyTranslate(array('s'=>'Yes'),$_smarty_tpl);?>
</a>
            </div>
            <div class="deny-b">
                <a href="#" class="deny-delete"><?php echo smartyTranslate(array('s'=>'No'),$_smarty_tpl);?>
</a>
            </div>
        </div>
    </div>  
    <?php if ($_smarty_tpl->getVariable('slider')->value['firstStart']){?>
        <div id="sendInfoWindow" class="confirmation_window">
            <div class="conf_header">
                <a href="#" class="close_confirm deny-delete">x</a>
                <h3><?php echo smartyTranslate(array('s'=>'Help Us!'),$_smarty_tpl);?>
</h3>
            </div>
            <div class="confirmation_content">
                <p><?php echo smartyTranslate(array('s'=>'By clicking to the YES button, you agree to send some basic information to us. This mean we can keep tracking how much active module we have.'),$_smarty_tpl);?>
</p>
                <p><b><?php echo smartyTranslate(array('s'=>'Don`t worry we`ll be discrete with this information'),$_smarty_tpl);?>
:</b></p>
                <ul>
                    <li><?php echo smartyTranslate(array('s'=>'Domain'),$_smarty_tpl);?>
: <b><?php echo $_smarty_tpl->getVariable('slider')->value['info']['domain'];?>
</b></li>
                    <li><?php echo smartyTranslate(array('s'=>'Version'),$_smarty_tpl);?>
: <b><?php echo $_smarty_tpl->getVariable('slider')->value['info']['version'];?>
</b></li>
                    <li><?php echo smartyTranslate(array('s'=>'PS Version'),$_smarty_tpl);?>
: <b><?php echo $_smarty_tpl->getVariable('slider')->value['info']['psVersion'];?>
</b></li>
                </ul>
                <form>
                    <p><?php echo smartyTranslate(array('s'=>'If you wish to riecive news about our updates, new modules (there will be...) than please fill this out.'),$_smarty_tpl);?>
</p>
                    <div>
                        <label><?php echo smartyTranslate(array('s'=>'Email'),$_smarty_tpl);?>
:</label>
                        <input type="text" id="sendInfoEmail" name="infoEmail" />
                    </div>  
                </form>
                <h3><?php echo smartyTranslate(array('s'=>'Thank you for your help!'),$_smarty_tpl);?>
</h3>
            </div>
            <div class="button_box">
                <div class="confirm-b">
                    <a href="#" id="sendInfo" class="confirm-delete"><?php echo smartyTranslate(array('s'=>'Yes'),$_smarty_tpl);?>
</a>
                </div>
                <div class="deny-b">
                    <a href="#" class="deny-delete"><?php echo smartyTranslate(array('s'=>'No'),$_smarty_tpl);?>
</a>
                </div>
            </div>
        </div>  
    <?php }?>
</div>			