<?php /* Smarty version Smarty-3.0.7, created on 2015-07-19 12:32:34
         compiled from "/Applications/MAMP/htdocs/fedexcollection/themes/prestashop/account.tpl" */ ?>
<?php /*%%SmartyHeaderCode:57733871255ab7cc28c9e13-70960220%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'bc3ca9496347f171fa53e6ff1c09080c5597417d' => 
    array (
      0 => '/Applications/MAMP/htdocs/fedexcollection/themes/prestashop/account.tpl',
      1 => 1409235180,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '57733871255ab7cc28c9e13-70960220',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>


<?php ob_start(); ?><?php echo smartyTranslate(array('s'=>'account'),$_smarty_tpl);?>
<?php  Smarty::$_smarty_vars['capture']['path']=ob_get_clean();?>
<?php $_template = new Smarty_Internal_Template(($_smarty_tpl->getVariable('tpl_dir')->value)."./breadcrumb.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php unset($_template);?>


<div id="PG_Zone1" class="PageZone">
        <h1>Account Management</h1>
        
        <div class="pw-acctsection">
               
            <div class="pw-acctsection-cat"><h2>Account</h2><div class="smalltext">Account Settings, Address Book &amp; Order History</div></div>
               
            <div class="pw-acctsection-menu">
                <ul>
                    <li class="pw-acctsection-menugroup"><span class="bold">Account Settings</span>
                        <ul class="pw-acctsection-menuitems">
                            <li><a href="<?php echo $_smarty_tpl->getVariable('link')->value->getPageLink('identity.php',true);?>
">Edit my profile</a></li>
                        </ul>
                    </li>
                    <li class="pw-acctsection-menugroup"><span class="bold">Address Book</span>
                        <ul class="pw-acctsection-menuitems">
                            <li><a href="<?php echo $_smarty_tpl->getVariable('link')->value->getPageLink('addresses.php',true);?>
">Manage Address Book</a></li>
                        </ul>
                    </li>
                    <li class="pw-acctsection-menugroup"><span class="bold">Order History</span>
                        <ul class="pw-acctsection-menuitems">
                            <li><a href="<?php echo $_smarty_tpl->getVariable('link')->value->getPageLink('history.php',true);?>
">View Orders</a></li>
                        </ul>
                    </li>
                    <?php if ($_smarty_tpl->getVariable('returnAllowed')->value){?>
                    <li class="pw-acctsection-menugroup"><span class="bold">Merchandise Returns</span>
                        <ul class="pw-acctsection-menuitems">
                            <li><a href="<?php echo $_smarty_tpl->getVariable('link')->value->getPageLink('order-follow.php',true);?>
">View Merchandise Returns</a></li>
                        </ul>
                    </li>
                    <?php }?>
                    <li class="pw-acctsection-menugroup"><span class="bold">Credit Slips</span>
                        <ul class="pw-acctsection-menuitems">
                            <li><a href="<?php echo $_smarty_tpl->getVariable('link')->value->getPageLink('order-slip.php',true);?>
">View Orders Slips</a></li>
                        </ul>
                    </li>
                    <?php if ($_smarty_tpl->getVariable('voucherAllowed')->value){?>
                    <li class="pw-acctsection-menugroup"><span class="bold">My Vouchers</span>
                        <ul class="pw-acctsection-menuitems">
                            <li><a href="<?php echo $_smarty_tpl->getVariable('link')->value->getPageLink('discount.php',true);?>
">View Vouchers</a></li>
                        </ul>
                    </li>
                    <?php }?>
                </ul>
                <div class="clear"></div>
            </div>
            <div class="clear"></div>
        </div>
    </div>


<style type="text/css">
#center_column {
width: 100%;
margin: 0 0 30px 0;
}
.PageZone{padding: 10px; margin:0 auto;}
h1 {padding: 0px; font-size: 1.30em; margin: 10px 0px 0px 0px; text-transform:capitalize; height:auto; color:#000;}
.pw-acctsection {margin-bottom: 0px; border: 1px solid #909090; border-radius: 8px; margin-top: 15px;}
.pw-acctsection .pw-acctsection-cat {float: left; padding: 0px 5px 5px 5px; width: 180px;}
.pw-acctsection h2 {font-size: 15px; padding: 0px; margin: 10px 0px 0px 0px; text-transform:capitalize; color:#666;}
.smalltext {font-size: 0.70em;}
.pw-acctsection .pw-acctsection-menu {border-left: 1px solid #909090; margin-left: 200px; padding: 10px;}
.pw-acctsection ul {list-style-type: none; margin: 0px; padding: 0px;}
.pw-acctsection ul li {float: left; width: 250px;}
div.pw-acctsection-menu ul li.pw-acctsection-menugroup {margin-bottom: 10px;}
.bold {font-weight: bold;}
.pw-acctsection ul {list-style-type: none; margin: 0px; padding: 0px;}
.pw-acctsection a {text-decoration: none; color: #4d148c; line-height: 140%;font-size: 0.85em;}
</style>


<!--<h1><?php echo smartyTranslate(array('s'=>'My account'),$_smarty_tpl);?>
</h1>
<h4><?php echo smartyTranslate(array('s'=>'Welcome to your account. Here you can manage your addresses and orders.'),$_smarty_tpl);?>
</h4>
<ul>
	<li><a href="<?php echo $_smarty_tpl->getVariable('link')->value->getPageLink('history.php',true);?>
" title="<?php echo smartyTranslate(array('s'=>'Orders'),$_smarty_tpl);?>
"><img src="<?php echo $_smarty_tpl->getVariable('img_dir')->value;?>
icon/order.gif" alt="<?php echo smartyTranslate(array('s'=>'Orders'),$_smarty_tpl);?>
" class="icon" /></a><a href="<?php echo $_smarty_tpl->getVariable('link')->value->getPageLink('history.php',true);?>
" title="<?php echo smartyTranslate(array('s'=>'Orders'),$_smarty_tpl);?>
"><?php echo smartyTranslate(array('s'=>'History and details of my orders'),$_smarty_tpl);?>
</a></li>
		<li><a href="<?php echo $_smarty_tpl->getVariable('link')->value->getPageLink('order-follow.php',true);?>
" title="<?php echo smartyTranslate(array('s'=>'Merchandise returns'),$_smarty_tpl);?>
"><img src="<?php echo $_smarty_tpl->getVariable('img_dir')->value;?>
icon/return.gif" alt="<?php echo smartyTranslate(array('s'=>'Merchandise returns'),$_smarty_tpl);?>
" class="icon" /></a><a href="<?php echo $_smarty_tpl->getVariable('link')->value->getPageLink('order-follow.php',true);?>
" title="<?php echo smartyTranslate(array('s'=>'Merchandise returns'),$_smarty_tpl);?>
"><?php echo smartyTranslate(array('s'=>'My merchandise returns'),$_smarty_tpl);?>
</a></li>
	<li><a href="<?php echo $_smarty_tpl->getVariable('link')->value->getPageLink('order-slip.php',true);?>
" title="<?php echo smartyTranslate(array('s'=>'Credit slips'),$_smarty_tpl);?>
"><img src="<?php echo $_smarty_tpl->getVariable('img_dir')->value;?>
icon/slip.gif" alt="<?php echo smartyTranslate(array('s'=>'Credit slips'),$_smarty_tpl);?>
" class="icon" /></a><a href="<?php echo $_smarty_tpl->getVariable('link')->value->getPageLink('order-slip.php',true);?>
" title="<?php echo smartyTranslate(array('s'=>'Credit slips'),$_smarty_tpl);?>
"><?php echo smartyTranslate(array('s'=>'My credit slips'),$_smarty_tpl);?>
</a></li>
	<li><a href="<?php echo $_smarty_tpl->getVariable('link')->value->getPageLink('addresses.php',true);?>
" title="<?php echo smartyTranslate(array('s'=>'Addresses'),$_smarty_tpl);?>
"><img src="<?php echo $_smarty_tpl->getVariable('img_dir')->value;?>
icon/addrbook.gif" alt="<?php echo smartyTranslate(array('s'=>'Addresses'),$_smarty_tpl);?>
" class="icon" /></a><a href="<?php echo $_smarty_tpl->getVariable('link')->value->getPageLink('addresses.php',true);?>
" title="<?php echo smartyTranslate(array('s'=>'Addresses'),$_smarty_tpl);?>
"><?php echo smartyTranslate(array('s'=>'My addresses'),$_smarty_tpl);?>
</a></li>
	<li><a href="<?php echo $_smarty_tpl->getVariable('link')->value->getPageLink('identity.php',true);?>
" title="<?php echo smartyTranslate(array('s'=>'Information'),$_smarty_tpl);?>
"><img src="<?php echo $_smarty_tpl->getVariable('img_dir')->value;?>
icon/userinfo.gif" alt="<?php echo smartyTranslate(array('s'=>'Information'),$_smarty_tpl);?>
" class="icon" /></a><a href="<?php echo $_smarty_tpl->getVariable('link')->value->getPageLink('identity.php',true);?>
" title="<?php echo smartyTranslate(array('s'=>'Information'),$_smarty_tpl);?>
"><?php echo smartyTranslate(array('s'=>'My personal information'),$_smarty_tpl);?>
</a></li>
		<li><a href="<?php echo $_smarty_tpl->getVariable('link')->value->getPageLink('discount.php',true);?>
" title="<?php echo smartyTranslate(array('s'=>'Vouchers'),$_smarty_tpl);?>
"><img src="<?php echo $_smarty_tpl->getVariable('img_dir')->value;?>
icon/voucher.gif" alt="<?php echo smartyTranslate(array('s'=>'Vouchers'),$_smarty_tpl);?>
" class="icon" /></a><a href="<?php echo $_smarty_tpl->getVariable('link')->value->getPageLink('discount.php',true);?>
" title="<?php echo smartyTranslate(array('s'=>'Vouchers'),$_smarty_tpl);?>
"><?php echo smartyTranslate(array('s'=>'My vouchers'),$_smarty_tpl);?>
</a></li>
</ul>
<p><a href="<?php echo $_smarty_tpl->getVariable('base_dir')->value;?>
" title="<?php echo smartyTranslate(array('s'=>'Home'),$_smarty_tpl);?>
"><img src="<?php echo $_smarty_tpl->getVariable('img_dir')->value;?>
icon/home.gif" alt="<?php echo smartyTranslate(array('s'=>'Home'),$_smarty_tpl);?>
" class="icon" /></a><a href="<?php echo $_smarty_tpl->getVariable('base_dir')->value;?>
" title="<?php echo smartyTranslate(array('s'=>'Home'),$_smarty_tpl);?>
"><?php echo smartyTranslate(array('s'=>'Home'),$_smarty_tpl);?>
</a></p>
-->