<?php /* Smarty version Smarty-3.0.7, created on 2015-08-07 12:39:33
         compiled from "/Applications/MAMP/htdocs/fedexcollection/themes/prestashop/contact-form.tpl" */ ?>
<?php /*%%SmartyHeaderCode:83641190055c48ae56f1433-35974324%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'c07112495abf8e4157cb6981ebdc12a944b7a5ff' => 
    array (
      0 => '/Applications/MAMP/htdocs/fedexcollection/themes/prestashop/contact-form.tpl',
      1 => 1438943967,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '83641190055c48ae56f1433-35974324',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php if (!is_callable('smarty_modifier_escape')) include '/Applications/MAMP/htdocs/fedexcollection/tools/smarty/plugins/modifier.escape.php';
?>

<?php ob_start(); ?><?php echo smartyTranslate(array('s'=>'Contact'),$_smarty_tpl);?>
<?php  Smarty::$_smarty_vars['capture']['path']=ob_get_clean();?>
<?php $_template = new Smarty_Internal_Template(($_smarty_tpl->getVariable('tpl_dir')->value)."./breadcrumb.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php unset($_template);?>

<h1><?php echo smartyTranslate(array('s'=>'Customer Service'),$_smarty_tpl);?>
 - <?php if (isset($_smarty_tpl->getVariable('customerThread',null,true,false)->value)&&$_smarty_tpl->getVariable('customerThread')->value){?><?php echo smartyTranslate(array('s'=>'Your reply'),$_smarty_tpl);?>
<?php }else{ ?><?php echo smartyTranslate(array('s'=>'Contact us'),$_smarty_tpl);?>
<?php }?></h1>

<?php if (isset($_smarty_tpl->getVariable('confirmation',null,true,false)->value)){?>
	<p><?php echo smartyTranslate(array('s'=>'Your message has been successfully sent to our team.'),$_smarty_tpl);?>
</p>
	<ul class="footer_links">
		<li><a href="<?php echo $_smarty_tpl->getVariable('base_dir')->value;?>
"><img class="icon" alt="" src="<?php echo $_smarty_tpl->getVariable('img_dir')->value;?>
icon/home.gif"/></a><a href="<?php echo $_smarty_tpl->getVariable('base_dir')->value;?>
"><?php echo smartyTranslate(array('s'=>'Home'),$_smarty_tpl);?>
</a></li>
	</ul>
<?php }elseif(isset($_smarty_tpl->getVariable('alreadySent',null,true,false)->value)){?>
	<p><?php echo smartyTranslate(array('s'=>'Your message has already been sent.'),$_smarty_tpl);?>
</p>
	<ul class="footer_links">
		<li><a href="<?php echo $_smarty_tpl->getVariable('base_dir')->value;?>
"><img class="icon" alt="" src="<?php echo $_smarty_tpl->getVariable('img_dir')->value;?>
icon/home.gif"/></a><a href="<?php echo $_smarty_tpl->getVariable('base_dir')->value;?>
"><?php echo smartyTranslate(array('s'=>'Home'),$_smarty_tpl);?>
</a></li>
	</ul>
<?php }else{ ?>
	<p class="bold"><?php echo smartyTranslate(array('s'=>'For questions about an order or for more information about our products'),$_smarty_tpl);?>
.</p>
	<?php $_template = new Smarty_Internal_Template(($_smarty_tpl->getVariable('tpl_dir')->value)."./errors.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php unset($_template);?>
	<form action="<?php echo smarty_modifier_escape($_smarty_tpl->getVariable('request_uri')->value,'htmlall','UTF-8');?>
" method="post" class="std" enctype="multipart/form-data">
		<fieldset style="background: #fff">
			<h3><?php echo smartyTranslate(array('s'=>'Send a message'),$_smarty_tpl);?>
</h3>
			<p class="select" style="display: none;">
				<label for="id_contact"><?php echo smartyTranslate(array('s'=>'Subject Heading'),$_smarty_tpl);?>
</label>
			<?php if (isset($_smarty_tpl->getVariable('customerThread',null,true,false)->value['id_contact'])){?>
				<?php  $_smarty_tpl->tpl_vars['contact'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('contacts')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['contact']->key => $_smarty_tpl->tpl_vars['contact']->value){
?>
					<?php if ($_smarty_tpl->tpl_vars['contact']->value['id_contact']==$_smarty_tpl->getVariable('customerThread')->value['id_contact']){?>
						<input type="text" id="contact_name" name="contact_name" value="<?php echo smarty_modifier_escape($_smarty_tpl->tpl_vars['contact']->value['name'],'htmlall','UTF-8');?>
" readonly="readonly" />
						<input type="hidden" name="id_contact" value="<?php echo $_smarty_tpl->tpl_vars['contact']->value['id_contact'];?>
" />
					<?php }?>
				<?php }} ?>
			<?php }else{ ?>
				<select id="id_contact" name="id_contact" onchange="showElemFromSelect('id_contact', 'desc_contact')">
				<?php  $_smarty_tpl->tpl_vars['contact'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('contacts')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['contact']->key => $_smarty_tpl->tpl_vars['contact']->value){
?>
					<option value="<?php echo intval($_smarty_tpl->tpl_vars['contact']->value['id_contact']);?>
" <?php if (isset($_POST['id_contact'])&&$_POST['id_contact']==$_smarty_tpl->tpl_vars['contact']->value['id_contact']){?>selected="selected"<?php }?>><?php echo smarty_modifier_escape($_smarty_tpl->tpl_vars['contact']->value['name'],'htmlall','UTF-8');?>
</option>
				<?php }} ?>
				</select>
			</p>
			<p id="desc_contact0" class="desc_contact">&nbsp;</p>
				<?php  $_smarty_tpl->tpl_vars['contact'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('contacts')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['contact']->key => $_smarty_tpl->tpl_vars['contact']->value){
?>
					<p id="desc_contact<?php echo intval($_smarty_tpl->tpl_vars['contact']->value['id_contact']);?>
" class="desc_contact" style="display:none;">
						<label>&nbsp;</label><?php echo smarty_modifier_escape($_smarty_tpl->tpl_vars['contact']->value['description'],'htmlall','UTF-8');?>

					</p>
				<?php }} ?>
			<?php }?>

            <p class="text">
                <label for="name"><?php echo smartyTranslate(array('s'=>'Your Name'),$_smarty_tpl);?>
</label>
                <input type="text" id="name" name="name" value="<?php echo $_smarty_tpl->getVariable('name')->value;?>
" />
            </p>


			<p class="text">
				<label for="email"><?php echo smartyTranslate(array('s'=>'Your E-mail'),$_smarty_tpl);?>
</label>
				<?php if (isset($_smarty_tpl->getVariable('customerThread',null,true,false)->value['email'])){?>
					<input type="text" id="email" name="from" value="<?php echo $_smarty_tpl->getVariable('customerThread')->value['email'];?>
" readonly="readonly" />
				<?php }else{ ?>
					<input type="text" id="email" name="from" value="<?php echo $_smarty_tpl->getVariable('email')->value;?>
" />
				<?php }?>
			</p>
            <p class="text">
                <label for="subject"><?php echo smartyTranslate(array('s'=>'Subject'),$_smarty_tpl);?>
</label>
                <input type="text" id="subject" name="subject" value="<?php echo $_smarty_tpl->getVariable('subject')->value;?>
" />
            </p>

		<p class="textarea">
			<label for="message"><?php echo smartyTranslate(array('s'=>'Message'),$_smarty_tpl);?>
</label>
			 <textarea id="message" name="message" rows="15" cols="20" style="width:340px;height:220px"><?php if (isset($_smarty_tpl->getVariable('message',null,true,false)->value)){?><?php echo stripslashes(smarty_modifier_escape($_smarty_tpl->getVariable('message')->value,'htmlall','UTF-8'));?>
<?php }?></textarea>
		</p>
		<p class="submit">
            <label>&nbsp;</label>
			<input type="submit" name="submitMessage" id="submitMessage" value="<?php echo smartyTranslate(array('s'=>'Send'),$_smarty_tpl);?>
" class="button_large" onclick="$(this).hide();" />
		</p>

            <div style="float: left; width: 49%">
                <p>
                    Email: <a href="mailto:emeastore@socatec.aero">emeastore@socatec.aero</a><br>
                    Téléphone 01 43 80 17 00<br>
                    Fax 01 43 80 17 02
                </p>


            </div>
            <div style="float: left; width: 49%">
                <p style="word-wrap: break-word">
                    MAIL :<br>
                    127 Boulevard Pereire,<br>
                    75017 Paris,France<br>
                    You can check online on the US sit if you need :<br> <a href="https://www.bdasites
        .com/fedexcompanystore/Store/Support/ContactUs">https://www.bdasites
                        .com/fedexcompanystore/Store/Support/ContactUs</a>
                </p>


            </div>
	</fieldset>
</form>
<?php }?>
