<?php /* Smarty version Smarty-3.0.7, created on 2015-06-30 14:03:01
         compiled from "/srv/data/web/vhosts/www.socatec.fr/htdocs/fedexcollection/modules/minicslider/tpl/admin-new.tpl" */ ?>
<?php /*%%SmartyHeaderCode:33263221655928575436933-35334238%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '94f1c4ffdd366f9414c992a20a7a8bfdd1c5ee83' => 
    array (
      0 => '/srv/data/web/vhosts/www.socatec.fr/htdocs/fedexcollection/modules/minicslider/tpl/admin-new.tpl',
      1 => 1409232626,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '33263221655928575436933-35334238',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>

 
<fieldset id="new_slide" class="hidden-div">
    <legend class="asd"><?php echo smartyTranslate(array('s'=>'Add New Slide'),$_smarty_tpl);?>
</legend> 
    <form method="post" action="<?php echo $_smarty_tpl->getVariable('slider')->value['postAction'];?>
" enctype="multipart/form-data">        
        <div id="new">
            <div id="new-slide">
                <div class="title input">
                    <label><?php echo smartyTranslate(array('s'=>'Title'),$_smarty_tpl);?>
</label>
                    <input type="text" name="title" size="41" class="ghost-text tooltip" value="<?php echo smartyTranslate(array('s'=>'The title of the slide'),$_smarty_tpl);?>
" title="<?php echo smartyTranslate(array('s'=>'This will be the title on the slide.'),$_smarty_tpl);?>
" /> 
                </div>
                <div class="url input">
                    <label><?php echo smartyTranslate(array('s'=>'Url'),$_smarty_tpl);?>
</label>
                    <input type="text" name="url" size="41" class="ghost-text tooltip" value="<?php echo smartyTranslate(array('s'=>'Link of the slide'),$_smarty_tpl);?>
" title="<?php echo smartyTranslate(array('s'=>'ex. http://myshop.com/promotions'),$_smarty_tpl);?>
" />           
                </div>
                <div class="target">
                    <label><?php echo smartyTranslate(array('s'=>'Blank target'),$_smarty_tpl);?>
</label>
                    <input type="checkbox" name="target" class="tooltip" value="1" title="<?php echo smartyTranslate(array('s'=>'Check this if you want to open the link in new window.'),$_smarty_tpl);?>
" />
                </div>
                <div class="image input">
                    <label><?php echo smartyTranslate(array('s'=>'Image'),$_smarty_tpl);?>
</label>
                    <input type="file" name="image" size="29" id="image-chooser" class="tooltip" title="<?php echo smartyTranslate(array('s'=>'Choose an image, only .jpg, .png, .gif are allowed.'),$_smarty_tpl);?>
" />
                </div>
                <div class="imageName input">
                    <label><?php echo smartyTranslate(array('s'=>'Image name'),$_smarty_tpl);?>
</label>
                    <input type="text" name="imageName" size="41" class="ghost-text tooltip" value="<?php echo smartyTranslate(array('s'=>'Image name'),$_smarty_tpl);?>
" title="<?php echo smartyTranslate(array('s'=>'Optional! The name of the uploaded image without extension.'),$_smarty_tpl);?>
" />           
                </div>
                <?php if ($_smarty_tpl->getVariable('slider')->value['options']['single']==1){?>
                    <div class="language">
                        <label><?php echo smartyTranslate(array('s'=>'Language'),$_smarty_tpl);?>
</label>
                        <select name="language" class="tooltip" title="<?php echo smartyTranslate(array('s'=>'The language of the slide.'),$_smarty_tpl);?>
">
                            <?php  $_smarty_tpl->tpl_vars['lang'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('slider')->value['lang']['all']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['lang']->key => $_smarty_tpl->tpl_vars['lang']->value){
?>
                                <option value="<?php echo $_smarty_tpl->tpl_vars['lang']->value['id_lang'];?>
" <?php if ($_smarty_tpl->tpl_vars['lang']->value['id_lang']==$_smarty_tpl->getVariable('slider')->value['lang']['default']['id_lang']){?>selected="selected"<?php }?>><?php echo $_smarty_tpl->tpl_vars['lang']->value['name'];?>
</option>
                            <?php }} ?>
                        </select>
                    </div>
                <?php }?>
                <div class="alt input">
                    <label><?php echo smartyTranslate(array('s'=>'Image alt'),$_smarty_tpl);?>
</label>
                    <input type="text" name="alt" size="41" class="ghost-text tooltip" value="<?php echo smartyTranslate(array('s'=>'An alternate text for the image'),$_smarty_tpl);?>
" title="<?php echo smartyTranslate(array('s'=>'The image alt, alternate text for the image'),$_smarty_tpl);?>
" />
                </div>
                <div class="caption"> 
                    <label><?php echo smartyTranslate(array('s'=>'Caption'),$_smarty_tpl);?>
</label>
                    <textarea type="text" name="caption" cols=40 rows=6 class="ghost-text tooltip" title="<?php echo smartyTranslate(array('s'=>'Be carefull, too long text isnt good and FULL HTML is allowed.'),$_smarty_tpl);?>
"><?php echo smartyTranslate(array('s'=>'The slide text'),$_smarty_tpl);?>
</textarea>                      
                </div>
            </div> 
        <div class="comments"> 
            <h3><?php echo smartyTranslate(array('s'=>"Few important notes"),$_smarty_tpl);?>
</h3>
            <p><?php echo smartyTranslate(array('s'=>'The Nivo Slider is now'),$_smarty_tpl);?>
 <b><a href="http://nivo.dev7studios.com/2012/05/30/the-nivo-slider-is-responsive/" target="_blank"><?php echo smartyTranslate(array('s'=>'responsive'),$_smarty_tpl);?>
</a></b>! This mean that the images will be resized! The best if your images is the same in size!</p>
            <p><?php echo smartyTranslate(array('s'=>'If you dont want title and/or captions text than leave empty those fields.'),$_smarty_tpl);?>
</p>
            <p><?php echo smartyTranslate(array('s'=>'The images wont be resized automatically to the same size, you need to resize them manually!'),$_smarty_tpl);?>
</p>
            <p><?php echo smartyTranslate(array('s'=>'You can upload different sized images for different language.'),$_smarty_tpl);?>
</p>
            <p><?php echo smartyTranslate(array('s'=>'If you want you can upload the same image to different language. They will be renamed but its better if you give them a normal name.'),$_smarty_tpl);?>
</p>
            <p><?php echo smartyTranslate(array('s'=>'More than 8 image looks ugly if you use thumbnails in center column.'),$_smarty_tpl);?>
</p>
        </div>
        <div class="button_cont">
            <input type="submit" name="submitNewSlide" value="<?php echo smartyTranslate(array('s'=>'Add Slide'),$_smarty_tpl);?>
" class="green" />
            <?php if ($_smarty_tpl->getVariable('slider')->value['options']['single']==0){?>
                <input type="hidden" name="language" value="<?php echo $_smarty_tpl->getVariable('slider')->value['lang']['default']['id_lang'];?>
" />
            <?php }?>
         </div>      
	    </div>
    </form>
</fieldset>