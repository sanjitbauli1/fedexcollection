<?php /* Smarty version Smarty-3.0.7, created on 2015-06-30 14:03:01
         compiled from "/srv/data/web/vhosts/www.socatec.fr/htdocs/fedexcollection/modules/minicslider/tpl/admin-options.tpl" */ ?>
<?php /*%%SmartyHeaderCode:6607704125592857524b611-38306554%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '49108479cdcf0919605323b70bea859816fc6b62' => 
    array (
      0 => '/srv/data/web/vhosts/www.socatec.fr/htdocs/fedexcollection/modules/minicslider/tpl/admin-options.tpl',
      1 => 1409232627,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '6607704125592857524b611-38306554',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>

 
<fieldset id="optionsForm" class="hidden-div">
    <legend><?php echo smartyTranslate(array('s'=>'Options'),$_smarty_tpl);?>
</legend>
	<form id="slider_options" method="post" action="<?php echo $_smarty_tpl->getVariable('slider')->value['postAction'];?>
">
		<div id="options">
            <div class="animation">
                <h3><?php echo smartyTranslate(array('s'=>'Animation'),$_smarty_tpl);?>
</h3>  
                <div class="select">      
                    <div class="first_select">
                        <label><?php echo smartyTranslate(array('s'=>'Unused effects'),$_smarty_tpl);?>
</label>
                        <select multiple="multiple" id="select1" name="nivo_effect[]" >
						    <?php  $_smarty_tpl->tpl_vars['effect'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('slider')->value['options']['effect']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['effect']->key => $_smarty_tpl->tpl_vars['effect']->value){
?>
							    <option><?php echo $_smarty_tpl->tpl_vars['effect']->value;?>
</option>
						    <?php }} ?>
				        </select>	
                        <input name="left2right" value="<?php echo smartyTranslate(array('s'=>'Add'),$_smarty_tpl);?>
" type="button" id="add" class="green tooltip" title="<?php echo smartyTranslate(array('s'=>'Click to add effect'),$_smarty_tpl);?>
">
					    <span class="info_ico tooltip" title="<?php echo smartyTranslate(array('s'=>'These are the animation effects, choose one or more and click to the Add button.'),$_smarty_tpl);?>
"></span>  
                    </div>
                    <div class="second_select">
                        <label><?php echo smartyTranslate(array('s'=>'Used effects'),$_smarty_tpl);?>
</label>
                        <select multiple="multiple" id="select2" name="nivo_current[]" >
						    <?php  $_smarty_tpl->tpl_vars['current'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('slider')->value['options']['current']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['current']->key => $_smarty_tpl->tpl_vars['current']->value){
?>
							    <option><?php echo $_smarty_tpl->tpl_vars['current']->value;?>
</option>
						    <?php }} ?>
					    </select>	
                        <input name="right2left" value="<?php echo smartyTranslate(array('s'=>'Remove'),$_smarty_tpl);?>
" type="button" id="remove" class="green tooltip" title="<?php echo smartyTranslate(array('s'=>'Click to remove effect'),$_smarty_tpl);?>
">
                        <span class="info_ico tooltip" title="<?php echo smartyTranslate(array('s'=>'These are the used animation effects, you can select and remove them, if its empty then all will be used ( random ).'),$_smarty_tpl);?>
"></span>
                    </div>                   
                </div>    
            </div>
            <div class="slice">
                <h3><?php echo smartyTranslate(array('s'=>'Configure Slice and Box animation'),$_smarty_tpl);?>
</h3>
                <div>
                    <label><?php echo smartyTranslate(array('s'=>'Slices'),$_smarty_tpl);?>
: </label>
				    <input type="text" name="slices" value="<?php echo $_smarty_tpl->getVariable('slider')->value['options']['slices'];?>
" class="tooltip" title="<?php echo smartyTranslate(array('s'=>'The number of Slices for Slice animation'),$_smarty_tpl);?>
">
				</div>
                <div>
                    <label><?php echo smartyTranslate(array('s'=>'BoxCols'),$_smarty_tpl);?>
: </label>
				    <input type="text" name="cols" value="<?php echo $_smarty_tpl->getVariable('slider')->value['options']['cols'];?>
" class="tooltip" title="<?php echo smartyTranslate(array('s'=>'The number of Cols for Box animations'),$_smarty_tpl);?>
">
			    </div>
                <div>
                	<label><?php echo smartyTranslate(array('s'=>'BoxRows'),$_smarty_tpl);?>
: </label>
			        <input type="text" name="rows" value="<?php echo $_smarty_tpl->getVariable('slider')->value['options']['rows'];?>
" class="tooltip" title="<?php echo smartyTranslate(array('s'=>'The number of Rows for Box animations'),$_smarty_tpl);?>
">
			    </div>
            </div>
            <div class="configuration">
                <h3><?php echo smartyTranslate(array('s'=>'Configuration'),$_smarty_tpl);?>
</h3>
                <div>
                    <label><?php echo smartyTranslate(array('s'=>'Speed'),$_smarty_tpl);?>
: </label>
					<input type="text" name="speed" value="<?php echo $_smarty_tpl->getVariable('slider')->value['options']['speed'];?>
" class="tooltip" title="<?php echo smartyTranslate(array('s'=>'Slide transition speed in miliseconds (default is 0.5 sec)'),$_smarty_tpl);?>
">                    
                </div>    
                <div>
                    <label><?php echo smartyTranslate(array('s'=>'Pause Time'),$_smarty_tpl);?>
: </label>
					<input type="text" name="pause" value="<?php echo $_smarty_tpl->getVariable('slider')->value['options']['pause'];?>
" class="tooltip" title="<?php echo smartyTranslate(array('s'=>'How long each slide will be shown in miliseconds (default is 3 sec)'),$_smarty_tpl);?>
">
                </div>
                <div class="switch">
                    <label><?php echo smartyTranslate(array('s'=>'Pause on Mouse Hover'),$_smarty_tpl);?>
: </label>
					<div class="field switch">
						<input type="radio" id="r-hover" class="" name="hover"  value="<?php echo $_smarty_tpl->getVariable('slider')->value['options']['hover'];?>
" checked="true" />
						<label for="r-hover" class="cb-enable <?php if ($_smarty_tpl->getVariable('slider')->value['options']['hover']==1){?>selected<?php }?>" ><span>ON</span></label>
						<label for="r-hover" class="cb-disable <?php if ($_smarty_tpl->getVariable('slider')->value['options']['hover']==0){?>selected<?php }?>" ><span>OFF</span></label>
					</div>
					<span class="info_ico tooltip" title="<?php echo smartyTranslate(array('s'=>'Pause the slider on mouse hover.'),$_smarty_tpl);?>
"></span>
                </div>
                <div class="switch">
                    <label><?php echo smartyTranslate(array('s'=>'Manual slide'),$_smarty_tpl);?>
: </label>
					<div class="field switch">
						<input type="radio" id="r-manual" class="" name="manual"  value="<?php echo $_smarty_tpl->getVariable('slider')->value['options']['manual'];?>
" checked="true" />
						<label for="r-manual" class="cb-enable <?php if ($_smarty_tpl->getVariable('slider')->value['options']['manual']==1){?>selected<?php }?>" ><span>ON</span></label>
						<label for="r-manual" class="cb-disable <?php if ($_smarty_tpl->getVariable('slider')->value['options']['manual']==0){?>selected<?php }?>" ><span>OFF</span></label>
					</div>
					<span class="info_ico tooltip" title="<?php echo smartyTranslate(array('s'=>'Turn it ON if you dont want the slider to auto slide.'),$_smarty_tpl);?>
"></span>
                </div>                    
            </div>
            <div class="navigation">
                <h3><?php echo smartyTranslate(array('s'=>'Navigation'),$_smarty_tpl);?>
</h3>
                <div class="switch">
                    <label><?php echo smartyTranslate(array('s'=>'Prev/Next button'),$_smarty_tpl);?>
: </label>
					<div class="field switch">
						<input type="radio" id="r-buttons" class="" name="buttons"  value="<?php echo $_smarty_tpl->getVariable('slider')->value['options']['buttons'];?>
" checked="true" />
						<label for="r-buttons" class="cb-enable <?php if ($_smarty_tpl->getVariable('slider')->value['options']['buttons']==1){?>selected<?php }?>" ><span>ON</span></label>
						<label for="r-buttons" class="cb-disable <?php if ($_smarty_tpl->getVariable('slider')->value['options']['buttons']==0){?>selected<?php }?>" ><span>OFF</span></label>
					</div>
					<span class="info_ico tooltip" title="<?php echo smartyTranslate(array('s'=>'If you want previous and next buttons on the two side of the slider, then turn this on.'),$_smarty_tpl);?>
"></span>
                </div>
                <div class="switch">
                    <label><?php echo smartyTranslate(array('s'=>'Control'),$_smarty_tpl);?>
: </label>
					<div class="field switch">
						<input type="radio" id="r-control" class="" name="control"  value="<?php echo $_smarty_tpl->getVariable('slider')->value['options']['control'];?>
" checked="true" />
						<label for="r-control" class="cb-enable <?php if ($_smarty_tpl->getVariable('slider')->value['options']['control']==1){?>selected<?php }?>" ><span>ON</span></label>
						<label for="r-control" class="cb-disable <?php if ($_smarty_tpl->getVariable('slider')->value['options']['control']==0){?>selected<?php }?>" ><span>OFF</span></label>
					</div>
					<span class="info_ico tooltip" title="<?php echo smartyTranslate(array('s'=>'This controls the navigation, by default these are the litle dots under the slider.'),$_smarty_tpl);?>
"></span>
                </div>
                <div class="switch">
                    <label><?php echo smartyTranslate(array('s'=>'Thumbnails'),$_smarty_tpl);?>
: </label>
					<div class="field switch">
						<input type="radio" id="r-thumbnail" class="" name="thumbnail"  value="<?php echo $_smarty_tpl->getVariable('slider')->value['options']['thumbnail'];?>
" checked="true" />
						<label for="r-thumbnail" class="cb-enable <?php if ($_smarty_tpl->getVariable('slider')->value['options']['thumbnail']==1){?>selected<?php }?>" ><span>ON</span></label>
						<label for="r-thumbnail" class="cb-disable <?php if ($_smarty_tpl->getVariable('slider')->value['options']['thumbnail']==0){?>selected<?php }?>" ><span>OFF</span></label>
					</div>
					<span class="info_ico tooltip" title="<?php echo smartyTranslate(array('s'=>'Turn it on if you want thumbnails in the place of the ( control ) litle dots.'),$_smarty_tpl);?>
"></span>
                </div>   
            </div>
            <div class="other">
                <h3><?php echo smartyTranslate(array('s'=>'Other'),$_smarty_tpl);?>
</h3>
                <div class="switch">
                    <label><?php echo smartyTranslate(array('s'=>'Random slide'),$_smarty_tpl);?>
: </label>
					<div class="field switch">
						<input type="radio" id="r-random" class="" name="random"  value="<?php echo $_smarty_tpl->getVariable('slider')->value['options']['random'];?>
" checked="true" />
						<label for="r-random" class="cb-enable <?php if ($_smarty_tpl->getVariable('slider')->value['options']['random']==1){?>selected<?php }?>" ><span>ON</span></label>
						<label for="r-random" class="cb-disable <?php if ($_smarty_tpl->getVariable('slider')->value['options']['random']==0){?>selected<?php }?>" ><span>OFF</span></label>
					</div>
					<span class="info_ico tooltip" title="<?php echo smartyTranslate(array('s'=>'Turn it ON if you want the slider to start with a random slide.'),$_smarty_tpl);?>
"></span>
                </div>
                <div>
                    <label><?php echo smartyTranslate(array('s'=>'Starting slide'),$_smarty_tpl);?>
: </label>
					<input type="text" name="startSlide" value="<?php echo $_smarty_tpl->getVariable('slider')->value['options']['startSlide'];?>
" class="tooltip" title="<?php echo smartyTranslate(array('s'=>'The starting slide, begining with 0. So if you have 5 slides and you want to start with the last slide than you write 4.'),$_smarty_tpl);?>
">
                </div> 
                <div class="switch">
                    <label><?php echo smartyTranslate(array('s'=>'I need multilanguage'),$_smarty_tpl);?>
: </label>
					<div class="field switch">
						<input type="radio" id="r-single" class="" name="single"  value="<?php echo $_smarty_tpl->getVariable('slider')->value['options']['single'];?>
" checked="true" />
						<label for="r-single" class="cb-enable <?php if ($_smarty_tpl->getVariable('slider')->value['options']['single']==1){?>selected<?php }?>" ><span>ON</span></label>
						<label for="r-single" class="cb-disable <?php if ($_smarty_tpl->getVariable('slider')->value['options']['single']==0){?>selected<?php }?>" ><span>OFF</span></label>
					</div>
					<span class="info_ico tooltip" title="<?php echo smartyTranslate(array('s'=>'Turn on if you want to use different slides for different languages, otherwise the default language slides will be used for all the languages.'),$_smarty_tpl);?>
"></span>
                </div>       
                <div>
                	<label><?php echo smartyTranslate(array('s'=>'Slider width'),$_smarty_tpl);?>
: </label>
					<input type="text" name="sliderWidth" value="<?php echo $_smarty_tpl->getVariable('slider')->value['options']['width'];?>
" class="tooltip" title="<?php echo smartyTranslate(array('s'=>'The width of the slider in pixels if you do want to be responsive.'),$_smarty_tpl);?>
">
				</div>
				<div>
					<label><?php echo smartyTranslate(array('s'=>'Slider height'),$_smarty_tpl);?>
: </label>
					<input type="text" name="sliderHeight" value="<?php echo $_smarty_tpl->getVariable('slider')->value['options']['height'];?>
" class="tooltip" title="<?php echo smartyTranslate(array('s'=>'The height of the slider in pixels if you do want to be responsive.'),$_smarty_tpl);?>
">
                </div>               
            </div>
            <div class="button_cont">
				<input type="submit" name="submitOptions" value="<?php echo smartyTranslate(array('s'=>'Update'),$_smarty_tpl);?>
" id="submitOptions" class="green" />
			</div>
		</div>
	</form>
</fieldset>