<?php
require_once('config/settings.inc.php');
$connection = mysql_connect(_DB_SERVER_, _DB_USER_, _DB_PASSWD_) or die("please contact avijit.sarkar@jhunsinfotech.com");
mysql_select_db(_DB_NAME_, $connection);
	
$case = isset($_GET['case'])?$_GET['case']:'';

switch($case)
{
	case 'get_country_list': get_country_list();break;
	case 'check_captcha': check_captcha();break;
	case 'custom_order_email_send': custom_order_email_send();break;	
};


function get_country_list(){
		
	$query=mysql_query("SELECT * FROM ps_countries");
	$str="";
	$str.="<option value='' title=''>-- Select Country --</option>";
	
	while($row=mysql_fetch_array($query))
	{
		$str.="<option value='".$row['countries_name']."' title='".$row['countries_name']."'";
		/*if($row['countries_id']==$selval)
		{
			$str.='selected';
		}*/
		$str.=">".$row['countries_name']."</option>";
	}
	echo $str;
}

function check_captcha(){
	extract($_POST);
	$res=0;
	require_once('./securimage/securimage.php');
	$securimage = new Securimage();

	if($securimage->check($captcha) == false){
		$res=1;
	}
	echo $res;
}

function custom_order_email_send(){
	
	extract($_POST);

	$query=mysql_query("SELECT * FROM ps_employee WHERE id_employee=1 AND id_profile=1");
		$result=mysql_fetch_array($query);
		$admin_email=$result['email'];
		
	

		$to = $admin_email; //ADMIN EMAIL
		$cc = $order_email; //CONTACT PERSON'S EMAIL
		$subject = "New Custom Order";
		
		$message = "<html>
		<head>
		<title>New Order Details</title>
		</head>
		<body>
		<p>Please find the below details.</p>
		
		<table width='100%' border='0' cellpadding='3' cellspacing='3'>
		<tr>
			<td colspan='2' style='background:#CCC;font-size:18px;font-weight:bold;'>Order Details</td>
		</tr>
		<tr>
			<td width='125'><strong>Full Name:</strong></td>
			<td>".$first_name." ".$last_name."</td>
		</tr>
		<tr>
			<td><strong>Country:</strong></td>
			<td>".$country."</td>
		</tr>
		<tr>
			<td><strong>Phone:</strong></td>
			<td>".$tel."</td>
		</tr>
		<tr>
			<td><strong>Email:</strong></td>
			<td>".$order_email."</td>
		</tr>
		<tr>
			<td width='100'><strong>Deadline:</strong></td>
			<td>".$deadline."</td>
		</tr>
		<tr>
			<td><strong>Delivery Address:</strong></td>
			<td>".nl2br($delivery_address)."</td>
		</tr>
		<tr>
			<td><strong>Billing Address:</strong></td>
			<td>".nl2br($billing_address)."</td>
		</tr>
		<tr>
			<td><strong>Quantity:</strong></td>
			<td>".$quantity."</td>
		</tr>
		<tr>
			<td><strong>Quality:</strong></td>
			<td>".$quality."</td>
		</tr>
		<tr>
			<td><strong>Description:</strong></td>
			<td>".nl2br($description)."</td>
		</tr>		
		</table>
		
		</body>
		</html>";
		
		//Always set content-type when sending HTML email
		$headers = "MIME-Version: 1.0" . "\r\n";
		$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
		$headers .= 'From: <info@snippydev.com>' . "\r\n";
		$headers .= 'Cc: '.$cc. "\r\n";
		if(mail($to,$subject,$message,$headers)){
			$msg='Thank you for your inquiry.<br> We will get back to you shortly.';
		}
		else
		{
			$msg='Email not sent!';
		}
	echo $msg;
}
?>